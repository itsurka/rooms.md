<?php
date_default_timezone_set('Europe/Chisinau');

error_reporting(E_ALL);
ini_set('display_errors', 1);

define("PROTECTED_PATH", dirname(__FILE__) . "/protected/");
defined('YII_DEBUG') or define('YII_DEBUG', !file_exists('production'));
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

$yii = dirname(__FILE__) . '/framework-1.1.16/YiiBase.php';
require_once($yii);

$config = require(PROTECTED_PATH . 'config/main_local.php');

class Yii extends YiiBase
{
    /**
     * @static
     * @return CWebApplication
     */
    public static function app()
    {
        return parent::app();
    }
}

$app = Yii::createWebApplication($config)->run();