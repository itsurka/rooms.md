<?php
date_default_timezone_set('Europe/Chisinau');

define("PROTECTED_PATH", dirname(__FILE__)."/protected/");
//defined('YII_DEBUG') or define('YII_DEBUG', !file_exists('production'));
define('YII_DEBUG', false);
//defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

$yii=dirname(__FILE__).'/framework-1.1.16/YiiBase.php';
require_once($yii);

$config=require(PROTECTED_PATH.'config/test.php');

class Yii extends YiiBase
{
	/**
	 * @static
	 * @return CWebApplication
	 */
	public static function app()
	{
		return parent::app();
	}
}

Yii::createWebApplication($config)->run();
