/**
 * Created by JetBrains PhpStorm.
 * User: test
 * Date: 02.01.13
 * Time: 16:50
 * To change this template use File | Settings | File Templates.
 */

function searcherInit()
{
    searcher=new Object();
    searcher.block=$('.searcher-form-block');
    searcher.list=searcher.block.find('.searchers-list');
    searcher.noSearchers=searcher.list.find('.no-searchers');
    searcher.exampleHtml=searcher.block.find('.searcher-example-wrapper').html();
}

function removeSearcher(searcherObject)
{
    searcherObject.remove();

    if(getSearchersCount()==0)
        searcher.noSearchers.show();
}

function addSearcher(searcherObject)
{
    console.log('addSearcher()');
    console.log('searcherObject');
    console.log(searcherObject);

    if(typeof(searcherObject)=='undefined')
        searcherObject=false;

    searcher.list.append(searcher.exampleHtml);
    searcher.noSearchers.hide();

    var addedSearcher=searcher.list.find('.searcher').eq(getLatestSearcherIndex()-1);
    if(searcherObject!==false)
    {
        for(var i in searcherObject)
        {
            if(searcherObject[i]!='')
            {
                addedSearcher.find('#'+i+' option:selected').removeAttr('selected');
                if(i=='city_id' && searcherObject[i])
                {
                    loadCityRegions_Enhanced(searcherObject.city_id, addedSearcher.find('.region-selector').eq(0), searcherObject.region_id!='' ? searcherObject.region_id : false);
                }
                addedSearcher.find('#'+i+' option[value="'+searcherObject[i]+'"]').attr('selected',true);
            }
        }
    }
}

function getSearchersCount()
{
    return searcher.list.find('.searcher').size();
}

function getLatestSearcherIndex()
{
    var searcherIndex=0;
    if(getSearchersCount()>0)
        searcherIndex=searcher.list.find('.searcher:last-child').index();
    return searcherIndex;
}

function toggleSearcherForm()
{
    searcher.block.toggle();
    /*if(searcher.block.is(':visible'))
        searcher.block.slideUp(450);
    else
        searcher.block.slideDown(450);*/
}

function onSelectCity_Searcher(elem)
{
    loadCityRegions_Enhanced(elem.val(), elem.closest('.searcher').find('.region-selector').eq(0));
}