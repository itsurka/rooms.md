$(document).ready(function() {

    // Глобалные переменные
    loginBlockID = 'loginWindow';
    loginBlock = generateDivBlock(loginBlockID, true, 'modal');
    loginFormIsLoaded = true;

	/**
	 * Main menu item hover
	 **/
	/*$("#nav li a").hover(
		function(){
			$(this).addClass("hov");
		},
		function(){
			$(this).removeClass("hov");
		}
	);*/

	/**
	 * Login checkboxes
	 **/
	$(".checker").hover(
		function() {
			$(this).addClass("hover");
		},
		function() {
			$(this).removeClass("hover");
		}
	);
	$(".checker input[type='checkbox']").click(
		function() {
			var checkbox = $(this);
			var parentSpan = $(this).parent("span");
			if($(checkbox+':checked').val()!==undefined)
				parentSpan.addClass("checked");
			else
				parentSpan.removeClass("checked");
		}
	);

    // Смена цвета для кнопки поиска при наведении
    $('input[name="catalog-search-submit"]').hover(
        function() {
            $(this).removeClass('white');
            $(this).addClass('orange');
        },
        function() {
            $(this).removeClass('orange');
            $(this).addClass('white');
        }
    );

    // При выборе города, погружаем его районы
    $('#cities-select').bind('change', function() {
        var _cityID = parseInt($(this).val());
        if(isNaN(_cityID))
            _cityID = 0;
        loadCityRegions(_cityID);
    });

    var _cityID = parseInt($('#cities-select').val());

    if(isNaN(_cityID))
        loadCityRegions(0);

    // показывает текст на кнопке после клика, кнопка блокируется
    $('.show-loading-text').click(function () {
        var btn = $(this);
        btn.button('loading');
    });

    advFileClone=$('.image_inputs_box ._item').eq(0).clone().wrap("<div />").parent().html();
});


/**
 * Add new file inout for images
 * when adding/edit an adv.
 */
function addNewFileInput()
{
	$(".image_inputs_box").append(advFileClone);
}


/**
 * Показываем окно авторизации
 */
function showLogin(loadLoginForm)
{
    if (!loadLoginForm)
        var loadLoginForm = false;

    if (!loginFormIsLoaded || loadLoginForm) {
        $.ajax({
            url: '/user/login',
            type: 'POST',
            async: true,
            success: function(data) {
                loginBlock.html(data);
                loginFormIsLoaded = true;
            }
        });
    } else {
        loginBlock.lightbox_me({
            overlaySpeed: 200,
            lightboxSpeed: 240,
            centered: false,
            modalCSS: {
                top: '150px'
            }
        });
    }
}


function login()
{
    // TODO
}


/**
 * Generates empty div block
 *
 * @param divID
 * @return div object
 * @param isHidden
 */
function generateDivBlock(divID, isHidden, classAttr)
{
    var block = $('#'+divID);
    var hide = '';
    
    if (typeof isHidden !== 'boolean')
        isHidden = true;

    if (!classAttr)
        var classAttr = '';

    if (isHidden)
        hide = 'style="display: none;"';

    if (block.size() === 0)
        $('body').append('<div id="'+divID+'" class="'+classAttr+'" '+hide+'></div>');

    block = $('#'+divID);
    return block;
}



function showModal(elemObj, params)
{
    elemObj.lightbox_me({
        overlaySpeed: 100,
        lightboxSpeed: 140,
        centered: false,
        modalCSS: {
            top: '150px'
        },
        destroyOnClose: true
    });
}

// Меняем цвет кнопки
function setNextButtonSearchColor()
{
    var colors = [
        'white',
        'blue',
        'orange',
        'green',
        'pink',
        'red'
    ];

    var searchButton = $('input[name="catalog-search-submit"]');
    var currentColor = searchButton.data('color');
    var currentColorPos;
    var nextColor;
    for(var i in colors)
    {
        if(colors[i] == currentColor)
            currentColorPos = parseInt(i);
    }
    if (typeof colors[currentColorPos+1]!=='undefined')
        nextColor = colors[parseInt(currentColorPos)+1];
    else
        nextColor = colors[0];

    searchButton.removeClass(currentColor).addClass(nextColor).data('color', nextColor);
}

function loadCityRegions(cityID)
{
    var regions = $('#regions-select');
    var emptyText = $('#regions-select option').eq(0).text();
    regions.html('<option value="">'+emptyText+'</option>');

    $('#regions-select').removeClass('chzn-done');

    try
    {
        if (cityID > 0)
        {
            $.ajax({
                url: '/catalog/cityRegions?cityID='+parseInt(cityID),
                type: 'POST',
                async: true,
                success: function(data) {
                    if (data.length > 0)
                    {
                        var html = regions.html();
                        for (var i in data)
                        {
                            var item = data[i];
                            html += '<option value="'+item.id+'">'+item.name+'</option>';
                        }
                        regions.html(html);
                    }

                    $('#regions-select_chzn').remove();
                    $("#regions-select").chosen();
                },
                dataType: 'JSON'
            });
        }
        else
        {
            $('#regions-select_chzn').remove();
            $("#regions-select").chosen();
        }
    }
    catch(e)
    {}
}

/**
 * @param cityID
 * @param targetRegionsSelector
 * @param selectValue
 */
function loadCityRegions_Enhanced(cityID, targetRegionsSelector, selectValue)
{
    var regions = targetRegionsSelector;
    var emptyText = regions.find('option').eq(0).text();
    regions.html('<option value="">'+emptyText+'</option>');

    if (typeof selectValue === 'undefined')
        selectValue = false;

    if (cityID > 0)
    {
        $.ajax({
            url: '/catalog/cityRegions?cityID=' + parseInt(cityID),
            type: 'POST',
            async: true,
            success: function(data) {
                if (data.length > 0)
                {
                    var html = regions.html();
                    for (var i in data)
                    {
                        var item = data[i];
                        var selected = '';

                        if (selectValue !== false && item.id == selectValue)
                            selected = 'selected=""';
                        html += '<option value="'+item.id+'" '+selected+'>'+item.name+'</option>';
                    }
                    regions.html(html);
                }
            },
            dataType: 'JSON'
        });
    }
}