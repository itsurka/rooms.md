/**
 * Created by JetBrains PhpStorm.
 * User: test
 * Date: 11.12.12
 * Time: 15:18
 * To change this template use File | Settings | File Templates.
 */

$(document).ready(function() {
    cs=new Object(); // Cote cs - catalog search
    cs.form=$('#catalog-search-form');
});

function cs_clickOption(elem)
{
    cs_toggleWindowLoaderLayer(true);

    var optionType=elem.data('option-type');
    var isChecked=elem.is(':checked');

    // расскрытие и закрытие районов города
    if (optionType=='city')
    {
        var option_elem = elem.parent();
        var sub_menu = option_elem.find('.cs-sub-menu');
        if (sub_menu.size())
        {
            if (isChecked)
            {
                if (sub_menu.hasClass('cs-sub-menu-closed'))
                {
                    sub_menu.removeClass('cs-sub-menu-closed');
                    sub_menu.addClass('cs-sub-menu-opened');
                }
            }
            else
            {
                if (sub_menu.hasClass('cs-sub-menu-opened'))
                {
                    sub_menu.removeClass('cs-sub-menu-opened');
                    sub_menu.addClass('cs-sub-menu-closed');
                }
            }
        }
    }

    // если убрана галочка с города, убираем галочки с его районов
    if (optionType=='city' && !isChecked)
    {
        $('.rel-city-'+elem.data('option-id')).each(function() {
            $(this).prop("checked", false);
        });
    }

    // если нажата категория, отмечаем город
    if (optionType=='region' && isChecked && !$('#sm-city-'+elem.data('rel-city-id')).is('checked'))
    {
        $('#sm-city-'+elem.data('rel-city-id')).prop("checked", true);
    }

    $.ajax({
        url: '/api/getCatalogSearchUrl',
        data: $('#catalog-search-form').serializeArray(),
        type: 'POST',
//        async: true,
        success: function(data) {
            if (data.success)
            {
                window.history.pushState({},"", data.url);
                cs_loadUrl(data.url);
            }
            else
                cs_toggleWindowLoaderLayer(false);
        },
        complete: function() {

        },
        dataType: 'json'
    });
}

function cs_loadUrl(url)
{
    var _url='/';
    if(typeof(url)!=='undefined')
        _url=url;
    
    $.ajax({
        url: _url,
        type: 'GET',
        async: false,
        success: function(data) {
            $('.js-center-content').html(data);
        },
        complete: function() {
            cs_toggleWindowLoaderLayer(false, 170);
        }
    });
}

function _event_click_checkbox(elem, check)
{
    var _check=!elem.is('checked');
    if (typeof(check)!=='undefined')
        _check=check;

    var parent = elem.parent();

    if(_check && !parent.hasClass('checked'))
    {
        parent.addClass('checked');
    }
    else if(!_check && parent.hasClass('checked'))
    {
        parent.removeClass('checked');
    }
}

 function processAjaxData(response, urlPath)
 {
     document.getElementById("content").innerHTML = response.html;
     document.title = response.pageTitle;
     window.history.pushState({"html":response.html,"pageTitle":response.pageTitle},"", urlPath);
     window.history.pushState({},"", urlPath);
 }

function cs_toggleWindowLoaderLayer(value, _timeOffset)
{
    var timeOffset = 0;
    if (typeof(timeOffset)!=='undefined')
        timeOffset = _timeOffset;

    var overlay = $('.overlay_1');

    setTimeout(function() {
        if (value)
        {
            overlay.show();
            overlay.fadeTo(0, 0.5, 'swing', function() {
                /*console.log('=1');*/
            });
        }
        else
        {
            overlay.fadeTo(300, 0.0, 'jswing', function() {
                /*console.log('=0');*/
                overlay.hide();
            });
        }
    }, timeOffset);
}