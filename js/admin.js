$(document).ready(function() {

    // Глобальные переменные
    advFileInputsBox = $('.files');
    advFileInputs = advFileInputsBox.find('.item');

    oldFilesBox = $('.old_files');
    oldFiles = oldFilesBox.find('.item');

    // Обработчики для загрузки новых файлов
    // Добавлние инпут файла
    $(document).find('.add_image').bind('click', function() {
        addNewFileInputAdmin();
    });
    // Удаление инпут файла
    advFileInputs.find('.delete_image').bind('click', function() {
        removeElement($(this).parent());
    });

    // Обработчики для старых файлов
    // Удаление файла
    oldFiles.find('.delete_image').bind('click', function() {
        removeElement($(this).parent());
    });
    
    addNewFileInputAdmin();

    // Генерим скрытое модальное окно
    adminModal = generateDivBlock('adminModal', true, 'modal');
});

function addNewFileInputAdmin()
{
    advFileInputsBox.append(getAdvFileInputClone());
    var item = advFileInputsBox.find('.item:last-child');
    if(item.size())
    {
        // Добавлние инпут файла
        item.find('.add_image').bind('click', function() {
            addNewFileInputAdmin();
        });
        // Добавлние инпут файла
        item.find('.delete_image').bind('click', function() {
            removeElement($(this).parent());
        });
    }
}

function getAdvFileInputClone()
{
    return advFileInputsBox.find('.item_example').clone().removeClass('item_example').addClass('item').show();
}

function removeElement(elem)
{
    elem.remove();
}



function showAdv(html)
{
    adminModal = generateDivBlock('adminModal', true, 'modal');
    adminModal.html(html);
    showModal(adminModal);
}