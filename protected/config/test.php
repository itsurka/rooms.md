<?php

return CMap::mergeArray(
	require(dirname(__FILE__).'/main_local.php'),
	array(
		'components'=>array(
			'fixture'=>array(
				'class'=>'system.test.CDbFixtureManager',
			),
			'db'=>array(
				// включаем профайлер
				'enableProfiling'=>true,
				// показываем значения параметров
				'enableParamLogging'=>true,
				'schemaCachingDuration'=>YII_DEBUG ? (30*60) : (30*60),
			),
			'log'=>array(
				'class'=>'CLogRouter',
				'routes'=>array(
					array(
						'class'=>'CFileLogRoute',
						'levels'=>'error, warning',
						'enabled'=>true,
					),
					// uncomment the following to show log messages on web pages
					array(
						'class' => 'CWebLogRoute',
						'categories' => 'application',
						'levels'=>'error, warning, trace, profile, info',
					),
					array(
						// направляем результаты профайлинга в ProfileLogRoute (отображается
						// внизу страницы)
						'class'=>'CProfileLogRoute',
						'levels'=>'profile',
						'enabled'=>true,
					),
				),
			),
			'urlManager'=>array(
				'urlFormat'=>'get',
				'showScriptName'=>true,
			)
		),
	)
);
