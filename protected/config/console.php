<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',
	'import'=>array(
		'application.commands.*',
		'application.models.*',
		'application.components.*',
		'application.helpers.*',
	),
	// application components
	'components'=>array(
		// uncomment the following to use a MySQL database
		'db'=>array(
			'connectionString'=>'mysql:host=p127444.mysql.ihc.ru;dbname=p127444_roomsmd',
			'emulatePrepare'=>true,
			'enableProfiling'=>true,
			'username'=>'p127444_roomsmd',
			'password'=>'SYdn8q',
			'charset'=>'utf8',
		),
		'request'=>array(
			'hostInfo'=>'http://rooms.md',
		),
	),
);