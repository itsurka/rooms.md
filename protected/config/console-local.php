<?php

return CMap::mergeArray(require dirname(__FILE__) . DIRECTORY_SEPARATOR . 'console.php', array(
	'components'=>array(
		'db'=>array(
			'connectionString'=>'mysql:host=localhost;dbname=doska',
			'emulatePrepare'=>true,
			'enableProfiling'=>true,
			'username'=>'root',
			'password'=>'root',
		),
		'request'=>array(
			'hostInfo'=>'http://dev.rooms.md',
		),
	),
));