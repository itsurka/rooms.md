<?php

return CMap::mergeArray(require dirname(__FILE__) . DIRECTORY_SEPARATOR . 'console.php', array(
    'components' => array(
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=doska',
            'emulatePrepare' => true,
            'enableProfiling' => true,
            'username' => 'root',
            'password' => '',
        ),
        'request' => array(
            'hostInfo' => 'http://rooms.local',
        ),
    ),
));