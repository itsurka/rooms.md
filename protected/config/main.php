<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Rooms.MD',
	'theme'=>'classic',

	// Internationalization
	'sourceLanguage'=>'ru_RU',
	'language'=>'ru',

	// Set default controller
	'defaultController'=>'catalog',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.components.html.*',
		'application.modules.user.models.*',
		'application.modules.user.components.*',
		'application.modules.controlPanel.models.*',
		'application.modules.controlPanel.components.*',
		'application.components.html.CIcon',
		'application.helpers.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool

		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'*&^%.gii.admin',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1', '92.115.184.133', 'localhost', '188.237.200.237'),
		),
		'user'=>array(),
		'controlPanel'=>array(
			'viewPath'=>'themes/controlPanel/views/'
		),
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'loginUrl'=>array('/user/login'),
		),
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'rules'=>array(

				// sitemap
				'sitemap.xml'=>'site/sitemapxml',

				// catalog actions
				'catalog/<action:(cityRegions|setCityRegion|setNumberOfRooms|setSortBy|mySearch)>'=>'catalog/<action>',

				// catalog actions
				'catalog/<period:(today|week|month)>'=>'catalog/period',

				// catalog view adv
				'catalog/<id:.*-\d+>'=>'catalog/view',
				'catalog/<id:\d+>'=>'catalog/view',

				// catalog search adv
				'catalog/<rentType:.*>/<numOfRooms:.*>/<city:.*>/<region:.*>/page<advertisements_page:\d+>'=>'catalog/index',
				'catalog/<rentType:.*>/<numOfRooms:.*>/<city:.*>/<region:.*>'=>'catalog/index',
				'catalog/<rentType:.*>/<numOfRooms:.*>/<city:.*>'=>'catalog/index',
				'catalog/<rentType:.*>/<numOfRooms:.*>'=>'catalog/index',
				'catalog/<rentType:.*>'=>'catalog/index',

				// OK!!!
				/*'catalog/<city:.*>/<region:.*>/<advertisements_page:\d+>'=>'catalog/index',
				'catalog/<city:.*>/<region:.*>'=>'catalog/index',
				'catalog/<city:.*>'=>'catalog/index',*/

				// common
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<id:.*-\d*>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				/*'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',*/
			),
		),
		// uncomment the following to use a MySQL database
		'db'=>array(
			'connectionString'=>'mysql:host=p127444.mysql.ihc.ru;dbname=p127444_roomsmd',
			'username'=>'p127444_roomsmd',
			'password'=>'SYdn8q',
			'charset'=>'utf8',
			'schemaCachingDuration'=>YII_DEBUG ? (0*60) : (30*60),
		),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'cache'=>array(
			'class'=>'system.caching.CFileCache',
		),
		'localtime'=>array(
			'class'=>'LocalTime',
		),
		'image'=>array(
			'class'=>'application.extensions.image.CImageComponent',
			'driver'=>'GD',
		),
		// for a production configuration you can have this
		'clientScript'=>array(
			'scriptMap'=>array(
				'jquery.js'=>'http://code.jquery.com/jquery-2.1.0.min.js',
				//'login.js'=>'site.min.js',
			),
		),
		'session'=>array(
			'timeout'=>60*60*24*31, // месяц
		),
		// for a development configuration you can have this
		/*'clientScript'=>array(
			'scriptMap'=>array(
				'register.js'=>'register.js',
				'login.js'=>'login.js',
			),
		),*/
		'robokassa'=>array(
			'class'=>'application.components.yii-robokassa.Robokassa',
			'sMerchantLogin'=>'test_rooms.md',
			'sMerchantPass1'=>'Dkan20dnCmadFia89',
			'sMerchantPass2'=>'CXMaa059DmsccloJ',
			'sCulture'=>'ru',
			'sIncCurrLabel'=>'',
			'orderModel'=>'Invoice', // ваша модель для выставления счетов
			'priceField'=>'amount', // атрибут модели, где хранится сумма
			'isTest'=>false, // тестовый либо боевой режим работы
		)
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		'developerEmail'=>'webmaster@rooms.md',
		'adminEmail'=>'admin@rooms.md',
		'ownerEmail'=>'turcaigor@gmail.com',

		// Company info
		'supportEmail'=>'admin@rooms.md',
		'contactPhoneNumber'=>'+ 0373 69890138',
		'advsUrl'=>'#',

		'companyName'=>'Rooms',
		'projectYearFrom'=>2012,
		'project'=>'Rooms.MD',

		'ADV_IMAGES_URL'=>'protected/uploads/',
		'ADV_THUMBS_URL'=>'protected/uploads/thumbs/',
		'IMAGES_URL'=>'images/',
	),
);
