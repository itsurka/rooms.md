<?php

/**
 *   /usr/bin/php /home/p127444/www/rooms.md/protected/yiic config reloadVIPAdvs
 */
class ConfigCommand extends MyConsoleCommand
{
	/**
	 * Перезагрузка VIP объявлений
	 */
	public function actionReloadVIPAdvs()
	{
		$this->log('Перезагрузка VIP объявлений...');

		$conn=Yii::app()->getDb();

		// удаление старых VIP
		$conn->createCommand()
			->update(Advertisements::model()->tableName(), [
				'is_vip'=>0,
			], 'is_vip = 1');

		// выбираем новые VIP об-ия
		$criteria=new CDbCriteria();
		$criteria->addInCondition('t.status', Advertisements::$allowPublicStatuses);
		$criteria->order='t.id DESC';
		$criteria->group='t.id';
		$criteria->with['files']=array('joinType'=>'INNER JOIN',);
		$criteria->together=true;
		$criteria->limit=3;
		$models=Advertisements::model()->findAll($criteria);

		$ids=array_map(function (Advertisements $model)
		{
			return $model->id;
		}, $models);

		// сохраняем об-ия как IP
		$conn->createCommand()
			->update(Advertisements::model()->tableName(), ['is_vip' => 1], ['in', 'id', $ids]);

		$advIDs=Config::model()->findByAttributes(['key'=>Config::VIP_ADVS]);
		if(!$advIDs)
		{
			$advIDs=new Config();
			$advIDs->key=Config::VIP_ADVS;
			$advIDs->name='VIP объявления';
		}
		$advIDs->value=[];

		if($ids)
		{
			$advIDs->value=$ids;
		}
		$advIDs->save(false);

		$this->log('Сохранено '.count($ids).' объявлений.');
	}
}
