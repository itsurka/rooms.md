<?php

/**
 *   /usr/bin/php /home/p127444/www/rooms.md/protected/yiic common test
 *   /usr/bin/php /home/p127444/www/rooms.md/protected/yiic migrate --interactive=false
 *   /usr/bin/php /home/p127444/www/rooms.md/protected/yiic common ClearOldImages --imagesLimit=100 // удаления старых фоток
 */
class CommonCommand extends MyConsoleCommand
{
	const DEBUG=true;

	public function actionTest()
	{
		$this->log('test');
	}

	/**
	 * Удаления фоток обявлений кроме главной для сохранения места на диске.
	 *
	 * @param int $imagesLimit Кол-во фоток для удаления
	 */
	public function actionClearOldImages($imagesLimit=100)
	{
		$this->log('Удаление старых фоток');

		$allImagesCount=Files::model()->count();
		$this->log('Общее кол-во фоток: '.$allImagesCount);

		$criteria = new CDbCriteria();
		$criteria->limit = (int)$imagesLimit;
		$criteria->order = 'id asc';

		$files = Files::model()->findAll($criteria);

		$deletedImages = 0;
		foreach($files as $eachFile)
		{
			$eachFile->delete();
			$deletedImages++;
		}

//		$command=Yii::app()->getDb()->createCommand();
//		$oldAdvs=$command
//			->select('a.id, count(f.id) as filesCount')
//			->from(Advertisements::model()->tableName().' a')
//			->join(AdvertisementFiles::model()->tableName().' af', 'a.id = af.advertisement_id')
//			->join(Files::model()->tableName().' f', 'af.file_id = f.id')
//			->having('filesCount > 0')
//			->group('a.id')
//			->order('a.id asc')
//			->limit($imagesLimit)
//			->queryAll();
//
//		var_dump(count($oldAdvs)); exit;
//
//		/*echo '$oldAdvs[]<pre>';
//		print_r($oldAdvs);
//		echo '</pre>';*/
//
//		$stop=false;
//		$deletedImages=0;
//		foreach($oldAdvs as $eachAdvInfo)
//		{
//			/** @var Advertisements $adv */
//			$adv=Advertisements::model()->findByPk($eachAdvInfo['id']);
//			/** @var Files[] $files */
//			$files=array_slice($adv->files, 1);
//			foreach($files as $eachFile)
//			{
//				$eachFile->delete();
//				$deletedImages++;
//				if($deletedImages>=$imagesLimit)
//					$stop=true;
//
//				if($stop)
//					break;
//			}
//			if($stop)
//				break;
//		}
		$this->log('Удалено '.$deletedImages.' фоток');
	}
}
