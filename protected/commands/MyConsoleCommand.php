<?php

/**
 *   /usr/bin/php /home/p127444/www/rooms.md/protected/yiic common test
 */

class MyConsoleCommand extends CConsoleCommand
{
	const DEBUG = true;

	protected function beforeAction($action, $params)
	{
		if(!parent::beforeAction($action, $params))
			return false;

		$this->log('start');

		return true;
	}

	protected function afterAction($action, $params)
	{
		parent::afterAction($action, $params);
		$this->log('end');
	}

	protected function log($text)
	{
		if(!self::DEBUG)
			return false;

		if($text=='start' || $text=='end' || $text == 'exit')
		{
			echo "\n";
			echo "\n";
			echo "--".strtoupper($text)."--";
			echo "\n";
			echo "\n";
			if ($text != 'start')
				exit;
		}
		else
		{
			if(!is_string($text))
				$text=CVarDumper::dumpAsString($text);
			echo "\n";
			echo "{$text}";
			echo "\n";
		}
		return true;
	}
}