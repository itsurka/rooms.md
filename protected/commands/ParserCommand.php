<?php

#################### Команды CRON #############################
#
# 0 */1 * * *  /usr/bin/php /home/p127444/www/rooms.md/protected/yiic parser run --parserType=makler/999
#
#################### Команды CRON #############################

class ParserCommand extends CConsoleCommand
{
	public $parsePagesByCall=1;
	public $defaultAction='run';

	/**
	 * @var Parser
	 */
	public $parserModel;

	/**
	 * @var WebParser
	 */
	public $webParser;

	public function beforeAction($action, $params)
	{
		if(!parent::beforeAction($action, $params))
			return false;

		ini_set('memory_limit', '256M');

		return true;
	}

	public function actionRun($parserType)
	{
		$this->log('start');
		$this->log('started at '.date('Y-m-d H:i:s'));

		if(!in_array($parserType, ['makler', '999']))
			throw new CException('Unknown parser type');

		if($parserType=='makler')
		{
			$this->parserModel=Parser::getActiveParserBySiteId(Parser::PARSER_ID_MAKLER);
			$this->webParser=new MaklerParser();
			$sourceWebsite=Parser::$sitesForParsing[Parser::PARSER_ID_MAKLER];
		}
		else
		{
			$this->parserModel=Parser::getActiveParserBySiteId(Parser::PARSER_ID_999);
			$this->webParser=new Parser999();
			$sourceWebsite=Parser::$sitesForParsing[Parser::PARSER_ID_999];
		}
		$result=$this->runParser();

		$this->log('success: '.($result['success'] ? 'true' : 'false'));
		$this->log('message: '.$result['message']);
		$this->log('source: '.$sourceWebsite);
		$this->log('totalParsedPages: '.$result['totalParsedPages']);
		$this->log('totalParsedItems: '.$result['totalParsedUsers']);
		$this->log('successParsedItems: '.$result['successParsedUsers']);
		$this->log('failedParsedItems: '.$result['failedParsedUsers']);

		$this->log('stopped at '.date('Y-m-d H:i:s'));
		$this->log('end');
	}

	private function log($text)
	{
		if($text=='start' || $text=='end' || $text=='exit')
		{
			echo "\n";
			echo "\n";
			echo "--".strtoupper($text)."--";
			echo "\n";
			echo "\n";
			if($text!='start')
			{
				exit;
			}
		}
		else
		{
			echo "\n";
			echo "{$text}";
			echo "\n";
		}
	}

	public function runParser()
	{
		$result=array(
			'success'=>false,
			'message'=>'Active parser not found',
			'totalParsedPages'=>0,
			'totalParsedUsers'=>0,
			'successParsedUsers'=>0,
			'failedParsedUsers'=>0
		);

		if(!is_null($this->parserModel))
		{
			// Check if last parser process is completed - Start {
			$criteria=new CDbCriteria();
			$criteria->condition='relParserId = :relParserId';
			$criteria->order='id DESC';
			$criteria->limit=1;
			$criteria->params=array('relParserId'=>$this->parserModel->id);
			/** @var ParserProcess $lastParserProcess */
			$lastParserProcess=ParserProcess::model()->find($criteria);
			if(!is_null($lastParserProcess) && $lastParserProcess->status==ParserProcess::STATUS_RUNNING)
			{
				$minutesDiff=round(time()-$lastParserProcess->dateStart/60);
				if($minutesDiff<25)
				{
					$result['message']='Last parser process is not completed, process id is '.$lastParserProcess->id;
					return $result;
				}
			}
			// Check if last parser process is completed - End }

			$this->parsePagesByCall=$this->parserModel->parsePagesCount;
			$parserProcessResult=$this->runParserProcess($this->parserModel);
			if($parserProcessResult['success'])
			{
				$this->parserModel->parsedPages+=$this->parsePagesByCall;
			}

			if(!$parserProcessResult['success'] || $this->parserModel->parsedPages>=$this->parserModel->parsePagesCount)
			{
				// не менять статусь чтоб он опстоянно работал
				// т.е. делаем его новым
				//$this->parserModel->status=Parser::STATUS_COMPLETED;
				$this->parserModel->status=Parser::STATUS_ON;
				$this->parserModel->parsedPages=0;
				$this->parserModel->completedDate=date('Y-m-d H:i:s');

				$criteria=new CDbCriteria();
				$criteria->condition='relParserId = :relParserId AND status = :status';
				$criteria->order='id DESC';
				$criteria->limit=1;
				$criteria->params=array('relParserId'=>$this->parserModel->id, 'status'=>ParserProcess::STATUS_COMPLETED);
				$lastParserProcess=ParserProcess::model()->find($criteria);
				$lastParserProcess->lastParsedPageUrl=$this->parserModel->startPageUrl;
				$lastParserProcess->save();
			}

			if(!$this->parserModel->save())
			{
				$result['message']='Cant save ParserProcess model';
				return $result;
			}
			$result=$parserProcessResult;
		}

		return $result;
	}

	/**
	 * @return array
	 */
	public function runParserProcess()
	{
		$result=array(
			'success'=>false,
			'message'=>'Unknown error',
			'totalParsedPages'=>0,
			'totalParsedUsers'=>0,
			'successParsedUsers'=>0,
			'failedParsedUsers'=>0
		);

		// Create parser process - Start {
		$parserProcessModel=new ParserProcess();
		$parserProcessModel->dateStart=date('Y-m-d H:i:s');
		$parserProcessModel->relParserId=$this->parserModel->id;
		if(!$parserProcessModel->save())
		{
			$result['message']='Cant save ParserProcess model, errors: '.join(', ', $parserProcessModel->errors);
			return $result;
		}
		// Create parser process - End }

		// Run parser bot - Start {
		$startPageUrl=$this->parserModel->getStartPageUrl();
		$parsingResult=$this->runParserBot($startPageUrl, $this->parsePagesByCall);
		// Run parser bot - End }

		// Complete parser process - Start {
		$parserProcessModel->dateEnd=date('Y-m-d H:i:s');
		$parserProcessModel->status=ParserProcess::STATUS_COMPLETED;
		$parserProcessModel->totalParsedPages=$parsingResult['totalParsedPages'];
		$parserProcessModel->totalParsedUsers=$parsingResult['totalParsedUsers'];
		$parserProcessModel->successParsedUsers=$parsingResult['successParsedUsers'];
		$parserProcessModel->failedParsedUsers=$parsingResult['failedParsedUsers'];
		$parserProcessModel->lastParsedPageUrl=$parsingResult['lastParsedPageUrl'];
		if(!$parserProcessModel->save())
		{
			$result['message']='Cant save ParserProcess model';
			return $result;
		}
		// Complete parser process - End }

		$result['success']=true;
		$result['message']='';
		$result['totalParsedPages']=$parsingResult['totalParsedPages'];
		$result['totalParsedUsers']=$parsingResult['totalParsedUsers'];
		$result['successParsedUsers']=$parsingResult['successParsedUsers'];
		$result['failedParsedUsers']=$parsingResult['failedParsedUsers'];
		return $result;
	}

	public function runParserBot($startPageUrl, $parsePagesCount)
	{
		$result=array(
			'success'=>true,
			'lastParsedPageUrl'=>'',
			'totalParsedPages'=>$parsePagesCount,
			'totalParsedUsers'=>0,
			'successParsedUsers'=>0,
			'failedParsedUsers'=>0
		);

		$this->webParser->searchPagesByCall=$parsePagesCount;
		$this->webParser->parseItemsUrls($startPageUrl);

		//echo "\n\n pageUrls count: ".count($this->webParser->pageUrls)." \n\n";
		$result['lastParsedPageUrl']=$this->webParser->lastParsedPageUrl;

		// Parse users pages - Start {
		if(count($this->webParser->pageUrls)>0)
		{
			foreach($this->webParser->pageUrls as $eachPageUrl)
			{
				$this->webParser->parseItemByPageUrl($eachPageUrl);
			}
		}
		$result['totalParsedUsers']=count($this->webParser->parsedItems);
		// Parse users pages - End }

		$importedItemsCount=$this->webParser->saveParsedAdvsData();
		$result['successParsedUsers']=$importedItemsCount;
		$result['failedParsedUsers']=$result['totalParsedUsers']-$result['successParsedUsers'];
		$result['users']=count($this->webParser->parsedItems);
		return $result;
	}

	/**
	 * command: php protected/yiic.php parser testrequest
	 */
	public function actionTestRequest()
	{
		// http://makler.md/ru/an/list/search/publisher/1/category/107/page/0?currency_id=1&direction=&field_115%5B%5D=1180&field_169=0&field_169-1=24&field_169_max=24&field_169_min=0&field_172=0&field_172-1=300&field_172_max=300&field_172_min=0&latitude_max=&latitude_min=&longitude_max=&longitude_min=&order=&page=0&price_max=12054600&price_min=0&selector=select&text=

		/**
		 * currency_id    1
		 * direction
		 * field_106[]    149
		 * field_115[]    1180
		 * field_169    0
		 * field_169-1    24
		 * field_169_max    24
		 * field_169_min    0
		 * field_172    0
		 * field_172-1    300
		 * field_172_max    300
		 * field_172_min    0
		 * latitude_max
		 * latitude_min
		 * longitude_max
		 * longitude_min
		 * order
		 * page    0
		 * price_max    12054600
		 * price_min    0
		 * selector    select
		 * text
		 */
		$postData=array(
			'currency_id'=>'1',
			'direction'=>'',
			'field_106[]'=>'149',
			'field_115[]'=>'1180',
			'field_169'=>'0',
			'field_169-1'=>'24',
			'field_169_max'=>'24',
			'field_169_min'=>'0',
			'field_172'=>'0',
			'field_172-2'=>'300',
			'field_172_max'=>'',
			'latitude_max'=>'',
			'latitude_min'=>'',
			'longitude_max'=>'',
			'longitude_min'=>'',
			'order'=>'',
			'page'=>'0',
			'price_max'=>'12054600',
			'price_min'=>'0',
			'selector'=>'select',
			'text'=>'',
		);
		$curl=curl_init();
		// http://makler.md/ru/an/list/search/publisher/1/category/107/page/0
		$url='http://makler.md/ru/an/list/search/publisher/1/category/107/page/0';
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($postData));
		$res=curl_exec($curl);
		if(!$res)
		{
			$error=curl_error($curl).'('.curl_errno($curl).')';
			echo $error;
		}
		else
		{
			echo $res;
		}
		curl_close($curl);
	}
}