<?php

/**
 *   /usr/bin/php /home/p127444/www/rooms.md/protected/yiic mailing addNewSiteNtfs
 *   /usr/bin/php /home/p127444/www/rooms.md/protected/yiic mailing runMailng
 */

Yii::import('application.components.MyMailer');

class MailingCommand extends CConsoleCommand
{
    public function actionRunMailng()
    {
        $this->log('start');
        $result=MyMailer::sendEmailQueues();

        $this->log('total sent: '.$result['totalSend']);
        $this->log('total failed: '.$result['totalFailed']);
        $this->log('end');
    }

    public function actionAddNewSiteNtfs()
    {
        $this->log('start');
        MyMailer::addNewSiteNtfs();
        $this->log('end');
    }

    public function actionAddNewAdvsNtfs()
    {
		$this->log('start');

		$this->log('DeleteOldAdvsNtfs');
		MyMailer::deleteOldAdvsNtfs();

		$this->log('AddNewAdvsNtfs');
        $added=MyMailer::addNewAdvsNtfs();
        $this->log('total added: '.$added);

        $this->log('end');
    }

    public function actionTest()
    {
        $file=dirname(__FILE__).'/../../test.log';
        $fh=fopen($file, 'a+');
        fwrite($fh, date('Y-m-d H:i:s'."\n"));
        fclose($fh);
    }

    private function log($text)
    {
        if($text=='start' || $text=='end' || $text == 'exit')
        {
            echo "\n";
            echo "\n";
            echo "--".strtoupper($text)."--";
            echo "\n";
            echo "\n";
            if ($text != 'start')
                exit;
        }
        else
        {
            echo "\n";
            echo "{$text}";
            echo "\n";
        }
    }
}