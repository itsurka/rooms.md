<?php

/**
 * This is the model class for table "files".
 *
 * The followings are the available columns in table 'files':
 * @property string $id
 * @property string $name
 * @property string $extension
 * @property string $mime_type
 * @property double $size
 * @property string $path
 * @property string $added
 */
class Files extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Files the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'files';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, extension, mime_type, size, path', 'required'),
			array('size', 'numerical'),
			array('name', 'length', 'max'=>100),
			array('extension', 'length', 'max'=>10),
			array('mime_type', 'length', 'max'=>30),
			array('path', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, extension, mime_type, size, path, added', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'=>'ID',
			'name'=>'Name',
			'extension'=>'Extension',
			'mime_type'=>'Mime Type',
			'size'=>'Size',
			'path'=>'Path',
			'added'=>'Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('extension', $this->extension, true);
		$criteria->compare('mime_type', $this->mime_type, true);
		$criteria->compare('size', $this->size);
		$criteria->compare('path', $this->path, true);
		$criteria->compare('added', $this->added, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	protected function afterDelete()
	{
		// Delete file - Start {
		$filename=$this->path.$this->name.'.'.$this->extension;
		if(file_exists($filename))
		{
			@unlink($filename);
		}
		$thumb=Yii::getPathOfAlias('application.uploads.thumbs').'/'.$this->name.'.'.$this->extension;;
		if(file_exists($thumb))
		{
			@unlink($thumb);
		}
		// Delete file - End }

		// Delete AdvertisementFiles - Start {
		AdvertisementFiles::model()->findByAttributes(array('file_id'=>$this->id))->delete();
		// Delete AdvertisementFiles - End }

		return parent::beforeDelete();
	}

	/**
	 * @return string
	 */
	public function getThumbUrl()
	{
		return '/'.Yii::app()->params['ADV_THUMBS_URL'].$this->name.'.'.$this->extension;
	}
}