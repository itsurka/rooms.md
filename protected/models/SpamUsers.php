<?php

/**
 * This is the model class for table "spam_users".
 *
 * The followings are the available columns in table 'spam_users':
 * @property string $id
 * @property string $email
 * @property string $fullname
 * @property string $is_reg_on_roomsmd
 * @property string $is_reg_on_kotvkadreru
 * @property string $is_unsubscribed
 * @property string $unsubscription_date
 */
class SpamUsers extends CActiveRecord
{
    public static $boolValues = array(
        0 => 'Нет',
        1 => 'Да',
    );

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SpamUsers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'spam_users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email, is_reg_on_roomsmd, is_reg_on_kotvkadreru', 'required'),
			array('is_reg_on_roomsmd, is_reg_on_kotvkadreru, is_unsubscribed, unsubscription_date', 'numerical', 'integerOnly'=>true),
			array('email', 'length', 'max'=>50),
			array('fullname', 'length', 'max'=>40),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, email, fullname, is_reg_on_roomsmd, is_unsubscribed, unsubscription_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => '#',
			'email' => 'E-mail',
			'fullname' => 'Имя',
			'is_reg_on_roomsmd' => 'Есть на rooms.md',
			'is_reg_on_kotvkadreru' => 'Есть на kotvkadre.ru',
			'is_unsubscribed' => 'Отписан от рассылки',
			'unsubscription_date' => 'Дата отписки',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('fullname',$this->fullname,true);
		$criteria->compare('is_reg_on_roomsmd',$this->is_reg_on_roomsmd);
		$criteria->compare('is_reg_on_kotvkadreru',$this->is_reg_on_kotvkadreru);
		$criteria->compare('is_unsubscribed',$this->is_unsubscribed);
		$criteria->compare('unsubscription_date',$this->unsubscription_date);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>25
            )
		));
	}

    public static function deleteUsersWithBadEmailAddress()
    {
        $emailValidator=new CEmailValidator();
        $users=self::model()->findAll();
        $deletedUsers=0;

        foreach($users as $user)
        {
            if(!$emailValidator->validateValue($user->email))
            {
                $user->delete();
                $deletedUsers++;
            }
        }
        return $deletedUsers;
    }

    /**
     * Scope
     * @param $limit
     * @return SpamUsers
     */
    public function scopeLimit($limit)
    {
        $criteria = $this->getDbCriteria();
        $criteria->limit=intval($limit);
        $this->setDbCriteria($criteria);
        return $this;
    }
}