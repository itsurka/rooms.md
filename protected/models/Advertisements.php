<?php

/**
 * This is the model class for table "advertisements".
 *
 * The followings are the available columns in table 'advertisements':
 * @property string $id
 * @property string $title
 * @property string $text
 * @property string $number_of_rooms
 * @property integer $owner_id
 * @property integer $owner_ip
 * @property integer $owner_phone
 * @property string $owner_name
 * @property integer $price
 * @property integer $city_id
 * @property integer $region_id
 * @property string $added
 * @property string $first_added
 * @property string $modified
 * @property integer $views
 * @property integer $uniqueViews
 * @property integer $currency_id
 * @property string $status
 * @property string $mdl_price
 * @property string $rent_type
 * @property string $is_vip
 * @property string $is_colored
 * @property string $is_parsed
 * @property string $parsed_from_url
 *
 * The followings are the available model relations:
 * @property Files[] $files
 * @property integer $filesCount
 * @property Currencies $currency
 */
class Advertisements extends CActiveRecord
{
	const ADV_NEW='new';
	const ADV_OK='ok';
	const ADV_MODIFIED='modified';
	const ADV_BLOCKED='blocked';
	const ADV_DELETED='deleted';

	public static $statuses=array(
		self::ADV_NEW=>'Новый, ждет проверки',
		self::ADV_OK=>'Показывается',
		self::ADV_MODIFIED=>'Изменен, ждет проверки',
		self::ADV_BLOCKED=>'Заблокировано',
		self::ADV_DELETED=>'Удален',
	);

	// тип обявлений, сдать, снять и т.д.
	public static $rentTypes=array(
		1=>'Сдаю',
		2=>'Сниму',
		3=>'Продаю',
		4=>'Куплю'
	);

	public static $rentTypesTranslit=array(
		1=>'sdaiu',
		2=>'snimu',
		3=>'prodaiu',
		4=>'cupliu'
	);

	public static $boolOptions=array(
		0=>'Нет',
		1=>'Да'
	);

	public static $allowPublicStatuses=array(
		self::ADV_OK,
		self::ADV_MODIFIED
	);

	public static $searchParams=array(
		'rentType'=>array(
			'key'=>'rentType',
			'attribute'=>'rentType',
			'defaultValue'=>'tip-obiavleniya',
		),
		'numOfRooms'=>array(
			'key'=>'numOfRooms',
			'attribute'=>'numOfRooms',
			'defaultValue'=>'kolicestvo-komnat',
		),
		'city'=>array(
			'key'=>'city',
			'attribute'=>'city',
			'defaultValue'=>'vse-goroda',
		),
		'region'=>array(
			'key'=>'region',
			'attribute'=>'region',
			'defaultValue'=>'vse-raiony',
		),
	);

	// сначение по-умолчанию для кол-ва комнат
	//public $number_of_rooms="1";

	// количество комнат
	public static $numbersOfRooms=array(
		1=>'1 комн.',
		2=>'2 комн.',
		3=>'3 комн.',
		4=>'4 комн. и более'
	);
	public static $numbersOfRoomsTranslit=array(
		1=>'1-komn',
		2=>'2-komn',
		3=>'3-komn',
		4=>'4-komn_i_bolee',
	);

	public static $sortOptions=array(
		'date'=>'По дате',
		'price'=>'По цене'
	);

	public $translitId;

	public function scopes()
	{
		$statuses=array();
		foreach(self::$allowPublicStatuses as $value)
		{
			$statuses[]="'{$value}'";
		}

		return array(
			'sitemap'=>array(
				'select'=>'id, title',
				'condition'=>'status IN ('.join(',', $statuses).')',
				'order'=>'added ASC'
			),
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Advertisements the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'advertisements';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('title, text, number_of_rooms, region_id, currency_id, rent_type', 'required'),
			array('number_of_rooms, owner_id, price, city_id, region_id, views, uniqueViews, currency_id, mdl_price, is_vip, is_colored, is_parsed', 'numerical', 'integerOnly'=>true),
			array('title, parsed_from_url', 'length', 'max'=>255),
			array('text', 'length', 'max'=>15000),
			array('status', 'length', 'max'=>8),
			array('owner_ip', 'length', 'max'=>50),
			array('owner_phone', 'application.components.validators.phoneNumber'),
			array('owner_name', 'length', 'max'=>25),
			array('rent_type', 'in', 'range'=>array_keys(self::$rentTypes), 'allowEmpty'=>false),
			array('id, title, text, number_of_rooms, owner_id, owner_ip, price, city_id, region_id, added, first_added, views, uniqueViews, currency_id, status, mdl_price', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'files'=>array(self::MANY_MANY, 'Files', 'advertisement_files(advertisement_id, file_id)'),
			'filesCount'=>array(self::STAT, 'Files', 'advertisement_files(advertisement_id, file_id)'),
			'owner'=>array(self::BELONGS_TO, 'Users', 'owner_id'),
			'city'=>array(self::BELONGS_TO, 'Cities', 'city_id'),
			'region'=>array(self::BELONGS_TO, 'CityRegions', 'region_id'),
			'currency'=>array(self::BELONGS_TO, 'Currencies', 'currency_id'),
			'vip'=>array(self::HAS_ONE, 'VipAdv', 'rel_adv_id'),
		);
	}

	public function behaviors()
	{
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'=>'ID',
			'title'=>'Заголовок',
			'text'=>'Текст',
			'number_of_rooms'=>'Количество комнат',
			'owner_id'=>'ID владельца',
			'owner_ip'=>'IP владельца',
			'owner_phone'=>'Телефон',
			'owner_name'=>'Имя',
			'price'=>'Цена',
			'city_id'=>'Город',
			'region_id'=>'Район',
			'added'=>'Добавлено',
			'first_added'=>'Добавлено',
			'views'=>'Просмотров',
			'uniqueViews'=>'Уникальных просмотров',
			'currency_id'=>'Валюта',
			'status'=>'Статус',
			'mdl_price'=>'Цена MDL',
			'rent_type'=>'Тип об-ия',
			'is_vip'=>'VIP',
			'is_colored'=>'Выделить цветом',
			'is_parsed'=>'Скопирован',
			'parsed_from_url'=>'Скопирован с URL',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=$this->getDbCriteria();

		$criteria->compare('id', $this->id, true);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('text', $this->text, true);
		$criteria->compare('number_of_rooms', $this->number_of_rooms);
		$criteria->compare('price', $this->price);
		$criteria->compare('city_id', $this->city_id);
		$criteria->compare('region_id', $this->region_id);
		$criteria->compare('added', $this->added, true);
		$criteria->compare('first_added', $this->first_added, true);
		$criteria->compare('views', $this->views);
		$criteria->compare('uniqueViews', $this->uniqueViews);
		$criteria->compare('status', $this->status, true);
		$criteria->compare('rent_type', $this->rent_type);
		$criteria->compare('owner_id', $this->owner_id);
		$criteria->compare('is_vip', $this->is_vip);
		$criteria->compare('is_colored', $this->is_colored);
		$criteria->compare('is_parsed', $this->is_parsed);
		$criteria->compare('parsed_from_url', $this->parsed_from_url, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'added DESC',
			),
		));
	}

	/**
	 * Get advertisements
	 * @throws CHttpException
	 * @param null $criteria
	 * @param array $pagination
	 * @param bool $returnDataProvider
	 * @return array|CActiveDataProvider
	 */
	public function retrieve($criteria=null, $pagination=array(), $returnDataProvider=true)
	{
		if($criteria===null)
			$criteria=$this->getDbCriteria();

		/*if($criteria->select=='*')
		{
			Currency::loadExchange();
			$criteria->select="t.*, CASE
                                        WHEN t.currency_id=1 THEN t.price
                                        WHEN t.currency_id=2 THEN t.price*".intval(Currency::$exchange->EUR)."
                                        ELSE t.price*".intval(Currency::$exchange->USD)."
                                        END AS mdl_price";
		}*/

		$criteria->group='t.id';
		$criteria->with['files'] = array();
		$criteria->together = true;

		if($returnDataProvider)
		{
			$dataProvider=new CActiveDataProvider($this->tableName(), array(
				'criteria'=>$criteria,
				'pagination'=>$pagination,
			));
		}
		else
		{
			$dataProvider=$this->findAll();
		}

		if($dataProvider===null)
		{
			throw new CHttpException(404, 'The requested page does not exist.');
		}

		return $dataProvider;
	}

	/**
	 * Get products by owner_id
	 *
	 * @param $user_id
	 * @return CActiveDataProvider
	 */
	public function retrieveByUserID($user_id)
	{
		$criteria=new CDbCriteria();
		$criteria->addNotInCondition('status', array(Advertisements::ADV_DELETED));
		$criteria->addInCondition('owner_id', array($user_id));
		$criteria->order='added DESC';
		$pagination=array();

		return $this->retrieve($criteria, $pagination);
	}

	/**
	 * @param $status
	 * @param bool $returnLabel
	 * @return bool
	 */
	public static function statusAlias($status, $returnLabel=false)
	{
		$result=self::$statuses[$status];

		switch($status)
		{
			case self::ADV_OK:
				$labelCssClass='success';
				break;
			case self::ADV_NEW:
			case self::ADV_MODIFIED:
				$labelCssClass='warning';
				break;
			case self::ADV_BLOCKED:
			case self::ADV_DELETED:
				$labelCssClass='danger';
				break;
			default:
				$labelCssClass='danger';
		}
		if($returnLabel)
			$result='<span class="label label-'.$labelCssClass.'">'.$result.'</span>';

		return $result;
	}

	/**
	 * Increment item views
	 *
	 * @param $id
	 * @param int $inc
	 * @return bool
	 */
	public function makeView($id, $inc=1)
	{
		$request=Yii::app()->getRequest();
		$result=false;
		$model=$this->findByPk($id);
		if($model)
		{
			// Update item view + 1
			$model->views+=$inc;

			// Check if view is unique
			$criteria=new CDbCriteria();
			$criteria->condition='advertisement_id=:advertisement_id';
			$criteria->params=array(':advertisement_id'=>$model->id);
			if(!Yii::app()->user->isGuest)
			{
				$criteria->condition.=' AND user_id=:user_id';
				$criteria->params=array_merge($criteria->params, array(':user_id'=>Yii::app()->user->id));
			}
			else
			{
				$criteria->condition.=' AND user_ip=:user_ip';
				$criteria->params=array_merge($criteria->params, array(':user_ip'=>$request->getUserHost()));
			}
			$oldView=AdvertisementViews::model()->find($criteria);

			if($oldView===null)
			{
				// Is unique view
				$model->uniqueViews+=$inc;

				$AdvertisementViewsModel=new AdvertisementViews();
				$AdvertisementViewsModel->advertisement_id=$model->id;
				if(!Yii::app()->user->isGuest)
				{
					$AdvertisementViewsModel->user_id=Yii::app()->user->id;
				}
				$AdvertisementViewsModel->user_ip=$request->getUserHost();
				$result=$AdvertisementViewsModel->save();
			}
			$model->save();
		}
		return $result;
	}

	/**
	 * @return array|mixed
	 */
	public function countItems()
	{
		$result=Yii::app()->cache->get("countItems");
		if(!$result)
		{
			$result=array();

			// Count items added today
			$today=date('Y-m-d');
			$criteria=new CDbCriteria();
			$criteria->condition="DATE(added)='{$today}'";
			$criteria->addInCondition('status', array(Advertisements::ADV_OK));
			$result[0]=$this->count($criteria);

			// Count all items
			$criteria=new CDbCriteria();
			$criteria->addInCondition('status', array(Advertisements::ADV_OK));
			$result[1]=$this->count($criteria);
			Yii::app()->cache->set("countItems", $result, 600);
		}
		Yii::app()->cache->delete('countItems');
		return $result;
	}

	/**
	 * @param $rooms
	 * @return array|mixed
	 */
	public function countItemsByRooms($rooms)
	{
		$result=Yii::app()->cache->get("countItemsByRooms_".$rooms);
		if(!$result)
		{
			$result=array();
			// Count items added today
			$from=date("Y-m-d");
			$criteria=new CDbCriteria();
			$criteria->condition='status=:status AND number_of_rooms=:number_of_rooms AND added>=:from';
			$criteria->params=array(
				':status'=>'ok',
				':number_of_rooms'=>$rooms,
				':from'=>$from,
			);
			$result[0]=$this->count($criteria);

			// Count all items
			$criteria=new CDbCriteria();
			$criteria->condition='status=:status AND number_of_rooms=:number_of_rooms';
			$criteria->params=array(
				':status'=>'ok',
				':number_of_rooms'=>$rooms,
			);
			$result[1]=$this->count($criteria);
			Yii::app()->cache->set("countItemsByRooms_".$rooms, $result, 600);
		}
		return $result;
	}

	protected function afterDelete()
	{
		parent::afterDelete();
		foreach($this->files as $eachFile)
		{
			Files::model()->findByPk($eachFile->id)->delete();
		}
	}

	protected function afterFind()
	{
		parent::afterFind();

		$this->translitId=Translit::encodestring($this->title.'-'.$this->id);
	}

	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->getIsNewRecord())
			{
				$this->first_added=date('Y-m-d H:i:s');
				$this->added=date('Y-m-d H:i:s');
			}
			$this->owner_phone=trim($this->owner_phone);

			return true;
		}
		else
			return false;
	}

	/**
	 * Путое значение для списков
	 * @static
	 * @return array
	 */
	public static function getEmptyOption()
	{
		return array(''=>'Выберите');
	}

	/**
	 * Номер телефона владельца об-ия
	 * @return string
	 */
	public function getOwnerPhoneNumber()
	{
		$phoneNumber='';
		if($this->owner_phone!='')
		{
			$phoneNumber=$this->owner_phone;
		}
		elseif($this->owner)
		{
			$phoneNumber=$this->owner->profile->phone();
		}

		return $phoneNumber;
	}

	/**
	 * @return string
	 */
	public function getOwnerName()
	{
		$ownerName='';
		if($this->owner_name!='')
		{
			$ownerName=$this->owner_name;
		}
		elseif($this->owner)
		{
			$ownerName=$this->owner->profile->getFullName();
		}

		return $ownerName;
	}

	public function isPublished()
	{
		return in_array($this->status, self::$allowPublicStatuses);
	}

	public function getStatusTextToOwner()
	{
		$status='';

		if(!$this->isPublished())
		{
			switch($this->status)
			{
				case Advertisements::ADV_NEW:
					$status='Ожидает проверки, не показывается';
					break;

				case Advertisements::ADV_BLOCKED:
					$status='Заблокировано, не показывется';
					break;

				case Advertisements::ADV_MODIFIED:
					$status='Изменено, ожидает проверки, не показывается';
					break;

				case Advertisements::ADV_DELETED:
					$status='Удалено';
					break;
			}
		}
		else
			$status='Показывается на сайте';

		return $status;
	}

	public function getViewUrl()
	{
		return Yii::app()->createUrl('catalog/view', array('id'=>Translit::encodestring($this->title.'-'.$this->id)));
	}

	public function getViewLink($etxt)
	{
		return Yii::app()->createUrl('catalog/view', array('id'=>Translit::encodestring($this->title.'-'.$this->id)));
	}

	public function getStatusImgToOwner()
	{
		$icon=$this->isPublished() ? 'accept' : 'error';
		return CIcon::init($icon)->getImg();
	}

	/**
	 * Scope
	 * @return Advertisements
	 */
	public function scopeIsPublished()
	{
		$criteria=$this->getDbCriteria();
		$criteria->addInCondition('status', Advertisements::$allowPublicStatuses);
		$this->setDbCriteria($criteria);
		return $this;
	}

	public function scopeGetVipAdv()
	{
		$criteria=$this->getDbCriteria();
		$criteria->join="INNER JOIN vip_adv va ON t.id = va.rel_adv_id";
		$criteria->order='va.vert_position ASC';
		$this->setDbCriteria($criteria);
		return $this;
	}

	/**
	 * Scope
	 * @return Advertisements
	 */
	public function scopeAddedToday()
	{
		$criteria=$this->getDbCriteria();
		$criteria->order='added DESC';
		$today=date('Y-m-d').' 00:00:00';
		$criteria->addCondition("added >= '{$today}' AND added <= 'UTC_TIMESTAMP()'");
		$this->setDbCriteria($criteria);
		return $this;
	}

	/**
	 * Scope
	 * @param int $time
	 * @return Advertisements
	 */
	public function scopeAddedFrom($time)
	{
		$criteria=$this->getDbCriteria();
		$timestamp=date('Y-m-d H:i:s', $time);
		$criteria->addCondition("t.added >= :added");
		$criteria->params['added']=$timestamp;
		$this->setDbCriteria($criteria);
		return $this;
	}

	/**
	 * Scope
	 * @param $limit
	 * @return Advertisements
	 */
	public function scopeLimit($limit)
	{
		$criteria=$this->getDbCriteria();
		$criteria->limit=intval($limit);
		$this->setDbCriteria($criteria);
		return $this;
	}

	/**
	 * Scope
	 * @param $order
	 * @return Advertisements
	 */
	public function scopeOrder($order)
	{
		$criteria=$this->getDbCriteria();
		$criteria->order=trim($order);
		$this->setDbCriteria($criteria);
		return $this;
	}

	/**
	 * Scope
	 * @return Advertisements
	 */
	public function scopeDefaultCatalogOrder()
	{
//		return $this->scopeOrder('id DESC, is_colored DESC');
		return $this->scopeOrder('id DESC');
	}

	public static function getVipAdv()
	{
		$criteria=new CDbCriteria();
		$criteria->addInCondition('t.status', Advertisements::$allowPublicStatuses);
		$criteria->addCondition('t.is_vip = 1');
		$criteria->order='t.id DESC';
		$criteria->group='t.id';
		$criteria->with['files']=array();
		$criteria->together=true;
		return Advertisements::model()->findAll($criteria);
	}

	/*public static function getCatalogSearchUrl($additionalParams=array())
	{
		self::$searchParams=array(
			array(
				'key'=>'a',
				'attribute'=>'rentType',
				'defaultValue'=>'sdam,snimu,kupliu,prodam',
			),
			array(
				'key'=>'b',
				'attribute'=>'numOfRooms',
				'defaultValue'=>'1-komn,2-komn,3-komn',
			),
			array(
				'key'=>'c',
				'attribute'=>'city',
				'defaultValue'=>'vse-goroda',
			)
		);

		$params=array('catalog/catalog');
		foreach (self::$searchParams as $eachParam)
		{
			if (array_key_exists($eachParam['attribute'],$additionalParams))
			{
				$_v=$additionalParams[$eachParam['attribute']];
			}
			elseif (isset($_REQUEST[$eachParam['key']]))
			{
				$_v=$_REQUEST[$eachParam['key']];
			}
			else
			{
				$_v=$eachParam['defaultValue'];
			}

			$params[$eachParam['key']]=trim($_v);
		}

		return $params;
	}*/

	public static function getCatalogSearchUrl($additionalParams=array())
	{
		/*self::$searchParams=array(
			array(
				'key'=>'a',
				'attribute'=>'rentType',
//                'defaultValue'=>'sdam,snimu,kupliu,prodam',
				'defaultValue'=>'tip-obiavleniya',
			),
			array(
				'key'=>'b',
				'attribute'=>'numOfRooms',
//                'defaultValue'=>'1-komn,2-komn,3-komn',
				'defaultValue'=>'1-2-3-komn',
			),
			array(
				'key'=>'c',
				'attribute'=>'city',
				'defaultValue'=>'vse-goroda',
			),
			array(
				'key'=>'d',
				'attribute'=>'region',
				'defaultValue'=>'vse-raiony',
			),
		);*/

		//$params=array('catalog/catalog');
		$params=array();
		foreach(self::$searchParams as $eachParam)
		{
			if(array_key_exists($eachParam['attribute'], $additionalParams))
			{
				$_v=$additionalParams[$eachParam['attribute']];
			}
			elseif(isset($_REQUEST[$eachParam['key']]))
			{
				$_v=$_REQUEST[$eachParam['key']];
			}
			else
			{
				$_v=$eachParam['defaultValue'];
			}

			$params[$eachParam['key']]=trim($_v);
		}

		return $params;
	}

	public function scopeApplySearchCityAndRegionsNames($citiesStr, $regionsStr='')
	{
		$citiesValues=Common::getParsedUriValue($citiesStr);
		$regionsValues=Common::getParsedUriValue($regionsStr);

		if(count($citiesValues)==0)
			return $this;

		$criteria=new CDbCriteria();
		$criteria->addInCondition('name_translit', $citiesValues);
		$cities=Cities::model()->findAll($criteria);

		if(count($cities)>0)
		{
			$queryArr=array();
			foreach($cities as $eachCity)
			{
				$regions=array();

				$criteria=new CDbCriteria();
				$criteria->addColumnCondition(array('city_id'=>$eachCity->id));
				$criteria->addInCondition('name_translit', $regionsValues);
				$cityRegions=CityRegions::model()->findAll($criteria);

				foreach($cityRegions as $eachCityRegion)
				{
					if($eachCityRegion->city_id==$eachCity->id)
						$regions[]=$eachCityRegion->id;
				}

				if(count($regions)==0)
				{
					$queryArr[]="(city_id={$eachCity->id})";
				}
				else
				{
					$queryArr[]="(city_id={$eachCity->id} AND region_id IN (".join(',', $regions)."))";
				}
			}
			if(count($queryArr)>0)
			{
				$criteria=$this->getDbCriteria();
				$query=join(' OR ', $queryArr);
				$criteria->addCondition($query);
				$this->setDbCriteria($criteria);
			}
		}

//		echo '$this->getDbCriteria()[]<pre>';
//		print_r($this->getDbCriteria()->condition);
//		echo '</pre>';

//		exit('exit in '.__METHOD__.' method');

		/*
		if (count($citiesArr)>0)
		{
			$criteria=$this->getDbCriteria();
			$criteria->addInCondition('city_id', array_keys($citiesArr));
			$this->setDbCriteria($criteria);
		}

		// aaaaa

		$values=Common::getParsedUriValue($value);

		if (count($values)==0)
			return $this;

		$criteria=new CDbCriteria();
		$criteria->addInCondition('name_translit', $values);
		$cityRegions=CityRegions::model()->findAll($criteria);
		$cityRegionsArr=CHtml::listData($cityRegions, 'id', 'name');

		if (count($cityRegionsArr)>0)
		{
			$criteria=$this->getDbCriteria();
			$criteria->addInCondition('region_id', array_keys($cityRegionsArr));
			$this->setDbCriteria($criteria);
		}

		return $this;*/

		return $this;
	}

	public function scopeApplySearchRentType($value)
	{
		$values=Common::getParsedUriValue($value);

		$rentTypes=array();
		foreach($values as $v)
		{
			if($k=array_search($v, Advertisements::$rentTypesTranslit))
				$rentTypes[]=$k;
		}

		if(count($rentTypes)==0)
			return $this;

		$criteria=$this->getDbCriteria();
		$criteria->addInCondition('rent_type', $rentTypes);
		$this->setDbCriteria($criteria);

		return $this;
	}

	public function scopeApplySearchNumOfRooms($value)
	{
		$values=Common::getParsedUriValue($value);

		$numOfRooms=array();
		foreach($values as $v)
		{
			if($k=array_search($v, Advertisements::$numbersOfRoomsTranslit))
				$numOfRooms[]=$k;
		}

		if(count($numOfRooms)==0)
			return $this;

		$criteria=$this->getDbCriteria();
		$criteria->addInCondition('number_of_rooms', $numOfRooms);
		$this->setDbCriteria($criteria);

		return $this;
	}

	/*public function scopeApplySearchCityRegionName($value)
	{
		$values=Common::getParsedUriValue($value);

		if (count($values)==0)
			return $this;

		$criteria=new CDbCriteria();
		$criteria->addInCondition('name_translit', $values);
		$cityRegions=CityRegions::model()->findAll($criteria);
		$cityRegionsArr=CHtml::listData($cityRegions, 'id', 'name');

		if (count($cityRegionsArr)>0)
		{
			$criteria=$this->getDbCriteria();
			$criteria->addInCondition('region_id', array_keys($cityRegionsArr));
			$this->setDbCriteria($criteria);
		}

		return $this;
	}*/

	public static function getIsDefaultValueSearchParam($param, $value)
	{
		return (isset(self::$searchParams[$param]) && self::$searchParams[$param]['defaultValue']==$value);
	}

	/**
	 * @return null|string
	 */
	public function getMainImageUrl()
	{
		$return='http://placehold.it/125x100';
		if($this->getHasImage())
			$return=Yii::app()->baseUrl.'/'.Yii::app()->params['ADV_THUMBS_URL'].$this->files[0]->name.'.'.$this->files[0]->extension;
		return $return;
	}

	/**
	 * @return null|string
	 */
	public function getHasImage()
	{
		return isset($this->files[0]);
	}

	/**
	 * Прорерка объявления если валидно
	 *
	 * @param $advArr
	 * @return bool
	 */
	public static function getIsValid($advArr)
	{
		return (empty($advArr['cityId']) || empty($advArr['regionId']) || empty($advArr['ownerPhone']) || empty($advArr['title']) || empty($advArr['text'])) ? false : true;
	}

	public function getCurrencyUserFriendly()
	{
		$return = 'Лей';
		if ($this->currency_id == 2) {
			$return = '&euro;';
		} elseif ($this->currency_id == 3) {
			$return = '$';
		}
		return $return;
	}
}
