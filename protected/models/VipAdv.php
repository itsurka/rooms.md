<?php

/**
 * This is the model class for table "vip_adv".
 *
 * The followings are the available columns in table 'vip_adv':
 * @property string $id
 * @property integer $rel_adv_id
 * @property integer $rel_user_id
 * @property integer $vert_position
 * @property string $created
 */
class VipAdv extends CActiveRecord
{
    public $grid_cell_vip_adv;
    
    /*const VIP_ADV_NEW=0;
    const VIP_ADV_OK=1;
    const VIP_ADV_HIDDEN=2;
    const VIP_ADV_DELETED=3;

	public static $statuses=array(
        self::VIP_ADV_NEW => 'Новый',
        self::VIP_ADV_OK => 'Активен',
        self::VIP_ADV_HIDDEN => 'Спрятан',
        self::VIP_ADV_DELETED => 'Удален',
    );*/

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return VipAdv the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vip_adv';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('rel_adv_id', 'required'),
			array('rel_adv_id, rel_user_id, vert_position', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, rel_adv_id, rel_user_id, vert_position, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'adv' => array(self::BELONGS_TO, 'Advertisements', 'rel_adv_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'rel_adv_id' => 'Rel Adv',
			'rel_user_id' => 'Rel User',
			'vert_position' => 'Vert Position',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=$this->getDbCriteria();

		$criteria->compare('id',$this->id,true);
		$criteria->compare('rel_adv_id',$this->rel_adv_id);
		$criteria->compare('rel_user_id',$this->rel_user_id);
		$criteria->compare('vert_position',$this->vert_position);
		$criteria->compare('created',$this->created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    protected function beforeSave()
    {
        if(parent::beforeSave())
        {
            if($this->getIsNewRecord())
                $this->vert_position=$this->findNewVertPosition();

            return true;
        }
        else
            return false;
    }

    /**
     * @return int
     */
    public function findNewVertPosition()
    {
        $newVertPosition=0;

        $criteria=new CDbCriteria();
        $criteria->order='vert_position DESC';
        $criteria->limit=1;
        $lastModel=self::model()->find($criteria);
        if($lastModel!==null)
        {
            $newVertPosition=$lastModel->vert_position+1;
        }
        return $newVertPosition;
    }
}