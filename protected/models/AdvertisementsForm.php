<?php

class AdvertisementsForm extends CFormModel
{

    public $text;
    public $number_of_rooms;
    public $images;


    public function rules()
    {
        return array(
            array('text', 'required'),
            array('number_of_rooms', 'numerical', 'integerOnly'=>true),
            array('images', 'file', 'types'=>'jpg, png, gif', 'maxFiles'=>1, 'allowEmpty'=>false),
        );
    }

}