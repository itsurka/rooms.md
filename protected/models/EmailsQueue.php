<?php

/**
 * This is the model class for table "emails_queue".
 *
 * The followings are the available columns in table 'emails_queue':
 * @property integer $email_queue_id
 * @property string $subject
 * @property string $body
 * @property integer $status
 * @property string $recipientEmail
 * @property integer $created
 * @property integer $modified
 * @property integer $send_since_date
 * @property integer $sent_date
 * @property integer $errorMessage
 */
class EmailsQueue extends CActiveRecord
{
    const EmailQueue_STATUS_NEW = 0;
    const EmailQueue_STATUS_SENT = 1;
    const EmailQueue_STATUS_SENDING = 2;
    const EmailQueue_STATUS_FAILED = 3;

    public static $statuses = array(
        self::EmailQueue_STATUS_NEW => 'В ожидании',
        self::EmailQueue_STATUS_SENT => 'Отправлен',
        self::EmailQueue_STATUS_SENDING => 'Отправляется',
        self::EmailQueue_STATUS_FAILED => 'Ошибка',
    );

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EmailsQueue the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'emails_queue';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('subject, body, recipientEmail', 'required'),
			array('status, created, modified, send_since_date, sent_date', 'numerical', 'integerOnly'=>true),
			array('subject, errorMessage', 'length', 'max'=>255),
			array('recipientEmail', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('email_queue_id, subject, body, status, recipientEmail, created, modified, send_since_date, sent_date, errorMessage', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'email_queue_id' => '#',
			'subject' => 'Тема',
			'body' => 'Сообщение',
			'status' => 'Статус',
			'recipientEmail' => 'E-mail адрес',
			'created' => 'Добавлено',
			'modified' => 'Изменено',
			'send_since_date' => 'Отправить с',
			'sent_date' => 'Дата отправки',
			'errorMessage' => 'Текст ошибки',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('email_queue_id',$this->email_queue_id);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('body',$this->body,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('recipientEmail',$this->recipientEmail,true);
		$criteria->compare('created',$this->created);
		$criteria->compare('modified',$this->modified);
		$criteria->compare('send_since_date',$this->send_since_date);
		$criteria->compare('sent_date',$this->sent_date);
		$criteria->compare('errorMessage',$this->errorMessage);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    protected function beforeSave()
    {
        if(!parent::beforeSave())
			return false;

		if($this->getIsNewRecord())
		{
			if(empty($this->send_since_date))
				$this->send_since_date=time();
			if(empty($this->created))
				$this->created=time();
		}
		return true;
    }
}