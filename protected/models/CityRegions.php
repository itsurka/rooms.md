<?php

/**
 * This is the model class for table "city_regions".
 *
 * The followings are the available columns in table 'city_regions':
 * @property integer $id
 * @property integer $city_id
 * @property string $name
 * @property string $name_translit
 */
class CityRegions extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CityRegions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'city_regions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('city_id, name', 'required'),
			array('city_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>25),
			array('name_translit', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, city_id, name, name_translit', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'city_id' => 'City',
			'name' => 'Name',
			'name_translit' => 'Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=$this->getDbCriteria();

		$criteria->compare('id',$this->id);
		$criteria->compare('city_id',$this->city_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('name_translit',$this->name_translit,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Retrieve items
	 */
	public function retrieve()
	{
		return $this->findAll();
	}

    /**
     * Scope
     * @return CityRegions
     */
    public function scopeApplySearchedCityID()
    {
        $session=new CHttpSession();
        $cityID=0;

        if($session->get('city')!==null && $session->get('city')!==0)
            $cityID=$session->get('city');

        $criteria = $this->getDbCriteria();
        $criteria->addColumnCondition(array('city_id'=>$cityID));
        $this->setDbCriteria($criteria);

        return $this;
    }
}