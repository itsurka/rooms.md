<?php

/**
 * This is the model class for table "config".
 *
 * The followings are the available columns in table 'config':
 * @property integer $id
 * @property integer $key
 * @property string $name
 * @property string $value
 * @property string $dateCreated
 */
class Config extends CActiveRecord
{
	/** @var array ID VIP об-ий */
	const VIP_ADVS = 0;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'config';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('key, name, dateCreated', 'required'),
			array('key', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>30),
			array('value', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, key, name, value, dateCreated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'=>'ID',
			'key'=>'Key',
			'name'=>'Name',
			'value'=>'Value',
			'dateCreated'=>'Date Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('key', $this->key);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('value', $this->value, true);
		$criteria->compare('dateCreated', $this->dateCreated, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Config the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	protected function afterFind()
	{
		parent::afterFind();

		$value=unserialize($this->value);
		$this->value=is_array($value) ? $value : [];
	}

	protected function beforeSave()
	{
		if(!parent::beforeSave())
			return false;

		$this->value=serialize($this->value);

		return true;
	}

	protected function afterSave()
	{
		parent::afterSave();

		$value=unserialize($this->value);
		$this->value=is_array($value) ? $value : [];
	}
}
