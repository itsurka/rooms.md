<?php

/**
 * This is the model class for table "access_log".
 *
 * The followings are the available columns in table 'access_log':
 * @property integer $access_log_id
 * @property string $user_ip
 * @property string $user_agent
 * @property string $request_uri
 * @property string $timestamp
 */
class AccessLog extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AccessLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'access_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_ip, user_agent, request_uri, timestamp', 'required'),
			array('user_ip', 'length', 'max'=>20),
			array('user_agent', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('access_log_id, user_ip, user_agent, request_uri, timestamp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'access_log_id'=>'Access Log',
			'user_ip'=>'User Ip',
			'user_agent'=>'User Agent',
			'request_uri'=>'Request Uri',
			'timestamp'=>'Timestamp',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('access_log_id', $this->access_log_id);
		$criteria->compare('user_ip', $this->user_ip, true);
		$criteria->compare('user_agent', $this->user_agent, true);
		$criteria->compare('request_uri', $this->request_uri, true);
		$criteria->compare('timestamp', $this->timestamp, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'t.access_log_id DESC',
			),
		));
	}

	/**
	 * @static
	 * @return bool
	 */
	public static function add()
	{
		$request=Yii::app()->getRequest();
		$comm=Yii::app()->getDb()->createCommand();

		if($request->getRequestType()=='GET')
		{
			return (bool)$comm->insert(self::model()->tableName(), [
				'user_ip'=>$request->getUserHostAddress(),
				'user_agent'=>$request->getUserAgent(),
				'request_uri'=>$request->getRequestUri(),
				'timestamp'=>date('Y-m-d H:i:s'),
			]);
		}
		return false;
	}

	/**
	 * @param int $week
	 * @return array
	 */
	public static function generateWeekStatistic($week = 2)
	{
		$sql = "SELECT
				  DATE(timestamp) AS   date,
				  count(access_log_id) hits,
				  user_ip,
				  user_agent
				FROM access_log
				WHERE DATE(timestamp) > DATE_SUB(CURRENT_DATE, INTERVAL ".intval($week)." WEEK)
				GROUP BY user_ip
				ORDER BY date(timestamp) DESC, hits DESC";

		return Yii::app()->getDb()->createCommand($sql)->queryAll();
	}
}
