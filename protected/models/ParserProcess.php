<?php

/**
 * This is the model class for table "tbl_parser_process".
 *
 * The followings are the available columns in table 'tbl_parser_process':
 * @property integer $id
 * @property integer $relParserId
 * @property string $dateStart
 * @property string $dateEnd
 * @property integer $totalParsedPages
 * @property integer $totalParsedUsers
 * @property integer $successParsedUsers
 * @property integer $failedParsedUsers
 * @property integer $lastParsedPageUrl
 * @property integer $status
 */
class ParserProcess extends CActiveRecord
{
	const STATUS_RUNNING=0;
	const STATUS_COMPLETED=1;

	public static $statuses=array(
		self::STATUS_RUNNING=>'Active',
		self::STATUS_COMPLETED=>'Not Active',
	);

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ParserProcess the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'parser_process';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('relParserId, dateStart', 'required'),
			array('relParserId, totalParsedPages, totalParsedUsers, successParsedUsers, failedParsedUsers, status', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, relParserId, dateStart, dateEnd, totalParsedPages, totalParsedUsers, successParsedUsers, failedParsedUsers, lastParsedPageUrl, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'relParserId' => 'Rel Parser',
			'dateStart' => 'Date Start',
			'dateEnd' => 'Date End',
			'totalParsedPages' => 'Total Parsed Pages',
			'totalParsedUsers' => 'Total Parsed Users',
			'successParsedUsers' => 'Success Parsed Users',
			'failedParsedUsers' => 'Failed Parsed Users',
			'lastParsedPageUrl' => 'lastParsedPageUrl',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('relParserId',$this->relParserId);
		$criteria->compare('dateStart',$this->dateStart,true);
		$criteria->compare('dateEnd',$this->dateEnd,true);
		$criteria->compare('totalParsedPages',$this->totalParsedPages);
		$criteria->compare('totalParsedUsers',$this->totalParsedUsers);
		$criteria->compare('successParsedUsers',$this->successParsedUsers);
		$criteria->compare('failedParsedUsers',$this->failedParsedUsers);
		$criteria->compare('lastParsedPageUrl',$this->lastParsedPageUrl);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function beforeSave()
	{
		if(!parent::beforeSave())
			return false;
		return true;
	}
}