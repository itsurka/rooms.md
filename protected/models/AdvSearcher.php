<?php

/**
 * This is the model class for table "adv_searcher".
 *
 * The followings are the available columns in table 'adv_searcher':
 * @property string $id
 * @property string $values
 * @property integer $rel_user_id
 * @property integer $email_address
 * @property integer $last_email_ntf_adv_id
 * @property integer $created
 */
class AdvSearcher extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AdvSearcher the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'adv_searcher';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('values, created', 'required'),
			array('rel_user_id, last_email_ntf_adv_id, created', 'numerical', 'integerOnly'=>true),
			array('email_address', 'required', 'on'=>'guestUser'),
			array('email_address', 'unique', 'on'=>'guestUser'),
			array('email_address', 'length', 'max'=>50),
			array('email_address', 'email'),
			array('rel_user_id', 'validateRelUserID', 'on'=>'guestUser'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, values, rel_user_id, email_address, last_email_ntf_adv_id, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user'=>array(self::BELONGS_TO, 'User', 'rel_user_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'values' => 'Values',
			'rel_user_id' => 'Пользователь',
			'email_address' => 'E-mail',
			'last_email_ntf_adv_id' => 'Полследнее отпр. об-ие',
			'created' => 'Добавлен',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('values',$this->values,true);
		$criteria->compare('rel_user_id',$this->rel_user_id);
		$criteria->compare('email_address',$this->email_address);
		$criteria->compare('last_email_ntf_adv_id',$this->last_email_ntf_adv_id);
		$criteria->compare('created',$this->created);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getSearchCriteria()
    {
        $criteria=new CDbCriteria();
        //$criteria=$this->criteria;
//        if ($this->last_email_ntf_adv_id>0)
//        $criteria->addCondition('');
        return $this->criteria;
    }

	protected function beforeSave()
	{
		if(!parent::beforeSave())
			return false;

		if($this->isNewRecord)
			$this->created=time();

		return true;
	}

	/**
	 * @return CDbCriteria
	 */
	public static function getDefaultSearchCriteria()
	{
		$values=new stdClass();

		$chisinau=Cities::model()->findByAttributes(array('name'=>'Кишинёв'));
		$criteria=new CDbCriteria();
		$criteria->addInCondition('status', array(Advertisements::ADV_OK, Advertisements::ADV_MODIFIED));
		$columnsCriterias=array(
			'city_id'=>$chisinau->id,
			'rent_type'=>1
		);
		$criteria->addColumnCondition($columnsCriterias, 'AND', 'AND');
		$values->criteria=$criteria;
		$values->columnsCriterias=array($columnsCriterias);

		return $values;
	}

	/**
	 * @param $attribute
	 * @return void
	 */
	public function validateRelUserID($attribute)
	{
		if($this->$attribute>0)
		{
			$advSearchers=self::model()->countByAttributes(array('rel_user_id'=>$this->$attribute));
			if($advSearchers>0)
				$this->addError($attribute, 'У вас уже настроена рассылка');
		}
	}
}