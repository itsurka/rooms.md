<?php

/**
 * This is the model class for table "invoice".
 *
 * The followings are the available columns in table 'invoice':
 * @property integer $id
 * @property integer $user_id
 * @property double $amount
 * @property string $description
 * @property string $created_at
 * @property string $paid_at
 *
 * @property User $user
 */
class Invoice extends CActiveRecord
{
	const MIN_AMOUNT = 5;
	const MAX_AMOUNT = 3000;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'invoice';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, amount, description', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('amount', 'numerical', 'integerOnly'=>true, 'min' => self::MIN_AMOUNT, 'max' => self::MAX_AMOUNT),
			array('description', 'length', 'max'=>200),
			array('description, paid_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, amount, description, created_at, paid_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user'=>[self::BELONGS_TO, 'User', 'user_id'],
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'=>'ID',
			'user_id'=>'User',
			'amount'=>'Сумма',
			'description'=>'Описание',
			'created_at'=>'Дата создания',
			'paid_at'=>'Дата оплаты',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('amount', $this->amount);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('created_at', $this->created_at, true);
		$criteria->compare('paid_at', $this->paid_at, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Invoice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
