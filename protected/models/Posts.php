<?php

/**
 * This is the model class for table "{{posts}}".
 *
 * The followings are the available columns in table '{{posts}}':
 * @property string $post_id
 * @property string $title
 * @property string $description
 * @property string $added
 * @property string $rel_user_id
 * @property string $rel_usid
 * @property string $status
 */
class Posts extends CActiveRecord
{
    public $allPhotos;

    public $newPhotos;

    public $newPhotosDescription;

    public $votesCount;

    const STATUS_NEW = 'new';
    const STATUS_OK = 'ok';
    const STATUS_MODIFIED = 'modified';
    const STATUS_HIDDEN = 'hidden';
    const STATUS_DELETED = 'deleted';

	public static $statuses = array(
        self::STATUS_NEW => 'Новый',
        self::STATUS_OK => 'Активен',
        self::STATUS_MODIFIED => 'Изменен',
        self::STATUS_HIDDEN => 'Спрятан',
        self::STATUS_DELETED => 'Удален',
    );

    public static $allowPublicStatuses = array(
//        self::STATUS_NEW,
        self::STATUS_OK,
//        self::STATUS_MODIFIED
    );

    public function init()
    {
        parent::init();
    }

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Posts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{posts}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('title', 'length', 'max'=>255),
			array('description', 'safe'),
            array('newPhotos', 'validatePhotos', 'on'=>'insert,update'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('post_id, title, description, added, rel_user_id, rel_usid, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'photos' => array(self::HAS_MANY, 'Photos', 'rel_post_id'),
            'owner' => array(self::BELONGS_TO, 'User', 'rel_user_id'),
            'viewsCount' => array(self::STAT, 'PostViews', 'rel_post_id', 'select'=>'COUNT(`post_view_id`)'),
//            'Rating'=>array(self::STAT, 'PostViews', 'rel_post_id', 'select'=>'COUNT(`post_view_id`)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'post_id' => '#',
			'title' => Yii::t('app', 'Title'),
			'description' => Yii::t('app', 'Description'),
			'added' => Yii::t('app', 'Date added'),
			'allPhotos' => Yii::t('app', 'Photos'),
			'newPhotos' => Yii::t('app', 'Photos'),
			'rel_user_id' => Yii::t('app', 'Added by'),
			'rel_usid' => Yii::t('app', 'Added by'),
			'owner' => Yii::t('app', 'Added by'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = $this->getDbCriteria();

        if(empty($this->status))
            $criteria->scopes = 'withoutTrash';

		$criteria->compare('post_id', $this->post_id, true);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('added',$this->added, true);
		$criteria->compare('rel_user_id', $this->rel_user_id, true);
		$criteria->compare('rel_usid', $this->rel_usid, true);
		$criteria->compare('status', $this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function validatePhotos($attribute, $params)
    {
        if(!self::getPostHasPhotos())
            $this->addError($attribute, 'Загрузите фотографию');
    }

    /**
     * Проверяем фотки, старые и новые (только что загруженные)
     * @return bool
     */
    protected function getPostHasPhotos()
    {
        $hasPhotos = false;
//        echo __METHOD__;
//        $old_photos = $this->photos;
//        $new_photos = Files::getUserUploadedTempFiles();
        $form_new_photos = $this->getFormPhotos();

//        var_dump(empty($old_photos), empty($new_photos), empty($form_new_photos));
//
//        echo '$form_new_photos[]<pre>';
//        print_r($form_new_photos);
//        echo '</pre>';

        if((count($form_new_photos)) > 0)
            $hasPhotos = true;

//        var_dump('$hasPhotos', $hasPhotos);

        return $hasPhotos;
    }

    /**
     * @return int
     */
    public function getPostCommentsCount()
    {
        Yii::import('application.modules.comments.models.Comment');
        return Comment::model()->countByAttributes(array('owner_name'=>__CLASS__, 'owner_id'=>$this->post_id));
    }

    protected function beforeSave()
    {
        if(parent::beforeSave())
        {
            return true;
        }
        else
            return false;
    }

    protected function beforeValidate()
    {
        if(parent::beforeValidate())
        {
            return true;
        }
        else
            return false;
    }

    protected function afterValidate()
    {
        parent::afterValidate();

        $postID = null;
        if(!$this->getIsNewRecord())
            $postID = $this->post_id;

        if($this->getError('newPhotos') === null)
            Files::syncFormFiles($postID);
    }

    protected function afterDelete()
    {
        parent::afterDelete();

        // Удаляем голоса поста
        $this->deleteVotes();

        // Удаляем фотки поста
        foreach($this->photos as $photo)
            $photo->delete();

        // Удаляем комменты поста
        $attributes = array(
            'owner_name' => __CLASS__,
            'owner_id' => $this->post_id,
        );
        Yii::app()->getModule('comments')->deleteCommentsByAttributes($attributes);
    }

    public function addVote()
    {
        $userIP = $_SERVER['REMOTE_ADDR'];
        //$userIP = "127.0.".rand(1, 9999).".".rand(1, 9999); // FOR TEST!!!
        return Votes::addVote(__CLASS__, $this->post_id, Votes::DEFAULT_MARK, $userIP);
    }

    public function getUserVoted()
    {
        $userMark = $this->getUserMark();
        return $userMark!==null ? true : false;
    }

    public function getUserMark()
    {
        $userIP = Common::getUserIP();
        $entityName = __CLASS__;
        $entityID = $this->post_id;

        $criteria = new CDbCriteria();
        $criteria->condition = "entity_name = '{$entityName}' AND entity_id = {$entityID} AND user_ip = '{$userIP}'";
        $model = Votes::model()->find($criteria);
        return $model!==null ? $model->mark : null;
    }

    public function deleteVotes()
    {
        $entityName = __CLASS__;
        $entityID = $this->post_id;

        $criteria = new CDbCriteria();
        $criteria->condition = "entity_name = '{$entityName}' AND entity_id = {$entityID}";
        Votes::model()->deleteAll($criteria);
    }

    public function getVotesInfo()
    {
        return Votes::getEntityVotesInfo(__CLASS__, $this->post_id);
    }

    /**
     * @param array $params
     * @return CActiveDataProvider
     */
    public function getPosts($params=array())
    {
        $entityName = 'Posts';
        $criteria = $this->getDbCriteria();

        $criteria->select = "t.*,
                            (
                            SELECT COUNT(*)
                            FROM tbl_votes v
                            WHERE v.entity_name='{$entityName}' AND t.post_id=v.entity_id
                            ) AS votesCount";

        if(empty($criteria->group))
            $criteria->group = "t.post_id";

        if(empty($criteria->order))
            $criteria->order = "votesCount DESC";



        if(isset($params['return']) && $params['return']=='models')
        {
            return self::model()->findAll($criteria);
        }
        else
        {
            $dataProvider = new CActiveDataProvider('Posts', array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 10
                )
            ));
            return $dataProvider;
        }
    }

    public function getBestPosts($limit=1)
    {
        return $this->scopeIsPublished()->scopeAddedToday()->scopeLimit($limit)->getPosts(array('return'=>'models'));
//        return $this->scopeIsPublished()->scopeLimit($limit)->getPosts(array('return'=>'models'));
    }

    /**
     * Scope: Все посты пользователя
     * @return Posts
     */
    public function scopeUserPosts()
    {
        $criteria = $this->getDbCriteria();
        $criteria->addColumnCondition(array('rel_user_id'=>Yii::app()->user->id));
        $criteria->order = 'post_id DESC';
        $this->setDbCriteria($criteria);

        return $this;
    }

    /**
     * Scope:
     * @return Posts
     */
    public function withoutTrash()
    {
        $criteria = $this->getDbCriteria();
        $criteria->addCondition("status != '".self::STATUS_DELETED."'");
        $this->setDbCriteria($criteria);

        return $this;
    }

    /**
     * Scope:
     * @return Posts
     */
    public function scopeIsPublished()
    {
        $criteria = $this->getDbCriteria();
        $criteria->addInCondition('status', self::$allowPublicStatuses);
        $this->setDbCriteria($criteria);

        return $this;
    }

    /**
     * Scope
     * @return Posts
     */
    public function sortDateDesc()
    {
        $criteria = $this->getDbCriteria();
        $criteria->order = 'added DESC';
        $this->setDbCriteria($criteria);

        return $this;
    }

    /**
     * Scope
     * @return Posts
     */
    public function scopeAddedToday()
    {
        $criteria = $this->getDbCriteria();
        $criteria->order = 'added DESC';
        $now = date('Y-m-d H:i:s');
        $today = date('Y-m-d').' 00:00:00';
        $criteria->addCondition("added >= '{$today}' AND added <= '{$now}'");
        $this->setDbCriteria($criteria);

        return $this;
    }

    /**
     * Scope
     * @return Posts
     */
    public function sortByRating()
    {
        $criteria = $this->getDbCriteria();
        $criteria->order = 'votesCount DESC';
        $this->setDbCriteria($criteria);

        return $this;
    }

    /**
     * Scope
     * @return Posts
     */
    public function scopeSortByViewsAndRating()
    {
        $entityName = __CLASS__;
        $criteria = $this->getDbCriteria();
        $criteria->select = "t.*,
                            (
                            SELECT COUNT(*)
                            FROM tbl_votes v
                            WHERE v.entity_name='{$entityName}' AND t.post_id=v.entity_id
                            ) AS votesCount";
        $criteria->order = 'votesCount DESC';
        $this->setDbCriteria($criteria);

        return $this;
    }

    /**
     * Scope
     * @param $limit
     * @return Posts
     */
    public function scopeLimit($limit)
    {
        $criteria = $this->getDbCriteria();
        $criteria->limit = $limit;
        $this->setDbCriteria($criteria);

        return $this;
    }

    /**
     * Scope
     * @return Posts
     */
    public function hasVotes()
    {
        $entityName = __CLASS__;
        $criteria = $this->getDbCriteria();
        $criteria->addCondition("(
                            SELECT COUNT(*)
                            FROM tbl_votes v
                            WHERE v.entity_name='{$entityName}' AND t.post_id=v.entity_id
                            ) > 0");
        $this->setDbCriteria($criteria);

        return $this;
    }

    /**
     * Scope
     * @param $from
     * @param $to
     * @return Posts
     */
    public function applyDateRange($from, $to=null)
    {
        $criteria = $this->getDbCriteria();

        $criteria->addCondition("added >= '{$from}'");
        if($to !== null)
            $criteria->addCondition("added <= '{$to}'");

        $this->setDbCriteria($criteria);

        return $this;
    }

    public function getIsPublished()
    {
        return in_array($this->status, self::$allowPublicStatuses);
    }

    /**
     * Берем все файлы в json формате
     * для отображения в форме
     * @return string
     */
    public function getFilesJSON()
    {
        $filesJSON = array();
        foreach($this->photos as $i=>$photo)
        {
            $file = $photo->file;
            $filesJSON[$i]['file_id'] = $file->id;
            $filesJSON[$i]['fileurl'] = $file->getUrl('200x160');
            $filesJSON[$i]['name'] = $file->name;
            $filesJSON[$i]['extension'] = $file->extension;
            $filesJSON[$i]['filename'] = $file->getFilename();
            $filesJSON[$i]['path'] = $file->path;
            $filesJSON[$i]['filepath'] = $file->getPath();
            $filesJSON[$i]['size'] = $file->size;
            $filesJSON[$i]['description'] = $photo->description;
            $filesJSON[$i]['success'] = true;
        }

        return json_encode($filesJSON);
    }

    /**
     * @return array
     */
    public function getFormPhotos()
    {
        $photos = isset($_POST['Posts']['newPhotos']) ? $_POST['Posts']['newPhotos'] : array();
        $returnPhotos = array();
        foreach($photos as $photo)
            if(!empty($photo))
                $returnPhotos[] = $photo;
        return $returnPhotos;
    }

    public function getPostOperations()
    {
        return array(
//            array('label'=>'List Posts', 'url'=>array('index')),
//            array('label'=>'Create Posts', 'url'=>array('create')),
            array('label'=>Yii::t('app', 'Edit'), 'url'=>array('update', 'id'=>$this->post_id)),
            array('label'=>Yii::t('app', 'Delete'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$this->post_id),'confirm'=>'Вы уверены что хотите удалить этот пост навсегда?')),
//            array('label'=>'Manage Posts', 'url'=>array('admin')),
        );
    }

    public function getVotesCountHtml()
    {
        $votesCount = $this->getVotesInfo()->votesCount;
        if($votesCount)
        {
            $html = "рейтинг: <span class='number'>{$votesCount}</span>";
        }
        else
            $html = Yii::t('app', 'No votes');

        $html = "<span class='icon vote-result-2'>{$html}</span>";

        return $html;
    }

    public function addPostView()
    {
        $added = false;
        if (!$this->getPostIsViewedByUser())
        {
            $model = new PostViews();
            $model->rel_post_id = $this->post_id;
            $model->rel_user_sid = CUserSessions::getUserSessionID();
            $added = $model->save();
        }
        return $added;
    }

    public function getPostIsViewedByUser()
    {
        $model = PostViews::model()->findByAttributes(array('rel_user_sid'=>CUserSessions::getUserSessionID(), 'rel_post_id'=>$this->post_id));
        return ($model !== null) ? true : false;
    }

    public static function getNewer()
    {
        return self::model()->scopeIsPublished()->sortDateDesc()->find();
    }

    public function getUrl()
    {
        return array('view', 'id'=>$this->post_id);
    }

    public function getUpdateUrl()
    {
        return array('update', 'id'=>$this->post_id);
    }

    public function getUpdateStatusUrl($status)
    {
        if(array_key_exists($status, self::$statuses))
            return array('changestatus', 'id'=>$this->post_id, 'status'=>$status);
        else
            return '#';
    }
}