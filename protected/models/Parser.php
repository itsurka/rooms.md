<?php

/**
 * This is the model class for table "tbl_parser".
 *
 * The followings are the available columns in table 'tbl_parser':
 * @property integer $id
 * @property string $targetSiteId
 * @property string $startPageUrl
 * @property integer $parsePagesCount
 * @property integer $parsedPages
 * @property integer $status
 * @property string $created
 * @property string $completedDate
 */
class Parser extends CActiveRecord
{
	const STATUS_OFF=0;
	const STATUS_ON=1;
	const STATUS_COMPLETED=2;

	public static $statuses=array(
		self::STATUS_ON=>'Active',
		self::STATUS_OFF=>'Not Active',
		self::STATUS_COMPLETED=>'Complete',
	);

	const PARSER_ID_MAKLER=1;
	const PARSER_ID_999=2;

	public static $sitesForParsing=array(
		self::PARSER_ID_MAKLER=>'Makler.md',
		self::PARSER_ID_999=>'999.md',
	);

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Parser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'parser';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('targetSiteId, startPageUrl, parsePagesCount', 'required'),
			array('parsePagesCount, parsedPages, status', 'numerical', 'integerOnly'=>true),
			array('startPageUrl', 'length', 'max'=>255),
			array('startPageUrl', 'url'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, targetSiteId, startPageUrl, parsePagesCount, parsedPages, status, created, completedDate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'targetSiteId' => 'Target Site',
			'startPageUrl' => 'Start Page Url',
			'parsePagesCount' => 'Parse Pages Count',
			'parsedPages' => 'Parsed Pages',
			'status' => 'Status',
			'created' => 'Created',
			'completedDate' => 'completedDate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('targetSiteId',$this->targetSiteId,true);
		$criteria->compare('startPageUrl',$this->startPageUrl,true);
		$criteria->compare('parsePagesCount',$this->parsePagesCount);
		$criteria->compare('parsedPages',$this->parsedPages);
		$criteria->compare('status',$this->status);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('completedDate',$this->completedDate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->created=date('Y-m-d H:i:s');
			}

			return true;
		}
		else
			return false;
	}

	/**
	 * @param $targetSiteId
	 * @return CActiveRecord
	 */
	public static function getActiveParserBySiteId($targetSiteId)
	{
		$criteria=new CDbCriteria();
		$criteria->condition='targetSiteId=:targetSiteId AND status=:status';
		$criteria->params=array('targetSiteId'=>$targetSiteId, 'status'=>Parser::STATUS_ON);
		$criteria->order='id DESC';
		$result=self::model()->find($criteria);
		return $result;
	}

	public function getStartPageUrl()
	{
		$startPageUrl=$this->startPageUrl;
		$criteria=new CDbCriteria();
		$criteria->order='id DESC';
		$criteria->limit=1;
		$criteria->condition='relParserId=:relParserId AND status=:status';
		$criteria->params=array('relParserId'=>$this->id, 'status'=>ParserProcess::STATUS_COMPLETED);
		$lastParserProcess=ParserProcess::model()->find($criteria);
		if(!is_null($lastParserProcess))
		{
			if($lastParserProcess->lastParsedPageUrl!='')
			{
				$startPageUrl=$lastParserProcess->lastParsedPageUrl;

				if($this->targetSiteId==self::PARSER_ID_MAKLER)
				{
					// брать первую страничку опять а не следущую
					//$startPageUrl=MaklerParser::getNextPageUrlFromUrl($startPageUrl);
				}
				else
					$startPageUrl=WebParser::getNextPageUrlFromUrl($startPageUrl);
			}
		}
		return $startPageUrl;
	}
}