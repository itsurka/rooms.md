<?php

class MainController extends ControlPanelController
{
    public function filters()
    {
        return array(
            'accessControl',
        );
    }
    
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
        return array(
            array(
                'allow',
                'actions'=>array('index'),
                'users'=>Yii::app()->getModule('user')->getAdmins(),
            ),
            array('deny'),
        );
	}



	/**
	 * Displays a particular model.
	 */
	public function actionIndex()
	{
//		$this->render('index');
        $this->redirect('advertisements/index');
	}
}
