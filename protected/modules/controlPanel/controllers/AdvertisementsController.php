<?php

class AdvertisementsController extends ControlPanelController
{
    /**
     * @var int num of ads on page
     **/
    public $itemsOnPage=4;

	/**
	 * @param null $command
	 */
	public function actionIndex($command=null)
    {
        $this->layout = '/layouts/column1';
		$request=Yii::app()->getRequest();

		if($request->getIsAjaxRequest())
		{
			if($command=='deleteAllNewAdvs')
			{
				Advertisements::model()->deleteAll('status = :status', array('status'=>Advertisements::ADV_NEW));
				Yii::app()->end();
			}
		}

        $model = new Advertisements('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Advertisements']))
			$model->attributes=$_GET['Advertisements'];

        // Render
        $this->render('index', array(
			'model' => $model,
		));
    }

    /**
     * @return void
     */
    public function actionVip()
    {
        $this->layout = '/layouts/column1';
        $criteria=new CDbCriteria();
        $criteria->order='vert_position ASC';
        $model = new VipAdv('search');
        $model->setDbCriteria($criteria);
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['VipAdv']))
			$model->attributes=$_GET['VipAdv'];

        // Render
        $this->render('vip', array(
			'model' => $model,
		));
    }

    /**
     * @param $id
     * @return void
     */
	public function actionView($id)
    {
		$model = $this->loadModel($id);

        $CHttpRequest = new CHttpRequest();

        if(!$CHttpRequest->isAjaxRequest)
        {
            $this->render('view', array(
                                       'model' => $model,
                                  ));
        }
        else
        {
            $this->renderPartial('view', array(
                                       'model' => $model,
                                  ));
            Yii::app()->end();
        }
	}

    /**
     * Редактирование об-ия
     * @param $id
     * @return void
     */
    public function actionUpdate($id)
    {
        $id = Common::getAdvsIDFromTranslitID($id);
        $modelUpload = new Uploader;

        if(isset($_POST['Advertisements']))
        {
            $model = $this->loadModel($id);
            $model->attributes = $_POST['Advertisements'];
            $model->added = new CDbExpression('UTC_TIMESTAMP()');

            if($model->save())
            {
                // Update old files
                if(!empty($model->files))
                {
                    $advFiles = $model->files;
                    $advOldFiles = empty($_POST['oldFiles']) ? array() : $_POST['oldFiles'];
                    foreach($advFiles as $key=>$eachFile)
                    {
                        if(!in_array($eachFile->name, $advOldFiles))
                            Files::model()->deleteByPk($eachFile->id);
                    }
                }

                // Upload files
                if(!empty($_FILES['Uploader']['name']['files']))
                {
                    $files = CUploadedFile::getInstances($modelUpload, "files");
                    Yii::import('application.extensions.upload.upload');

                    foreach($files as $eachFile)
                    {
                        if(!$eachFile->hasError && Common::isImage($eachFile))
                        {
                            // Save image if uploaded
                            $dir = Yii::getPathOfAlias('application.uploads');
                            $dirThumb = Yii::getPathOfAlias('application.uploads.thumbs');
                            $filename = sha1(date('YmdHis').rand(1000, 9999));

                            $uploadedFile = array(
                                'name'=>$eachFile->getName(),
                                'type'=>$eachFile->getType(),
                                'tmp_name'=>$eachFile->getTempName(),
                                'error'=>$eachFile->getError(),
                                'size'=>$eachFile->getSize(),
                            );
                            $Upload = new upload($uploadedFile);

                            if($Upload->processed)
                            {
                                // Big image
                                $Upload->file_new_name_body = $filename;
                                $Upload->image_resize = true;
                                $Upload->image_x = 400;
                                $Upload->image_ratio_y = true;
                                $Upload->process($dir);
                                // Thumb
                                $Upload->file_new_name_body = $filename;
                                $Upload->image_resize = true;
                                $Upload->image_x = 200;
                                $Upload->image_ratio_y = true;
                                $Upload->process($dirThumb);
                                $Upload->clean();

                                // Insert to files
                                $fileModel = new Files();
                                $fileModel->name = $filename;
                                $fileModel->extension = $eachFile->getExtensionName();
                                $fileModel->mime_type = $eachFile->getType();
                                $fileModel->size = $eachFile->getSize();
                                $fileModel->path = $dir . '/';
                                if($fileModel->save())
                                {
                                    // Assign image to adv
                                    $advFileModel = new AdvertisementFiles();
                                    $advFileModel->advertisement_id = $model->id;
                                    $advFileModel->file_id = $fileModel->id;
                                    $advFileModel->save();
                                }
                            }
                            else
                                throw new CHttpException(500, 'Не удалось сохранить файл');

                        }
                    }
                }
                $this->redirect(array('index'));
            }
        }

        $model = $this->loadModel($id);

        // Render
        $this->render('update', array(
                                     'model' => $model,
                                     'modelUpload'=>$modelUpload,
                                ));
    }

    /**
     * @throws CHttpException
     * @param $id
     * @return void
     */
	public function actionDelete($id)
    {
		if(Yii::app()->request->isPostRequest)
		{
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

    /**
     * @throws CHttpException
     * @param $id
     * @return CActiveRecord
     */
	public function loadModel($id)
    {
		$model = Advertisements::model()->findByPk($id);
		// Error if model didn't exist or model doesn't belong to user.
		if($model===null)
            throw new CHttpException(404, 'The requested page does not exist.');

		return $model;
	}

    /**
     * Change adv item status
     *
     * @param $id
     * @param $status
     * @return boolean
     */
    public function actionChangeAdvStatus($id, $status)
    {
        if(!(array_key_exists($status, Advertisements::$statuses))) {
            return false;
        }
        $criteria = new CDbCriteria();
        $criteria->addInCondition('id', array($id));
        $model = Advertisements::model()->find($criteria);
        $model->status = $status;
        $model->save();
        return true;
    }

    /**
     * Делаем об-ие VIP
     * Будет отображаться как платное
     * @param $id
     * @return void
     */
    /**
     * @param $id
     * @return void
     */
    public function actionMakeVIP($id)
    {
        $model=VipAdv::model()->findByAttributes(array('rel_adv_id'=>intval($id)));
        if($model===null)
        {
            $advModel=Advertisements::model()->findByPk($id);
            if($advModel!==null)
            {
                $vipAdvModel=new VipAdv();
                $vipAdvModel->rel_adv_id=$advModel->id;
                $vipAdvModel->rel_user_id=$advModel->owner_id;
                $vipAdvModel->save(false);
            }
        }
        else
            $model->delete();
    }

    /**
     * @param string $moveDirection
     * @param $id
     * @return bool
     */
    public function actionMoveVipAdv($moveDirection='up',$id)
    {
        $targetVipAdv=VipAdv::model()->findByAttributes(array('rel_adv_id'=>intval($id)));

        if($targetVipAdv===null)
            return false;

        if($moveDirection=='up')
        {
            if($targetVipAdv->vert_position==0)
                return false;

            $criteria=new CDbCriteria();
            $criteria->condition="vert_position < {$targetVipAdv->vert_position}";
            $criteria->order='vert_position DESC';
            $vipAdvOverTarget=VipAdv::model()->find($criteria);
            if($vipAdvOverTarget===null)
                return false;

            $posOver=$vipAdvOverTarget->vert_position;
            $vipAdvOverTarget->vert_position=$targetVipAdv->vert_position;
            $vipAdvOverTarget->save(false);
            $targetVipAdv->vert_position=$posOver;
            $targetVipAdv->save(false);
        }
        elseif($moveDirection=='down')
        {
            $criteria=new CDbCriteria();
            $criteria->condition="vert_position > {$targetVipAdv->vert_position}";
            $criteria->order='vert_position ASC';
            $vipAdvUnderTarget=VipAdv::model()->find($criteria);
            if($vipAdvUnderTarget===null)
                return false;

            $posUnder=$vipAdvUnderTarget->vert_position;
            $vipAdvUnderTarget->vert_position=$targetVipAdv->vert_position;
            $vipAdvUnderTarget->save(false);
            $targetVipAdv->vert_position=$posUnder;
            $targetVipAdv->save(false);
        }
        return true;
    }

    public function actionToggleColor($id)
    {
        $model=Advertisements::model()->findByPk(intval($id));
        if($model!==null)
        {
            $newIsColoredVal=$model->is_colored ? 0 : 1;
            $model->saveAttributes(array('is_colored'=>$newIsColoredVal));
        }
    }
}