<?php

class AccessLogController extends ControlPanelController
{
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index', 'view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create', 'update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin', 'delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new AccessLog('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['AccessLog']))
			$model->attributes=$_GET['AccessLog'];

		$logStatistic = AccessLog::generateWeekStatistic();

		$this->render('admin', array(
			'model'=>$model,
			'logStatistic'=>$logStatistic,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return AccessLog the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=AccessLog::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param AccessLog $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='access-log-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
