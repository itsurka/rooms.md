<?php
/**
 * Yii-Control Panel module
 * 
 * @author Tsurka Igor <turcaigor@gmail.com>
 */

class ControlPanelModule extends CWebModule
{
	public $mainUrl = array('/controlPanel/main');

	public function init()
    {
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'controlPanel.models.*',
			'controlPanel.components.*',
		));
    }

    /**
     * @static
     * @param string $str
     * @param array $params
     * @param string $dic
     * @return string
     */
	public static function t($str='', $params=array(), $dic='controlPanel')
    {
		return Yii::t("ControlPanelModule.".$dic, $str, $params);
	}
}
