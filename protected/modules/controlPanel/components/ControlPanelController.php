<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */

class ControlPanelController extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='/layouts/column2';



	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();



	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs = array();

	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	public function accessRules()
	{
		return array(
			array(
				'allow',
				'actions'=>array(
					'index',
					'view',
					'update',
					'delete',
					'changeAdvStatus',
					'makeVIP',
					'toggleColor',
					'vip',
					'moveVipAdv',
					'admin',
					'create',
				),
				'users'=>Yii::app()->getModule('user')->getAdmins(),
			),
			array('deny'),
		);
	}

	public function beforeAction($action)
    {
        $session = new CHttpSession();
        $session->add('module_user_theme', 'controlPanel');
        $options = array(
            'theme'=>$session->contains('module_user_theme')
        );

        if(parent::beforeAction($action, $options)) {

            return true;
        }
        else
            return false;
	}
}