<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class UController extends Controller
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column2';



	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();



	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs = array();



    public function beforeAction($action, $options=array())
    {
        $session = new CHttpSession();
        $params = array(
            'theme'=>$session->get('module_user_theme')
        );

        if(parent::beforeAction($action, $params)){

            if( ($session->contains('module_user_theme')) && ($session->get('module_user_theme')=='controlPanel') ){
                Yii::app()->theme = 'controlPanel';
                $this->module->setViewPath(Yii::getPathOfAlias('webroot.themes.controlPanel.views'));
            } else {
                Yii::app()->theme = 'classic';
                $this->module->setViewPath(Yii::getPathOfAlias('webroot.themes.classic.views'));
            }

            return true;
        }
        else
            return false;
    }
}