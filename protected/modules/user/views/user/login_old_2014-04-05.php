<div class="form_place margin_top_0">

	<?php if(Yii::app()->user->hasFlash('loginMessage')): ?>
		<div class="alert succes_msg">
			<?php echo Yii::app()->user->getFlash('loginMessage'); ?>
		</div>
	<?php endif; ?>

	<?php echo CHtml::beginForm(); ?>

		<?=Chtml::errorSummary($model, NULL, NULL, array('class'=>'alert error_msg'))?>

		<section class="form_row">
			<div>
				<?php echo CHtml::activeLabelEx($model,'username'); ?>
				<div class="block_content">
					<?php echo CHtml::activeTextField($model,'username',array('class'=>'i-format', 'style'=>'min-width: 200px; width: 230px;')) ?>
				</div>
			</div>
			<div class="clear"></div>
		</section>

		<section class="form_row">
			<div>
				<?php echo CHtml::activeLabelEx($model,'password'); ?>
				<div class="block_content">
					<?php echo CHtml::activePasswordField($model,'password',array('class'=>'i-format', 'style'=>'min-width: 200px; width: 230px;')) ?>
					<div>
						<?php echo CHtml::link(UserModule::t("Register"),Yii::app()->getModule('user')->registrationUrl); ?>
						&nbsp;|&nbsp;
						<?php echo CHtml::link(UserModule::t("Lost Password?"),Yii::app()->getModule('user')->recoveryUrl); ?>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</section>

		<section class="form_row">
			<div>
				<div class="block_content label400">
					<div id="uniform-undefined" class="checker">
						<span>
							<?php echo CHtml::activeCheckBox($model,'rememberMe',array('style'=>'opacity: 0;')); ?>
						</span>
					</div>
					<?php echo CHtml::activeLabelEx($model,'rememberMe'); ?>
					<div>
						<?php echo CHtml::submitButton(UserModule::t("Login"),array('class'=>'button darkgray')); ?>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</section>
	
	<?php echo CHtml::endForm(); ?>
	
</div>
<? /*
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<h1><?php echo UserModule::t("Login"); ?></h1>

<?php if(Yii::app()->user->hasFlash('loginMessage')): ?>

<div class="alert succes_msg">
	<?php echo Yii::app()->user->getFlash('loginMessage'); ?>
</div>

<?php endif; ?>

<p><?php echo UserModule::t("Please fill out the following form with your login credentials:"); ?></p>


<?php echo CHtml::beginForm(); ?>

	<p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>
	
	<?php echo CHtml::errorSummary($model); ?>
	
	<div class="row">
		<?php echo CHtml::activeLabelEx($model,'username'); ?>
		<?php echo CHtml::activeTextField($model,'username') ?>
	</div>
	
	<div class="row">
		<?php echo CHtml::activeLabelEx($model,'password'); ?>
		<?php echo CHtml::activePasswordField($model,'password') ?>
	</div>
	
	<div class="row">
		<p class="hint">
		<?php echo CHtml::link(UserModule::t("Register"),Yii::app()->getModule('user')->registrationUrl); ?> | <?php echo CHtml::link(UserModule::t("Lost Password?"),Yii::app()->getModule('user')->recoveryUrl); ?>
		</p>
	</div>
	
	<div class="row rememberMe">
		<?php echo CHtml::activeCheckBox($model,'rememberMe'); ?>
		<?php echo CHtml::activeLabelEx($model,'rememberMe'); ?>
	</div>

	<div class="row submit">
		<?php echo CHtml::submitButton(UserModule::t("Login")); ?>
	</div>
	
<?php echo CHtml::endForm(); ?>
 *
 */?>