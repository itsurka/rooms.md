<?/*
<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Change Password");
$this->breadcrumbs=array(
	UserModule::t("Profile") => array('/user/profile'),
	UserModule::t("Change Password"),
);
?>

<h2><?php echo UserModule::t("Change password"); ?></h2>
<?php echo $this->renderPartial('menu'); ?>
 *
 */?>

<div class="form">
<?php $form=$this->beginWidget('UActiveForm', array(
	'id'=>'changepassword-form',
	'enableAjaxValidation'=>true,
)); ?>

	<div class="alert info_msg"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></div>
	<?php echo Chtml::errorSummary($model, NULL, NULL, array('class'=>'alert error_msg')); ?>
	
	<section class="form_row">
		<div>
			<?php echo $form->labelEx($model,'password'); ?>
			<div class="block_content">
				<?php echo $form->passwordField($model,'verifyPassword',array('class'=>'i-format', 'style'=>'min-width: 200px; width: 230px;')); ?>
				<p class="hint">
					<?php echo UserModule::t("Minimal password length 4 symbols."); ?>
				</p>
			</div>
		</div>
		<div class="clear"></div>
	</section>

	<section class="form_row">
		<div>
			<?php echo $form->labelEx($model,'verifyPassword'); ?>
			<div class="block_content">
				<?php echo $form->passwordField($model,'verifyPassword',array('class'=>'i-format', 'style'=>'min-width: 200px; width: 230px;')); ?>
			</div>
		</div>
		<div class="clear"></div>
	</section>

	<section class="form_row">
		<div>
			<div class="block_content">
				<?php echo CHtml::submitButton(UserModule::t("Save"), array('class'=>'button darkgray')); ?>
			</div>
		</div>
		<div class="clear"></div>
	</section>

	<?/*
	<div class="row">
	<?php echo $form->labelEx($model,'password'); ?>
	<?php echo $form->passwordField($model,'password'); ?>
	<?php echo $form->error($model,'password'); ?>
	<p class="hint">
	<?php echo UserModule::t("Minimal password length 4 symbols."); ?>
	</p>
	</div>
	
	<div class="row">
	<?php echo $form->labelEx($model,'verifyPassword'); ?>
	<?php echo $form->passwordField($model,'verifyPassword'); ?>
	<?php echo $form->error($model,'verifyPassword'); ?>
	</div>
	
	
	<div class="row submit">
	<?php echo CHtml::submitButton(UserModule::t("Save")); ?>
	</div>
	 *
	 */?>

<?php $this->endWidget(); ?>
</div><!-- form -->