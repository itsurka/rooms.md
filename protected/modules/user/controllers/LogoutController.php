<?php

class LogoutController extends UController
{
	public $defaultAction = 'logout';
	
	/**
	 * Logout the current user and redirect to returnLogoutUrl.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->getBaseUrl(true)); // TODO: Yii::app()->controller->module->returnLogoutUrl
	}
}