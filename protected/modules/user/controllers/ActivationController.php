<?php

class ActivationController extends UController
{
	public $defaultAction = 'activation';

	
	/**
	 * Activation user account
	 */
	public function actionActivation ()
    {
		$email = $_GET['email'];
		$activkey = $_GET['activkey'];
		if ($email && $activkey)
        {
			$find = User::model()->notsafe()->findByAttributes(array('email'=>$email));
			if (isset($find)&&$find->status)
            {
			    $this->render('/user/message',array('title'=>UserModule::t("User activation"),'content'=>UserModule::t("You account is active.")));
			}
            elseif(isset($find->activkey) && ($find->activkey==$activkey))
            {
				$find->activkey = UserModule::encrypting(microtime());
				$find->status = 1;
				$find->save();

                // Login user - Start {
                $model=new UserLogin;
                $model->username = $find->username;
                $model->password = $find->password;

                $a = false;
                if($model->validate())
                {
                    $a = true;
                    $lastVisit = User::model()->notsafe()->findByPk(Yii::app()->user->id);
                    $lastVisit->lastvisit = time();
                    $b = $lastVisit->save();
                    
                    $identity = new UserIdentity($find->username, $find->password);
                    $c = $identity->authenticate();
                    $this->redirect(array('/catalog/index?registration=success'));
                }
                // Login user - End }

                /*$enterLink = CHtml::link('Войти', Yii::app()->getModule('user')->loginUrl);
                $content = UserModule::t("You account is activated.").' '.html_entity_decode($enterLink);
                $this->render('/user/message',array('title'=>UserModule::t("User activation"),'content'=>$content));*/
			}
            else
			    $this->render('/user/message',array('title'=>UserModule::t("User activation"),'content'=>UserModule::t("Incorrect activation URL.")));
		}
        else
			$this->render('/user/message',array('title'=>UserModule::t("User activation"),'content'=>UserModule::t("Incorrect activation URL.")));
	}
}