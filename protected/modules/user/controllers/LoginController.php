<?php

class LoginController extends UController
{
	public $defaultAction='login';

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new UserLogin();

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['UserLogin']))
		{
			$model->attributes=$_POST['UserLogin'];
			// validate user input and redirect to previous page if valid
			if($model->validate())
				$this->lastViset();
		}
		$this->redirect(Yii::app()->getHomeUrl());
	}

	private function lastViset()
	{
		$lastVisit=User::model()->notsafe()->findByPk(Yii::app()->user->id);
		$lastVisit->lastvisit=time();
		$lastVisit->save();
	}
}