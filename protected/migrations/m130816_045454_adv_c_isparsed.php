<?php

class m130816_045454_adv_c_isparsed extends CDbMigration
{
	public function up()
	{
		$this->addColumn(
			"advertisements",
			"is_parsed",
			"tinyint(1) not null default 0"
		);
	}

	public function down()
	{
		$this->dropColumn("advertisements",
			"is_parsed"
		);
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}