<?php

class m130816_043100_parser_process extends CDbMigration
{
	public function up()
	{
		$this->execute("
						CREATE TABLE IF NOT EXISTS `parser_process` (
						  `id` int(11) NOT NULL AUTO_INCREMENT,
						  `relParserId` int(11) NOT NULL,
						  `dateStart` datetime NOT NULL,
						  `dateEnd` datetime NOT NULL,
						  `totalParsedPages` int(11) NOT NULL DEFAULT '0',
						  `totalParsedUsers` int(11) NOT NULL DEFAULT '0',
						  `successParsedUsers` int(11) NOT NULL DEFAULT '0',
						  `failedParsedUsers` int(11) NOT NULL DEFAULT '0',
						  `lastParsedPageUrl` varchar(255) DEFAULT NULL,
						  `status` int(11) NOT NULL DEFAULT '0',
						  PRIMARY KEY (`id`)
						) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
	}

	public function down()
	{
		$this->dropTable("parser_process");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}