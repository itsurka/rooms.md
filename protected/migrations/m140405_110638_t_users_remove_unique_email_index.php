<?php

class m140405_110638_t_users_remove_unique_email_index extends CDbMigration
{
	public function safeUp()
	{
		$this->dropIndex('email', 'users');
	}

	public function safeDown()
	{
	}
}