<?php

class m140908_093330_create_table_invoice extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('invoice', [
			'id'=>'pk',
			'user_id'=>'integer',
			'amount'=>'float UNSIGNED',
			'description'=>'text',
			'created_at'=>'timestamp not null default CURRENT_TIMESTAMP()',
			'paid_at'=>'timestamp',
		]);
	}

	public function safeDown()
	{
		$this->dropTable('invoice');
	}
}