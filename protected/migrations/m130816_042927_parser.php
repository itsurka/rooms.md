<?php

class m130816_042927_parser extends CDbMigration
{
	public function up()
	{
		$this->execute("--
						-- Table structure for table `parser`
						--

						CREATE TABLE IF NOT EXISTS `parser` (
						  `id` int(11) NOT NULL AUTO_INCREMENT,
						  `targetSiteId` int(11) NOT NULL,
						  `startPageUrl` varchar(255) NOT NULL,
						  `parsePagesCount` int(11) NOT NULL,
						  `parsedPages` int(11) NOT NULL DEFAULT '0',
						  `status` int(11) NOT NULL DEFAULT '0',
						  `created` datetime NOT NULL,
						  `completedDate` datetime DEFAULT NULL,
						  PRIMARY KEY (`id`)
						) ENGINE=InnoDB  DEFAULT CHARSET=ucs2 AUTO_INCREMENT=1 ;");
	}

	public function down()
	{
		$this->dropTable("parser");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}