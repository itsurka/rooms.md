<?php

class m130811_044745_t_user_add_c_advSearcherMailing extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE `users` ADD `advSearcherMailing` TINYINT( 1 ) NOT NULL DEFAULT \'0\' AFTER `superuser` ');
	}

	public function down()
	{
		$this->execute('ALTER TABLE `users` DROP `advSearcherMailing`');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}