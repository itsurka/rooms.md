<?php

class m140908_102220_table_users_add_column_balance extends CDbMigration
{
	public function safeUp()
	{
		$this->addColumn('users', 'balance', 'float unsigned not null default 0');
	}

	public function safeDown()
	{
		$this->dropColumn('users', 'balance');
	}
}