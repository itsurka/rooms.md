<?php

class m130811_051615_t_users_add_c_lastsearcheradvid extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE `users` ADD `lastSearcherMailingAdvId` INT NOT NULL DEFAULT \'0\' AFTER `advSearcherMailing` ');
	}

	public function down()
	{
		$this->execute('ALTER TABLE `users` DROP `lastSearcherMailingAdvId`');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}