<?php

class m130816_043819_adv_searcher_c_email_address extends CDbMigration
{
	public function up()
	{
		$this->addColumn(
			"adv_searcher",
			"email_address",
			"varchar(50) not null"
		);
	}

	public function down()
	{
		$this->dropColumn("adv_searcher", "email_address");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}