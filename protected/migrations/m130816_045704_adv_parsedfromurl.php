<?php

class m130816_045704_adv_parsedfromurl extends CDbMigration
{
	public function up()
	{
		$this->addColumn(
			"advertisements",
			"parsed_from_url",
			"varchar(255) not null"
		);
	}

	public function down()
	{
		$this->dropColumn(
			"advertisements",
			"parsed_from_url"
		);
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}