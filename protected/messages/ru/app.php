<?php

return array(

    'All' => 'Все',
    'Added' => 'Добавил',
    'Advertisements' => 'Объявления',
    'Activate' => 'Активировать',
    'Author' => 'Автор',

    'Control panel' => 'Панель управления',
    'currency' => 'валюта',
    'Currency' => 'Валюта',
    'City' => 'Город',

    'Delete' => 'Удалить',

    'Edit' => 'Редактировать',

    'Fields with * are required' => 'Поля, отмеченные <span class="required">*</span> нужно заполнить',

    'Image' => 'Фото',

    'views' => 'просмотров',

    'lei' => 'лей',

    'Logout' => 'Выйти',

    'Manage' => 'Управление',
    'Main' => 'Главная',
    'My advertisements' => 'Мои объявления',

    'Number of rooms' => 'Кол-во комнат',
    'now'=> 'сейчас',

    'Price' => 'Цена',
    'Price in' => 'Цена в',

	'Region' => 'Район',
	'Rooms' => 'Комнаты',

    'Same advertisements' => 'Похожие объявления',
    'Search' => 'Поиск',

    'Today' => 'Сегодня',
    'today' => 'сегодня',
    'This phone number already exists.' => 'Этот номер телефона уже существует.',

    'Users' => 'Пользователи',

    'Yesterday' => 'Вчера',
    'yesterday' => 'вчера',

	'Website' => 'Сайт',
    'Wrong phone number(min. 5 digits)' => 'Неверный номер телефона(мин. 5 цифр)',
);