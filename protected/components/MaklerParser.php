<?php

/**
 * Class MaklerParser
 */
class MaklerParser extends WebParser
{
	public $searchPagesByCall=1;
	public $searchPageId=null;
	public $pageUrls=array(); // адреса объявлений
	public $parsedItems=array(); // спаршенные объявления

	protected $targetWebsiteHost='http://makler.md';

	/**
	 * Копируем ссылки на страницы с объялениями
	 * @param $parseFromPage
	 * @return bool
	 */
	function parseItemsUrls($parseFromPage)
	{
		if(is_null($this->searchPageId))
			$this->searchPageId=1;

		$nextPageUrl=$parseFromPage;
		$this->lastParsedPageUrl=$nextPageUrl;

		while($this->searchPageId<=$this->searchPagesByCall)
		{
			//echo "\n searchPageId: $this->searchPageId \n";
			if(!empty($nextPageUrl))
			{
				$pageDom=false;
				$pageHtml=self::getPageHtml($nextPageUrl);

				if($pageHtml!==false)
					$pageDom=str_get_html($pageHtml);

				/*echo $pageHtml;
				var_dump('$pageHtml', empty($pageHtml), gettype($pageHtml), gettype($pageDom));
				exit;*/

				if(is_object($pageDom))
				{
					//var_dump('$mainAnList', !empty($mainAnList));
					$mainAnLists=$pageDom->find('.ls-detail');
					if (!$mainAnLists) {
						return false;
					}
					$mainAnList=end($mainAnLists);
					if(!empty($mainAnList)) // есть ли об-ия на странице
					{
						$items=$mainAnList->find('li');
						//echo "\n items count: ".count($items)." \n";
						if(is_array($items) && count($items)>0)
						{
							foreach($items as $eachItem)
							{
//								if(strpos($eachItem->class, 'top')!==false)
//									continue;

								$_anchor=$eachItem->find('.m > div > a', 0);
								$_pageUrl=trim($_anchor->href);
								// проверяем если уникально
								if(!empty($_pageUrl))
								{
									$_title=trim($_anchor->plaintext);
									$importUrl=trim($this->targetWebsiteHost.$_pageUrl);
									if(!self::checkItemIsUnique($importUrl, $_title))
										continue;

									$this->pageUrls[]=$importUrl;
								}
							}
						}
					}
					else
					{
						//echo "\n mainAnList is empty at page url: $nextPageUrl \n";
					}
				}

				// инкриментация номера странички, след. страничка - start {
				$this->lastParsedPageUrl=$nextPageUrl;
				$_nextPageUrl=self::getNextPageUrlFromUrl($nextPageUrl);
				if(!empty($_nextPageUrl) && $_nextPageUrl!=$nextPageUrl)
					$nextPageUrl=$_nextPageUrl;
				else
					$nextPageUrl=false;
				// инкриментация номера странички, след. страничка - end }
			}
			$this->searchPageId++;
		}
		//echo "\n pageUrls: ".count($this->pageUrls)."\n";
		return true;
	}

	/**
	 * Парсинг страницы объявления
	 * @param $pageUrl
	 * @return array
	 */
	public function parseItemByPageUrl($pageUrl)
	{
		$itemParsedData=array(
			'pageUrl'=>$pageUrl,
			'title'=>'',
			'text'=>'',
			'numberOfRooms'=>1,
			'cityId'=>1, // Кишинев
			'regionId'=>1, // Центр
			'currencyId'=>1, // MDL
			'ownerPhone'=>'',
			'price'=>0,
		);
		$pageDom=file_get_html($pageUrl);

		if(empty($pageDom))
			return $this->parsedItems;

		$mainContainer=$pageDom->find('.bg .canvas', 0);

		// title
		$titleObj=$mainContainer->find('.title > h1	> strong', 0);
		if(empty($titleObj))
		{
			// об-ие не активно уже
			return $this->parsedItems;
		}
		$itemParsedData['title']=trim($titleObj->plaintext);

		// images
		$itemParsedData['images']=array();
		$images=$mainContainer->find('.itmedia > ul > li a img');
		foreach($images as $eachImage)
		{
			$_imageUrl=$eachImage->src;
			$_imageUrl=str_replace(array('/preview/', '/item/'), '/original/', $_imageUrl);
			$itemParsedData['images'][]=$_imageUrl;
		}

		// text
		$itemParsedData['text']=trim($mainContainer->find('#anText', 0)->plaintext);

		// всякое
		$tds=$mainContainer->find('td.td1');
		foreach($tds as $eachTd)
		{
			$text=trim($eachTd->plaintext);

			if($text=='Количество комнат')
			{
				$numOfRooms=$eachTd->next_sibling()->plaintext;
				$numOfRooms=explode(' ', $numOfRooms);
				if($numOfRooms[0]>0)
				{
					$itemParsedData['numberOfRooms']=intval($numOfRooms[0]);
				}
			}
			elseif($text=='Район')
			{
				$value=trim($eachTd->next_sibling()->plaintext);
				if(mb_strpos($value, 'Рышкань')!==false)
				{
					$value='Рышкан';
				}

				if($value!='')
				{
					$criteria=new CDbCriteria();
					$criteria->addSearchCondition('name', $value);
					$criteria->limit=1;
					$region=CityRegions::model()->find($criteria);
					if(!is_null($region))
					{
						$itemParsedData['cityId']=$region->city_id;
						$itemParsedData['regionId']=$region->id;
					}
				}
			}
		}

		// цена
		$price=$mainContainer->find('.itprice', 0);
		if(!is_null($price))
		{
			$priceText=$price->plaintext;
			$priceText=trim(str_replace('Цена:', '', $priceText));
			$priceArray=explode(' ', $priceText);
			if($priceArray[0]>0)
			{
				$itemParsedData['price']=intval($priceArray[0]);
			}
			if(isset($priceArray[1]) && $priceArray[1]!='')
			{
				$priceArray[1]=htmlspecialchars(trim($priceArray[1]));
				if(strpos($priceArray[1], 'euro')!==false)
				{
					$itemParsedData['currencyId']=2;
				}
				elseif($priceArray[1]=='$')
				{
					$itemParsedData['currencyId']=3;
				}
			}
		}

		// телефон
		$phoneNumber=$mainContainer->find('.itphone li', 1);
		if(!is_null($phoneNumber))
		{
			$phoneNumberText=$phoneNumber->plaintext;
			$phoneNumberText=trim(str_replace('+373', '', $phoneNumberText));
			$phoneNumberText=preg_replace('/\D/', '', $phoneNumberText).'';
			if(strlen($phoneNumberText)>4)
			{
				$itemParsedData['ownerPhone']=$phoneNumberText;
			}
		}

		if(!empty($itemParsedData['title']) && empty($itemParsedData['text']))
			$itemParsedData['text']=$itemParsedData['title'];

		if(Advertisements::getIsValid($itemParsedData))
			$this->parsedItems[]=$itemParsedData;

		return $this->parsedItems;
	}

	public static function getPageHtml($pageUrl)
	{
		$pageHtml=false;
		$postData=array(
			'currency_id'=>'1',
			'direction'=>'',
//			'field_106[]'  =>'149',
			'field_115[]'=>'1180',
			'field_169'=>'0',
			'field_169-1'=>'24',
			'field_169_max'=>'24',
			'field_169_min'=>'0',
			'field_172'=>'0',
			'field_172-1'=>'300',
			'field_172_max'=>'300',
			'field_172_min'=>'0',
			'latitude_max'=>'',
			'latitude_min'=>'',
			'longitude_max'=>'',
			'longitude_min'=>'',
			'order'=>'',
			'page'=>self::getPageIdFromUrl($pageUrl),
			'price_max'=>'150000000',
			'price_min'=>'0',
			'selector'=>'select',
			'text'=>'',
		);

		$httpHeader=array(
//			"Accept: text/html, */*; q=0.01",
//			"Accept-Encoding: gzip, deflate",
//			"Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3",
//			"Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3",
//			"Cookie: __utma=37299260.1793552229.1361172640.1371737395.1375427305.3; __utmz=37299260.1361172640.1.1.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=%D0%B2%D1%8B%D0%B7%D0%B2%D0%B0%D1%82%D1%8C%20%D1%8D%D0%BB%D0%B5%D0%BA%D1%82%D1%80%D0%B8%D0%BA%D0%B0%20%D0%BA%D0%B8%D1%88%D0%B8%D0%BD%D0%B5%D0%B2; OAID=0bcd558c4d3a5bd0138739226bce4150; __atuvc=3%7C25; MaklerMd=t3845g08k2e6kvj57gusks6ha5; __utmb=37299260.11.10.1375427305; __utmc=37299260; _ym_visorc=b; currentLang=ru",
//			"Host: makler.md",
//			"Referer: http://makler.md/chisinau/real-estate/apartments",
//			"User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:22.0) Gecko/20100101 Firefox/22.0",
			"X-Requested-With: XMLHttpRequest",
		);

		//echo "\n url: $pageUrl, pageid: ".$postData['page']."\n";

		$uagent="Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:22.0) Gecko/20100101 Firefox/22.0";
		//$pageUrl='http://makler.md/ru/an/list/search/publisher/1/category/107/page/0';
		$ch=curl_init($pageUrl);
		curl_setopt($ch, CURLOPT_URL, $pageUrl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeader);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_ENCODING, "");
		curl_setopt($ch, CURLOPT_USERAGENT, $uagent);
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		curl_setopt($ch, CURLOPT_FAILONERROR, 0);
		curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

		$content=curl_exec($ch);
		$err=curl_errno($ch);
		/*$errmsg=curl_error($ch);
		$header=curl_getinfo($ch);*/
		curl_close($ch);

		if(empty($err))
		{
			if(!empty($content))
			{
				$pageHtml=$content;
			}
		}
		/*echo "$errmsg";
		echo "\n$content";
		var_dump('$content', $content);*/
		return $pageHtml;
	}

	/**
	 * @param $url
	 * @param int $pageIncrementValue
	 * @param int $pageInitValue
	 * @return string
	 */
	public static function getNextPageUrlFromUrl($url, $pageIncrementValue=1, $pageInitValue=0)
	{
		$returnUrl=false;
		$nextPageId=$pageInitValue;
		preg_match('/\/page\/(\d+)/', $url, $m);
		if(isset($m[1]))
			$nextPageId=intval($m[1])+$pageIncrementValue;
		$_returnUrl=preg_replace('/\/page\/\d{1,}/', '/page/'.$nextPageId, $url);
		if(!empty($_returnUrl))
			$returnUrl=$_returnUrl;
		return $returnUrl;
	}

	public static function getPageIdFromUrl($pageUrl)
	{
		$pageId=false;
		preg_match('/\/page\/(\d+)/', $pageUrl, $m);
		if(isset($m[1]))
			$pageId=intval($m[1]);
		return $pageId;
	}
}
