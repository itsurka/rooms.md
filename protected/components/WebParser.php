<?php

include_once(Yii::app()->basePath.'/components/simplehtmldom/simple_html_dom.php');

/**
 * Class WebParser
 */
abstract class WebParser
{
	public $lastParsedPageUrl;
	protected $targetWebsiteHost;
	public $parsedItems=[]; // спаршенные объявления
	public $searchPagesByCall=1;
	public $searchPageId=null;
	public $pageUrls=[]; // адреса объявлений

	/**
	 * @param $url
	 * @param string $pageParamName
	 * @param int $pageIncrementValue
	 * @param int $pageInitValue
	 * @return string
	 */
	public static function getNextPageUrlFromUrl($url,$pageParamName='page',$pageIncrementValue=1,$pageInitValue=1)
	{
		$urlParams2=trim(substr($url,strpos($url,'?')+1));
		$urlParams2=explode('&',$urlParams2);
		$urlParams=array();
		foreach($urlParams2 as $v)
		{
			$keyValueArray=explode('=',$v);
			$urlParams[trim($keyValueArray[0])]=trim($keyValueArray[1]);
		}

		if(!isset($urlParams[$pageParamName]))
			$urlParams[$pageParamName]=$pageInitValue;
		else
			$urlParams[$pageParamName]=intval($urlParams[$pageParamName]);

		$urlParams[$pageParamName]+=$pageIncrementValue;

		$urlParams2=$urlParams;
		$urlParams=array();

		foreach($urlParams2 as $k=>$v)
			$urlParams[]="{$k}={$v}";

		$urlParamsStr="?".join('&',$urlParams);
		$returnUrl=substr($url,0,strpos($url,'?')).$urlParamsStr;

		return $returnUrl;
	}

	/**
	 * @param $url
	 * @param array $postParams
	 * @return mixed
	 */
	public static function makePostXmlHttpRequest($url, $postParams=[])
	{
		return self::makeRequest($url, $postParams, 'POST', true);
	}

	/**
	 * @param $url
	 * @return mixed
	 */
	public static function makeGetXmlHttpRequest($url)
	{
		return self::makeRequest($url, [], 'GET', true);
	}

	/**
	 * @param $url
	 * @param array $params
	 * @param string $method
	 * @param bool $XMLHttpRequest
	 * @param bool $debug
	 * @return mixed
	 * @throws CException
	 */
	public static function makeRequest($url, $params=[], $method = 'GET', $XMLHttpRequest = false, $debug = false)
	{
		if ($method=='GET' && count($params)>0)
			$url.='?'.http_build_query($params);

		$HTTP_HEADER=[];
		if($XMLHttpRequest)
			$HTTP_HEADER[]="X-Requested-With: XMLHttpRequest";

		$ch=curl_init($url);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $HTTP_HEADER);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_ENCODING, "");
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:22.0) Gecko/20100101 Firefox/22.0");
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		curl_setopt($ch, CURLOPT_FAILONERROR, 0);
		curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
		if($method=='POST')
		{
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		}

		$content=curl_exec($ch);
		$err=curl_errno($ch);

		if($debug)
		{
			echo "<h1>DEBUG ON</h1>";
			echo "<pre>";
			echo "<h1>header</h1>";
			print_r(curl_getinfo($ch));
			echo "<h1>url</h1>";
			print($method." ".$url);
			echo "<h1>params</h1>";
			print_r($params);
			echo "<h1>error message</h1>";
			var_dump(curl_error($ch));
			echo "<h1>error code</h1>";
			var_dump($err);
			echo "</pre>";
		}

		if(!empty($err))
		{
			$errorMsg=curl_error($ch);
			throw new CException("cURL ERROR \"{$err}\" with message \"{$errorMsg}\"");
		}
		curl_close($ch);
		return $content;
	}

	/**
	 * @param $html
	 * @return bool|simple_html_dom
	 * @throws CException
	 */
	public static function getHtmlDom($html)
	{
		$domObject=str_get_html($html);
		if(!is_object($domObject))
			throw new CException("domObject is not object");
		return $domObject;
	}

	public static function checkItemIsUnique($itemUrl, $title, $searchByTitleLastItems=10)
	{
		$result=false;
		$conn=Yii::app()->db;
		$command=$conn->createCommand('SELECT COUNT(*) FROM advertisements WHERE is_parsed = 1 AND parsed_from_url = :parsed_from_url');
		$command->params['parsed_from_url']=$itemUrl;
		$rows1=$command->queryScalar();
		$command=$conn->createCommand('SELECT COUNT(*) FROM advertisements WHERE is_parsed = 1 AND title = :title ORDER BY id DESC LIMIT '.intval($searchByTitleLastItems));
		$command->params['title']=$title;
		$rows2=$command->queryScalar();
		if($rows1==0 && $rows2==0)
			$result=true;
		return $result;
	}

	/**
	 * Сохранение спаршеных объявлений
	 * @return int
	 */
	public function saveParsedAdvsData()
	{
		$savedItemsCount=0;

		foreach($this->parsedItems as $eachItem)
		{
			// add adv - start {
			$model=new Advertisements();
			$model->setScenario('userIsGuest');

			$model->title=$eachItem['title'];
			$model->text=$eachItem['text'];
			$model->number_of_rooms=$eachItem['numberOfRooms'];
			$model->owner_id='';
			$model->owner_ip='';
			$model->owner_phone=preg_replace("/[^\d]/", "", $eachItem['ownerPhone']);
			$model->owner_name=isset($eachItem['ownerPhone']) ? $eachItem['ownerPhone'] : '';
			$model->price=$eachItem['price'];
			$model->city_id=$eachItem['cityId'];
			$model->region_id=$eachItem['regionId'];
			$model->currency_id=$eachItem['currencyId'];
			//$model->status=Advertisements::ADV_NEW;
			$model->status=Advertisements::ADV_OK;
			$model->rent_type=1; // сдаю
			$model->is_vip=0;
			$model->is_colored=0;
			$model->is_parsed=1;
			$model->parsed_from_url=$eachItem['pageUrl'];

			if($model->save())
			{
				$savedItemsCount++;
				Yii::import('application.extensions.image.Image');
				$dir=Yii::getPathOfAlias('application.uploads');
				$dirThumb=Yii::getPathOfAlias('application.uploads.thumbs');

				// upload images - start {
				// todo много места занимают картинки
				$eachItem['images'] = array_slice($eachItem['images'], 0, rand(1, 2));
				foreach($eachItem['images'] as $eachImageUrl)
				{
					// Save image if uploaded
					$filename=sha1(date('YmdHis').rand(1000, 9999));
					$ext=pathinfo($eachImageUrl, PATHINFO_EXTENSION);
					$destFile=$dir.'/'.$filename.'.'.$ext;
					$destThumbFile=$dirThumb.'/'.$filename.'.'.$ext;

					if(file_exists($destFile) || copy($eachImageUrl, $destFile))
					{
						$image=new Image($destFile);
						$image->resize(300, 0)->quality(100)->save($destThumbFile);
						$image->resize(500, 0)->quality(100)->save();

						$mimeType='image/jpeg';
						if($ext=='png')
						{
							$mimeType='image/png';
						}
						$fileSize=filesize($destFile);

						// Insert to files
						$fileModel=new Files();
						$fileModel->name=$filename;
						$fileModel->extension=$ext;
						$fileModel->mime_type=$mimeType;
						$fileModel->size=$fileSize;
						$fileModel->path=$dir.'/';
						if($fileModel->save())
						{
							// Assign image to adv
							$advFileModel=new AdvertisementFiles();
							$advFileModel->advertisement_id=$model->id;
							$advFileModel->file_id=$fileModel->id;
							$advFileModel->save();
						}
					}
				}
				// upload images - end }
			}
			else
			{
				/*echo '$model->errors[]<pre>';
				print_r($model->errors);
				echo '</pre>';

				echo '$eachItem[]<pre>';
				print_r($eachItem);
				echo '</pre>';

				echo '$model->attributes[]<pre>';
				print_r($model->attributes);
				echo '</pre>';*/
				//echo '<br> error: cant save advs model in ' . __FILE__ . ' at line ' . __LINE__ . '<br>';
			}
		}

		return $savedItemsCount;
	}

	abstract public function parseItemsUrls($startPageUrl);

	abstract public function parseItemByPageUrl($pageUrl);
}