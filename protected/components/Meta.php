<?php

/**
 * Class generates data
 * for meta tags
 */
class Meta
{
    private static $_isInit = false;
    private static $_pagesMeta;
    private static $_pageMeta;


    // Defaults meta - Start {
    private static $_keywordsAds_general = 'Снять, Квартира, Сдать, Кишинёв, Помесячно';
    private static $_descriptionAds_general = 'Снять квартиру помесячно, сдать квартиру, квартиры в Кишинёве';
    // Defaults meta - End }


    public static function init($pageData=array())
    {
        self::$_isInit = true;

        self::$_pageMeta = array(
            'title'=>'',
            'keywords'=>'',
            'description'=>''
        );

        self::_setPagesMeta();
        self::_loadData($pageData);
    }


    public static function isInit()
    {
        return self::$_isInit;
    }


    private static function _loadData($pageData=array())
    {
        $_pageMeta = null;
        if (empty($pageData)) {
            if (array_key_exists(self::_getCurrentPageID(), self::$_pagesMeta)) {
                self::$_pageMeta = self::$_pagesMeta[self::_getCurrentPageID()];
            } else {
                // TODO
            }
        } else {
            if ($pageData['pageID'] == 'catalog_view') {
//                self::$_pageMeta['title'] = $pageData['title'];
//                self::$_pageMeta['title'] .= ', ' . Common::appartamentNumRoomsToString($pageData['number_of_rooms']) . ' квартиры';
//                self::$_pageMeta['title'] .= ', ' . $pageData['region'];
                self::$_pageMeta['title'] = self::generateTitle($pageData['model']);
                self::$_pageMeta['keywords'] = self::generateKeywords($pageData['model']);
                self::$_pageMeta['description'] = $pageData['text'];
            } else {
                self::$_pageMeta['title'] = $pageData['title'];
                self::$_pageMeta['keywords'] = $pageData['keywords'];
                self::$_pageMeta['description'] = $pageData['description'];
            }
        }
    }


    private static function _getCurrentPageID()
    {
		return Yii::app()->controller->id . '_' . Yii::app()->controller->action->id;
	}


    public static function getTitle()
    {
        return self::$_pageMeta['title'];
    }


    public static function getKeywords()
    {
        return self::$_pageMeta['keywords'];
    }


    public static function getDescription()
    {
        return self::$_pageMeta['description'];
    }


    private static function _setPagesMeta()
    {
        self::$_pagesMeta = array(
            /*2012-12-03*/
            'catalog_index' => array(
//                'title' => 'Снять квартиру, сдать квартиру, Квартиры в Молдове, квартиры в Приднестровье, Кишинев, Тирасполь, Бельцы, Бендеры',
                'title' => 'Аренда квартир в Кишинёве',
//                'keywords' => 'квартира, кишинев, 1-комнатные, 2-комнатные, 3-комнатные, снять, сдать, квартира в кишиневе, квартира в тирасполе, сниму квартиру, сдам квартиру',
                'keywords' => 'квартиры, комнаты, сдать квартиру, снять квартиру, сдать комнату, 1-комнатные квартиры, 2-комнатные квартиры, 3-комнатные квартиры',
                'description' => 'Аренда квартир в Кишинёве, Тирасполе, Бендерах, Бельцах.',
            ),
            'catalog_today' => array(
                'title' => 'Новые объявления квартир за сегодня',
//                'keywords' => 'сегодня, квартира, кишинев, 1-комнатные, 2-комнатные, 3-комнатные, снять, сдать, квартира в кишиневе, квартира в тирасполе, сниму квартиру, сдам квартиру',
                'keywords' => 'квартиры, комнаты, сдать квартиру, снять квартиру, новые квартиры',
//                'description' => 'Аренда недвижимости в Молдове и Приднестровье',
                'description' => 'Новые объявления квартир',
            ),
            'catalog_mySearch' => array(
                'title' => 'Мой поиск квартиры',
                'keywords' => 'поиск квартиры',
                'description' => '',
            ),
            'ads_create' => array(
                'title' => 'Добавить квартиру',
                'keywords' => 'квартира, сдам, сниму, сниму квартиру, сдам квартиру',
                'description' => 'Добавить объявление',
            ),
            'ads_index' => array(
                'title' => 'Мои объявления',
                'keywords' => 'Объявления, Мои Обявления, Профиль, Личный кабинет',
                'description' => 'Все мои обявления, объявления добавленные мной',
            ),
            'ads_update' => array(
                'title' => 'Редактировать обявление',
                'keywords' => 'Редактировать, Объявления, Изменить, Текст',
                'description' => 'Редактировать мои объявления, изменить текст объявления, цену, описание',
            ),
            'site_contact' => array(
                'title' => 'Наши контакты',
                'keywords' => 'Контактные данные, позвонить, связаться, телефон, е-майл, скайп',
                'description' => 'Наши контактные данные, связаться с нами, позвонить нам',
            ),
            'profile_profile' => array(
                'title' => 'Личный кабинет',
                'keywords' => 'Кабинет, Профиль, Личные, Данные, Инормация',
                'description' => 'Личный кабинет, мои данные, изменить данные',
            ),
            'profile_edit' => array(
                'title' => 'Редактировать личные данные',
                'keywords' => 'Редактировать, Данные',
                'description' => 'Редактировать личные данные профайла',
            ),
            'profile_changepassword' => array(
                'title' => 'Изменить пароль',
                'keywords' => 'Изменить, Пароль',
                'description' => 'Изменить пароль',
            ),
            'login_login' => array(
                'title' => 'Войти',
                'keywords' => 'Войти',
                'description' => 'Войти',
            ),
            'registration_registration' => array(
                'title' => 'Регистрация',
                'keywords' => 'Регистрация',
                'description' => 'Регистрация',
            ),
            'recovery_recovery' => array(
                'title' => 'Востановить пароль',
                'keywords' => 'Востановить пароль',
                'description' => 'Востановить пароль',
            ),
        );
    }

    public static function generateKeywords($model)
    {
        Yii::import('ext.autokeyword.autokeyword');

        $text='';

        if($model===null)
            return $text;

        $text.=' '.$model->title;
        $text.=' '.$model->text;
//        $text.=' '.$model->city->name.', '.$model->city->name;
//        $text.=' '.$model->region->name.', '.$model->region->name;
//        $text.=' '.$model->city->name.', '.$model->region->name;
//        $text.=' '.Common::getAppNumRoomsAsString($model->number_of_rooms, false).', '.Common::getAppNumRoomsAsString($model->number_of_rooms, false);
//        $text.=' '.Common::getAppNumRoomsAsString($model->number_of_rooms, false) . ' в '.Common::getWhereFormCityName($model->city->name).', '.$model->region->name;
//        $text.=' '.Common::getAppNumRoomsAsString($model->number_of_rooms, false) . ' в '.Common::getWhereFormCityName($model->city->name).', '.$model->region->name;

        $params=array();
        $params['content'] = $text; //page content
        //set the length of keywords you like
        $params['min_word_length'] = 3;  //minimum length of single words
        $params['min_word_occur'] = 2;  //minimum occur of single words

        $params['min_2words_length'] = 3;  //minimum length of words for 2 word phrases
        $params['min_2words_phrase_length'] = 9; //minimum length of 2 word phrases
        $params['min_2words_phrase_occur'] = 2; //minimum occur of 2 words phrase

        $params['min_3words_length'] = 3;  //minimum length of words for 3 word phrases
        $params['min_3words_phrase_length'] = 9; //minimum length of 3 word phrases
        $params['min_3words_phrase_occur'] = 2; //minimum occur of 3 words phrase

        $keyword=new autokeyword($params, 'UTF-8');

        header('Content-type: text/html; charset=utf-8');

        $keywords='';
        $keywords.=$keyword->get_keywords();
        $keywords.= $keywords!='' ? ', ' : '';
//        $keywords.='квартира';
        $keywords.=Common::getRentTypeStr($model->rent_type, 1) . ' квартиру';
        $keywords.=', '.$model->region->name;
        $keywords.=', '.$model->city->name;
        $keywords.=', '.Common::getAppNumRoomsAsString($model->number_of_rooms, false);
//        $keywords.=', квартира в '.Common::getWhereFormCityName($model->city->name).', район '.$model->region->name;
        $keywords.=', '.Common::getAppNumRoomsAsString($model->number_of_rooms, false) . ' в '.Common::getWhereFormCityName($model->city->name);

//        echo $keywords;
//        exit('<br>exit in ' . __METHOD__ . ' method');

        return $keywords;
    }

    public static function generateTitle($model)
    {
        return $model->title.', '.Common::getAppNumRoomsAsString($model->number_of_rooms, false) . ' в '.Common::getWhereFormCityName($model->city->name).', '.$model->region->name;
    }
}
