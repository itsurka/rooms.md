<?php
/**
 * Description of Common
 *
 * @author Tsurka Igor
 *
 * @version 1.0 Date: 2012-08-15
 *
 * @version 1.1 Date: 2012-09-04
 * -- Add isImage() function
 *
 * @version 1.2 Date: 2012-09-06
 * -- Add timestampToHowMuchTimeHasPassed()
 * -- Add isTimestamp()
 *
 * @version 1.3 Date: 2012-09-13
 * -- Add cutStringToWholeLastWord()
 *
 * @version 1.4 Date: 2012-09-14
 * -- Add getNewerTimestamp()
 *
 * @version 1.5 Date: 2012-12-26
 */

class Common
{
    static function cutString($string, $maxLenght, $endString='')
    {
        $returnString = $string;
        $stringCut = '';
        if(strlen($returnString)>$maxLenght)
        {
            $arrayString = explode(' ', $returnString);
            foreach ($arrayString as $key => $value)
            {
                if(strlen($stringCut.' '.$value)<$maxLenght)
                {
                    $stringCut .= ' '.$value;
                }
            }
        }
        return $stringCut!='' ? $stringCut.$endString : $returnString;
    }

    static function cutStringToWholeLastWord($string, $maxLenght, $endString='')
    {
        $string = trim($string);
        $lenght = mb_strlen($string, 'UTF-8');
        $returnString = '';
        $stoped = false;

        if ($lenght > $maxLenght)
        {
            $words = explode(' ', $string);
            foreach($words as $eachWord)
            {
                if($stoped)
                    continue;

                if(mb_strlen($returnString . ' ' . $eachWord.$endString, 'UTF-8') < $maxLenght)
                    $returnString .= ' ' . $eachWord;
                else
                    $stoped = true;
            }
        }
        else
            $returnString = $string;

        $returnString = trim($returnString);
        if($stoped && $endString!='')
            $returnString .= $endString;
        
        /*var_dump($returnString, $string);*/

        return $returnString;
    }

	static function formatDateTime($timestamp)
	{
		$date = date("Y-m-d", strtotime($timestamp));
		$time = date("H:i", strtotime($timestamp));
		
		if($date == date("Y-m-d"))
		{
			$result = Yii::t("app", "Today").', '.$time;
		}
		else if($date == date('Y-m-d', time() - (24 * 60 * 60)))
		{
			$result = Yii::t("app", "Yesterday").', '.$time;
		}
		else
		{
			$result = Yii::app()->dateFormatter->format("d MMMM", $timestamp);
		}

		return $result;
	}

    /**
     * This function count time that has
     * been passed from $timestamp
     *
     * Ex: $timestamp is 2012-09-06 21:00, now is 2012-09-06 22:00 // return: 1 hour
     *
     * @static
     * @param $timestamp
     * @return string
     * @version 1.0 // 2012-09-06
     */
    static function timestampToHowMuchTimeHasPassed($timestamp)
    {
        if (!self::isTimestamp($timestamp)) {
            return $timestamp;
        }

        $yesterday = date("Y-m-d", strtotime("yesterday"));
        $isYesterday = date('Y-m-d', strtotime($timestamp)) == $yesterday ? true : false;
        $now = new DateTime();
        $targetDate = new DateTime($timestamp);

        $diff = $now->diff($targetDate);

        if ($diff->y>0 || $diff->m>0 || ($diff->d>6)) {
            // CASE 4(more than 6 days):
            $returnStr = Yii::app()->dateFormatter->format('d MMM', $timestamp);

        } elseif ($diff->d>0 || $isYesterday) {
            // CASE 3: min 1 day
            if ($isYesterday) {
                $returnStr = Yii::t('app', 'yesterday');
            } else {
                $days = $diff->d;
            if ($diff->h>0 || $diff->i>0 || $diff->s>0)
                $days++;
            $returnStr = "{$days} д.";
            }
        } elseif ($diff->h>4) {
            // CASE 2.1: // return today OR yesterday
            $timestampDay = intval(date('d', strtotime($timestamp)));
            $today = intval(date('d'));
            if ($timestampDay === $today) {
                $returnStr = Yii::t('app', 'today');
            } else {
                $returnStr = Yii::t('app', 'yesterday');
            }
        } elseif ($diff->h<5 && $diff->h>0) {
            // CASE 2.2: max 4 hours // return hours
            $returnStr = "{$diff->h} ч.";
        } elseif($diff->i>0) {
            // CASE 1: minutes
            $returnStr = "{$diff->i} мин.";
        } else {
            // CASE 0.9: now
            $returnStr = Yii::t('app', 'now');
        }

        return $returnStr;
    }

    static function isTimestamp($timestamp) {
        if (strtotime($timestamp)) {
            return true;
        }
        return false;
    }

	static function appartamentNumRoomsToString($numRooms, $type='комнатные')
    {
        $result = "{$numRooms} {$type}";
		return $result;
	}

	/**
	 * Return city regions
	 */
	/*static function cityRegions($CityID = null)
	{
		$CityRegionsModel = new CityRegions();
		$data = $CityRegionsModel->retrieve();

		return $data;
	}*/

	/**
	 * Set city region
	 * save in session
	 */
	/*static function setCityRegion($id)
	{
		if(is_numeric($id) && $id > 0)
		{
			if(!in_array($id, self::selectedCityRegions()))
			{
				$_SESSION['city_regions'][] = $id;
			}
			else
			{
				$key = array_search($id, $_SESSION['city_regions']);
				unset($_SESSION['city_regions'][$key]);
			}
		}
	}*/

    /**
     * Set catalog search form var to session
     *
     * @static
     * @param $key
     * @param $value
     * @return boolean
     */
    public static function searchFormSetParam($key, $value)
    {
        $session = new CHttpSession();
        switch($key){

            case 'rent_type':
                if(array_key_exists($value, Advertisements::$rentTypes)){
                    $session->add($key, $value);
                } else {
					$session->remove($key);
				}
				return true;
                break;

            case 'city':
            case 'region':
            case 'num_rooms':
            case 'currency':
                if( ($value=intval($value))>=0 ){
                    $session->add($key, $value);
                    return true;
                }
                break;

            case 'sort':
                if(in_array($value, array('date', 'price'))){
                    $session->add($key, $value);
                    return true;
                }
                break;
        }
        return false;
    }

    /**
     * Get catalog search form value
     *
     * @static
     * @param $key
     * @return mixed
     */
    public static function searchFormGetParam($key)
    {
        $session = new CHttpSession();
        return $session->get($key);
    }

    /**
     * Reads $_REQUEST values and set each var
     *
     * @static
     * @return void
     */
    public static function searchFormAutoSetParams()
    {
        $params = array(
            'city',
            'region',
            'num_rooms',
            'sort',
            'currency',
            'rent_type'
        );
        $request = new CHttpRequest();

        if($request->getParam('catalog-search-submit'))
        {
            foreach($params as $value)
                self::searchFormSetParam($value, $request->getParam($value));
        }
    }

	/**
	 * Set sort by
	 * save in session
     * @static
     * @param $id
     * @return void
	 */
	/*static function setSortBy($id)
	{
		if(array_key_exists($id, self::sortSettings())){
			$_SESSION['sort_type'] = $id;
		}
	}*/
	/*static function selectedCityRegions()
	{
		if(!isset($_SESSION['city_regions']))
		{
			$_SESSION['city_regions'] = array();
		}

		return $_SESSION['city_regions'];
	}*/

	/**
	 * Selected number ogf rooms
	 * @return array
	 */
	/*static function selectedNumberOfRooms()
	{
		if(!isset($_SESSION['number_of_rooms']))
		{
			$_SESSION['number_of_rooms'] = array();
		}

		return $_SESSION['number_of_rooms'];
	}*/

	/**
	 * Selected sort type
	 * @return string
	 */
	/*static function selectedSortType()
	{
		if(!isset($_SESSION['sort_type']))
		{
			$_SESSION['sort_type'] = self::defaultSearchSetting("sort_type");
		}

		return $_SESSION['sort_type'];
	}*/

    /**
     * Sort settings array
     *
     * @static
     * @return array
     */
	/*static function sortSettings()
	{
		$sortSettings = array("date"=>"по дате добавления", "price"=>"по цене");
		return $sortSettings;
	}*/

    /**
     * Reset search settings
     *
     * @static
     * @param string $setting
     * @return bool
     */
	/*static function resetSearchSettings($setting="all")
	{
		$searchSettings = array("city_regions", "number_of_rooms", "sort_type");
		if(!in_array($setting, $searchSettings) && $setting !== "all")
		{
			return false;
		}
		switch($setting)
		{
			case "city_regions":
			case "number_of_rooms":
				$_SESSION[$setting] = array();
				break;
			case "sort_type":
				$_SESSION[$setting] = "";
				break;
			default:
				$_SESSION["city_regions"] = array();
				$_SESSION["number_of_rooms"] = array();
				$_SESSION["sort_type"] = self::defaultSearchSetting("sort_type");
				break;
		}
		return true;
	}*/

	/**
	 * Default search setting value
     *
     * @static
     * @param $setting
     * @return array|bool|string
     */
	static function defaultSearchSetting($setting)
	{
		switch ($setting)
		{
			case "city_regions":
			case "number_of_rooms":
				return array();
				break;
			case "sort_type":
				return "date";
				break;
			default:
				return false;
				break;
		}
	}

    /**
     * Check if file is image
     *
     * @static
     * @param CUploadedFile $CUploadedFile
     * @return bool
     */
    public static function isImage(CUploadedFile $CUploadedFile)
    {
        $allowedImages = array('image/jpeg', 'image/png');
        if (in_array($CUploadedFile->getType(), $allowedImages)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Compare 2 timestamps for newer
     *
     * @static
     * @param $timestamp1
     * @param $timestamp2
     * @return string $timestamp
     */
    public static function getNewerTimestamp($timestamp1, $timestamp2)
    {
        return $timestamp1 >= $timestamp2 ? $timestamp1 : $timestamp2;
    }

	public static function getAdvsIDFromTranslitID($translitID)
	{
		if(is_numeric($translitID) && intval($translitID)==$translitID)
		{
			return $translitID;
		}

		$pattern='/\-([0-9]*)$/';
		preg_match($pattern, $translitID, $mathes);
		if(isset($mathes[1]))
			return $mathes[1];
		else
			return $translitID;
	}

    public static function getAppNumRoomsAsString($numRooms, $plural=true, $addApartamentsWord=true)
    {
        if($plural===true)
        {
            if($numRooms==1)
                return '1-комнатные квартиры';
            elseif($numRooms==2)
                return '2-комнатные квартиры';
            elseif($numRooms==3)
                return '3-комнатные квартиры';
        }
        else
        {
            if($numRooms==1)
                return '1-комнатная квартира';
            elseif($numRooms==2)
                return '2-комнатная квартира';
            elseif($numRooms==3)
                return '3-комнатная квартира';
        }
    }

    public static function getAppNumRoomsStr_Enhanced($numRooms, $pattern='NUM_ROOMS-комнатная квартира')
    {
        return str_replace('NUM_ROOMS', $numRooms, $pattern);
    }

    public static function getWhereFormCityName($city)
    {
        switch ($city)
        {
            case 'Кишинёв':
                return 'Кишинёве';
                break;

            case 'кишинёв':
                return 'кишинёве';
                break;

            case 'Тирасполь':
                return 'Тирасполе';
                break;

            case 'тирасполь':
                return 'тирасполе';
                break;

            case 'Бельцы':
                return 'Бельцах';
                break;

            case 'бельцы':
                return 'бельцах';
                break;

            case 'Бендеры':
                return 'Бендерах';
                break;

            case 'бендеры':
                return 'бендерах';
                break;
        }
        return '';
    }

    public static function getRentTypeStr($rentType, $type=1)
    {
        $return=$rentType;
        if ($type==1)
        {
            switch ($rentType)
            {
                case 1:
                    $return='сдаю';
                    break;
                case 2:
                    $return='сниму';
                    break;
                case 3:
                    $return='продаю';
                    break;
                case 4:
                    $return='куплю';
                    break;
            }
        }
        return $return;
    }

    /**
     * @static
     * @return string
     */
    public static function getUTC()
    {
        $UTC='';
        $session=new CHttpSession();
        if(($res=$session->get('UTC'))!==null)
            $UTC=$res;
        return $UTC;
    }

    /**
     * @static
     * @return string
     */
    public static function getDefaultUTC()
    {
        $UTC='GMT 2';
        return $UTC;
    }

    /**
     * @static
     * @param $UTC
     * @return void
     */
    public static function setUTC($UTC)
    {
        $session=new CHttpSession();
        $session->add('UTC', $UTC);
    }

    /**
     * @static
     * @param $value
     * @return array
     */
	public static function getParsedUriValue($value)
	{
		$return = array();
		$value = trim($value);

		if ($value=='')
			return $return;

		$arr = explode(',', $value);

		foreach ($arr as $k=>$v)
			$arr[$k] = trim($v);

		if (is_array($arr) && count($arr)>0)
			$return = $arr;

		return $return;
	}

	public static function getUnsubscribeUrl($email)
	{
		return Yii::app()->request->hostInfo.'/site/unsubscribe?key='.sha1('ffss043nm7fb45'.$email.'77hhfdvvdaahhs');
	}

    public static function getPhoneNumberFriendlyFormat($phoneNumber)
    {
        $phoneNumber=str_replace(array('-',' '),'',$phoneNumber);

        if(strlen($phoneNumber)>6)
        {
            if(!preg_match('/^0/',$phoneNumber))
                $phoneNumber='0'.$phoneNumber;
            
            if(preg_match( '/^(\d{3})(\d{3})(\d{3})$/',$phoneNumber,$matches))
                $phoneNumber = $matches[1] . '-' .$matches[2] . '-' . $matches[3];

            if(preg_match( '/^\d(\d{4})(\d{3})(\d{3})$/',$phoneNumber,$matches))
                $phoneNumber = $matches[1] . '-' .$matches[2] . '-' . $matches[3];
        }

        return $phoneNumber;
    }
}