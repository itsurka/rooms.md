<div class="title" style="margin-bottom: 28px">
    Доброе утро, <?php echo $user->fullname; ?>!
</div>

<p style="padding: 0; margin: 11px 0;">
    Новые объявления на сайте <?php echo CHtml::link('Rooms.md', 'http://rooms.md/'); ?>:<br>
    <ul>
        <?php foreach($advs as $eachAdv): ?>
            <?php
            $title = Common::cutStringToWholeLastWord($eachAdv->title, 65, '...');
            $title = CHtml::encode($title);
            ?>
            <li style="margin: 7px 0;">
                <?php echo CHtml::link($title, 'http://rooms.md/catalog/'.Translit::encodestring($eachAdv->title.'-'.$eachAdv->id)); ?>
                <?php if(intval($eachAdv->mdl_price) > 0): ?>
                    <?php echo $eachAdv->price; ?><?php echo $eachAdv->currency->htmlValue; ?>
                <?php endif; ?> <i>(<?php echo $eachAdv->city->name ?>, <?php echo $eachAdv->region->name ?>)</i>
            </li>
        <?php endforeach; ?>
    </ul>
    <br>
    <br>
    <b><?php echo CHtml::link('Добавить объявление!', 'http://www.rooms.md/ads/create'); ?></b> <span style="font-size: 10px;"><i>Добавить объявление можно бесплатно, без регистрации</i></span><br><br>
    Найти квартиру на нашем сайте очень просто! <span style="font-size: 10px;"><i>Есть удобный поиск по городу, району, и т.д.</i></span><br>
</p>