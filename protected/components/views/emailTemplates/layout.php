<?php /*header('Content-type: text/html; charset=utf-8'); */?>
<html>
    <head>

        <style type="text/css">

            .logo{
                float: left;
            }

            .websiteUrl{
                float: right;
            }

            .clear{
                clear: both;
            }

            .content p{
                padding: 0;
                margin: 21px 0;
            }

            .main{
                width: 800px;
                font-family: sans-serif;
                font-size: 0.9em;
            }

            .footer{
                color: gray;
                font-size: 0.9em;
                margin-top: 50px;
            }

            .content{
                margin-top: 26px;
            }

            .title{
                margin-bottom: 28px
            }

            .actName{
                margin-bottom: 40px;
            }

            .title2{
                font-size: 1.1em;
            }

            .coupon{
                margin-bottom: 40px;
            }

            .coupon .coupon-title{
                margin-bottom: 10px;
            }

            .coupon .coupon-title a{
                color: #006FFF;
                font-weight: bold;
            }

            .coupon .coupon-left{
                float: left;
                width: 300px;
            }

            .coupon .coupon-right{
                float: left;
                margin-left: 30px;
            }

            .coupon ul{
                margin-bottom: 10px;
                margin-top: 10px;
                padding-left: 10px;
                list-style-type: none;
            }

            .coupon .coupon-code{
                color: #FF7500;
                font-size: 2em;
                margin: 4px 0;
            }

            .coupon .left-border{
                border-left: 3px solid red;
                padding-left: 12px;
            }

            .coupon .organization-info{
                font-size: 0.9em;
            }

        </style>

    </head>
    <body>

        <div class="main" style="width: 800px; font-family: sans-serif; font-size: 0.9em; color: #000000;">
            <div class="header">
                <div class="logo" style="float: left;">
                    <a href="http://rooms.md/">
                        <?php echo CHtml::image('http://rooms.md/images/reddot.jpg'); ?>
                    </a>
                </div>
                <!--<div class="websiteUrl" style="float: right;">
                    <?php /*echo CHtml::link('rooms.md', 'http://rooms.md/'); */?>
                </div>-->
            </div>

            <div class="clear" style="clear: both;"></div>

            <div class="content" style="margin-top: 26px;">
                <?php echo $content; ?>
            </div>

            <div class="clear" style="clear: both;"></div>

            <div class="footer" style="color: gray; font-size: 0.9em; margin-top: 50px;">
                С уважением,<br>Администация сайта <?php echo CHtml::link('Rooms.md', 'http://rooms.md/'); ?>. Написать нам <?php echo CHtml::mailto('admin@rooms.md', 'admin@rooms.md'); ?>.
				<br><br><span>Если Вы не хотите получать уведомления на Ваш е-майл, нажмите чтобы <?php echo CHtml::link('отписаться от рассылки.', Common::getUnsubscribeUrl($user->email)); ?></span>
            </div>

            <!--<div class="footer" style="color: gray; font-size: 0.9em; margin-top: 50px;">
                <div style="margin: 0px 0;">************************************</div>
                Адрес сайта - <?php /*echo CHtml::link('Rooms.md', 'http://rooms.md/'); */?><br>
                Администация - <?php /*echo CHtml::mailto('admin@rooms.md', 'admin@rooms.md'); */?>
                <div style="margin: 4px 0;">************************************</div>
            </div>-->
        </div>
    </body>
</html>