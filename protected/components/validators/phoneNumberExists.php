<?php
/**
 * @author Igor Tsurka
 * @date 2012-09-03 15:56
 * @version 1.0
 */

Yii::import('application.components.validators.phoneNumber');

class phoneNumberExists extends phoneNumber
{
    protected function validateAttribute($object, $attribute)
    {
        $phoneExists = false;
        $value = $object->$attribute;

        if (!parent::validateAttribute($object, $attribute)) {
            return false;
        }

        // Search in advs - Start {
        $advs = Advertisements::model()->findAllByAttributes(array('owner_phone'=>$value));
        if ($advs)
            $phoneExists = true;
        // Search in advs - End }

        // Search by users phone - Start {
        $criteria = new CDbCriteria();
        $criteria->addInCondition('profile.phone', array($value));
        $criteria->with = 'profile';
        $UserModule = Yii::app()->getModule('user');
        $UsersModel = $UserModule->getUsersModel();
        $users = $UsersModel::model()->findAll($criteria);
        if ($users)
            $phoneExists = true;
        // Search by users phone - End }

        if ($phoneExists) {
            $this->addError($object, $attribute, Yii::t('app', Yii::t('app', 'This phone number already exists.')));
            return false;
        }

        return true;
    }
}
