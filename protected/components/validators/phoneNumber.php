<?php
/**
 * @author Igor Tsurka
 * @date 2012-08-18 15:35
 * @version 1.0
 */

class phoneNumber extends CValidator
{

    private static $pattern = '/^[0-9\-]{5,12}$/';


    /**
     * Validates the attribute of the object.
     * If there is any error, the error message is added to the object.
     *
     * @param CModel $object the object being validated
     * @param string $attribute the attribute being validated
     * @return bool
     *
     */
    protected function validateAttribute($object, $attribute)
    {
        $value=$object->$attribute;
        $value=trim($value);
        if(!preg_match(self::$pattern, $value))
        {
            $this->addError($object, $attribute, Yii::t('app', 'Wrong phone number(min. 5 digits)'));
            return false;
        }
        return true;
    }
}
