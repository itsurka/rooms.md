<?php
/**
 * Extending CButtonColumn to customize CGridView
 *
 * @author Tsurka Igor
 * @version 1.0 Date: 2012-08-20
 * @since 1.0
 */
 
class MyCButtonColumn extends CButtonColumn
{
    public $template = '{view}{update}{delete}';

    /**
     * @param $row
     * @param $data
     * @return void
     */
    protected function renderDataCellContent($row, $data)
    {
        $tr = array();
        ob_start();
        foreach ($this->buttons as $id => $button) {

            /*$returnArray = $this->_checkButton($id, $button, $row, $data);
            $id = $returnArray['id'];
            $button = $returnArray['button'];
            $row = $returnArray['row'];
            $data = $returnArray['data'];*/

            $this->renderButton($id, $button, $row, $data);
            $tr['{' . $id . '}'] = ob_get_contents();
            ob_clean();
        }
        ob_end_clean();
        echo strtr($this->template, $tr);
    }



    /**
     * @param $id
     * @param $button
     * @param $row
     * @param $data
     * @return array
     */
    private function _checkButton($id, $button, $row, $data)
    {
        $found = true;
        
        switch($id) {
            case 'activate':
                    if($data->status === Advertisements::ADV_OK) {
                        $button['visible'] = 'false';
                    }
                break;
            case 'block':
                    if($data->status === Advertisements::ADV_BLOCKED) {
                        $button['visible'] = 'false';
                    }
                break;
            default:
                $found = false;
                break;
        }

        if($found) {
            $button['label'] = ucfirst(strtolower(str_replace('ADV_', '', $id)));
        }

        return array(
            'id'=>$id,
            'button'=>$button,
            'row'=>$row,
            'data'=>$data,
        );
    }
}
