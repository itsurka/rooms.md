<?php

/**
 * Class Parser999
 */
class Parser999 extends WebParser
{
	public $searchPagesByCall=1;
	public $searchPageId=null;
	public $pageUrls=array(); // адреса объявлений
	public $parsedItems=array(); // спаршенные объявления

	protected $targetWebsiteHost='http://999.md';

	/**
	 * Копируем ссылки на страницы с объялениями
	 * @param $parseFromPage
	 * @return bool
	 */
	function parseItemsUrls($parseFromPage)
	{
		if(is_null($this->searchPageId))
			$this->searchPageId=1;

		$nextPageUrl=$parseFromPage;
		$this->lastParsedPageUrl=$nextPageUrl;

		while($this->searchPageId<=$this->searchPagesByCall)
		{
			//echo "\n searchPageId: $this->searchPageId \n";
			if(!empty($nextPageUrl))
			{
				$html=self::makeGetXmlHttpRequest($nextPageUrl);
				$pageDom=self::getHtmlDom($html);

				$mainList=$pageDom->find('.ads-list-table', 0);
				//var_dump(!empty($html), !empty($pageDom), !empty($mainList));
				if(!empty($mainList)) // есть ли об-ия на странице
				{
					$items=$mainList->find('tr');
					//echo "\n items count: ".count($items)." \n";
					if(is_array($items) && count($items)>0)
					{
						foreach($items as $eachItem)
						{
							$link=$eachItem->find('.ads-list-table-title a', 0);
							if(!$link)
								continue;

							// убираем vip об-ия
							if (mb_strpos($link->parent->attr['class'], 'is-colored')!==false)
								continue;

							$title=trim($link->plaintext);
							$importUrl=trim($this->targetWebsiteHost.$link->href);
							if(!self::checkItemIsUnique($importUrl, $title))
								continue;

							$this->pageUrls[]=$importUrl;
						}
					}
				}

				// инкриментация номера странички, след. страничка - start {
				$this->lastParsedPageUrl=$nextPageUrl;
				$_nextPageUrl=self::getNextPageUrlFromUrl($nextPageUrl);
				if(!empty($_nextPageUrl) && $_nextPageUrl!=$nextPageUrl)
					$nextPageUrl=$_nextPageUrl;
				else
					$nextPageUrl=false;
				// инкриментация номера странички, след. страничка - end }
			}
			$this->searchPageId++;
		}
		return true;
	}

	/**
	 * Парсинг страницы объявления
	 * @param $pageUrl
	 * @return array
	 */
	public function parseItemByPageUrl($pageUrl)
	{
		$itemParsedData=[
			'pageUrl'=>$pageUrl,
			'title'=>'',
			'text'=>'',
			'numberOfRooms'=>1,
			'cityId'=>'',
			'regionId'=>'',
			'currencyId'=>1, // MDL
			'ownerPhone'=>'',
			'ownerName'=>'',
			'price'=>0,
			'images'=>[],
		];
		$pageDom=file_get_html($pageUrl);

		if(empty($pageDom))
			return $this->parsedItems;

		$mainContainer=$pageDom->find('.adPage', 0);

		// заголовок
		$itemParsedData['title']=trim(html_entity_decode(strip_tags($mainContainer->find('.adPage__header h1', 0)->plaintext)));

		// имя
		$itemParsedData['ownerName']=trim($mainContainer->find('.adPage__header__stats', 0)->find('dl', 0)->find('a', 0)->plaintext);

		$adPageContent=$mainContainer->find('.adPage__content', 0);

		// фото
		if($images=$mainContainer->find('.adPage__content__photos .mfp-zoom'))
		{
			foreach($images as $eachImage)
			{
				$itemParsedData['images'][]=$eachImage->href;
			}
		}

		// текст
		$itemParsedData['text']=trim(html_entity_decode($adPageContent->find('.adPage__content__description', 0)->plaintext));

		// кол-во комнат
		if($adPageContentFeaturesCol=$adPageContent->find('.adPage__content__features__col', 0))
		{
			$liArr=$adPageContentFeaturesCol->find('li.m-value');
			foreach($liArr as $eachLi)
			{
				$firstSpan=$eachLi->find('span',0);
				$secondSpan=$eachLi->find('span',1);
				$liTitle=trim($firstSpan->plaintext);
				if(mb_strpos($liTitle, 'Кол. комнат')!==false)
					$itemParsedData['numberOfRooms']=intval(trim($secondSpan->plaintext)[0]);
			}
		}

		// цена
		if($adPageContentPrice=$adPageContent->find('.adPage__content__price', 0))
		{
			$itemParsedData['price']=intval($adPageContentPrice->find('span', 0)->plaintext);
			$span2=$adPageContentPrice->find('span', 1);
			if($span2->attr['content']=='EUR')
				$itemParsedData['currencyId']=2;
			elseif($span2->attr['content']=='USD')
				$itemParsedData['currencyId']=3;
		}

		// город, район
		if($adPageContentRegion=$adPageContent->find('.adPage__content__region', 0))
		{
			$regionStr=str_replace('Регион: ', '', trim($adPageContentRegion->plaintext));
			$regionArr=explode(',', $regionStr);
			if(isset($regionArr[1])) // город
			{
				if(isset($regionArr[2])) // район
				{
					/** @var CityRegions $region */
					if($region=CityRegions::model()->findByAttributes(['name'=>trim($regionArr[2])]))
					{
						$itemParsedData['cityId']=$region->city_id;
						$itemParsedData['regionId']=$region->id;
					}
				}
			}
		}

		// телефон
		if($adPageContentPhone=$adPageContent->find('.adPage__content__phone', 0))
		{
			$ownerPhone=trim($adPageContentPhone->find('a', 0)->find('strong', 0)->plaintext);
			$itemParsedData['ownerPhone']=$ownerPhone;
		}

		if(Advertisements::getIsValid($itemParsedData))
			$this->parsedItems[]=$itemParsedData;

		return $this->parsedItems;
	}

	/**
	 * @param $url
	 * @param int $pageIncrementValue
	 * @param int $pageInitValue
	 * @return string
	 */
	public static function getNextPageUrlFromUrl($url, $pageIncrementValue=1, $pageInitValue=0)
	{
		// http://999.md/list/real-estate/apartments-and-rooms/2?o_33_1=912&view_type=1&_pjax=%23js-pjax-container
		$returnUrl=false;
		$nextPageId=$pageInitValue;
		preg_match('/\/apartments-and-rooms\/(\d+)/', $url, $m);
		if(isset($m[1]))
			$nextPageId=intval($m[1])+$pageIncrementValue;
		$_returnUrl=preg_replace('/\/apartments-and-rooms\/\d{1,}/', '/apartments-and-rooms/'.$nextPageId, $url);
		if(!empty($_returnUrl))
			$returnUrl=$_returnUrl;
		return $returnUrl;
	}

	public static function getPageIdFromUrl($url)
	{
		$pageId=0;
		preg_match('/\/apartments-and-rooms\/(\d+)/', $url, $m);
		if(isset($m[1]))
			$pageId=intval($m[1]);
		return $pageId;
	}
}
