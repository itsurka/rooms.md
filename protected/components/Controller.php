<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();

	public static $menu2=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */

	/**
	 * @var User
	 */
	public $user;

	/**
	 * @var Currencies[]
	 */
	public $currencies=array();

	public $breadcrumbs=array();

	protected function beforeAction($action, $options=array())
	{
		if(empty($options['theme']))
		{
			$session=new CHttpSession();
			$session->add('module_user_theme', 'classic');
		}

		/*AccessLog::add();*/

		if(parent::beforeAction($action))
		{
			//$this->setMenuItems();

			// Проверяем если пользователь новичок - Start {
			$CHttpSession=new CHttpSession();
			if(Yii::app()->user->isGuest && $CHttpSession->get('isFirstCame')===null)
			{
				if(Yii::app()->getModule('user')->isFirstCame())
					$CHttpSession->add('isFirstCame', true);
				else
					$CHttpSession->add('isFirstCame', false);
			}
			else
			{
				$this->user=User::model()->findByPk(Yii::app()->user->id);
			}

			return true;
		}
		else
			return false;
	}

	public function init()
	{
		parent::init();

		Yii::app()->setTimeZone('Europe/Chisinau');
		Yii::app()->localtime->Locale='ru_md'; // eg 'en_gb'
		Yii::app()->localtime->TimeZone='Europe/Chisinau'; // eg 'Europe/London'

		$user=Yii::app()->getUser();
		$session=Yii::app()->session;
		if(!$user->isGuest && is_null($session->get('advsearcher_updated')))
		{
			$this->user=User::model()->findByPk($user->id);
			AdvSearcher::model()->updateAll(array('rel_user_id'=>$this->user->id), 'email_address = "'.$this->user->email.'"');
			$session->add('advsearcher_updated', 1);
		}

//		// массив валют по индексу
//		$this->currencies=Yii::app()->cache->get('currencies');
//		if($this->currencies===false)
//		{
//			/** @var Currencies[] $_currencies */
//			$_currencies = Currencies::model()->findAll();
//			foreach($_currencies as $eachCurrency)
//				$this->currencies[$eachCurrency->currency_id] = $eachCurrency;
//			Yii::app()->cache->set('currencies', $this->currencies);
//		}
	}

	public function setMenuItems()
	{
//        $AdvertisementsModel = new Advertisements();
//        $countItems = $AdvertisementsModel->countItems(); // Count added items, today/all
//		self::$menu2 = array(array('label'=>'Главная<br /><em>Все квартиры ('.$countItems[0].'/'.$countItems[1].')</em>', 'url'=>'/'));

		/*for($i=1;$i<=3;$i++) {
			if($i==1)
				$t1 = 'Одна';
			elseif($i==2)
				$t1 = 'Две';
			elseif($i==3)
				$t1 = 'Три';

			$t2 = $i<2 ? 'Комната' : 'Комнаты';
			array_push(self::$menu2, array('label'=>$t1.'<br /><em>'.$t2.'</em>', 'url'=>array('/catalog/rooms', 'id'=>$i)));
		}*/
		self::$menu2=array();
		/*array_push(self::$menu2, array('label'=>'Главная<br /><em>Все квартиры</em>', 'url'=>'/'));
		array_push(self::$menu2, array('label'=>'Добавить<br /><em>Объявление</em>', 'url'=>array('/ads/create'), 'linkOptions'=>array('class'=>'')));
		array_push(self::$menu2, array('label'=>'Контакты<br /><em>Написать нам</em>', 'url'=>array('/site/contact')));*/

		array_push(self::$menu2, array('label'=>'Главная', 'url'=>'/'));
		array_push(self::$menu2, array('label'=>'Добавить', 'url'=>array('/ads/create'), 'linkOptions'=>array('class'=>'')));
		array_push(self::$menu2, array('label'=>'Контакты', 'url'=>array('/site/contact')));

		return self::$menu2;
	}
}
