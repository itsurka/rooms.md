<?php
/**
 * Description of Currency
 *
 * @use:
 * Currency::toAnotherCurrency($data->price, $data->currency->code, 'MDL'); // 100$ -> 999MDL

 * @author Tsurka Igor
 * @version 1.0 Date: 2012-08-15
 * @since 1.0
 */

class Currency
{

	public static $isInit;
	public static $exchange;
	public static $currencies;
	public static $allCurrencies;


	public static function init()
	{
		if(self::$isInit)
			return TRUE;
			
		self::$isInit = TRUE;
		self::$currencies = array('EUR', 'USD');
		self::$allCurrencies = array(1=>'MDL', 'EUR', 'USD');
	}


	public static function updateExchange()
	{
		try {
			// check if exchange is old
			$today = date("Y-m-d");
			$data = self::getExchangeByDate($today);
			if($data!==NULL)
				return TRUE;
			// load new version
			$todayForXML = date("d.m.Y");
			Yii::import('application.extensions.xml.xml2array');
			// for test
			//$dir = Yii::getPathOfAlias('application.uploads.xml');
			//$xml_file = $dir . '/myxml.xml';
			$xml_file = "http://www.bnm.org/ru/official_exchange_rates?get_xml=1&date={$todayForXML}";
			$xml_data = file_get_contents($xml_file);
			if(!$xml_data)
				return FALSE;
			$xml2array = new xml2array();
			$xml_array = $xml2array->parse($xml_data);
			if(count($xml_array)>0)
			{
				// save to db
				$exchanges = array();
				$ExchangesModel = new Exchanges();
				// find EUR, USD
				foreach ($xml_array['ValCurs']['Valute'] as $key => $value)
				{
					if( in_array($value['CharCode']['value'], self::$currencies))
					{
						$exchanges[$value['CharCode']['value']] = $value['Value']['value'];
					}
				}
				if(count($exchanges)>0)
				{
					$ExchangesModel->exchange = json_encode($exchanges);
					$ExchangesModel->bank = "BNM"; // Banca Nationala din Moldova
					$ExchangesModel->date = $today;
					$r = $ExchangesModel->save();
					return TRUE;
				}
				else
					return TRUE;
			}
			else
				return TRUE;

		} catch (Exception $exc) {
			echo $exc->getTraceAsString();
		}
	}

    
	public static function loadExchange()
	{
		self::init();
		self::updateExchange();
		$data = self::getLastExchange();
		if($data===NULL)
			throw new CHttpException(404, 'Could not load exchange');
		
		self::$exchange = json_decode($data->exchange);
	}


	public static function getExchangeByDate($date)
	{
		$criteria = new CDbCriteria();
		$criteria->addColumnCondition(array('date'=>$date));
		return self::getExchange($criteria);
	}


	public static function getLastExchange()
	{
		$criteria = new CDbCriteria();
		$criteria->order = "date DESC";
		return self::getExchange($criteria);
	}


	public static function getExchange($criteria=array())
	{
		$ExchangesModel = new Exchanges();
		$data = $ExchangesModel->retrieve($criteria);
		return $data;
	}


	public static function calculate($value, $currency="USD", $round=0)
	{
		if(!self::$isInit || empty($value) || !in_array($currency, self::$currencies))
			return $value;

		$result = self::$exchange[strtoupper($currency)] * $value;
		$result = round($result, intval($round));
		return $result;
	}


	public static function toAnotherCurrency($value, $fromCurrency, $toCurrency="MDL", $decimals=0)
	{
		if(!self::$isInit){
            self::loadExchange();
        }

        if(is_numeric($fromCurrency)){
            $fromCurrency = self::currencyIDToCode($fromCurrency);
        }

        if(is_numeric($toCurrency)){
            $toCurrency = self::currencyIDToCode($toCurrency);
        }

		if(!self::$isInit || $fromCurrency===NULL || $toCurrency===NULL || !(is_numeric($value) && (int) $value > 0) || ( (!in_array($fromCurrency, self::$currencies) &&  $fromCurrency!="MDL") || (!in_array($toCurrency, self::$currencies) &&  $toCurrency!="MDL")) )
			return 0;

		$value = (int) $value;
		$result = 0;
		$valueMDL = 0;
		// Step 1: Count MDL
		$fromCurrency = strtoupper($fromCurrency);
		if(in_array($fromCurrency, self::$currencies))
			$valueMDL = self::$exchange->$fromCurrency * $value;
		else
			$valueMDL = $value;

		// Step 2: Count another currency
		$toCurrency = strtoupper($toCurrency);
		if($toCurrency=='MDL'){
            $result = $valueMDL;
        } else {
			/*$result = self::$exchange->$toCurrency * $valueMDL;*/
			$result = $valueMDL / self::$exchange->$toCurrency;
		}

		$result = round($result, $decimals);
		return $result;
	}


    /**
     * Convert currency_id to his code
     *
     * @static
     * @param $currency_id
     * @return string|int
     */
    public static function currencyIDToCode($currency_id)
    {
        $currency_id = intval($currency_id);
        if(isset(self::$allCurrencies[$currency_id]))
            return self::$allCurrencies[$currency_id];

        return $currency_id;
    }
}
