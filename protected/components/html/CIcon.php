<?php
/**
 * Created by JetBrains PhpStorm.
 * User: igor
 * Date: 05.12.12
 * Time: 19:12
 * To change this template use File | Settings | File Templates.
 */
 
class CIcon extends CHtml
{
    protected static $_obj;

    protected $iconsStyle='fatcow-hosting-icons-3000';
    protected $iconsFolder='/images/all_icons';
    protected $iconName='';
    protected $iconSize='16x16';

    public function __construct()
    {

    }

    public static function getObj()
    {
        return self::$_obj;
    }

    public static function setObj($obj)
    {
        return self::$_obj=$obj;
    }

    public function getIconUrlByName($iconName=null)
    {
		$useIconName=$this->iconName;
		if(!is_null($iconName))
			$useIconName=$iconName;
        return $this->iconsFolder . '/' . $this->iconsStyle . '/' . $this->iconSize . '/' . $useIconName . '.png';
    }

    public static function init($iconName=null)
    {
		$_obj=new CIcon();

        if($iconName!==null)
            $_obj->setIconName($iconName);
        
        return $_obj;
    }

    /**
     * @param $iconName
     * @return void
     */
    public function setIconName($iconName)
    {
        $this->iconName=$iconName;
    }

    /**
     * @param $iconSize 24x24, 16x16
     * @return CIcon
     */
    public function setIconSize($iconSize)
    {
        $this->iconSize=$iconSize;
        return $this;
    }

    public function getImg($htmlOptions=array())
    {
        return self::image($this->getIconUrlByName($this->iconName), $this->iconName, $htmlOptions);
    }

    public function getImgUrl()
    {
        return $this->getIconUrlByName($this->iconName);
    }
}
