<?php
/**
 * Created by JetBrains PhpStorm.
 * User: test
 * Date: 02.01.13
 * Time: 17:16
 * To change this template use File | Settings | File Templates.
 */
 
class CIconButton extends CIcon
{
	protected static $_obj;

	public static function init($iconName=null)
	{
		$_obj=new CIconButton();

		if($iconName!==null)
			$_obj->setIconName($iconName);

		return $_obj;
	}

	public function getButton($contentPattern=null,$htmlOptions=array(),$imgHtmlOptions=array())
	{
		if(is_null($contentPattern))
			$contentPattern='[IMG]';
		$img=$this->getImg($imgHtmlOptions);
		$content=str_replace('[IMG]',$img,$contentPattern);
        if(!isset($htmlOptions['class']))
            $htmlOptions['class']='';
        if(strpos($htmlOptions['class'],'span-btn')===false)
            $htmlOptions['class'].=' span-btn';
		return CHtml::tag('span',$htmlOptions,$content);
	}
}