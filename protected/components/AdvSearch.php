<?php
/**
 * Created by JetBrains PhpStorm.
 * User: test
 * Date: 11.12.12
 * Time: 12:24
 * To change this template use File | Settings | File Templates.
 */
 
class AdvSearch extends Common
{
    /**
     * @static
     * @return array
     */
	public static function getSearchedCityIDs()
	{
		$request=Yii::app()->request;
		$citysIDs=array();
		$values=self::getParsedUriValue($request->getParam('city'));

		if (count($values)==0)
			return $citysIDs;

		$criteria=new CDbCriteria();
		$criteria->addInCondition('name_translit', $values);
		$cities=Cities::model()->findAll($criteria);
		$citiesArr=CHtml::listData($cities, 'id', 'name');

		if (count($citiesArr)>0)
			$citysIDs=array_keys($citiesArr);

		return $citysIDs;
	}

    /**
     * @static
     * @param $paramType
     * @param $paramValue
     * @param null $additionalParamType
     * @param null $additionalParamValue
     * @return bool
     */
    public static function getExistRequestParam($paramType,$paramValue,$additionalParamType=null,$additionalParamValue=null)
    {
        $request=Yii::app()->request;
        $values=self::getParsedUriValue($request->getParam($paramType));

        $exist=in_array($paramValue,$values);

        // если это проверка района, тогда проверяем если город тоже выбран,
        // потому что есть одинаковые районы в разных городах
        if($exist && $additionalParamType!==null && $additionalParamValue!==null)
        {
            $values=self::getParsedUriValue($request->getParam($additionalParamType));
            $exist=in_array($additionalParamValue,$values);
        }
        
        return $exist;
    }
}