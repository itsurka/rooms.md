<?php

class AdsController extends Controller
{

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/column2';

	/**
	 * @var int num of ads on page
	 **/
	public $itemsOnPage=10;

	/**
	 * @var string default action
	 **/
	public $defaultAction='edit';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow autenticated users to perform 'index' actions
				'actions'=>array('*'),
				'users'=>array('@'),
			),
		);
	}

	public function init()
	{
		parent::init();
	}

	protected function beforeAction($action, $options=array())
	{
		if(!parent::beforeAction($action, $options=array()))
			return false;

		if(in_array($this->action->id, array('index', 'update')))
			$this->menu=User::getProfileMenu();

		return true;
	}

	/**
	 * Display user ads
	 */
	public function actionIndex()
	{
		$model=new Advertisements();
		$model->unsetAttributes();

		$criteria=$model->getDbCriteria();
		$criteria->addNotInCondition('status', array(
			Advertisements::ADV_BLOCKED,
			Advertisements::ADV_DELETED
		));

		if(isset($_GET['Advertisements']))
			$model->setAttributes($_GET['Advertisements']);

		$model->owner_id = Yii::app()->user->id;

		$this->render('index', array('model'=>$model));
	}

	/**
	 * Change adv item status
	 *
	 * @param $id
	 * @param $status
	 * @return boolean
	 */
	public function actionChangeAdvStatus($id, $status)
	{
		$result=array('success'=>false, 'message'=>'');
		if(Yii::app()->user->isGuest || !isset(Advertisements::$statuses[$status]))
		{
			echo CJSON::encode($result);
			Yii::app()->end();
		}
		$criteria=new CDbCriteria();
		$criteria->addCondition('id = :id');
		$criteria->addCondition('owner_id = :owner_id');
		$criteria->params['id']=$id;
		$criteria->params['owner_id']=Yii::app()->user->id;
		if($model=Advertisements::model()->find($criteria))
		{
			$model->status=$status;
			$model->save();
			$result['success']=true;
		}
		echo CJSON::encode($result);
		Yii::app()->end();
	}

	/**
	 * Manage user ads
	 */
	public function actionManage()
	{
		$model=new Advertisements('search');
		$model->unsetAttributes(); // clear any default values
		if(isset($_GET['Advertisements']))
			$model->attributes=$_GET['Advertisements'];

		// Render
		$this->render('manage', array(
			'model'=>$model,
		));
	}

	/**
	 * @param $id
	 * @return void
	 */
	public function actionView($id)
	{
		$id=Common::getAdvsIDFromTranslitID($id);

		$model=$this->loadModel($id);

		// Render
		$this->render('/catalog/view', array(
			'data'=>$model,
		));
	}

	/**
	 * @param $id
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$id=Common::getAdvsIDFromTranslitID($id);

		$modelUpload=new Uploader;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		// Save
		if(isset($_POST['Advertisements']))
		{
			$model=$this->loadModel($id);
			$model->attributes=$_POST['Advertisements'];

			if($model->save())
			{
				// Update old files
				if(!empty($model->files))
				{
					$advFiles=$model->files;
					$advOldFiles=empty($_POST['oldFiles']) ? array() : $_POST['oldFiles'];
					foreach($advFiles as $key=>$eachFile)
					{
						if(!in_array($eachFile->name, $advOldFiles))
							Files::model()->deleteByPk($eachFile->id);
					}
				}

				// Upload files
				if(!empty($_FILES['Uploader']['name']['files']))
				{
					$files=CUploadedFile::getInstances($modelUpload, "files");
					Yii::import('application.extensions.upload.upload');

					foreach($files as $eachFile)
					{
						if(!$eachFile->hasError && Common::isImage($eachFile))
						{
							// Save image if uploaded
							$dir=Yii::getPathOfAlias('application.uploads');
							$dirThumb=Yii::getPathOfAlias('application.uploads.thumbs');
							$filename=sha1(date('YmdHis').rand(1000, 9999));

							$uploadedFile=array(
								'name'=>$eachFile->getName(),
								'type'=>$eachFile->getType(),
								'tmp_name'=>$eachFile->getTempName(),
								'error'=>$eachFile->getError(),
								'size'=>$eachFile->getSize(),
							);
							$Upload=new upload($uploadedFile);

							if($Upload->processed)
							{
								// Big image
								$Upload->file_new_name_body=$filename;
								$Upload->image_resize=true;
								$Upload->image_x=400;
								$Upload->image_ratio_y=true;
								$Upload->process($dir);
								// Thumb
								$Upload->file_new_name_body=$filename;
								$Upload->image_resize=true;
								$Upload->image_x=200;
								$Upload->image_ratio_y=true;
								$Upload->process($dirThumb);
								$Upload->clean();

								// Insert to files
								$fileModel=new Files();
								$fileModel->name=$filename;
								$fileModel->extension=mb_strtolower($eachFile->getExtensionName());
								$fileModel->mime_type=$eachFile->getType();
								$fileModel->size=$eachFile->getSize();
								$fileModel->path=$dir.'/';
								if($fileModel->save())
								{
									// Assign image to adv
									$advFileModel=new AdvertisementFiles();
									$advFileModel->advertisement_id=$model->id;
									$advFileModel->file_id=$fileModel->id;
									$advFileModel->save();
								}
							}
							else
							{
								// exception
							}
						}
					}
				}

				$this->redirect(array('index'));
			}
		}

		$model=$this->loadModel($id);

		// Render
		$this->render('update', array(
			'model'=>$model,
			'modelUpload'=>$modelUpload,
		));
	}

	/**
	 * @throws CHttpException
	 * @param $id
	 * @return void
	 */
	public function actionDelete($id)
	{
		$id=Common::getAdvsIDFromTranslitID($id);

		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$model=$this->loadModel($id);
			$model->status='deleted';
			$model->save();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * @throws CHttpException
	 * @param $id
	 * @return void
	 */
	public function actionActivate($id)
	{
		$id=Common::getAdvsIDFromTranslitID($id);

		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$model=$this->loadModel($id);
			$model->status='ok';
			$model->save();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * @throws CHttpException
	 * @param $id
	 * @return CActiveRecord
	 */
	public function loadModel($id)
	{
		$model=Advertisements::model()->findByPk($id);

		// Error if model didn't exist or model doesn't belong to user.
		if (!empty($_GET['test'])) {
			echo '$var[]<pre>';
			print_r($model->attributes);
			echo '</pre>';exit;
		}
		if($model===null || $model->owner_id!=Yii::app()->user->id)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * @throws CHttpException
	 */
	public function actionCreate()
	{
		$request=Yii::app()->request;
		$this->layout='/layouts/column3';

		//AccessLog::add();
		$model=new Advertisements;
		$model->rent_type=0;
		$modelUpload=new Uploader;

		if(Yii::app()->user->isGuest)
		{
			$model->setScenario('userIsGuest');
		}
		elseif (!$request->isPostRequest)
		{
			$model->owner_phone = $this->user->profile->phone;
			$model->owner_name = $this->user->profile->firstname;
		}

		if(isset($_POST['Advertisements']))
		{
			$model->attributes=$_POST['Advertisements'];
			$modelUpload->attributes=$_POST['Advertisements'];
			$model->status=Advertisements::ADV_NEW;

			// Save Adv
			if($model->save())
			{
				// Assign adv to user
				if(!Yii::app()->user->isGuest)
					$model->owner_id=Yii::app()->user->id;
				$request=new CHttpRequest();
				$model->owner_ip=$request->getUserHostAddress();
				$model->save();

				// Upload images
				if(!empty($_FILES['Uploader']['name']['files']))
				{

					$files=CUploadedFile::getInstances($modelUpload, "files");
					Yii::import('application.extensions.upload.upload');

					foreach($files as $eachFile)
					{
						if(!$eachFile->hasError && Common::isImage($eachFile))
						{
							// Save image if uploaded
							$dir=Yii::getPathOfAlias('application.uploads');
							$dirThumb=Yii::getPathOfAlias('application.uploads.thumbs');
							$filename=sha1(date('YmdHis').rand(1000, 9999));

							$uploadedFile=array(
								'name'=>$eachFile->getName(),
								'type'=>$eachFile->getType(),
								'tmp_name'=>$eachFile->getTempName(),
								'error'=>$eachFile->getError(),
								'size'=>$eachFile->getSize(),
							);
							$Upload=new upload($uploadedFile);

							if($Upload->processed)
							{
								// Thumb
								$Upload->file_new_name_body=$filename;
								$Upload->image_resize=true;
								$Upload->image_x=300;
								$Upload->image_ratio_y=true;
								$Upload->process($dirThumb);
								// Big
								$Upload->file_new_name_body=$filename;
								$Upload->image_resize=true;
								$Upload->image_x=500;
								$Upload->image_ratio_y=true;
								$Upload->process($dir);
								$Upload->clean();

								// Insert to files
								$fileModel=new Files();
								$fileModel->name=$filename;
								$fileModel->extension=mb_strtolower($eachFile->getExtensionName());
								$fileModel->mime_type=$eachFile->getType();
								$fileModel->size=$eachFile->getSize();
								$fileModel->path=$dir.'/';
								if($fileModel->save())
								{
									// Assign image to adv
									$advFileModel=new AdvertisementFiles();
									$advFileModel->advertisement_id=$model->id;
									$advFileModel->file_id=$fileModel->id;
									$advFileModel->save();
								}
							}
							else
							{
								throw new CHttpException(500, 'Upload file process error');
							}
						}
					}
				}
				$url=Yii::app()->user->isGuest ? 'catalog/index' : 'ads/index';
				$this->redirect($this->createUrl($url, array('added'=>'success')));
			}
		}

		$this->render('create', array(
			'model'=>$model,
			'modelUpload'=>$modelUpload,
		));
	}
}