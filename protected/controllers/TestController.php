<?php

class TestController extends Controller
{
	public function actionIndex()
	{
		$postData=array(
			'currency_id'  =>'1',
			'direction'    =>'',
			'field_106[]'  =>'149',
			'field_115[]'  =>'1180',
			'field_169'    =>'0',
			'field_169-1'  =>'24',
			'field_169_max'=>'24',
			'field_169_min'=>'0',
			'field_172'    =>'0',
			'field_172-1'  =>'300',
			'field_172_max'=>'300',
			'field_172_min'=>'0',
			'latitude_max' =>'',
			'latitude_min' =>'',
			'longitude_max'=>'',
			'longitude_min'=>'',
			'order'        =>'',
			'page'         =>'0',
			'price_max'    =>'12054600',
			'price_min'    =>'0',
			'selector'     =>'select',
			'text'         =>'',
		);
		$url     ='http://makler.md/ru/an/list/search/publisher/1/category/107/page/0';

		$viewData=array(
			'postData'=>CJSON::encode($postData),
			'url'     =>$url,
		);
		$this->render('index', $viewData);
	}

	public function actionEcho()
	{
		$files=Files::model()->count();
		var_dump('$files', $files);
	}

	public function actionUploadFile()
	{
		// Save image if uploaded
		$dir          =Yii::getPathOfAlias('application.uploads');
		$dirThumb     =Yii::getPathOfAlias('application.uploads.thumbs');
		$filename     =sha1(date('YmdHis').rand(1000, 9999));
		$eachImageUrl ='http://kotvkadre.ru/upload/photos/585x585/6543075ab92289f7bf07fa60e776e638bd66ebb3.jpg';
		$ext          =pathinfo($eachImageUrl, PATHINFO_EXTENSION);
		$destFile     =$dir.'/'.$filename.'.'.$ext;
		$destThumbFile=$dirThumb.'/'.$filename.'.'.$ext;

		$eachImageUrl='http://kotvkadre.ru/upload/photos/585x585/6543075ab92289f7bf07fa60e776e638bd66ebb3.jpg';

		if(file_exists($destFile) || copy($eachImageUrl, $destFile))
		{
			Yii::import('application.extensions.image.Image');

			$image=new Image($destFile);
			$image->resize(300, 0)->quality(100)->save($destThumbFile);
			$image->resize(500, 0)->quality(100)->save();

			$mimeType='image/jpeg';
			if($ext=='png')
				$mimeType='image/png';
			$fileSize=filesize($destFile);

			// Insert to files
			$fileModel           =new Files();
			$fileModel->name     =$filename;
			$fileModel->extension=$ext;
			$fileModel->mime_type=$mimeType;
			$fileModel->size     =$fileSize;
			$fileModel->path     =$dir.'/';
			if($fileModel->save())
			{
				// Assign image to adv
				$advFileModel=new AdvertisementFiles();
//				$advFileModel->advertisement_id = $model->id;
				$advFileModel->file_id=$fileModel->id;
				$advFileModel->save();
			}
		}
	}

	public function actionParseItemByPageUrl()
	{
		header('Content-Type: text/html; charset=utf-8');

		$pageUrl     ='http://makler.md/ru/chisinau/real-estate/apartments/an/477031';
		$pageUrl     ='http://makler.md/ru/chisinau/real-estate/apartments/an/482251';
		$maklerParser=new MaklerParser();
		$parsedItems =$maklerParser->parseItemByPageUrl($pageUrl);

		echo '$parsedItems[]<pre>';
		print_r($parsedItems);
		echo '</pre>';
	}

	public function actionParser()
	{
		header('Content-Type: text/html; charset=utf-8');

		/** @var Parser $parser */
		$parser=Parser::model()->find(array('condition'=>'targetSiteId = '.Parser::PARSER_ID_MAKLER));
		if(!$parser)
			throw new CException('Parser not found');

		$maklerParser=new MaklerParser();
		$maklerParser->parseItemsUrls($parser->startPageUrl);

		echo '$maklerParser->pageUrls[]<pre>';
		print_r($maklerParser->pageUrls);
		echo '</pre>';
	}

	public function actionTestRequest()
	{
		/**
		 * currency_id	1
		direction
		field_106[]	149
		field_115[]	1180
		field_169	0
		field_169-1	24
		field_169_max	24
		field_169_min	0
		field_172	0
		field_172-1	300
		field_172_max	300
		field_172_min	0
		latitude_max
		latitude_min
		longitude_max
		longitude_min
		order
		page	0
		price_max	12054600
		price_min	0
		selector	select
		text
		 */
		$postData=array(
			'currency_id'=>'1',
			'direction'=>'',
			'field_106[]'=>'149',
			'field_115[]'=>'1180',
			'field_169'=>'0',
			'field_169-1'=>'24',
			'field_169_max'=>'24',
			'field_169_min'=>'0',
			'field_172'=>'0',
			'field_172-1'=>'300',
			'field_172_max'=>'300',
			'field_172_min'=>'0',
			'latitude_max'=>'',
			'latitude_min'=>'',
			'longitude_max'=>'',
			'longitude_min'=>'',
			'order'=>'',
			'page'=>'0',
			'price_max'=>'12054600',
			'price_min'=>'0',
			'selector'=>'select',
			'text'=>'',
		);

		$httpHeader=array(
//			"Accept: text/html, */*; q=0.01",
//			"Accept-Encoding: gzip, deflate",
//			"Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3",
//			"Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3",
//			"Cookie: __utma=37299260.1793552229.1361172640.1371737395.1375427305.3; __utmz=37299260.1361172640.1.1.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=%D0%B2%D1%8B%D0%B7%D0%B2%D0%B0%D1%82%D1%8C%20%D1%8D%D0%BB%D0%B5%D0%BA%D1%82%D1%80%D0%B8%D0%BA%D0%B0%20%D0%BA%D0%B8%D1%88%D0%B8%D0%BD%D0%B5%D0%B2; OAID=0bcd558c4d3a5bd0138739226bce4150; __atuvc=3%7C25; MaklerMd=t3845g08k2e6kvj57gusks6ha5; __utmb=37299260.11.10.1375427305; __utmc=37299260; _ym_visorc=b; currentLang=ru",
//			"Host: makler.md",
//			"Referer: http://makler.md/chisinau/real-estate/apartments",
//			"User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:22.0) Gecko/20100101 Firefox/22.0",
			"X-Requested-With: XMLHttpRequest",
		);

		$uagent = "Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:22.0) Gecko/20100101 Firefox/22.0";
		$url='http://makler.md/ru/an/list/search/publisher/1/category/107/page/0';
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeader);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_ENCODING, "");
		curl_setopt($ch, CURLOPT_USERAGENT, $uagent);
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		curl_setopt($ch, CURLOPT_FAILONERROR, 0);
		curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
//		curl_setopt($ch, CURLOPT_COOKIE, '__utma=37299260.1793552229.1361172640.1371737395.1375427305.3; __utmz=37299260.1361172640.1.1.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=%D0%B2%D1%8B%D0%B7%D0%B2%D0%B0%D1%82%D1%8C%20%D1%8D%D0%BB%D0%B5%D0%BA%D1%82%D1%80%D0%B8%D0%BA%D0%B0%20%D0%BA%D0%B8%D1%88%D0%B8%D0%BD%D0%B5%D0%B2; OAID=0bcd558c4d3a5bd0138739226bce4150; __atuvc=3%7C25; MaklerMd=t3845g08k2e6kvj57gusks6ha5; __utmb=37299260.11.10.1375427305; __utmc=37299260; _ym_visorc=b; currentLang=ru');
//		curl_setopt($ch, CURLOPT_COOKIEJAR, "z://coo.txt");
//		curl_setopt($ch, CURLOPT_COOKIEFILE,"z://coo.txt");

		$content = curl_exec( $ch );
		$err = curl_errno( $ch );
		$errmsg = curl_error( $ch );
		$header = curl_getinfo( $ch );
		curl_close( $ch );


		echo "$errmsg";

		echo "\n\n$content";
		var_dump('$content', $content);

		exit;

		$curl = curl_init();
		// http://makler.md/ru/an/list/search/publisher/1/category/107/page/0
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_POST, 1);
//		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($postData));
		$res = curl_exec($curl);
		if(!$res){
			$error = curl_error($curl).'('.curl_errno($curl).')';
			echo $error;
		}
		else{
			echo $res;
		}
		curl_close($curl);
	}

	public function actionTestResponse()
	{
		echo '$_SERVER[]<pre>';
		print_r($_SERVER);
		echo '</pre>';

		echo '$_POST[]<pre>';
		print_r($_POST);
		echo '</pre>';
	}

	public function actionTimezone()
	{

//		date_default_timezone_set('Etc/GMT-9');
		echo date_default_timezone_get();
	}

	public function actionTime()
	{

		echo 'date_default_timezone_get: ' . date_default_timezone_get();
		echo '<br>';
//        date_default_timezone_set('Europe/Chisinau');
		echo '2013-07-15 00:00:00 ' . strtotime(date('2013-07-15 00:00:00'));
		echo '<br>';
		echo 'yesterday, 00:00 ' . strtotime(date('Y-m-d 00:00:00', strtotime('- 1 days')));
		echo '<br>';
		echo 'today, 00:00 ' . strtotime(date('Y-m-d 00:00:00'));
		echo '<br>';
		echo '10:00: ' . strtotime(date('Y-m-d 08:00:00'));
		echo '<br>';
		echo 'now: ' . time() . ', ' . date('Y-m-d H:i:s');
		echo '<br>';

        $ts = Yii::app()->getDb()->createCommand('SELECT CURRENT_TIMESTAMP()')->queryScalar();
		echo 'mysql timestamp: ' . $ts;

//		echo file_get_contents('http://mail.ru/');
	}

	public function actionPhpinfo()
	{
		phpinfo();
	}

	public function actionSearcher()
	{
		$model=AdvSearcher::model()->findByAttributes(array('id'=>16));
		$values=unserialize($model->values);
		echo '1[]<pre>';
		print_r($values);
		echo '</pre>';


//		$model=AdvSearcher::model()->findByAttributes(array('id'=>11));
//		$values=unserialize($model->values);
//		echo '2[]<pre>';
//		print_r($values);
//		echo '</pre>';
	}

	public function actionUpdateAdvSearcherUsers()
	{
		$models=AdvSearcher::model()->findAll('email_address = "" AND rel_user_id > 0');
		/*var_dump(count($models));
		exit;*/

		foreach($models as $each)
		{
			/**
			 * @var $each AdvSearcher
			 */
			$each->email_address=$each->user->email;
			$each->save(false);
		}
	}

	public function actionCreateColumn()
	{
		Yii::app()
			->getDb()
			->createCommand()
			->addColumn(
				"adv_searcher",
				"email_address",
				"varchar(50) not null"
			);
	}

    public function actionOpenPage()
    {
        $url='http://makler.md/ru/an/list/search/publisher/1/category/107/page/0';
//        $url='http://makler.md/ru/chisinau/real-estate/apartments/an/253888';
//        $url='http://makler.md/ru/chisinau/real-estate/apartments/an/253111';
        /*try {
            if(file_get_contents($url))
                echo '=1';
            else
                echo '=2';
        } catch(Exception $e) {
            echo 'Exception: ' . $e->getMessage();
        }*/

//        echo self::curlExec($url);
        echo MaklerParser::getPageHtml($url);
    }

    public static function curlExec($url, $data=array())
    {
        $ch=curl_init($url);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        //curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeader);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        //curl_setopt($ch, CURLOPT_USERAGENT, $uagent);
        curl_setopt($ch, CURLOPT_TIMEOUT, 120);
        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
//        curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

        $content=curl_exec($ch);

//        $statusCode=curl_getinfo($ch, CURLINFO_HTTP_CODE);
//        echo $statusCode;
//        exit;

        $err=curl_errno($ch);
        curl_close($ch);
        var_dump($err);

        return 'sddsd'.$content.'sds';
    }

	public function actionTestPageUrl()
	{
		$url='http://999.md/list/real-estate/apartments-and-rooms/1?applied=1&o_32_7=35&o_33_1=912&_pjax=%23js-pjax-container';
		$parser=new Parser999();
		$parser->parseItemsUrls($url);

		echo '$parser->pageUrls[]<pre>';
		print_r($parser->pageUrls);
		echo '</pre>';
	}

	public function actionParseByPageUrl()
	{
		$url='http://999.md/10335560';
		$parser=new Parser999();
		$parser->parseItemByPageUrl($url);
	}

	public function actionOldImagesCount()
	{
		header('Content-Type: text/html; charset=utf-8');
		var_dump(Files::model()->count());
	}
}