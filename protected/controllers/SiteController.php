<?php

class SiteController extends Controller
{
	public $layout='//layouts/column2';

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),

			'sitemapxml'=>array(
				'class'=>'ext.sitemap.ESitemapXMLAction',
				'classConfig'=>array(
					array('baseModel'=>'Advertisements',
						'route'=>'/catalog/view',
						'params'=>array('id'=>'translitId')
					),
				),
				'importListMethod'=>'getBaseSitePageList',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		//AccessLog::add();
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$headers="From: {$model->email}\r\nReply-To: {$model->email}";
				mail(Yii::app()->params['ownerEmail'], $model->subject, $model->body, $headers);
				//Thank you for contacting us. We will respond to you as soon as possible.
				Yii::app()->user->setFlash('contact', 'Спасибо за письмо. Мы скоро ответим Вам!');
				$this->refresh();
			}
		}
		$this->render('contact', array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login', array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	/**
	 *
	 */
	public function actionTestPage()
	{
//		$this->layout = 'column_test';
		$this->render("test");
	}

	/**
	 * @throws CHttpException
	 * @param $key
	 * @return void
	 */
	public function actionUnsubscribe($key)
	{
		if(empty($key))
			throw new CHttpException(404, 'Ключ не найден.');

		$criteria=new CDbCriteria();
		$criteria->condition="MD5(SHA1(CONCAT('ffss043nm7fb45',email,'77hhfdvvdaahhs'))) = MD5('{$key}')";
		$user=SpamUsers::model()->find($criteria);

		if($user===null)
			throw new CHttpException(404, 'E-mail не найден');

		$message='';
		$hideForm=false;

		if(Yii::app()->request->isPostRequest)
		{
			$hideForm=true;
			if(!$user->is_unsubscribed)
			{
				$user->is_unsubscribed=1;
				$user->unsubscription_date=time();
				$user->save(false);
				$message='Вы отписаны от рассылки';
			}
			else
				$message='Вы уже отписаны от рассылки';
		}
		elseif($user->is_unsubscribed)
		{
			$hideForm=true;
			$message='Вы уже отписаны от рассылки';
		}

		$viewData=array('user'=>$user, 'message'=>$message, 'hideForm'=>$hideForm);
		$this->render('unsubscribe', $viewData);
	}

	/**
	 * Provide the static site pages which are not database generated
	 *
	 * Each array element represents a page and should be an array of
	 * 'loc', 'frequency' and 'priority' keys
	 *
	 * @return array[]
	 */
	public function getBaseSitePageList()
	{

		$list=array(
			array(
				'loc'=>Yii::app()->createAbsoluteUrl('/'),
				'frequency'=>'daily',
				'priority'=>'1',
			),
			array(
				'loc'=>Yii::app()->createAbsoluteUrl('/catalog/today'),
				'frequency'=>'daily',
				'priority'=>'0.8',
			),
			array(
				'loc'=>'http://rooms.md/catalog/sdaiu/kolicestvo-komnat/vse-goroda/vse-raiony',
				'frequency'=>'daily',
				'priority'=>'0.8',
			),
			array(
				'loc'=>'http://rooms.md/catalog/snimu/kolicestvo-komnat/vse-goroda/vse-raiony',
				'frequency'=>'weekly',
				'priority'=>'0.8',
			),
			array(
				'loc'=>'http://rooms.md/catalog/prodaiu/kolicestvo-komnat/vse-goroda/vse-raiony',
				'frequency'=>'weekly',
				'priority'=>'0.8',
			),
			array(
				'loc'=>'http://rooms.md/catalog/cupliu/kolicestvo-komnat/vse-goroda/vse-raiony',
				'frequency'=>'weekly',
				'priority'=>'0.8',
			),
			array(
				'loc'=>Yii::app()->createAbsoluteUrl('/site/contact'),
				'frequency'=>'yearly',
				'priority'=>'0.6',
			),
		);
		return $list;
	}
}