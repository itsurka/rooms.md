<?php

/**
 * PaymentController.php
 *
 * @author Igor Tsurka <turcaigor@gmail.com>
 * @date 08.09.14
 */
class PaymentController extends Controller
{
	/*
		Всё начинается здесь. Заводим в базе запись с новым выставленным счетом,
		и передаем компоненту его ID, сумму, краткое описание и опционально
		e-mail пользователя. Можно не выносить эти данные в отдельную модель,
		а использовать атрибуты оформленного пользователем заказа
		(для интернет-магазинов).
	*/
	public function actionIndex()
	{
		// todo проверка чтобы юзер был авторизован тут, и чтобы у него был емайл

		$request=Yii::app()->request;
		$invoice=new Invoice;
		$invoice->amount=Invoice::MIN_AMOUNT;
		$invoice->description='Внесение средств на личный счет.';

		if($request->isPostRequest && $request->getPost('Invoice'))
		{
			// Выставляем счет
			$invoice->attributes=$_POST['Invoice'];
			$invoice->user_id=$this->user->id;

			if($invoice->save())
			{
				$invoice->refresh();

				// Компонент переадресует пользователя в свой интерфейс оплаты
				/** @var Robokassa $RC */
				$RC=Yii::app()->robokassa;
				$RC->pay(
					$invoice->amount,
					$invoice->id,
					$invoice->description,
					$this->user->email
				);
			}
		}

		$this->render('index', array(
			'model'=>$invoice
		));
	}

	/*
		К этому методу обращается робокасса после завершения интерактива
		с пользователем. Это может произойти мгновенно либо в течение нескольких
		минут. Здесь следует отметить счет как оплаченный либо обработать
		отказ от оплаты.
	*/
	public function actionResult()
	{
		/** @var Robokassa $rc */
		$rc=Yii::app()->robokassa;

		// Коллбэк для события "оплата произведена"
		$rc->onSuccess=function ($event)
		{
			$transaction=Yii::app()->db->beginTransaction();
			// Отмечаем время оплаты счета
			$InvId=Yii::app()->request->getParam('InvId');
			/** @var Invoice $invoice */
			$invoice=Invoice::model()->findByPk($InvId);
			$invoice->paid_at=new CDbExpression('NOW()');
			if(!$invoice->save())
			{
				$transaction->rollback();
				throw new CException("Unable to mark Invoice #$InvId as paid.\n"
					.CJSON::encode($invoice->getErrors()));
			}
			$transaction->commit();

			if($invoice->user)
			{
				$invoice->user->addBalance($invoice->amount);
			}
		};

		// Коллбэк для события "отказ от оплаты"
		$rc->onFail=function ($event)
		{
			// Например, удаляем счет из базы
			$InvId=Yii::app()->request->getParam('InvId');
			Invoice::model()->findByPk($InvId)->delete();
		};

		// Обработка ответа робокассы
		$rc->result();
	}

	/*
		Сюда из робокассы редиректится пользователь
		в случае отказа от оплаты счета.
	*/
	public function actionFailure()
	{
		Yii::app()->user->setFlash('global_danger', 'Отказ от оплаты. Если вы столкнулись
            с трудностями при внесении средств на счет, свяжитесь
            с нашей технической поддержкой.');

		$this->redirect(['/user/profile/edit']);
	}

	/*
		Сюда из робокассы редиректится пользователь в случае успешного проведения
		платежа. Обратите внимание, что на этот момент робокасса возможно еще
		не обратилась к методу actionResult() и нам неизвестно, поступили средства
		на счет или нет.
	*/
	public function actionSuccess()
	{
		$InvId=Yii::app()->request->getParam('InvId');
		/** @var Invoice $invoice */
		$invoice=Invoice::model()->findByPk($InvId);
		if($invoice)
		{
			if($invoice->paid_at)
			{
				// Если робокасса уже сообщила ранее, что платеж успешно принят
				Yii::app()->user->setFlash('global_success',
					'Средства зачислены на ваш личный счет. Спасибо.');
			}
			else
			{
				// Если робокасса еще не отзвонилась
				Yii::app()->user->setFlash('global_success', 'Ваш платеж принят. Средства
                    будут зачислены на ваш личный счет в течение нескольких минут.
                    Спасибо.');
			}
		}

		$this->redirect(['/user/profile/edit']);
	}
}