<?php

class CatalogController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column3';
    
    public $defaultAction='index';

	/**
	 * @var AdvSearcher
	 */
	public $advSearcherModel;


    /**
     * @var int num of ads on page
     **/
    public $itemsOnPage=15;


	protected function beforeAction($action, $options=array())
    {
	    if(!parent::beforeAction($action, $options=array()))
            return false;

		Common::searchFormAutoSetParams();

		return true;
	}

	public function init()
	{
		parent::init();

		$_showAdvSubscriptionForm=false;
		$showAdvSubscriptionForm=Yii::app()->user->getState('showAdvSubscriptionForm');
		if(is_null($showAdvSubscriptionForm))
			$showAdvSubscriptionForm=true;

		if(($showAdvSubscriptionForm && !is_null($this->user) && $this->user->advSearchersCount==0)
				|| ($showAdvSubscriptionForm && is_null($this->user))
				|| (!is_null($this->user) && $this->user->advSearchersCount==0))
		{
			$_showAdvSubscriptionForm=true;
		}

		if($_showAdvSubscriptionForm)
			$this->advSearcherModel=new AdvSearcher('guestUser');
	}

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
			array('allow',  // allow autenticated users to perform 'index' actions
				  'actions'=>array('mySearch'),
				  'users'=>array('@'),
			),
            array('allow',  // allow all users to perform 'index' and 'view' actions
                    'actions'=>array('index','rooms','cityRegions','view','period'),
                    'users'=>array('*'),
            ),
			array('deny',  // deny all users
				  'users'=>array('*'),
			),
        );
    }

    public function behaviors()
    {
        return array();
    }

    public function actionToday()
    {
        $advModel=new Advertisements();
		$criteria=$advModel->getDbCriteria();
        $advModel->scopeIsPublished()->scopeAddedToday();
        $criteria->order='is_colored DESC, added DESC';

        $pagination=array(
            'pageSize'=>$this->itemsOnPage,
        );
        $dataProvider=$advModel->retrieve($criteria, $pagination);

        // Assign data
        $arrayVeiwData=array(
            'dataProvider' => $dataProvider,
        );

        // Render
        $this->render('index', $arrayVeiwData);
    }

	public function actionPeriod($period)
	{
		switch($period)
		{
			case 'week':
				$timeFrom=strtotime('- 7 days');
				$this->pageTitle='Все квартиры за последнюю неделю';
				break;
			case 'month':
				$timeFrom=strtotime('- '.cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y')).' day');
				$this->pageTitle='Все квартиры за последний месяц';
				break;
			default:
				throw new CHttpException(404, 'The requested page does not exist.');
		}

		$advModel=new Advertisements();
		$criteria=$advModel->scopeIsPublished()->scopeAddedFrom($timeFrom)->getDbCriteria();
		$criteria->order='t.is_colored DESC, t.added DESC';

		$pagination=array(
			'pageSize'=>$this->itemsOnPage,
		);
		$dataProvider=$advModel->retrieve($criteria, $pagination);

		// Assign data
		$arrayViewData=array(
			'dataProvider'=>$dataProvider,
		);

		// Render
		$this->render('index', $arrayViewData);
	}

    /*public function actionIndex()
    {
        AccessLog::add();
        Yii::app()->getModule('user')->isFirstCame();

        $advModel=new Advertisements();
		$criteria=new CDbCriteria();
        $criteria->order='is_colored DESC, added DESC';
		$criteria->addInCondition('status', array('ok'));

        // Apply search form params - Start {
        if ($_city=Common::searchFormGetParam('city')) {
            $criteria->addInCondition('city_id', array($_city));
        }
        if ($_region=Common::searchFormGetParam('region')) {
            $criteria->addInCondition('region_id', array($_region));
        }
        if ($_num_rooms=Common::searchFormGetParam('num_rooms')) {
            $criteria->addInCondition('number_of_rooms', array($_num_rooms));
        }
        if ($_sort=Common::searchFormGetParam('sort')) {
            if ($_sort == 'date') {
                $criteria->order="is_colored DESC, added DESC";
            }
            if ($_sort == 'price') {
                $criteria->order="is_colored DESC, mdl_price ASC";
                $criteria->addNotInCondition('price', array(0));
            }
        }
        if ($_rent_type=Common::searchFormGetParam('rent_type')) {
            $criteria->addInCondition('rent_type', array($_rent_type));
        }
        // Apply search form params - End }

        $pagination=array(
            'pageSize'=>$this->itemsOnPage,
        );
        $dataProvider=$advModel->retrieve($criteria, $pagination);

        // Assign data
        $arrayVeiwData=array(
            'dataProvider' => $dataProvider,
        );

        // Render
        $this->render('index', $arrayVeiwData);
    }*/

    /**
     * @param string $rentType
     * @param string $numOfRooms
     * @param string $city
     * @param string $region
     * @return void
     */
    public function actionIndex($rentType='', $numOfRooms='', $city='', $region='')
    {
        $advModel=new Advertisements();
		$advModel->scopeIsPublished()->scopeDefaultCatalogOrder();

        // Apply uri params - Start {
		if (trim($rentType)!='' && !Advertisements::getIsDefaultValueSearchParam('rentType', trim($rentType)))
			$advModel->scopeApplySearchRentType($rentType);

		if (trim($numOfRooms)!='' && !Advertisements::getIsDefaultValueSearchParam('numOfRooms', trim($numOfRooms)))
			$advModel->scopeApplySearchNumOfRooms($numOfRooms);

		if (trim($city)!='' && !Advertisements::getIsDefaultValueSearchParam('city', trim($city)))
		{
			$_reqion='';
			if(trim($region)!='' && !Advertisements::getIsDefaultValueSearchParam('region', trim($region)))
				$_reqion=$region;
			$advModel->scopeApplySearchCityAndRegionsNames($city,$_reqion);
		}

		/*if (trim($region)!='' && !Advertisements::getIsDefaultValueSearchParam('region', trim($region)))
			$advModel->scopeApplySearchCityRegionName($region);*/
        // Apply uri params - End }

        $pagination=array(
            'pageSize'=>$this->itemsOnPage,
        );
        $dataProvider=$advModel->retrieve(null, $pagination);

        // Сохраняем в кэш критерию
        /*$catalogCriteriaCache=Yii::app()->cache->get('catalogCriteria');
        if($catalogCriteriaCache===false)
            Yii::app()->cache->set('catalogCriteria',$advModel->getDbCriteria());*/

        $criteria = $advModel->getDbCriteria();
        $criteria->order='t.id desc';
        Yii::app()->cache->set('catalogCriteria',$advModel->getDbCriteria());

        // Assign data
        $arrayVeiwData=array(
            'dataProvider' => $dataProvider,
        );

        if(!Yii::app()->request->getIsAjaxRequest())
        {
        	$this->render('index', $arrayVeiwData);
        }
		else
        {
			$this->renderPartial('index', $arrayVeiwData);
        }
    }

    /**
     * Displays selected item
     *
     * @param $id
     * @return void
     */
    public function actionView($id)
    {
		$this->layout='/layouts/column3';

        //AccessLog::add();
        $id=Common::getAdvsIDFromTranslitID($id);

        // Retrieve item
		/** @var Advertisements $dataProvider */
		$dataProvider=$this->loadModel($id);

		// Make view
		$AdvertisementsModel=new Advertisements();

		$session=Yii::app()->session;
		$sessKey='view_adv_'.$id;
		if(is_null($session->get($sessKey)))
		{
			$dataProvider->uniqueViews+=1;
			$dataProvider->views+=rand(1, 5);
			$dataProvider->update(array('views', 'uniqueViews'));
			$session->add($sessKey, 1);
		}

		/*$AdvertisementsModel->makeView($dataProvider->id, rand(1, 5));*/

        // Get same advs - Start {
        $limitItems=7;
        $criteria=new CDbCriteria();
        $criteria->addInCondition('t.region_id', array($dataProvider->region_id));
        $criteria->addInCondition('t.number_of_rooms', array($dataProvider->number_of_rooms));
        $criteria->addInCondition('t.status', Advertisements::$allowPublicStatuses);
        $criteria->addNotInCondition('t.id', array($dataProvider->id));
        $criteria->order='t.added DESC';
        $criteria->group='t.id';
        $criteria->limit=7;

        $criteria->with['files'] = array();
        $criteria->together = true;

        $sameAdvs=new CActiveDataProvider(
            $AdvertisementsModel->tableName(),
            array(
                'criteria'=>$criteria,
                'pagination' => array('pageSize' => $limitItems),
            )
        );
        if ($sameAdvs->totalItemCount==0)
        {
            $criteria=new CDbCriteria();
            $criteria->addInCondition('city_id', array($dataProvider->city_id));
            $criteria->addInCondition('status', Advertisements::$allowPublicStatuses);
            $criteria->order='added DESC';
            $criteria->limit=$limitItems;
            $sameAdvs=new CActiveDataProvider(
                $AdvertisementsModel->tableName(), 
                array(
                    'criteria'=>$criteria,
                    'pagination' => array('pageSize' => $limitItems),
                )
            );
        }
        // Get same advs - End }

        // Assign data
        $arrayVeiwData=array(
            'data' => $dataProvider,
            'sameAdvs'=>$sameAdvs
        );

		// Set meta data - Start {
		Meta::init(
            array(
                "title"=>$dataProvider->title,
                "text"=>$dataProvider->text,
                "number_of_rooms"=>$dataProvider->number_of_rooms,
                "region"=>$dataProvider->region->name,
                'pageID'=>'catalog_view',
                'model'=>$dataProvider
            )
        );
        // Set meta data - End }

		// Render
        $this->render('view', $arrayVeiwData);
    }


    /**
     * @throws CHttpException
     * @param $id
     * @return CActiveRecord
     */
	public function loadModel($id)
	{
		$model=Advertisements::model()->findByPk($id);

		// Error if model didn't exist or model doesn't belong to user.
		if($model===null)
			throw new CHttpException(404, 'The requested page does not exist.');
		
		return $model;
	}

    /**
     * Set selected region_id
     *
     * @return void
     */
	public function actionSetCityRegion()
	{
		Common::setCityRegion(@$_POST['id']);
	}


	/**
	 * Set number of rooms
	 * @return void
	 */
	public function actionSetNumberOfRooms()
	{
		Common::setNumberOfRooms(@$_POST['id']);
	}


	/**
	 * Set sort by
	 * @return void
	 */
	public function actionSetSortBy()
	{
		Common::setSortBy(@$_POST['id']);
	}

    /**
     * Районы города
     *
     * @throws CHttpException
     * @param $cityID
     * @return array
     */
    public function actionCityRegions($cityID)
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $return=array();
            $model=Cities::model()->findByPk(intval($cityID));

            if($model!==null && count($model->regions))
            {
                foreach($model->regions as $eachRegion)
                    $return[]=$eachRegion->attributes;
            }
            echo CJSON::encode($return);
            Yii::app()->end();
        }
        else
            throw new CHttpException(404, 'Invalid request. Please do not repeat this request again.');
    }

	/**
	 * @return void
	 */
	public function actionMySearch()
	{
		// Get user adv searcher
		$AdvSearcher=AdvSearcher::model()->findByAttributes(array('rel_user_id'=>Yii::app()->user->id));
        $AdvSearcherIsNull=false;
		if($AdvSearcher===null)
        {
            $AdvSearcherIsNull=true;
			$AdvSearcher=new AdvSearcher();
        }

		$request=Yii::app()->getRequest();
        // сохраняем модель
		if($request->isPostRequest)
		{
			$userData=$request->getPost('User');
			$advSearcherMailing=$userData['advSearcherMailing'];
			$this->user->advSearcherMailing=intval($advSearcherMailing);
			$this->user->save(false);

			$searchersCount=count($request->getParam('rent_type'));
			$columnsCriterias=array();

			$rentTypes=$request->getParam('rent_type');
			$cities=$request->getParam('city_id');
			$regions=$request->getParam('region_id');
			$numRooms=$request->getParam('number_of_rooms');
			for($i=0; $i<$searchersCount; $i++)
			{
				// Проверяем если поисковик пустой(не настроен)
				$rentTypeIsEmpty=$rentTypes[$i]=='' || $rentTypes[$i]==0 ? true : false;
				$cityIsEmpty=$cities[$i]=='' || $cities[$i]==0 ? true : false;
				$regionIsEmpty=$regions[$i]=='' || $regions[$i]==0 ? true : false;
				$numRoomsIsEmpty=$numRooms[$i]=='' || $numRooms[$i]==0 ? true : false;
				if($rentTypeIsEmpty && $cityIsEmpty && $regionIsEmpty && $numRoomsIsEmpty)
					continue;

				// Берем значения поисковика
				$newColumnsCriteria=array();
				if(is_numeric($rentTypes[$i]) && $rentTypes[$i]>0)
					$newColumnsCriteria['rent_type']=intval($rentTypes[$i]);
				if(is_numeric($cities[$i]) && $cities[$i]>0)
					$newColumnsCriteria['city_id']=intval($cities[$i]);
				if(is_numeric($regions[$i]) && $regions[$i]>0)
					$newColumnsCriteria['region_id']=intval($regions[$i]);
				if(is_numeric($numRooms[$i]) && $numRooms[$i]>0)
					$newColumnsCriteria['number_of_rooms']=intval($numRooms[$i]);

				if(count($newColumnsCriteria)==0)
					continue;

				$criteriaAllreadyExists=false;
				foreach($columnsCriterias as $eachColumnsCriteria)
				{
					if ($eachColumnsCriteria===$newColumnsCriteria)
					{
						$criteriaAllreadyExists=true;
						break;
					}
				}

				if ($criteriaAllreadyExists)
					continue;

				$columnsCriterias[]=$newColumnsCriteria;
			}

			if (count($columnsCriterias)>0)
			{
				$criteria=new CDbCriteria();
				$subCriteria=new CDbCriteria();
				$criteria->addInCondition('status', Advertisements::$allowPublicStatuses);

				foreach($columnsCriterias as $key=>$eachColumnsCriteria)
					$subCriteria->addColumnCondition($eachColumnsCriteria,'AND','OR');

                $criteria->mergeWith($subCriteria);

				$AdvSearcher->rel_user_id=$this->user->id;
				$AdvSearcher->email_address=$this->user->email;
				$values=new stdClass();
				$values->criteria=$criteria;
				$values->columnsCriterias=$columnsCriterias;
				$AdvSearcher->values=serialize($values);
				$AdvSearcher->created=time();
				$AdvSearcher->save(false);
			}
            elseif (!$AdvSearcher->getIsNewRecord())
            {
                $AdvSearcher->delete();
            }

            $this->redirect($this->createUrl('catalog/mySearch'));
		}

		$searchers=array();
		if(!$AdvSearcherIsNull)
		{
			$values=unserialize($AdvSearcher->values);
			$searchers=$values->columnsCriterias;
			$searcherCriteria=$values->criteria;
			/*echo '$searchers[]<pre>';
			print_r($searchers);
			echo '</pre>';*/
			/*echo 'criteria[]<pre>';
			print_r($values->criteria);
			echo '</pre>';*/
		}

        // вытаскиваем об-ия
        $advModel=new Advertisements();
        $pagination=array('pageSize'=>50);
        $dataProvider=$advModel->retrieve(isset($searcherCriteria) ? $searcherCriteria : null, $pagination);
        if ($AdvSearcherIsNull)
            $dataProvider->setData(array());

		$arrayVeiwData=array(
			'searchers'=>$searchers,
			'searchersCount'=>count($searchers),
			'hasSearchers'=>count($searchers)>0,
			'hasSearcher'=>!$AdvSearcherIsNull,
			'dataProvider'=>$dataProvider,
			'userModel'=>$this->user,
		);

		$this->render('mySearch', $arrayVeiwData);
	}
}
