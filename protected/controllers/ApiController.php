<?php

class ApiController extends Controller
{
	public $layout='//layouts/column1';

	public function actions()
	{
		return array();
	}

	public function response($return=array())
	{
		echo CJSON::encode($return);
	}

	public function afterAction($action)
	{
		parent::afterAction($action);
		Yii::app()->end();
	}

	public function actionSetUtc()
	{
        $return=array('success'=>false, 'message'=>'');
        $request=Yii::app()->request;
        $UTC=$request->getParam('UTC');
        if($UTC!==null)
        {
            // TODO Validate $UTC
            $return['success']=true;
            Common::setUTC($UTC);
        }

        $this->response($return);
	}

	/**
	 * @return void
	 */
	public function actionGetCatalogSearchUrl()
	{
		$return=array('success'=>true, 'message'=>'', 'url'=>'');
		
		$allParams=array();
		
		// Get selected rent types
		$allParams['rentType']=!empty($_REQUEST['RentTypes']) ? join(',', $_REQUEST['RentTypes']) : Advertisements::$searchParams['rentType']['defaultValue'];

		// Get selected num. of rooms
		$allParams['numOfRooms']=!empty($_REQUEST['NumOfRooms']) ? join(',', $_REQUEST['NumOfRooms']) : Advertisements::$searchParams['numOfRooms']['defaultValue'];

		// Get selected cities
		$allParams['city']=!empty($_REQUEST['Cities']) ? join(',', $_REQUEST['Cities']) : Advertisements::$searchParams['city']['defaultValue'];

		// Get selected city regions
		$allParams['region']=!empty($_REQUEST['Regions']) ? join(',', $_REQUEST['Regions']) : Advertisements::$searchParams['region']['defaultValue'];

		$return['url']=urldecode(Yii::app()->createUrl('catalog/index', Advertisements::getCatalogSearchUrl($allParams)));

		$this->response($return);
	}
}