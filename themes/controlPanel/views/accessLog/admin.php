<?php
/* @var $this AccessLogController */
/* @var $model AccessLog */
/* @var $logStatistic array */

$this->breadcrumbs=array(
	'Access Logs'=>array('index'),
	'Manage',
);
?>
<h1>Статистика за 2 недели</h1>

<table>
	<thead>
	<tr>
		<th>Дата</th>
		<th>Кол-во</th>
		<th>IP</th>
		<th>Клиент</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($logStatistic as $eachRow): ?>
		<tr>
			<td><?php echo $eachRow['date']; ?></td>
			<td><?php echo $eachRow['hits']; ?></td>
			<td><?php echo $eachRow['user_ip']; ?></td>
			<td><?php echo $eachRow['user_agent']; ?></td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>

<h1>Логи</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'access-log-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'access_log_id',
		'user_ip',
		'user_agent',
		'request_uri',
		'timestamp',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
