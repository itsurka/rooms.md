<?php
$this->breadcrumbs=array(
	'Spam Users'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SpamUsers', 'url'=>array('index')),
	array('label'=>'Create SpamUsers', 'url'=>array('create')),
	array('label'=>'View SpamUsers', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SpamUsers', 'url'=>array('admin')),
);
?>

<h1>Update SpamUsers <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>