<?php
$this->breadcrumbs=array(
	'Spam Users',
);

$this->menu=array(
	array('label'=>'Create SpamUsers', 'url'=>array('create')),
	array('label'=>'Manage SpamUsers', 'url'=>array('admin')),
);
?>

<h1>Spam Users</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
