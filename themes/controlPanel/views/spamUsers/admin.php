<?php
$this->breadcrumbs=array(
	'Spam Users'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List SpamUsers', 'url'=>array('index')),
	array('label'=>'Create SpamUsers', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('spam-users-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Spam Users</h1>
<?/*
<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
*/?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'spam-users-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'fullname',
		'email',
		array(
			'name' => 'is_unsubscribed',
			'type'=>'raw',
			'value'=>'SpamUsers::$boolValues[$data->is_unsubscribed]',
			'filter'=>SpamUsers::$boolValues,
		),
		array(
			'name' => 'unsubscription_date',
			'type'=>'raw',
			'value'=>'$data->unsubscription_date ? Yii::app()->dateFormatter->format("d MMMM, H:mm", $data->unsubscription_date) : ""',
		),
		array(
			'name' => 'is_reg_on_roomsmd',
			'type'=>'raw',
			'value'=>'SpamUsers::$boolValues[$data->is_reg_on_roomsmd]',
            'filter'=>SpamUsers::$boolValues,
		),
		array(
			'name' => 'is_reg_on_kotvkadreru',
			'type'=>'raw',
			'value'=>'SpamUsers::$boolValues[$data->is_reg_on_kotvkadreru]',
            'filter'=>SpamUsers::$boolValues,
		),
		array(
			'class'=>'CButtonColumn',
		),
	),
    'pager'=>array(
        'header'=>'',
    )
)); ?>
