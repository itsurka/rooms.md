<?php
$this->breadcrumbs=array(
	'Spam Users'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List SpamUsers', 'url'=>array('index')),
	array('label'=>'Create SpamUsers', 'url'=>array('create')),
	array('label'=>'Update SpamUsers', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete SpamUsers', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SpamUsers', 'url'=>array('admin')),
);
?>

<h1>View SpamUsers #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'email',
		'fullname',
		'is_reg_on_roomsmd',
		'is_reg_on_kotvkadreru',
		'is_unsubscribed',
	),
));