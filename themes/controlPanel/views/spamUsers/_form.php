<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'spam-users-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fullname'); ?>
		<?php echo $form->textField($model,'fullname',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'fullname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'is_reg_on_roomsmd'); ?>
		<?php echo CHtml::activeDropDownList($model,'is_reg_on_roomsmd', SpamUsers::$boolValues, array('style'=>'width: 100px;')); ?>
		<?php echo $form->error($model,'is_reg_on_roomsmd'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'is_reg_on_kotvkadreru'); ?>
		<?php echo CHtml::activeDropDownList($model,'is_reg_on_kotvkadreru', SpamUsers::$boolValues, array('style'=>'width: 100px;')); ?>
		<?php echo $form->error($model,'is_reg_on_kotvkadreru'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'is_unsubscribed'); ?>
		<?php echo CHtml::activeDropDownList($model,'is_unsubscribed', SpamUsers::$boolValues, array('style'=>'width: 100px;')); ?>
		<?php echo $form->error($model,'is_unsubscribed'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->