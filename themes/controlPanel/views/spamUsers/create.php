<?php
$this->breadcrumbs=array(
	'Spam Users'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SpamUsers', 'url'=>array('index')),
	array('label'=>'Manage SpamUsers', 'url'=>array('admin')),
);
?>

<h1>Create SpamUsers</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>