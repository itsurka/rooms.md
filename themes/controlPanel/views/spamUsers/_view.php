<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fullname')); ?>:</b>
	<?php echo CHtml::encode($data->fullname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_reg_on_roomsmd')); ?>:</b>
	<?php echo CHtml::encode($data->is_reg_on_roomsmd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_reg_on_kotvkadreru')); ?>:</b>
	<?php echo CHtml::encode($data->is_reg_on_kotvkadreru); ?>
	<br />


</div>