<?php
$eachVipAdv = $data->adv;
?>

<section class="widget vip-adv">

        <div class="vip-adv-inner">
            <?php if(count($eachVipAdv->files)>0): ?>
                <?php
                $imageUrl=Yii::app()->baseUrl.'/'.Yii::app()->params['ADV_THUMBS_URL'].$eachVipAdv->files[0]->name.'.'.$eachVipAdv->files[0]->extension;
                ?>
                <div class="vip-adv-image">
                    <a href="<?php echo Yii::app()->createUrl('catalog/view', array('id'=>Translit::encodestring($eachVipAdv->title.'-'.$eachVipAdv->id))); ?>">
                        <?php echo CHtml::image($imageUrl,'',array('style'=>'width: 170px;')); ?>
                    </a>
                </div>
            <?php endif; ?>

            <div class="vip-adv-info">
                <a href="<?php echo Yii::app()->createUrl('catalog/view', array('id'=>Translit::encodestring($eachVipAdv->title.'-'.$eachVipAdv->id))); ?>">
                    <?php echo $eachVipAdv->city->name; ?>, <?php echo $eachVipAdv->region->name; ?>, <?php echo Common::getAppNumRoomsStr_Enhanced($eachVipAdv->number_of_rooms, 'NUM_ROOMS-комн.'); ?><?php if($eachVipAdv->price>0): ?>, <?php echo $eachVipAdv->price; ?> <?php echo $eachVipAdv->currency->htmlValue; ?><?php endif; ?>
                </a>
            </div>
        </div>

</section>
