<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admin.css" />

    <!--JavaScript-->
    <?php
    $script = 'var HOST = "'.Yii::app()->request->hostInfo.'";'."\n";
    $script .= 'var SCRIPT_URL = "'.Yii::app()->request->requestUri.'";'."\n";
    Yii::app()->clientScript->registerScript('global_vars', $script, CClientScript::POS_BEGIN);
    ?>
    <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery.lightbox_me.js', CClientScript::POS_END); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/admin.js', CClientScript::POS_END); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/common.js', CClientScript::POS_END); ?>
    <!--/JavaScript-->

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">
	<div id="header">
		<div id="logo" style="font-size: 14px;">
            <?php echo CHtml::link(Yii::t('app', 'Control panel'), Yii::app()->getModule('controlPanel')->mainUrl); ?>
            <?php echo CHtml::link(Yii::t('app', 'Website'), Yii::app()->homeUrl); ?>
        </div>
	</div><!-- header -->
	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
                array('label'=>Yii::t('app', 'Users'), 'url'=>Yii::app()->getModule('user')->indexUrl),
                array('label'=>'Подписчики', 'url'=>Yii::app()->getModule('user')->advSubscribersUrl),
                array('label'=>Yii::t('app', 'Advertisements'), 'url'=>array('/controlPanel/advertisements/index')),
                array('label'=>Yii::t('app', 'VIP Объвления'), 'url'=>array('/controlPanel/advertisements/vip')),
                array('label'=>'Рассылка', 'url'=>array('/controlPanel/EmailsQueue/admin')),
                array('label'=>'Спам адреса', 'url'=>array('/controlPanel/SpamUsers/admin')),
                array('label'=>'Парсер', 'url'=>array('/controlPanel/parser/admin')),
                array('label'=>'Просмотры', 'url'=>array('/controlPanel/accessLog/admin')),
            ),
		)); ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
            'homeLink'=>CHtml::link(Yii::t('app', 'Main'), Yii::app()->getModule('controlPanel')->mainUrl),
			'links'=>$this->breadcrumbs,
    )); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by <?=Yii::app()->params->companyName?>.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
