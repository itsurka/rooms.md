<?php
$this->breadcrumbs=array(
	'Parsers',
);

$this->menu=array(
	array('label'=>'Create Parser', 'url'=>array('create')),
	array('label'=>'Manage Parser', 'url'=>array('admin')),
);
?>

<h1>Parsers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
