<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('targetSiteId')); ?>:</b>
	<?php echo CHtml::encode($data->targetSiteId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('startPageUrl')); ?>:</b>
	<?php echo CHtml::encode($data->startPageUrl); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parsePagesCount')); ?>:</b>
	<?php echo CHtml::encode($data->parsePagesCount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parsedPages')); ?>:</b>
	<?php echo CHtml::encode($data->parsedPages); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('completedDate')); ?>:</b>
	<?php echo CHtml::encode($data->completedDate); ?>
	<br />

	*/ ?>

</div>