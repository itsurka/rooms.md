<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'parser-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'targetSiteId'); ?>
		<?php echo $form->textField($model,'targetSiteId'); ?>
		<?php echo $form->error($model,'targetSiteId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'startPageUrl'); ?>
		<?php echo $form->textField($model,'startPageUrl',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'startPageUrl'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'parsePagesCount'); ?>
		<?php echo $form->textField($model,'parsePagesCount'); ?>
		<?php echo $form->error($model,'parsePagesCount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'parsedPages'); ?>
		<?php echo $form->textField($model,'parsedPages'); ?>
		<?php echo $form->error($model,'parsedPages'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created'); ?>
		<?php echo $form->textField($model,'created'); ?>
		<?php echo $form->error($model,'created'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'completedDate'); ?>
		<?php echo $form->textField($model,'completedDate'); ?>
		<?php echo $form->error($model,'completedDate'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->