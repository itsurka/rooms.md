<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'targetSiteId'); ?>
		<?php echo $form->textField($model,'targetSiteId'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'startPageUrl'); ?>
		<?php echo $form->textField($model,'startPageUrl',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'parsePagesCount'); ?>
		<?php echo $form->textField($model,'parsePagesCount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'parsedPages'); ?>
		<?php echo $form->textField($model,'parsedPages'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created'); ?>
		<?php echo $form->textField($model,'created'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'completedDate'); ?>
		<?php echo $form->textField($model,'completedDate'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->