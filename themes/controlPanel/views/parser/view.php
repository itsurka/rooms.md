<?php
$this->breadcrumbs=array(
	'Parsers'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Parser', 'url'=>array('index')),
	array('label'=>'Create Parser', 'url'=>array('create')),
	array('label'=>'Update Parser', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Parser', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Parser', 'url'=>array('admin')),
);
?>

<h1>View Parser #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'targetSiteId',
		'startPageUrl',
		'parsePagesCount',
		'parsedPages',
		'status',
		'created',
		'completedDate',
	),
)); ?>
