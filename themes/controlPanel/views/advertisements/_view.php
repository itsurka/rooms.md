<div class="view">
	
    <div class="row">
        <strong><?=CHtml::encode($data->getAttributeLabel('title')); ?>:</strong>
        <?=CHtml::link(CHtml::encode($data->title), $this->createUrl('catalog/view', array('id'=>$data->id)))?>
    </div>

    <div class="row">
        <strong><?=CHtml::encode($data->getAttributeLabel('text')); ?>:</strong>
        <?=CHtml::link(CHtml::encode($data->text), $this->createUrl('catalog/view', array('id'=>$data->id)))?>
    </div>

    <?php if($data->price>0): ?>
        <div class="row">
            <strong><?=CHtml::encode($data->getAttributeLabel('price')); ?>:</strong>
            <?=$data->price?>
        </div>
    <?php endif; ?>
    
    <?php if(count($data->files)>0): ?>
        <div class="row">
            <?=CHtml::link(Chtml::image(Yii::app()->params['ADV_THUMBS_URL'].$data->files[0]->name.'.'.$data->files[0]->extension, $data->id, array('width'=>'120')), $this->createUrl('catalog/view', array('id'=>$data->id))); ?>
        </div>
    <?php endif; ?>

    <div class="row">
        <strong>Добавил:</strong>
        <?=$data->owner->profile->firstname?> <?=$data->owner->profile->lastname?>
    </div><br />

</div>