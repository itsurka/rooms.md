<?php
$this->breadcrumbs=array(
	Yii::t('app', 'My advertisements')
);
?>

<?php echo CHtml::ajaxSubmitButton(
	'Удалить все новые объявления',
	Yii::app()->createUrl('/controlPanel/advertisements/index', array('command'=>'deleteAllNewAdvs')),
	array('complete'=>'js:function(){ $("#advertisements-grid").yiiGridView("update"); }')
); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'advertisements-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'ajaxUpdate'=>true,
	'columns'=>array(
		array(
			'name' => 'id',
			'type'=>'raw',
			'value' => '$data->id',
		),
		array(
			'name' => 'title',
			'type'=>'raw',
			'value' => '(($data->filesCount) ? "<b>".$data->filesCount." фото</b> " : "").Common::cutStringToWholeLastWord($data->title, 100, "...")',
		),
		array(
			'name' => 'text',
			'type'=>'raw',
			'value' => 'Common::cutStringToWholeLastWord($data->text, 100, "...")."<br>".$data->owner_ip',
		),
		array(
			'name' =>'owner_id',
			'type'=>'raw',
			'value'=>'$data->owner ? $data->owner->username : ""',
            'filter'=>CHtml::listData(User::model()->active()->findAll(), 'id', 'username'),
		),
		array(
			'name' => 'added',
			'type'=>'raw',
			'value' => 'Yii::app()->dateFormatter->format("d MMMM, H:mm", $data->added)',
		),
		array(
			'name' => 'status',
			'type'=>'raw',
			'value'=>'Advertisements::statusAlias($data->status)',
            'filter'=>Advertisements::$statuses,
		),
		array(
			'name' => 'is_parsed',
			'type'=>'raw',
			'value'=>'$data->is_parsed',
            'filter'=>Advertisements::$boolOptions,
		),
		array(
			'name' => 'parsed_from_url',
			'type'=>'raw',
			'value'=>'$data->parsed_from_url',
		),
		array(
			'class'=>'CButtonColumn',
            'template'=>'{view}{update}{delete}{activate}{block}{make_vip}{make_unvip}{make_colored}{make_uncolored}',
            'buttons'=>array(
                'view'=>array(
                    'label'=>'Просмотреть',
                    'url'=>'$this->grid->controller->createUrl("advertisements/view", array("id"=>$data->id))',
					'imageUrl'=>Yii::app()->request->baseUrl.'/images/all_icons/coquette/24x24/search_page.png',
                    'options'=>array(
                        'ajax'=>array(
                            'type'=>'POST',
                            'url'=>"js:$(this).attr('href')",
                            'success'=>'js:function(id){ showAdv(id); }',
                        ),
                    ),
                ),
                'update'=>array(
                    'label'=>'Редактировать',
					'imageUrl'=>Yii::app()->request->baseUrl.'/images/all_icons/coquette/24x24/edit.png',
                ),
                'delete'=>array(
                    'label'=>'Удалить',
					'imageUrl'=>Yii::app()->request->baseUrl.'/images/all_icons/coquette/24x24/delete.png',
                ),
                'activate'=>array(
                    'label'=>'Активировать',
					'imageUrl'=>Yii::app()->request->baseUrl.'/images/all_icons/coquette/24x24/accept.png',
                    'url'=>'$this->grid->controller->createUrl("changeAdvStatus", array("id"=>$data->id, "status"=>Advertisements::ADV_OK))',
                    'visible'=>'$data->status === Advertisements::ADV_OK ? false : true',
                    'options'=>array(
                        'ajax'=>array(
                            'type'=>'POST',
                            'url'=>"js:$(this).attr('href')",
                            'success'=>'js:function(id){$.fn.yiiGridView.update(\'advertisements-grid\');}',
                        ),
                    ),
                ),
                'block'=>array(
                    'label'=>'Заблокировать',
					'imageUrl'=>Yii::app()->request->baseUrl.'/images/all_icons/coquette/24x24/block.png',
                    'url'=>'array("changeAdvStatus", "id"=>"$data->id", "status"=>"'.(Advertisements::ADV_BLOCKED).'")',
                    'visible'=>'$data->status === Advertisements::ADV_BLOCKED ? false : true',
                    'options'=>array(
                        'ajax'=>array(
                            'type'=>'POST',
                            'url'=>"js:$(this).attr('href')",
                            'success'=>'js:function(id){$.fn.yiiGridView.update(\'advertisements-grid\');}',
                        ),
                    ),
                ),
                'make_vip'=>array(
                    'label'=>'Сделать VIP',
					'imageUrl'=>CIcon::init('money_add')->getImgUrl(),
                    'url'=>'array("makeVIP", "id"=>"$data->id")',
                    'visible'=>'$data->status===Advertisements::ADV_OK && !$data->vip',
                    'options'=>array(
                        'ajax'=>array(
                            'type'=>'POST',
                            'url'=>"js:$(this).attr('href')",
                            'success'=>'js:function(id){$.fn.yiiGridView.update(\'advertisements-grid\');}',
                        ),
                    ),
                ),
                'make_unvip'=>array(
                    'label'=>'Убрать VIP',
					'imageUrl'=>CIcon::init('money_delete')->getImgUrl(),
                    'url'=>'array("makeVIP", "id"=>"$data->id")',
                    'visible'=>'$data->vip',
                    'options'=>array(
                        'ajax'=>array(
                            'type'=>'POST',
                            'url'=>"js:$(this).attr('href')",
                            'success'=>'js:function(id){$.fn.yiiGridView.update(\'advertisements-grid\');}',
                        ),
                    ),
                ),
                'make_colored'=>array(
                    'label'=>'Выделить цветом',
					'imageUrl'=>Yii::app()->request->baseUrl.'/images/all_icons/coquette/24x24/light_bulb.png',
                    'url'=>'array("toggleColor", "id"=>"$data->id")',
                    'visible'=>'!$data->is_colored ? true : false',
                    'options'=>array(
                        'ajax'=>array(
                            'type'=>'POST',
                            'url'=>"js:$(this).attr('href')",
                            'success'=>'js:function(id){$.fn.yiiGridView.update(\'advertisements-grid\');}',
                        ),
                    ),
                ),
                'make_uncolored'=>array(
                    'label'=>'Убрать выделение цветом',
					'imageUrl'=>Yii::app()->request->baseUrl.'/images/all_icons/coquette/24x24/light_bulb_disabled.png',
                    'url'=>'array("toggleColor", "id"=>"$data->id")',
                    'visible'=>'$data->is_colored ? true : false',
                    'options'=>array(
                        'ajax'=>array(
                            'type'=>'POST',
                            'url'=>"js:$(this).attr('href')",
                            'success'=>'js:function(id){$.fn.yiiGridView.update(\'advertisements-grid\');}',
                        ),
                    ),
                ),
            ),
		),
	)
));
?>