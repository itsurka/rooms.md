<?php
$this->breadcrumbs=array(
	'Мои объявления'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List', 'url'=>array('index')),
	array('label'=>'Create', 'url'=>array('create')),
	array('label'=>'Delete', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<!--<h1>View Articles #<?php /*echo $model->id; */?></h1>-->

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
		'text',
		array(
			'name' => 'added',
			'type'=>'raw',
			'value' => Yii::app()->dateFormatter->format("d MMMM, H:mm", $model->added),
		),
        array(
            'name' => 'price',
            'type'=>'raw',
            'value' => $model->price.' '.$model->currency->name,
        ),
		'number_of_rooms',
        array(
            'name' => 'owner_phone',
            'type'=>'raw',
            'value' => $model->getOwnerPhoneNumber(),
        ),
        array(
            'name' => 'owner_name',
            'type'=>'raw',
            'value' => $model->getOwnerName(),
        ),
        array(
            'name' => 'owner_id',
            'type'=>'raw',
            'value' => $model->owner_id ? $model->owner_id.'(Имя: '.$model->owner->profile->getFullName().', логин: '.$model->owner->username.')' : 'Аноним',
        ),
        array(
            'name' => 'region_id',
            'type'=>'raw',
            'value' => $model->region->name,
        ),
        array(
            'name' => 'city_id',
            'type'=>'raw',
            'value' => $model->city->name,
        ),
	),
)); ?>
