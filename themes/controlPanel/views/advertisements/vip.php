<?php
$this->breadcrumbs=array(
	Yii::t('app', 'VIP advertisements')
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('advvip-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>VIP Объявления</h1>

<?php
$controller=Yii::app()->controller;
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'advvip-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'ajaxUpdate'=>true,
	'columns'=>array(
		array(
			'name' => 'adv.id',
			'type'=>'raw',
			'value' => '$data->adv->id',
		),
		array(
			'name' => 'grid_cell_vip_adv',
			'type'=>'raw',
			'value' => function($data, $row) use ($controller){ return $controller->renderPartial('/blocks/_vipAdvView', array('data'=>$data), true); },
		),
        array(
            'name' => 'adv.title',
			'type'=>'raw',
			'value' => '$data->adv->title',
		),
		array(
			'name' => 'adv.text',
			'type'=>'raw',
			'value' => 'Common::cutStringToWholeLastWord($data->adv->text, 100, "...")',
		),
		array(
			'name' =>'adv.owner_id',
			'type'=>'raw',
			'value'=>'$data->adv->owner ? $data->adv->owner->username : ""',
            'filter'=>CHtml::listData(User::model()->active()->findAll(), 'id', 'username'),
		),
		array(
			'name' => 'created',
			'type'=>'raw',
			'value' => 'Yii::app()->dateFormatter->format("d MMMM, H:mm", $data->created)',
		),
		/*array(
			'name' => 'status',
			'type'=>'raw',
			'value'=>'VipAdv::$statuses[$data->status]',
            'filter'=>VipAdv::$statuses,
		),*/
		array(
			'class'=>'CButtonColumn',
            'template'=>'{make_unvip}{moveVipAdvUp}{moveVipAdvDown}',
            'buttons'=>array(
                'make_unvip'=>array(
                    'label'=>'Убрать VIP',
					'imageUrl'=>CIcon::init('money_delete')->getImgUrl(),
                    'url'=>'array("makeVIP", "id"=>$data->adv->id)',
                    'options'=>array(
                        'ajax'=>array(
                            'type'=>'POST',
                            'url'=>"js:$(this).attr('href')",
                            'success'=>'js:function(id){$.fn.yiiGridView.update(\'advvip-grid\');}',
                        ),
                    ),
                ),
                'moveVipAdvUp'=>array(
                    'label'=>'Поднять вверх',
					'imageUrl'=>CIcon::init('arrow_up')->getImgUrl(),
                    'url'=>'array("moveVipAdv", "moveDirection"=>"up", "id"=>$data->adv->id)',
                    'options'=>array(
                        'ajax'=>array(
                            'type'=>'POST',
                            'url'=>"js:$(this).attr('href')",
                            'success'=>'js:function(id){$.fn.yiiGridView.update(\'advvip-grid\');}',
                        ),
                    ),
                ),
                'moveVipAdvDown'=>array(
                    'label'=>'Отпустить вниз',
					'imageUrl'=>CIcon::init('arrow_down')->getImgUrl(),
                    'url'=>'array("moveVipAdv", "moveDirection"=>"down", "id"=>$data->adv->id)',
                    'options'=>array(
                        'ajax'=>array(
                            'type'=>'POST',
                            'url'=>"js:$(this).attr('href')",
                            'success'=>'js:function(id){$.fn.yiiGridView.update(\'advvip-grid\');}',
                        ),
                    ),
                ),
            ),
		),
	)
));