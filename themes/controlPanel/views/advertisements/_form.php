<div class="adv_form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id'=>'advertisements-form',
        'enableAjaxValidation'=>false,
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),
    ));
    ?>

	<div class="alert note_msg">
		<?=Yii::t("app", 'Fields with * are required'); ?>
	</div>

	<?php echo $form->errorSummary($model, NULL, NULL, array('class'=>'alert error_msg')); ?>

	<section class="form_row">
		<div>
			<?php echo $form->labelEx($model, 'title'); ?>
			<div class="block_content">
				<?php echo Chtml::activeTextField($model,'title'); ?>
			</div>
		</div>
		<div class="clear"></div>
	</section>

	<section class="form_row">
		<div>
			<?php echo $form->labelEx($model, 'text'); ?>
			<div class="block_content">
				<?php echo Chtml::activeTextarea($model,'text'); ?>
			</div>
		</div>
		<div class="clear"></div>
	</section>

	<section class="form_row">
		<div>
			<?php echo $form->labelEx($model, 'number_of_rooms'); ?>
			<div class="block_content">
				<?php
                echo $form->radioButtonList(
                    $model,
                    'number_of_rooms',
                    array('1'=>'Одна', '2'=>'Две', '3'=>'Три'),
                    array('separator'=>' ', 'template'=>'{label} {input}')
                );
                ?>
			</div>
		</div>
		<div class="clear"></div>
	</section>

	<section class="form_row">
		<div>
			<?php echo $form->labelEx($model, 'price'); ?>
			<div class="block_content">
				<?php echo Chtml::activeTextField($model, 'price', array('style'=>'width: 100px;')); ?>
                <?php echo CHtml::activeDropDownList($model, 'currency_id', CHtml::listData(Currencies::model()->findAll(), 'currency_id', 'code'), array('style'=>'width: 100px;')); ?>
			</div>
		</div>
		<div class="clear"></div>
	</section>

	<section class="form_row">
		<div>
			<?php echo CHtml::label('Тип объявления', 'rent_type');?>
			<div class="block_content">
				<?=Chtml::activeDropDownList($model, 'rent_type', Advertisements::$rentTypes, array('class'=>'chzn-select-deselect', 'style'=>'width: 77px;', 'id'=>'rent_type'))?>
			</div>
		</div>
		<div class="clear"></div>
	</section>

	<section class="form_row">
		<div>
			<?php echo $form->labelEx($model, 'city_id'); ?>
			<div class="block_content">
				<?php echo Chtml::activeDropDownList($model,'city_id', CHtml::listData(Cities::model()->findAll(), 'id', 'name')); ?>
			</div>
		</div>
		<div class="clear"></div>
	</section>

	<section class="form_row">
		<div>
			<?php echo $form->labelEx($model, 'region_id'); ?>
			<div class="block_content">
				<?php echo Chtml::activeDropDownList($model,'region_id', CHtml::listData(CityRegions::model()->findAll(), 'id', 'name')); ?>
			</div>
		</div>
		<div class="clear"></div>
	</section>

	<section class="form_row">
        <div>
            <?php echo Chtml::label(Yii::t("app", "Image") . '<span class="add_image"></span>', "files[]"); ?>&nbsp;
            <div class="block_content">
                <div class="files">
                    <!--Рыба-->
                    <div class="item_example" style="display: none;">
                        <?php echo Chtml::activeFileField($modelUpload, 'files[]'); ?>
                        <span class="delete_image"></span>
                        <div class="clear"></div>
                    </div>
                    <!--/Рыба-->
                </div>
                <?php if( (!empty($model)) && (count($model->files)>0) ): ?>
                <div class="old_files">
                    <?php foreach($model->files as $key=>$eachFile): ?>
                    <div class="item">
                        <?php echo Chtml::image(Yii::app()->baseUrl . '/' . Yii::app()->params['ADV_THUMBS_URL'] . $eachFile->name . '.' . $eachFile->extension, $model->id, array('width'=>'120')); ?>
                        <?php echo Chtml::hiddenField('oldFiles[]', $eachFile->name); ?>
                        <div class="delete_image"></div>
                        <div class="clear"></div>
                    </div>
                    <?php endforeach; ?>
                    <div class="clear"></div>
                </div>
                <?php endif; ?>
            </div>
        </div>
		<div class="clear"></div>
	</section>

    <section class="form_row">
        <div>
            <?php echo $form->labelEx($model, 'owner_phone'); ?>
            <div class="block_content">
                <?php echo Chtml::activeTextField($model, 'owner_phone'); ?>
            </div>
        </div>
        <div class="clear"></div>
    </section>

    <section class="form_row">
        <div>
            <?php echo $form->labelEx($model, 'owner_name'); ?>
            <div class="block_content">
                <?php echo Chtml::activeTextField($model, 'owner_name'); ?>
            </div>
        </div>
        <div class="clear"></div>
    </section>

	<section class="form_row">
		<div>
            <?php echo Chtml::label(Yii::t('app', 'Author'), ''); ?>
			<div class="block_content">
				<?php echo Chtml::activeDropDownList($model, 'owner_id', CHtml::listData(User::model()->findAll(), 'id', 'username'), array('empty'=>Yii::t('app', 'Author'))); ?>
			</div>
		</div>
		<div class="clear"></div>
	</section>

	<section class="form_row">
		<div>
            <div class="left">
                <?php echo Chtml::label('Выделить цветом', '', array('style'=>'color: #F86D0D; width: 110px')); ?>
            </div>
            <div class="block_content left" style="margin-left: 5px;">
                <?php echo Chtml::activeCheckBox($model, 'is_colored', array('class'=>'', 'style'=>'margin-left: 5px;')); ?>
            </div>
            <div class="clear"></div>
            <span class="note"><i>(ваше объявление будт выделено цветом, что привлечет больше просмотров)</i></span>
		</div>
		<div class="clear"></div>
	</section>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array("class"=>"button blue large")); ?>
    </div>
    <?/*=CHtml::resetButton("Очистить", array("class"=>"button white"))*/?>
    <?php $this->endWidget(); ?>

</div>