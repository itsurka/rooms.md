<?php
$this->breadcrumbs=array(
	'Моя объявления'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List', 'url'=>array('index')),
	array('label'=>'View', 'url'=>array('view', 'id'=>$model->id)),
);
?>

<h1>Update Articles <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>