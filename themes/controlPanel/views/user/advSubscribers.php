<?php
$this->breadcrumbs=array(
	UserModule::t("Users"),
);
?>

<h1><?php echo UserModule::t("List User"); ?></h1>
<?php if(UserModule::isAdmin()) {
	?><ul class="actions">
	<li><?php echo CHtml::link(UserModule::t('Manage User'),array('/user/admin')); ?></li>
	<li><?php echo CHtml::link(UserModule::t('Manage Profile Field'),array('profileField/admin')); ?></li>
</ul><!-- actions --><?php 
} ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		'id',
		array(
			'name' => 'rel_user_id',
			'type'=>'raw',
			'value' => '$data->rel_user_id>0 ? $data->user->username : ""',
		),
		'email_address',
		'last_email_ntf_adv_id',
		array(
			'name' => 'created',
			'value' => 'date("d.m.Y H:i:s",$data->created)',
		),
	),
)); ?>
