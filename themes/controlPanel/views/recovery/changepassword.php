<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Change Password");
$this->breadcrumbs=array(
	UserModule::t("Login") => array('/user/login'),
	UserModule::t("Change Password"),
);
?>

<h1><?php echo UserModule::t("Change Password"); ?></h1>


<div class="form_place margin_top_0">
<?php echo CHtml::beginForm(); ?>

	<div class="alert info_msg"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></div>
	<?php echo Chtml::errorSummary($form, NULL, NULL, array('class'=>'alert error_msg')); ?>

	<section class="form_row">
		<div>
			<?php echo CHtml::activeLabelEx($form,'password'); ?>
			<div class="block_content">
				<?php echo CHtml::activePasswordField($model,'verifyPassword',array('class'=>'i-format', 'style'=>'min-width: 200px; width: 230px;')); ?>
				<p class="hint">
					<?php echo UserModule::t("Minimal password length 4 symbols."); ?>
				</p>
			</div>
		</div>
		<div class="clear"></div>
	</section>

	<section class="form_row">
		<div>
			<?php echo CHtml::activeLabelEx($form,'verifyPassword'); ?>
			<div class="block_content">
				<?php echo CHtml::activePasswordField($model,'verifyPassword',array('class'=>'i-format', 'style'=>'min-width: 200px; width: 230px;')); ?>
			</div>
		</div>
		<div class="clear"></div>
	</section>

	<section class="form_row">
		<div>
			<div class="block_content">
				<?php echo CHtml::submitButton(UserModule::t("Save"), array('class'=>'button darkgray')); ?>
			</div>
		</div>
		<div class="clear"></div>
	</section>

<?php echo CHtml::endForm(); ?>
</div><!-- form -->