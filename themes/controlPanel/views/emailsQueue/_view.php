<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('email_queue_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->email_queue_id), array('view', 'id'=>$data->email_queue_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subject')); ?>:</b>
	<?php echo CHtml::encode($data->subject); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('body')); ?>:</b>
	<?php echo CHtml::encode($data->body); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('recipientEmail')); ?>:</b>
	<?php echo CHtml::encode($data->recipientEmail); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified')); ?>:</b>
	<?php echo CHtml::encode($data->modified); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('send_since_date')); ?>:</b>
	<?php echo CHtml::encode($data->send_since_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sent_date')); ?>:</b>
	<?php echo CHtml::encode($data->sent_date); ?>
	<br />

	*/ ?>

</div>