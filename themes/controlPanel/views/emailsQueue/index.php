<?php
$this->breadcrumbs=array(
	'Emails Queues',
);

$this->menu=array(
	array('label'=>'Create EmailsQueue', 'url'=>array('create')),
	array('label'=>'Manage EmailsQueue', 'url'=>array('admin')),
);
?>

<h1>Emails Queues</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
