<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'emails-queue-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'subject'); ?>
		<?php echo $form->textField($model,'subject',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'subject'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'body'); ?>
		<?php echo $form->textArea($model,'body',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'body'); ?>
	</div>

	<!--<div class="row">
		<?php /*echo $form->labelEx($model,'status'); */?>
		<?php /*echo $form->textField($model,'status'); */?>
		<?php /*echo $form->error($model,'status'); */?>
	</div>-->

	<div class="row">
		<?php echo $form->labelEx($model,'recipientEmail'); ?>
		<?php echo $form->textField($model,'recipientEmail',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'recipientEmail'); ?>
	</div>

	<!--<div class="row">
		<?php /*echo $form->labelEx($model,'created'); */?>
		<?php /*echo $form->textField($model,'created'); */?>
		<?php /*echo $form->error($model,'created'); */?>
	</div>

	<div class="row">
		<?php /*echo $form->labelEx($model,'modified'); */?>
		<?php /*echo $form->textField($model,'modified'); */?>
		<?php /*echo $form->error($model,'modified'); */?>
	</div>

	<div class="row">
		<?php /*echo $form->labelEx($model,'send_since_date'); */?>
		<?php /*echo $form->textField($model,'send_since_date'); */?>
		<?php /*echo $form->error($model,'send_since_date'); */?>
	</div>

	<div class="row">
		<?php /*echo $form->labelEx($model,'sent_date'); */?>
		<?php /*echo $form->textField($model,'sent_date'); */?>
		<?php /*echo $form->error($model,'sent_date'); */?>
	</div>-->

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->