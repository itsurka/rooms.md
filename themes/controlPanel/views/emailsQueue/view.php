<?php
$this->breadcrumbs=array(
	'Emails Queues'=>array('index'),
	$model->email_queue_id,
);

$this->menu=array(
	array('label'=>'List EmailsQueue', 'url'=>array('index')),
	array('label'=>'Create EmailsQueue', 'url'=>array('create')),
	array('label'=>'Update EmailsQueue', 'url'=>array('update', 'id'=>$model->email_queue_id)),
	array('label'=>'Delete EmailsQueue', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->email_queue_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EmailsQueue', 'url'=>array('admin')),
);
?>

<h1>View EmailsQueue #<?php echo $model->email_queue_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'email_queue_id',
		'subject',
		'body',
		'status',
		'recipientEmail',
		'created',
		'modified',
		'send_since_date',
		'sent_date',
	),
)); ?>
