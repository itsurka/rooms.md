<?php
$this->breadcrumbs=array(
	'Emails Queues'=>array('index'),
	$model->email_queue_id=>array('view','id'=>$model->email_queue_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EmailsQueue', 'url'=>array('index')),
	array('label'=>'Create EmailsQueue', 'url'=>array('create')),
	array('label'=>'View EmailsQueue', 'url'=>array('view', 'id'=>$model->email_queue_id)),
	array('label'=>'Manage EmailsQueue', 'url'=>array('admin')),
);
?>

<h1>Update EmailsQueue <?php echo $model->email_queue_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>