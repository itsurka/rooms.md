<?php
$this->breadcrumbs=array(
	'Emails Queues'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List EmailsQueue', 'url'=>array('index')),
	array('label'=>'Create EmailsQueue', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('emails-queue-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Emails Queues <?php echo CHtml::link('Add', array('create')); ?></h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<br>
<br>
<b>Статистика за неделю</b>
<br>
<br>
<ul>
<?php
for($i=0; $i<7; $i++)
{
    if($i==0)
    {
        $day=date('Y-m-d');
        $fromDay=date('Y-m-d').' 00:00:00';
        $toDay=date('Y-m-d').' 23:59:59';
    }
    else
    {
        $day=date('Y-m-d', strtotime('- '.$i.' days'));
        $fromDay=date('Y-m-d', strtotime("- {$i} days")).' 00:00:00';
        $toDay=date('Y-m-d', strtotime("- {$i} days")).' 23:59:59';
    }

    $fromDay=strtotime($fromDay);
    $toDay=strtotime($toDay);
//    var_dump($i, $fromDay, $toDay);
    $criteria=new CDbCriteria();
    $criteria->condition="sent_date >= '{$fromDay}' AND sent_date <= '{$toDay}' AND status=".EmailsQueue::EmailQueue_STATUS_SENT;
//    echo $criteria->condition;
    $emailsSent=EmailsQueue::model()->count($criteria);
    $criteria=new CDbCriteria();
    $criteria->condition="sent_date >= '{$fromDay}' AND sent_date <= '{$toDay}' AND status=".EmailsQueue::EmailQueue_STATUS_FAILED;
//    echo $criteria->condition;
    $emailsFailed=EmailsQueue::model()->count($criteria);
    ?>
    <li style="margin: 6px 0;">
        <?php echo Yii::app()->dateFormatter->format("d MMMM", $day); ?>, отправлено - <?php echo $emailsSent ?>, не отправлено - <?php echo $emailsFailed ?>.
    </li>
    <?
}
?>
</ul>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'emails-queue-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'email_queue_id',
        array(
            'name'=>'recipientEmail',
			'type'=>'raw',
			'value' => '$data->recipientEmail'
        ),
        array(
			'name' => 'status',
			'type'=>'raw',
			'value'=>'EmailsQueue::$statuses[$data->status]',
            'filter'=>EmailsQueue::$statuses,
        ),
        array(
			'name' => 'created',
			'type'=>'raw',
			'value' => 'Yii::app()->dateFormatter->format("d MMMM, H:mm", $data->created)',
        ),
        array(
			'name' => 'send_since_date',
			'type'=>'raw',
			'value' => 'Yii::app()->dateFormatter->format("d MMMM, H:mm", $data->send_since_date)',
        ),
        array(
			'name' => 'sent_date',
			'type'=>'raw',
			'value' => '$data->sent_date ? Yii::app()->dateFormatter->format("d MMMM, H:mm:ss", $data->sent_date) : ""',
        ),
		/*
		'modified',
		'send_since_date',
		'sent_date',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>