<?php
$this->breadcrumbs=array(
	'Emails Queues'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EmailsQueue', 'url'=>array('index')),
	array('label'=>'Manage EmailsQueue', 'url'=>array('admin')),
);
?>

<h1>Create EmailsQueue</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>