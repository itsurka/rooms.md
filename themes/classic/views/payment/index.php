<?php
/**
 * @var Controller $this
 * @var CActiveForm $form
 * @var Invoice $model
 */
?>

<div class="well bs-component panel">
	<?php
	$form=$this->beginWidget('CActiveForm', array(
		'id'=>'invoice-form',
		'htmlOptions'=>array(
			'class'=>'form-horizontal'
		),
	));
	?>
	<fieldset>
		<legend>
			Пополнение счёта
		</legend>

		<div class="form-group">
			<?php echo $form->labelEx($model, 'amount', array('class'=>'col-lg-4 control-label')); ?>
			<div class="col-lg-2">
				<?php echo CHtml::activeNumberField($model, 'amount', array('class'=>'form-control', 'style'=>'')); ?>
				<?php echo $form->error($model, 'amount'); ?>
				<span class="help-block"><b>Мин. сумма <?php echo Invoice::MIN_AMOUNT; ?> лей.</b></span>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model, 'description', array('class'=>'col-lg-4 control-label')); ?>
			<div class="col-lg-4">
				<?php echo CHtml::activeTextArea($model, 'description', array('class'=>'form-control', 'style'=>'')); ?>
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-10 col-lg-offset-4">
				<?php echo CHtml::submitButton('Перейти к оплате', array("class"=>"btn btn-primary", 'id'=>'form-submit', 'data-loading-text'=>'Подождите...')); ?>
			</div>
		</div>
	</fieldset>
	<?php $this->endWidget(); ?>
</div>