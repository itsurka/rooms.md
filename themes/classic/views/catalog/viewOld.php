<section id="blog" class="float-none">
	<article class="full_entry">
		<div>
			<div class="title">
				<h1 class="float-left" style="font-size: 1.2em; margin-top: 0;">
					<strong><?=CHtml::encode($data->title)?></strong>
                </h1>
				<div class="clear"></div>

                <?/*
				<div class="meta" style="margin-top: 0px; margin-left: 0px; padding-top: 0px; padding-bottom: 0px;">
                    <span class="float-left">
                        <em><?=Yii::t('app', 'City')?>:</em>&nbsp;<b><?=$data->city->name?></b>
                    </span>
                    <span class="float-left">
                        <em><?=Yii::t('app', 'Region')?>:</em>&nbsp;<b><?=$data->region->name?></b>
                    </span>
                    <span class="float-left">
                        <em>Комнаты:</em>&nbsp;<b><?=Common::getAppNumRoomsStr_Enhanced($data->number_of_rooms,'NUM_ROOMS-комн.')?></b>
                    </span>
                    <?php if($data->price > 0): ?>
                        <span class="float-left">
                            <em><?=Yii::t('app', 'Price')?>:</em>&nbsp;<b><?=$data->price?>&nbsp;<?php echo $data->currency->name; ?></b>
                        </span>
                    <?php endif; ?>
                    <?php if($data->uniqueViews>0): ?>
                        <span class="views float-right" style="margin-left: 8px;">
                            <?=$data->uniqueViews?>?>
                        </span>
                    <?php endif; ?>
                    <span class="date float-right"><?php echo Common::formatDateTime($data->added); ?></span>
					<div class="clear"></div>

				</div>
                */?>

<!--                <h2>-->
                    <div class="meta" style="margin-top: 0px; margin-left: 0px; padding-top: 0px; padding-bottom: 0px;">
                        <span class="float-left">
                            <b><?=$data->city->name?>, <?=$data->region->name?>, <?=Common::getAppNumRoomsStr_Enhanced($data->number_of_rooms,'NUM_ROOMS-комн. квартира')?><?php if($data->price > 0): ?>, <?=$data->price?>&nbsp;<?php echo $data->currency->name; ?><?php endif; ?></b>
                        </span>
                        <?php if($data->uniqueViews>0): ?>
                            <span class="views float-right" style="margin-left: 8px;">
                                <?=$data->uniqueViews?>
                            </span>
                        <?php endif; ?>
                        <span class="date float-right"><?php echo Common::formatDateTime($data->added); ?></span>
                        <div class="clear"></div>
                    </div>
<!--                </h2>-->


				<!--<div>
					<?/*=Yii::t('app', 'Region')*/?>:&nbsp;<em><?/*=$data->region->name*/?></em>&nbsp;&nbsp;
					<?/*=Yii::t('app', 'Number of rooms')*/?>:&nbsp;<em><?/*=$data->number_of_rooms*/?></em>
					<?php /*if($data->price > 0): */?>
						&nbsp;&nbsp;<?/*=Yii::t('app', 'Price')*/?>:&nbsp;<em><?/*=$data->price*/?>&nbsp;<?php /*echo $data->currency->name; */?></em>
					<?php /*endif;*/ ?>
				</div>-->

				<?php if(count($data->files) > 0): ?>
					<div style="margin-top: 5px;">
						<?php foreach($data->files as $Key=>$EachFile): ?>
							<?php
                            $thumbImg = Yii::app()->baseUrl.'/'.Yii::app()->params['ADV_THUMBS_URL'] . $EachFile->name . '.' . $EachFile->extension;
                            $bigImg = Yii::app()->baseUrl.'/'.Yii::app()->params['ADV_IMAGES_URL'] . $EachFile->name . '.' . $EachFile->extension;
                            echo CHtml::link(CHtml::image($thumbImg, $data->title, array('style'=>'max-width: 120px; max-height: 90px; margin-bottom: 5px;')), $bigImg, array("class"=>"img_preview_big"));
                            ?>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
                <h2 class="adv-text">
                    <?php echo nl2br(CHtml::encode($data->text)); ?>
                </h2>
                <div>
                    <span style="">Телефон:</span>&nbsp;
                    <span style="font-size: 1.2em;">
                        <strong><em>
                            <?php echo Common::getPhoneNumberFriendlyFormat($data->owner_phone); ?>
                        </em></strong>
                    </span>
                    <?php if(trim($data->owner_name)!=''): ?>
                        <span class="author" style="display: inline-block; margin: 0px 0px 0px 6px; padding: 0px 0px 0px 18px; position: relative; bottom: 1px;">
                            <?php echo $data->owner_name; ?>
                        </span>
                    <?php endif; ?>
                </div>
			</div>
		</div>
	</article>
</section>

<?php if(isset($sameAdvs) && $sameAdvs->getData()): ?>
    <section id="blog" class="float-none">
        <span style="font-weight: bold; padding-left: 3px; display: block; margin-bottom: 10px; font-size: 1.2em; margin-top: 56px;">
            <?php echo Yii::t('app', 'Same advertisements'); ?>
        </span>
        <?php $this->widget('zii.widgets.CListView', array(
                                                          'dataProvider'=>$sameAdvs,
                                                          'itemView'=>'_view',
                                                          'ajaxUpdate'=>false,
                                                          'summaryText'=>'Объявления с {start} - {end}',
                                                          'htmlOptions'=>array('class'=>''),
                                                          'template'=>'{items}',
                                                          'pagerCssClass'=>'pager float-right',
                                                     )); ?>
    </section>
<?php endif; ?>

<script type="text/javascript">

    $(document).ready(function() {

        if ($('a.img_preview').size() > 0) {
            $('a.img_preview').imgPreview({
                imgCSS: {
                    width: '200px'
                }
            });
        }

        if ($('a.img_preview_big').size() > 0) {
            $('a.img_preview_big').imgPreview({
                imgCSS: {
                    width: '270px'
                }
            });
        }

        $('.items .block').on('mouseover', function() {
            if(!$(this).hasClass('adv-is-colored'))
                $(this).addClass('adv-hover');
        });

        $('.items .block').on('mouseout', function() {
            if($(this).hasClass('adv-hover'))
                $(this).removeClass('adv-hover');
        });

    });
</script>