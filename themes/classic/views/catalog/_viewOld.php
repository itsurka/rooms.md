<?php
$currency = Common::searchFormGetParam('currency');
$hasImage = count($data->files)>0 ? true : false;
//$title = Common::cutString($data->title, 85);
$title = Common::cutStringToWholeLastWord($data->title, 65, '...');
$text = Common::cutString($data->text, $hasImage ? 176 : 226, ' ...');
$title = CHtml::encode($title);
$text = CHtml::encode($text);
?>
<article class="style4">

    <div class="block <?php if($data->is_colored && !isset($show_owner_actions)) echo 'adv-is-colored'; ?> <?php echo isset($show_owner_actions) ? "adv-status-{$data->status}" : ""; ?>" style="<?php if(isset($show_owner_actions)) echo 'padding: 10px;' ?>">
        <?php if(isset($show_owner_actions)): ?>
            <div class="float-right">
                <em style="color: #666666">
                    <?php echo Yii::app()->dateFormatter->format('d MMMM', $data->added); ?>
                </em>
            </div>
        <?php endif; ?>

        <?php if(count($data->files)>0): ?>
            <div class="add_preview float-left">
                <?php
                $icon = count($data->files)<2 ? 'image.png' : 'images.png';
                $image = Yii::app()->baseUrl.'/'.Yii::app()->params['ADV_THUMBS_URL'].$data->files[0]->name.'.'.$data->files[0]->extension;
                ?>
                <?=CHtml::link(Chtml::image(Yii::app()->baseUrl . '/' . Yii::app()->params['IMAGES_URL'] . $icon, '', array()), $image, array("class"=>"img_preview")); ?>
            </div>
        <?php endif; ?>
        <div class="add_title float-left" style="<?php if(isset($show_owner_actions)) echo 'padding: 0;'; ?>">
            <?php echo CHtml::link($title, array('catalog/view', 'id'=>Translit::encodestring($data->title.'-'.$data->id)), array()); ?>
        </div>
        <?php if(empty($show_owner_actions) && !$data->is_colored): ?>
            <div class="float-right adv-right-info">
                <div class="adv-info-date">
                    <em><?php echo Common::timestampToHowMuchTimeHasPassed($data->added); ?></em>
                </div>
                <div class="adv-info-price">
                    <strong>
                        <?php if(intval($data->mdl_price) > 0): ?>
                            <?php if( ($currency) && ($currency!=$data->currency_id) ){ ?>
                                <?php echo Currency::toAnotherCurrency($data->mdl_price, 'MDL', Common::searchFormGetParam('currency')); ?> <?php echo Currency::currencyIDToCode(Common::searchFormGetParam('currency')); ?>
                            <?php } else { ?>
                                <?php echo $data->price; ?> <?php echo $data->currency->htmlValue; ?>
                            <?php } ?>
                        <?php endif; ?>
                    </strong>
                </div>
            </div>
        <?php endif; ?>
        <?php ?>
        <div class="clear"></div>
        <?php if(isset($show_owner_actions)): ?>
        <div style="margin-top: 7px;">
            <?php $this->renderPartial('_owner_actions', array('data'=>$data)); ?>
        </div>
        <?php endif; ?>
    </div>

	<? /*

    <div class="row">
        <strong><?=CHtml::encode($data->getAttributeLabel('title')); ?>:</strong>
        <?=CHtml::link(CHtml::encode($data->title), $this->createUrl('catalog/view', array('id'=>$data->id)))?>
    </div>

    <div class="row">
        <strong><?=CHtml::encode($data->getAttributeLabel('text')); ?>:</strong>
        <?=CHtml::link(CHtml::encode($data->text), $this->createUrl('catalog/view', array('id'=>$data->id)))?>
    </div>

    <?php if($data->price>0): ?>
        <div class="row">
            <strong><?=CHtml::encode($data->getAttributeLabel('price')); ?>:</strong>
            <?=$data->price?>
        </div>
    <?php endif; ?>
    
	<?php if($data->owner): ?>
		<div class="row">
			<strong>Добавил:</strong>
			<?=$data->owner->profile->firstname?> <?=$data->owner->profile->lastname?>
		</div>
    <?php endif; ?>

    <div class="row">
        <strong><?=CHtml::encode($data->getAttributeLabel('uniqueViews')); ?>:</strong>
        <?=$data->uniqueViews?>
    </div>
	<br />
	 *
	 */?>

	<div class="clear"></div>
</article>