<?php
$CHttpSession = new CHttpSession();
$emptyText =  '<div class="fixed_alert info_msg">По Вашему запросу не найдено ни одного объявления</div>';
?>

<?php if(isset($_REQUEST['registration']) && $_REQUEST['registration']=='success'): ?>
    <?php echo $this->renderPartial('/blocks/_registrationSucces'); ?>
<?php endif; ?>

<?php /*if(isset($_REQUEST['added']) && $_REQUEST['added']=='success'): */?><!--
    <?php /*echo $this->renderPartial('/blocks/_advAddedSucces'); */?>
--><?php /*endif; */?>

<?php if($CHttpSession->get('isFirstCame')): ?>
    <?php $this->renderPartial("//blocks/_userIsFirstComeText"); ?>
    <?php
    $CHttpSession->add('isFirstCame', false);
    ?>
<?php endif; ?>

<?php //$this->renderPartial("//blocks/_searchBar"); ?>

<section id="blog" class="float-none">


	<?php $this->widget('zii.widgets.CListView', array(
		'dataProvider'=>$dataProvider,
		'itemView'=>'_view',
		'ajaxUpdate'=>false,
		'summaryText'=>'Объявления с {start} - {end}',
		'htmlOptions'=>array('class'=>'advs-list'),
		'template'=>'{items}{pager}',
		'pagerCssClass'=>'pager',
        'emptyText'=>$emptyText,
        'pager'=>array(
            'class'=>'application.components.LinkPager',
            'header'=>'',
            'firstPageLabel' => '',
            'prevPageLabel'=>'Пред.',
            'nextPageLabel'=>'След.',
            'lastPageLabel' => '',
            'id'=>'list_pagination',
        )
	)); ?>
</section>

<?php
// Init image preview - Start {
$script = "
if ($('a.img_preview').size() > 0) {
    $('a.img_preview').imgPreview({
        imgCSS: {
            width: '200px'
        }
    });
}"."\n";
Yii::app()->clientScript->registerScript('imgPreview_init', $script, CClientScript::POS_READY);
// Init image preview - End }
