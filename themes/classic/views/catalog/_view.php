<?php
/** @var $this Controller */
/** @var $data Advertisements */
$currency=Common::searchFormGetParam('currency');
$title=Common::cutStringToWholeLastWord($data->title, 40, '...');
/*if($data->is_colored)
	$title=Common::cutStringToWholeLastWord($data->title, 38, '...');*/
$text=Common::cutString($data->text, $data->getHasImage() ? 176 : 226, ' ...');
?>

<!--<li class="media">
	<a href="#" class="pull-left">
		<img src="http://photos.foter.com/66/carcacha_300x300.jpg" style="" alt="64x64" data-src="holder.js/64x64" class="media-object">
	</a>

	<div class="media-body">
		<h4 class="media-heading">Egestas eget quam <span class="label label-success pull-right">$12,000</span></h4>
		Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus
		odio, vestibulum in vulputate at, tempus viverra turpis.
	</div>
</li>-->

<li class="media <?php echo $data->is_colored ? 'premium-item' : ''; ?>">
	<?php if($data->getHasImage()): ?>
		<a href="#" class="pull-left">
			<?php echo CHtml::image($data->getMainImageUrl(), $title, array('class'=>'media-object', 'width'=>'125', 'height'=>'100')); ?>
		</a>
	<?php endif; ?>

	<div class="media-body">
		<h4 class="media-heading">
			<?php echo CHtml::link(CHtml::encode($title), array('catalog/view', 'id'=>Translit::encodestring($data->title.'-'.$data->id)), array()); ?>
<!--			--><?php //if($data->is_colored): ?><!--<span class="label label-info">Premium</span>--><?php //endif; ?>
			<?php if(intval($data->price)>3): ?>
<!--				<span class="label label-success pull-right">--><?php //echo $data->price; ?><!-- --><?php //echo $this->currencies[$data->currency_id]->htmlValue; ?><!--</span>-->
				<span class="label label-success pull-right"><?php echo $data->price; ?> <?php echo $data->getCurrencyUserFriendly(); ?></span>
			<?php endif; ?>
		</h4>
		<?php echo CHtml::encode($text); ?>
	</div>
</li>
