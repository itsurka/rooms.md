<?php
$arrayRooms = array(
	'1 комната',
	'2 комнаты',
	'3 комнаты'
);
?>
<ul>
	<?php foreach($arrayRooms as $key=>$eachRoom): ?>
		<li>
			<?php echo CHtml::link(CHtml::encode($eachRoom), $this->createUrl('catalog/rooms', array('id'=>$key+1)), array('class'=>($key+1)==@$_GET['id'] ? 'active' : '')); ?>
		</li>
	<?php endforeach; ?>
</ul>