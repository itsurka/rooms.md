<?php
/**
 * @var $userModel User
 */
$emptyText =  '<div class="fixed_alert info_msg">По Вашему запросу не найдено ни одного объявления</div>';
?>

<section class="searcher-wrapper">

	<?php echo CHtml::link('Настроить поиск','javascript:;',array('onclick'=>'toggleSearcherForm();', 'class'=>'toggle-searcher-block')); ?>

	<div class="searcher-form-block" style="<?php if($hasSearchers) echo ''; ?>">

		<?php echo CIconButton::init('add')->setIconSize('16x16')->getButton('[IMG]&nbsp;Добавить',array('onclick'=>'addSearcher();')); ?><br>

		<div class="searcher-example-wrapper" style="display: none;">
			<div class="searcher">
				<?php echo CHtml::dropDownList('rent_type[]', '', Advertisements::$rentTypes, array('class'=>'', 'empty'=>'Тип объявления')); ?>
				<?php echo CHtml::dropDownList('city_id[]', '', CHtml::listData(Cities::model()->findAll(), 'id', 'name'), array('class'=>'', 'empty'=>'Город', 'class'=>'city-selector', 'onchange'=>'onSelectCity_Searcher($(this))')); ?>
				<?php echo CHtml::dropDownList('region_id[]', '', CHtml::listData(CityRegions::model()->scopeApplySearchedCityID()->findAll(), 'id', 'name'), array('class'=>'', 'empty'=>array(Yii::t('app', 'Region')), 'class'=>'region-selector')); ?>
				<?php echo CHtml::dropDownList('number_of_rooms[]', '', Advertisements::$numbersOfRooms, array('class'=>'', 'empty'=>array(Yii::t('app', 'Number of rooms')))); ?>
				<?php echo CIconButton::init('delete')->setIconSize('16x16')->getButton(null,array('onclick'=>'removeSearcher($(this).closest(\'.searcher\'));')); ?>
			</div>
		</div>

		<?php
		$form = $this->beginWidget('CActiveForm', array(
												   'id'=>'searcher-form'
											  ));
		?>

			<div class="searchers-list">
				<div class="no-searchers fixed_alert info_msg" style="<?php if($hasSearchers) echo 'display: none;'; ?>">Нажмите <b>"Добавить"</b> чтобы добавить фильтры для поиска</div>
			</div>

			<div style="margin-top: 12px;">
				<?php echo CHtml::label('Присылать мне объявления на почту', 'advSearcherMailing'); ?>
				<?php echo CHtml::checkBox('User[advSearcherMailing]', $userModel->advSearcherMailing, array('id'=>'advSearcherMailing')); ?>
			</div>

			<div style="margin-top: 12px;">
				<?php echo CHtml::submitButton('Сохранить', array('class'=>'button white', 'id'=>'searcher-submit', 'name'=>'searcher-submit', 'data-color'=>'white')); ?>
				<div class="clear"></div>
			</div>

		<?php $this->endWidget(); ?>
	</div>

</section>

<section id="blog" class="float-none" style="margin-top: 20px;">

	<?php $this->widget('zii.widgets.CListView', array(
		'dataProvider'=>$dataProvider,
		'itemView'=>'_view',
		'ajaxUpdate'=>false,
		'summaryText'=>'Объявления с {start} - {end}',
		'htmlOptions'=>array('class'=>'advs-list'),
		'template'=>'{items}{pager}',
		'pagerCssClass'=>'pager',
        'emptyText'=>$hasSearcher ? $emptyText : '',
        'pager'=>array(
            'class'=>'application.components.LinkPager',
            'header'=>'',
            'firstPageLabel' => '',
            'prevPageLabel'=>'Пред.',
            'nextPageLabel'=>'След.',
            'lastPageLabel' => '',
            'id'=>'list_pagination',
        )
	)); ?>

</section>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/searcher.js', CClientScript::POS_END); ?>

<script type="text/javascript">

    $(document).ready(function() {

        if ($('a.img_preview').size() > 0) {
            $('a.img_preview').imgPreview({
                imgCSS: {
                    width: '200px'
                }
            });
        }

        $('.items .block').on('mouseover', function() {
            if(!$(this).hasClass('adv-is-colored'))
                $(this).addClass('adv-hover');
        });

        $('.items .block').on('mouseout', function() {
            if($(this).hasClass('adv-hover'))
                $(this).removeClass('adv-hover');
        });

		// Setup Adv Searcher - Start {
		searcherInit();

		var searchersCount=<?php echo $searchersCount; ?>;
		var searchers=<?php echo CJSON::encode($searchers); ?>;
		if(searchersCount>0)
		{
			for(var i in searchers)
				addSearcher(searchers[i]);
		}
		// Setup Adv Searcher - Start {
    });
</script>