<?php
$this->breadcrumbs=array(
    'Объявления'=>array('/catalog/index'),
    $countRoomsText,
);
?>

<?php //echo $this->renderPartial('_menu_rooms'); ?>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
    'ajaxUpdate'=>false,
)); ?>