<?php if(isset($_REQUEST['registration']) && $_REQUEST['registration']=='success'): ?>
	<?php echo $this->renderPartial('/blocks/_registrationSucces'); ?>
<?php endif; ?>

<?php /*if(isset($_REQUEST['added']) && $_REQUEST['added']=='success'): */ ?><!--
    <?php /*echo $this->renderPartial('/blocks/_advAddedSucces'); */ ?>
--><?php /*endif; */ ?>

<?php if(Yii::app()->session->get('isFirstCame')): ?>
	<?php
	$this->renderPartial("//blocks/_userIsFirstComeText");
	Yii::app()->session->add('isFirstCame', false);
	?>
<?php endif; ?>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'ajaxUpdate'=>false,
	'summaryText'=>'Объявления с {start} - {end}',
	'htmlOptions'=>array('class'=>''),
	'template'=>'{items}{pager}',
	'pagerCssClass'=>'pager',
	'emptyText'=>'<div class="alert alert-info">По Вашему запросу не найдено ни одного объявления</div>',
	'itemsTagName'=>'ul',
	'itemsCssClass'=>'media-list',
	'pager'=>array(
		'header'=>'',
		'hiddenPageCssClass'=>'hide',
		'firstPageCssClass'=>'hide',
		'firstPageLabel'=>'',
		'prevPageLabel'=>'&larr;',
		'nextPageLabel'=>'&rarr;',
		'lastPageCssClass'=>'hide',
		'lastPageLabel'=>'',
		'selectedPageCssClass'=>'active',
		'htmlOptions'=>array('class'=>'pagination')
	)
)); ?>
<div class="overlay_1" style="display: none;"></div>

<script type="text/javascript">
	$(document).ready(function ()
	{
		if($('a.img_preview').size()>0)
		{
			$('a.img_preview').imgPreview({
				imgCSS: {
					width: '200px'
				}
			});
		}

		$('.items .block').on('mouseover', function ()
		{
			if(!$(this).hasClass('adv-is-colored'))
			{
				$(this).addClass('adv-hover');
			}
		});

		$('.items .block').on('mouseout', function ()
		{
			if($(this).hasClass('adv-hover'))
			{
				$(this).removeClass('adv-hover');
			}
		});

	});
</script>