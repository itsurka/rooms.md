<?php /** @var Advertisements $data */ ?>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="page-header" style="margin-top: 10px;">
			<h1 class="" style="font-size: 1.8em; margin-top: 0;">
				<strong><?php echo CHtml::encode($data->title); ?></strong>
			</h1>
		</div>
		<div class="clearfix"></div>

		<div class="">
		<span class="pull-left">
			<b><?= $data->city->name ?>, <?= $data->region->name ?>
				, <?= Common::getAppNumRoomsStr_Enhanced($data->number_of_rooms, 'NUM_ROOMS-комн. квартира') ?><?php if($data->price>0): ?>, <?= $data->price ?>&nbsp;<?php echo $data->currency->name; ?><?php endif; ?></b>
		</span>
			<?php if($data->uniqueViews>0): ?>
				&nbsp;<span class="pull-right" style="margin-left: 8px;">
					<span class="glyphicon glyphicon-eye-open"></span> <?= $data->uniqueViews ?>
				</span>
			<?php endif; ?>
			<span class="date pull-right">
				<span class="glyphicon glyphicon-calendar"></span> <?php echo Common::formatDateTime($data->added); ?>
			</span>
		</div>
		<div class="clearfix"></div>

		<?php if(count($data->getHasImage())): ?>
			<div class="pull-left" style="margin-top: 20px">
				<?php foreach($data->files as $Key=>$EachFile): ?>
					<?php
					$thumbImg=Yii::app()->baseUrl.'/'.Yii::app()->params['ADV_THUMBS_URL'].$EachFile->name.'.'.$EachFile->extension;
					$bigImg=Yii::app()->baseUrl.'/'.Yii::app()->params['ADV_IMAGES_URL'].$EachFile->name.'.'.$EachFile->extension;
					echo CHtml::link(CHtml::image($thumbImg, $data->title, array('style'=>'max-width: 120px; max-height: 90px; margin-bottom: 5px;')), $bigImg, array("class"=>"img_preview_big"));
					?>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
		<div class="clearfix"></div>

		<p class="pull-left" style="margin: 15px 0">
			<?php echo nl2br($data->text); ?>
		</p>

		<div class="clearfix"></div>

		<div class="pull-left">
<!--			<span style="">Телефон:</span>&nbsp;-->
			<span class="glyphicon glyphicon-phone"></span>&nbsp;
			<span style="font-size: 1.2em;">
				<strong><em><?php echo Common::getPhoneNumberFriendlyFormat($data->owner_phone); ?></em></strong>
			</span>
			<?php if(trim($data->owner_name)!=''): ?>
				<em><span class="author" style="display: inline-block; margin: 0px 0px 0px 6px; padding: 0px 0px 0px 2px; position: relative; bottom: 1px;">
						<span class="glyphicon glyphicon-user"></span> <?php echo $data->owner_name; ?>
				</span></em>
			<?php endif; ?>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<?php if(isset($sameAdvs) && $sameAdvs->getData()): ?>
	<h3 style="margin-top: 56px;"><b>Похожие объявления</b></h3>
	<?php $this->widget('zii.widgets.CListView', array(
		'dataProvider'=>$sameAdvs,
		'itemView'=>'_view',
		'ajaxUpdate'=>false,
		'summaryText'=>'Объявления с {start} - {end}',
		'htmlOptions'=>array('class'=>''),
		'template'=>'{items}',
		'pagerCssClass'=>'pager float-right',
		'itemsTagName'=>'ul',
		'itemsCssClass'=>'media-list col',
	)); ?>
<?php endif; ?>

<script type="text/javascript">

	$(document).ready(function ()
	{
		if($('a.img_preview').size()>0)
		{
			$('a.img_preview').imgPreview({
				imgCSS: {
					width: '200px'
				}
			});
		}

		if($('a.img_preview_big').size()>0)
		{
			$('a.img_preview_big').imgPreview({
				imgCSS: {
					width: '270px'
				}
			});
		}
	});
</script>
