<?php
/**
 * @var Advertisements $model
 */
$addLink=CHtml::link('добавить', array('/ads/create'));
$emptyText='<div class="fixed_alert note_msg">Вы не добавили еще ни одного объявления, '.$addLink.'.</div>';
?>

<?php if(isset($_REQUEST['added']) && $_REQUEST['added']=='success'): ?>
	<?php echo $this->renderPartial('/blocks/_advAddedSucces'); ?>
<?php endif; ?>

<div class="panel panel-default">
	<div class="panel-heading">Мои объявления</div>
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'advertisements-grid',
		'dataProvider'=>$model->search(),
		'ajaxUpdate'=>true,
		'template'=>'{items}{pager}',
		'itemsCssClass'=>'table table-striped',
		'columns'=>array(
			array(
				'name'=>'id',
				'type'=>'raw',
				'value'=>'$data->id',
			),
			array(
				'name'=>'title',
				'type'=>'raw',
				'value'=>'CHtml::link(Common::cutStringToWholeLastWord($data->title, 200, "..."), $data->getViewUrl())',
			),
			array(
				'name'=>'text',
				'type'=>'raw',
				'value'=>'Common::cutStringToWholeLastWord($data->text, 200, "...")',
			),
			array(
				'name'=>'added',
				'type'=>'raw',
				'value'=>'Yii::app()->dateFormatter->format("d MMMM, H:mm", $data->added)',
			),
			array(
				'name'=>'status',
				'type'=>'raw',
				'value'=>'Advertisements::statusAlias($data->status, true)',
				'filter'=>Advertisements::$statuses,
			),
			array(
				'class'=>'CButtonColumn',
				'template'=>'{update}{delete}',
				'buttons'=>array(
					'update'=>array(
						'label'=>'Редактировать',
						'imageUrl'=>'/images/all_icons/coquette/24x24/edit.png',
					),
					'delete'=>array(
						'label'=>'Удалить',
						'imageUrl'=>'/images/all_icons/coquette/24x24/delete.png',
						'url'=>'array("/ads/changeAdvStatus", "id"=>"$data->id", "status"=>"'.(Advertisements::ADV_BLOCKED).'")',
						'options'=>array(
							'onclick'=>'if(!confirm("Вы уверены?")) return false;',
							'ajax'=>array(
								'type'=>'POST',
								'url'=>"js:$(this).attr('href')",
								'success'=>'js:function(id){$.fn.yiiGridView.update(\'advertisements-grid\');}',
							),
						),
					),
				),
			),
		),
		'pager'=>array(
			'header'=>'',
			'hiddenPageCssClass'=>'hide',
			'firstPageCssClass'=>'hide',
			'firstPageLabel'=>'',
			'prevPageLabel'=>'&larr;',
			'nextPageLabel'=>'&rarr;',
			'lastPageCssClass'=>'hide',
			'lastPageLabel'=>'',
			'selectedPageCssClass'=>'active',
			'htmlOptions'=>array('class'=>'pagination')
		)
	)); ?>
</div>