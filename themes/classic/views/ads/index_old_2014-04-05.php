<?php
$addLink = CHtml::link('добавить', array('/ads/create'));
$emptyText =  '<div class="fixed_alert note_msg">Вы не добавили еще ни одного объявления, '.$addLink.'.</div>';
?>

<?php if(isset($_REQUEST['added']) && $_REQUEST['added']=='success'): ?>
    <?php echo $this->renderPartial('/blocks/_advAddedSucces'); ?>
<?php endif; ?>

<section id="blog" class="float-none">
	<?php $this->widget('zii.widgets.CListView', array(
		'dataProvider'=>$dataProvider,
		'itemView'=>'/catalog/_view',
		'ajaxUpdate'=>false,
		'summaryText'=>'Объявления с {start} - {end}',
		'htmlOptions'=>array('class'=>''),
		'template'=>'{items}{pager}',
		'pagerCssClass'=>'pager float-right',
        'viewData'=>array('show_owner_actions'=>true),
        'emptyText'=>$emptyText
	)); ?>
</section>

<?php
// Init image preview - Start {
$script = "
$('a.img_preview').imgPreview({
    imgCSS: {
        width: '200px'
    }
});"."\n";
Yii::app()->clientScript->registerScript('imgPreview_init', $script, CClientScript::POS_READY);
// Init image preview - End }
?>