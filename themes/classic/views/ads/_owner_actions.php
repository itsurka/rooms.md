<div class="adv-owner-actions">
    <?php echo CIcon::init('note_edit')->getImg(); ?>
    <?php echo Chtml::link(Yii::t("app", "Edit"), array('/ads/update', 'id'=>$data->id)); ?>&nbsp;

    <?php if($data->status != 'deleted'): ?>
        <?php echo CIcon::init('note_delete')->getImg(); ?>
        <?php echo Chtml::link(Yii::t("app", "Delete"), array('/ads/delete', 'id'=>$data->id), array('class'=>'delete', 'submit'=>array('/ads/delete', 'id'=>$data->id))); ?>
    <?php else: ?>
        <?php echo Chtml::link(Yii::t("app", "Activate"), array('/ads/activate', 'id'=>$data->id), array('submit'=>array('/ads/activate', 'id'=>$data->id))); ?>
    <?php endif; ?>
    <div class="right">
        <i><?php echo $data->getStatusImgToOwner(); ?> <?php echo $data->getStatusTextToOwner(); ?></i>
    </div>
</div>
