<?php

if(!Yii::app()->request->getIsPostRequest() && !Yii::app()->user->isGuest)
{
    $user=User::model()->findByPk(Yii::app()->user->id);

    if($user!==null)
    {
        $profile=$user->profile;

        if($model->owner_name=='')
        {
            // имя автора
            $ownerName='';

            if($profile->firstname!='')
                $ownerName .= $profile->firstname;
            if($profile->lastname!='')
                $ownerName .= ' ' . $profile->lastname;

            $model->owner_name=trim($ownerName);
        }

        if($model->owner_phone=='')
        {
            // телефон автора
            $model->owner_phone=$profile->phone;
        }
    }
}
?>

<div class="advertisements-form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id'=>'advertisements-form',
        'enableAjaxValidation'=>false,
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),
    ));
    ?>

    <!--<div class="alert note_msg">
        <?php /*echo Yii::t("app", 'Fields with * are required'); */?>
    </div>-->

    <?php echo $form->errorSummary($model, NULL, NULL, array('class'=>'alert error_msg')); ?>
    
    <div class="table-grid">

        <div class="grid-row">
            <div class="grid-25">
                <div class="cell-inner">
                    <?php echo CHtml::label('Тип объявления', 'rent_type');?>
                    <div class="block_content">
                        <?php echo CHtml::activeDropDownList($model, 'rent_type', Advertisements::$rentTypes, array('class'=>'chzn-select-deselect', 'style'=>'', 'id'=>'rent_type')); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid-row">
            <div class="grid-25">
                <div class="cell-inner">
                    <?php echo CHtml::label('Город', 'city_id');?>
                    <div class="block_content">
                        <?php echo CHtml::activeDropDownList($model, 'city_id', CHtml::listData(Cities::model()->findAll(), 'id', 'name'), array('class'=>'chzn-select-deselect', 'id'=>'cities-select')); ?>
                    </div>
                </div>
            </div>
            <div class="grid-25">
                <div class="cell-inner">
                    <?php echo $form->labelEx($model, 'region_id'); ?>
                    <div class="block_content">
                        <?php
                        $cities = Cities::model()->findAll();
                        $findRegions = array('city_id'=>isset($_REQUEST['Advertisements']['city_id']) ? $_REQUEST['Advertisements']['city_id'] : !empty($model->city_id) ? $model->city_id : $cities[0]->id);
                        ?>
                        <?php echo Chtml::activeDropDownList($model, 'region_id', CHtml::listData(CityRegions::model()->findAllByAttributes($findRegions), 'id', 'name'), array('class'=>'chzn-select-deselect', 'id'=>'regions-select', 'empty'=>Advertisements::getEmptyOption())); ?>
                    </div>
                </div>
            </div>
            <div class="grid-25">
                <div class="cell-inner">
                    <?php echo $form->labelEx($model, 'number_of_rooms'); ?>
                    <div class="block_content">
                        <?php echo CHtml::activeDropDownList($model, 'number_of_rooms', Advertisements::$numbersOfRooms, array('class'=>'chzn-select-deselect', 'style'=>'', 'id'=>'number_of_rooms', 'empty'=>Advertisements::getEmptyOption())); ?>
                    </div>
                </div>
            </div>
            <div class="grid-25">
                <div class="cell-inner">
                    <?php echo $form->labelEx($model, 'price', array('class'=>'label_Advertisements_price')); ?>
                    <div class="block_content">
                        <div class="price"><?php echo Chtml::activeTextField($model, 'price', array('class'=>'i-format', "style"=>"")); ?></div>
                        <div class="currency"><?php echo CHtml::activeDropDownList($model, 'currency_id', CHtml::listData(Currencies::model()->findAll(), 'currency_id', 'name'), array('class'=>'chzn-select-deselect', 'style'=>'width: 66px;')); ?></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid-row">
            <div class="grid-50">
                <div class="cell-inner">
                    <?php echo $form->labelEx($model, 'title'); ?>
                    <div class="block_content">
                        <?php echo Chtml::activeTextField($model,'title',array('class'=>'i-format')); ?>
                    </div>
                </div>
            </div>
            <div class="grid-25">
                <div class="cell-inner">
                    <?php /*if(Yii::app()->user->isGuest): */?>
                        <?php echo CHtml::label($model->getAttributeLabel('owner_phone'), '', array('required'=>true)); ?>
                        <div class="block_content">
                            <?php echo Chtml::activeTextField($model, 'owner_phone', array('class'=>'i-format', 'style'=>'')); ?>
                        </div>
                    <?php /*endif; */?>
                </div>
            </div>
            <div class="grid-25">
                <div class="cell-inner">
                    <?php /*if(Yii::app()->user->isGuest): */?>
                        <?php echo $form->labelEx($model, 'owner_name'); ?>
                        <div class="block_content">
                            <?php echo Chtml::activeTextField($model,'owner_name',array('class'=>'i-format', 'style'=>'')); ?>
                        </div>
                    <?php /*endif; */?>
                </div>
            </div>
        </div>

        <div class="grid-row">
            <div class="grid-50">
                <div class="cell-inner">
                    <?php echo $form->labelEx($model, 'text'); ?>
                    <div class="block_content">
                        <?php echo Chtml::activeTextarea($model,'text',array('class'=>'default', 'style'=>'height: 103px;')); ?>
                    </div>
                </div>
            </div>
            <div class="grid-50">
                <div class="cell-inner">
                    <?php echo Chtml::label('Добавить фото', "files[]"); ?>
                    <?php echo Chtml::image(Yii::app()->baseUrl . "/images/fugue/plus.png", "", array("class"=>"", "onclick"=>"addNewFileInput();", "style"=>"cursor: pointer; margin-bottom: 3px;"));?>
                    <div class="block_content">
                        <div class="image_inputs_box">
                            <div class="_item" onmouseover="$(this).find('._delete').show();" onmouseout="$(this).find('._delete').hide();">
                                <div class="_file">
                                    <?php echo Chtml::activeFileField($modelUpload, 'files[]'); ?>
                                </div>
                                <?php echo Chtml::image(Yii::app()->baseUrl . "/images/fugue/cross-script.png", "", array("class"=>"_delete", "onclick"=>"$(this).parent().remove();", "style"=>"display: none;"));?>
								<div class="clear"></div>
                            </div>
                        </div>
                        <?php if( (!empty($model)) && (count($model->files)>0) ): ?>
                        <div class="old_images_box">
                            <?php foreach($model->files as $key=>$eachFile): ?>
                            <div id="image_<?php echo $key?>" class="_item" onmouseover="$(this).find('._delete').show();" onmouseout="$(this).find('._delete').hide();">
                                <div class="_delete" onclick="$(this).parent().remove();" style="display: none;"></div>
                                <?php echo Chtml::image(Yii::app()->baseUrl . '/' . Yii::app()->params['ADV_THUMBS_URL'] . $eachFile->name . '.' . $eachFile->extension, $model->id, array('width'=>'120')); ?>
                                <?php echo Chtml::hiddenField('oldFiles[]', $eachFile->name); ?>
                            </div>
                            <?php endforeach; ?>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid-row">
            <div class="grid-100">
                <div class="cell-inner">
                    <div class="left">
                        <?php echo $form->labelEx($model, 'is_colored', array('style'=>'color: #F86D0D;')); ?>
                    </div>
                    <div class="block_content left" style="margin-left: 5px;">
                        <?php echo Chtml::activeCheckBox($model, 'is_colored', array('class'=>'', 'style'=>'margin-left: 5px;')); ?>
                    </div>
                    <div class="clear"></div>
                    <span class="note"><i>(ваше объявление будт выделено цветом, что привлечет больше просмотров)</i></span>
                </div>
            </div>
        </div>

        <div class="grid-row">
            <div class="grid-100">
                <div class="cell-inner">
                    <div class="row buttons">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array("class"=>"button blue large")); ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <?php $this->endWidget(); ?>

</div>