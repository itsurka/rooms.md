<?php
/**
 * @var Controller $this
 * @var CActiveForm $form
 * @var Advertisements $model
 * @var Uploader $modelUpload
 */
?>
<div class="well bs-component panel">
	<?php
	$form=$this->beginWidget('CActiveForm', array(
		'id'=>'advertisements-form',
		'htmlOptions'=>array(
			'enctype'=>'multipart/form-data',
			'class'=>'form-horizontal'
		),
	));
	?>
	<fieldset>
		<legend>
			<?php echo $model->isNewRecord ? 'Новое' : 'Редактировать'; ?> объявление
		</legend>

		<div class="form-group <?php echo $model->hasErrors('region_id') ? 'has-error' : ''; ?>">
			<?php echo CHtml::label('Город', 'city_id', array('class'=>'col-lg-4 control-label')); ?>

			<div class="col-lg-7">
				<div class="input-group">
					<?php echo CHtml::activeDropDownList($model, 'city_id', CHtml::listData(Cities::model()->findAll(), 'id', 'name'), array('class'=>'form-control', 'id'=>'cities-select')); ?>

					<?php
					$cities=Cities::model()->findAll();
					$findRegions=array('city_id'=>isset($_REQUEST['Advertisements']['city_id']) ? $_REQUEST['Advertisements']['city_id'] : !empty($model->city_id) ? $model->city_id : $cities[0]->id);
					?>
					<?php echo CHtml::activeDropDownList($model, 'region_id', CHtml::listData(CityRegions::model()->findAllByAttributes($findRegions), 'id', 'name'), array('class'=>'form-control', 'id'=>'regions-select', 'empty'=>'район...')); ?>
				</div>
			</div>
		</div>

		<div class="form-group <?php echo $model->hasErrors('rent_type') ? 'has-error' : ''; ?>">
			<?php echo CHtml::label('Тип объявления', 'rent_type', array('class'=>'col-lg-4 control-label')); ?>
			<?php /*echo CHtml::activeDropDownList($model, 'rent_type', Advertisements::$rentTypes, array('class'=>'form-control', 'id'=>'rent_type', 'empty'=>Advertisements::getEmptyOption())); */ ?>
			<div class="col-lg-7">
				<div class="btn-group" data-toggle="buttons">
					<?php foreach(Advertisements::$rentTypes as $eachKey=>$eachValue): ?>
						<label class="btn btn-default <?php echo $model->rent_type==$eachKey ? 'active' : ''; ?>">
							<input type="radio" name="Advertisements[rent_type]" id="option<?php echo $eachKey; ?>" value="<?php echo $eachKey; ?>" <?php echo $model->rent_type==$eachKey ? 'checked="checked"' : ''; ?>> <?php echo $eachValue; ?>
							<!--							--><?php //echo CHtml::activeRadioButton($model, 'rent_type', array()); ?><!-- --><?php //echo $eachValue; ?>
						</label>
					<?php endforeach; ?>
				</div>
			</div>
		</div>

		<div class="form-group <?php echo $model->hasErrors('number_of_rooms') ? 'has-error' : ''; ?>">
			<?php echo $form->labelEx($model, 'number_of_rooms', array('class'=>'col-lg-4 control-label','style'=>'padding-top: 1px;')); ?>
			<div class="col-lg-7">
				<!--				--><?php //echo CHtml::activeDropDownList($model, 'number_of_rooms', Advertisements::$numbersOfRooms, array('class'=>'form-control', 'id'=>'number_of_rooms', 'empty'=>Advertisements::getEmptyOption())); ?>
				<div class="btn-group" data-toggle="buttons">
					<?php foreach(Advertisements::$numbersOfRooms as $eachKey=>$eachValue): ?>
						<label class="btn btn-default btn-xs <?php echo $model->number_of_rooms==$eachKey ? 'active' : ''; ?>">
							<input type="radio" name="Advertisements[number_of_rooms]" id="option<?php echo $eachKey; ?>" value="<?php echo $eachKey; ?>" <?php echo $model->number_of_rooms==$eachKey ? 'checked="checked"' : ''; ?>> <?php echo $eachValue; ?>
						</label>
					<?php endforeach; ?>
				</div>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model, 'price', array('class'=>'col-lg-4 control-label')); ?>
			<div class="col-lg-7">
				<div class="input-group">
					<?php echo CHtml::activeTextField($model, 'price', array('class'=>'form-control')); ?>
					<?php echo CHtml::activeDropDownList($model, 'currency_id', CHtml::listData(Currencies::model()->findAll(), 'currency_id', 'name'), array('class'=>'form-control')); ?>
				</div>
			</div>
		</div>

		<div class="form-group <?php echo $model->hasErrors('title') ? 'has-error' : ''; ?>">
			<?php echo $form->labelEx($model, 'title', array('class'=>'col-lg-4 control-label')); ?>
			<div class="col-lg-8">
				<?php echo CHtml::activeTextField($model, 'title', array('class'=>'form-control')); ?>
			</div>
		</div>

		<div class="form-group <?php echo $model->hasErrors('text') ? 'has-error' : ''; ?>">
			<?php echo $form->labelEx($model, 'text', array('class'=>'col-lg-4 control-label')); ?>
			<div class="col-lg-8">
				<?php echo CHtml::activeTextarea($model, 'text', array('class'=>'form-control')); ?>
			</div>
		</div>

		<div class="form-group <?php echo $model->hasErrors('owner_phone') ? 'has-error' : ''; ?>">
			<?php echo $form->labelEx($model, 'owner_phone', array('class'=>'col-lg-4 control-label', 'required'=>true, 'label'=>'Телефон <span class="required">*</span>')); ?>
			<div class="col-lg-4">
				<?php echo CHtml::activeTextField($model, 'owner_phone', array('class'=>'form-control')); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model, 'owner_name', array('class'=>'col-lg-4 control-label')); ?>
			<div class="col-lg-4">
				<?php echo CHtml::activeTextField($model, 'owner_name', array('class'=>'form-control', 'style'=>'')); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo CHtml::label('Добавить фото', "files[]", array('class'=>'col-lg-4 control-label')); ?>
			<div class="col-lg-7">
				<div class="image_inputs_box">
					<div class="_item" style="margin-bottom: 5px;">
						<div class="_file pull-left"><?php echo Chtml::activeFileField($modelUpload, 'files[]'); ?></div>
						<button type="button" class="_delete btn btn-default btn-sm pull-left" onclick="$(this).parent().remove();">
							<span class="glyphicon glyphicon-remove"></span>
						</button>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="old_images_box">
					<?php foreach($model->files as $key=>$eachFile): ?>
						<div id="image_<?php echo $key?>" class="_item">
							<?php echo Chtml::image($eachFile->getThumbUrl(), $model->id, array('width'=>'120', 'class'=>'img-thumbnail')); ?>
							<button type="button" class="_delete btn btn-default btn-sm" onclick="$(this).parent().remove();">
								<span class="glyphicon glyphicon-remove"></span>
							</button>
							<?php echo Chtml::hiddenField('oldFiles[]', $eachFile->name); ?>
						</div>
					<?php endforeach; ?>
				</div>
				<button type="button" class="btn btn-default btn-sm" onclick="addNewFileInput();" style="margin-top: 10px;">
					<span class="glyphicon glyphicon-plus"></span> Добавить еще
				</button>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model, 'is_colored', array('class'=>'col-lg-4 control-label')); ?>
			<div class="col-lg-7">
				<div class="checkbox">
					<?php echo CHtml::activeCheckBox($model, 'is_colored', array('class'=>'')); ?>
				</div>
				<span class="note"><i>(ваше объявление будт выделено цветом, что привлечет больше просмотров)</i></span>
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-10 col-lg-offset-4">
				<?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array("class"=>"btn btn-primary show-loading-text", 'id'=>'form-submit', 'data-loading-text'=>'Подождите...')); ?>
			</div>
		</div>

	</fieldset>
	<?php $this->endWidget(); ?>
</div>