<div class="form_place margin_top_0">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id'=>'advertisements-form',
        'enableAjaxValidation'=>false,
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),
    ));
    ?>

	<div class="alert note_msg">
		<?=Yii::t("app", 'Fields with * are required')?>
	</div>

	<?=$form->errorSummary($model, NULL, NULL, array('class'=>'alert error_msg'))?>

	<section class="form_row">
		<div>
			<?=$form->labelEx($model, 'title')?>
			<div class="block_content">
				<?=Chtml::activeTextField($model,'title',array('class'=>'i-format'))?>
			</div>
		</div>
		<div class="clear"></div>
	</section>

	<section class="form_row">
		<div>
			<?=$form->labelEx($model, 'text')?>
			<div class="block_content">
				<?=Chtml::activeTextarea($model,'text',array('class'=>'default', 'style'=>'height: 103px;'))?>
			</div>
		</div>
		<div class="clear"></div>
	</section>

	<section class="form_row">
		<div>
			<?=$form->labelEx($model, 'number_of_rooms')?>
			<div class="block_content">
				<?php
                echo $form->radioButtonList(
                    $model,
                    'number_of_rooms',
                    array('1'=>'Одна', '2'=>'Две', '3'=>'Три'),
                    array('separator'=>' ', 'template'=>'{label} {input}')
                );
                ?>
			</div>
		</div>
		<div class="clear"></div>
	</section>

	<section class="form_row">
		<div>
			<?/*=$form->labelEx($model, 'rent_type')*/?>
			<?php echo CHtml::label('Тип объявления', 'rent_type');?>
			<div class="block_content">
				<?=Chtml::activeDropDownList($model, 'rent_type', Advertisements::$rentTypes, array('class'=>'chzn-select-deselect', 'style'=>'width: 77px;', 'id'=>'rent_type'))?>
			</div>
		</div>
		<div class="clear"></div>
	</section>

	<section class="form_row">
		<div>
			<?=$form->labelEx($model, 'price')?>
			<div class="block_content">
				<?=Chtml::activeTextField($model,'price',array('class'=>'i-format', "style"=>"min-width: 0px; width: 143px;"))?>
                <?=CHtml::tag('div', array('style'=>'display: inline-block; position: relative; top: 9px;'), CHtml::activeDropDownList($model, 'currency_id', CHtml::listData(Currencies::model()->findAll(), 'currency_id', 'name'), array('class'=>'chzn-select-deselect', 'style'=>'width: 67px;')))?>
			</div>
		</div>
		<div class="clear"></div>
	</section>

	<section class="form_row">
		<div>
			<?=$form->labelEx($model, 'region_id')?>
			<div class="block_content">
				<?=Chtml::activeDropDownList($model,'region_id', CHtml::listData(CityRegions::model()->findAll(), 'id', 'name'), array('class'=>'chzn-select-deselect', 'style'=>'width: 224px;'))?>
			</div>
		</div>
		<div class="clear"></div>
	</section>

	<section class="form_row">
		<?=Chtml::label(Yii::t("app", "Image"), "files[]")?>&nbsp;
		<?=Chtml::image(Yii::app()->baseUrl . "/images/fugue/plus.png", "", array("class"=>"", "onclick"=>"addNewFileInput();", "style"=>"cursor: pointer; margin-bottom: 3px;"));?>
		<div class="block_content">
			<div class="image_inputs_box">
				<div class="_item" onmouseover="$(this).find('._delete').show();" onmouseout="$(this).find('._delete').hide();">
					<div class="_file">
						<?=Chtml::activeFileField($modelUpload, 'files[]')?>
					</div>
					<?=Chtml::image(Yii::app()->baseUrl . "/images/fugue/cross-script.png", "", array("class"=>"_delete", "onclick"=>"$(this).parent().remove();", "style"=>"display: none;"));?>
					<div class="clear"></div>
				</div>
			</div>
            <?php if( (!empty($model)) && (count($model->files)>0) ): ?>
            <div class="old_images_box">
                <?php foreach($model->files as $key=>$eachFile): ?>
                <div id="image_<?=$key?>" class="_item" onmouseover="$(this).find('._delete').show();" onmouseout="$(this).find('._delete').hide();">
                    <div class="_delete" onclick="$(this).parent().remove();" style="display: none;"></div>
                    <?=Chtml::image(Yii::app()->baseUrl . '/' . Yii::app()->params['ADV_THUMBS_URL'] . $eachFile->name . '.' . $eachFile->extension, $model->id, array('width'=>'120')); ?>
                    <?=Chtml::hiddenField('oldFiles[]', $eachFile->name)?>
                </div>
                <?php endforeach; ?>
                <div class="clear"></div>
            </div>
            <?php endif; ?>
		</div>
		<div class="clear"></div>
	</section>

    <?php if(Yii::app()->user->isGuest): ?>
        <section class="form_row">
            <div>
                <?=$form->labelEx($model, 'owner_phone')?>
                <div class="block_content">
                    <?php echo Chtml::activeTextField($model,'owner_phone',array('class'=>'i-format', 'style'=>'min-width: 0px; width: 214px;')); ?>
                </div>
            </div>
            <div class="clear"></div>
        </section>
    <?php endif; ?>

    <?php if(Yii::app()->user->isGuest): ?>
        <section class="form_row">
            <div>
                <?=$form->labelEx($model, 'owner_name')?>
                <div class="block_content">
                    <?php echo Chtml::activeTextField($model,'owner_name',array('class'=>'i-format', 'style'=>'min-width: 0px; width: 214px;')); ?>
                </div>
            </div>
            <div class="clear"></div>
        </section>
    <?php endif; ?>

	<div class="row buttons">
		<?=CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array("class"=>"button blue large"))?>
    </div>

    <?/*=CHtml::resetButton("Очистить", array("class"=>"button white"))*/?>

    <?php $this->endWidget(); ?>

</div>