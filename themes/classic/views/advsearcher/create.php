<?php
$this->breadcrumbs=array(
	'Adv Searchers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AdvSearcher', 'url'=>array('index')),
	array('label'=>'Manage AdvSearcher', 'url'=>array('admin')),
);
?>

<h1>Create AdvSearcher</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>