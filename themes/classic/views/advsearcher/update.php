<?php
$this->breadcrumbs=array(
	'Adv Searchers'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AdvSearcher', 'url'=>array('index')),
	array('label'=>'Create AdvSearcher', 'url'=>array('create')),
	array('label'=>'View AdvSearcher', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AdvSearcher', 'url'=>array('admin')),
);
?>

<h1>Update AdvSearcher <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>