<?php
$this->breadcrumbs=array(
	'Adv Searchers',
);

$this->menu=array(
	array('label'=>'Create AdvSearcher', 'url'=>array('create')),
	array('label'=>'Manage AdvSearcher', 'url'=>array('admin')),
);
?>

<h1>Adv Searchers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
