<?php
$this->breadcrumbs=array(
	'Adv Searchers'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AdvSearcher', 'url'=>array('index')),
	array('label'=>'Create AdvSearcher', 'url'=>array('create')),
	array('label'=>'Update AdvSearcher', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AdvSearcher', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AdvSearcher', 'url'=>array('admin')),
);
?>

<h1>View AdvSearcher #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'values',
		'rel_user_id',
		'last_email_ntf_adv_id',
		'created',
	),
)); ?>
