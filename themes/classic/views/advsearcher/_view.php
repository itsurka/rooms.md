<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('values')); ?>:</b>
	<?php echo CHtml::encode($data->values); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rel_user_id')); ?>:</b>
	<?php echo CHtml::encode($data->rel_user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_email_ntf_adv_id')); ?>:</b>
	<?php echo CHtml::encode($data->last_email_ntf_adv_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />


</div>