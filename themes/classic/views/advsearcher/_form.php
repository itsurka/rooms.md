<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'adv-searcher-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'values'); ?>
		<?php echo $form->textArea($model,'values',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'values'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rel_user_id'); ?>
		<?php echo $form->textField($model,'rel_user_id'); ?>
		<?php echo $form->error($model,'rel_user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'last_email_ntf_adv_id'); ?>
		<?php echo $form->textField($model,'last_email_ntf_adv_id'); ?>
		<?php echo $form->error($model,'last_email_ntf_adv_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created'); ?>
		<?php echo $form->textField($model,'created'); ?>
		<?php echo $form->error($model,'created'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->