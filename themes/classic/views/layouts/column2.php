<?php $this->beginContent('/layouts/main'); ?>

	<div class="col-md-3 col-sm-4">
		<?php $this->renderPartial("/blocks/leftSidebar"); ?>
	</div>
	<div class="col-md-9 col-sm-8 js-center-content">
		<?php echo $content; ?>
	</div>

<?php $this->endContent(); ?>