<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <link rel="icon" type="image/png" href="/images/favicon.ico" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
        <?php
        if (!Meta::isInit())
        {
            Meta::init();
        }
        ?>
		<title><?php echo CHtml::encode(Meta::getTitle() ? Meta::getTitle() : $this->pageTitle).' - '.Yii::app()->params['project']; ?></title>
        <meta name="description" content="<?php echo CHtml::encode(Meta::getDescription()); ?>" />
        <meta name="keywords" content="<?php echo CHtml::encode(Meta::getKeywords()); ?>" />
        <!--<meta name="author" content="Tsurka Igor" />-->

        <? /*
        <!-- blueprint CSS framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
        <![endif]-->

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
        */?>

        <!--Google fonts-->
<!--        <link href='http://fonts.googleapis.com/css?family=Jura' rel='stylesheet' type='text/css'>-->
<!--        <link href='http://fonts.googleapis.com/css?family=Poiret+One&subset=latin,cyrillic,latin-ext' rel='stylesheet' type='text/css'>-->
<!--        <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,700italic,400italic&subset=latin,cyrillic,latin-ext' rel='stylesheet' type='text/css'>-->
        <!--/Google fonts-->

        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/reset.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/skitter.styles.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/config.css">
		
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/my.css">
        <!--[if IE 7]><link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie7.css"><![endif]-->
        <!--[if IE 8]><link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie8.css"><![endif]-->

        <?php
        $script = 'var HOST = "'.Yii::app()->request->hostInfo.'";'."\n";
        $script .= 'var SCRIPT_URL = "'.Yii::app()->request->requestUri.'";'."\n";
        Yii::app()->clientScript->registerScript('global_vars', $script, CClientScript::POS_BEGIN);
        ?>
        
        <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
		
<!--        --><?php //Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins.js', CClientScript::POS_END); ?>
        <?php //Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/share.cufonfonts.js', CClientScript::POS_END); ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/flowslider.jquery.js', CClientScript::POS_END); ?>
        <?php //Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/scripts_call_head.js', CClientScript::POS_END); ?>
        <!--[if IE]><?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/html5.js', CClientScript::POS_END); ?><![endif]-->
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery.animate-colors-min.js', CClientScript::POS_END); ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery.easing.1.3.js', CClientScript::POS_END); ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery.skitter-min.js', CClientScript::POS_END); ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/imgpreview.full.jquery.js', CClientScript::POS_END); ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery.lightbox_me.js', CClientScript::POS_END); ?>

        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/common.js', CClientScript::POS_END); ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/catalog.js', CClientScript::POS_END); ?>

        <?php
        /*Yii::app()->clientScript->registerScript('search', '
            $(document).ready(function(){
                $(".box_skitter_slider").skitter({dots:true})});
                (function(a){a(document).ready(function(){a("#home_pf_items").FlowSlider({position:"0",speed:"10",marginLeft:"5",marginRight:"5"})})})
            (jQuery);
        ', CClientScript::POS_END);*/
        ?>
        <?php
        $stylesArray = array(
//            'violet',
//            'tealgray',
//            'teal',
//            'red',
//            'purple',
//            'orange',
//            'olive',
//            'lime',
//            'lightblue',
//            'indigo',
//            'indianred',
//            'green',
//            'green2',
//            'gray',
//            'darkorange',
//            'darkblue',
//            'brown',
//            'brown2',
//            'black',
        );
        ?>
        <?/*
        <?php foreach($stylesArray as $eachStyle): ?>
            <link rel="alternate stylesheet" type="text/css" media="screen" title="<?=$eachStyle?>-theme" href="<?php echo Yii::app()->request->baseUrl; ?>/css/color_scheme/<?=$eachStyle?>.css">
        <?php endforeach; ?>
        */?>

        <?php //Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/switcher/styleswitch.js', CClientScript::POS_END); ?>
        <?php /*Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/scripts.js', CClientScript::POS_END); */?>

        <!--Google analytics - Start {-->
        <?php
        $script = "
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-34753586-1']);
        _gaq.push(['_trackPageview']);

        (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();";
        Yii::app()->clientScript->registerScript('google_analitics', $script, CClientScript::POS_HEAD);
        ?>
        <!--Google analytics - End }-->

        <script type="text/javascript">
            $(document).ready(function() {
                if("<?php echo Common::getUTC(); ?>".length==0){
                    var visitortime = new Date();
                    var visitortimezone = "GMT " + -visitortime.getTimezoneOffset()/60;
                    console.log('visitortimezone: '+visitortimezone);
                    $.ajax({
                        type: "GET",
                        url: "/api/setUtc/",
                        data: 'UTC='+ visitortimezone,
                        success: function(json) {
                            if(json.success)
                                location.reload();
                        }
                    });
                }
            });
    </script>
</head>
    <body>
        <div style="display: none; position: absolute; z-index: 110; left: 400; top: 100; width: 15; height: 15" id="preview_div"></div>
        <?/*
        <div id="color-switcher">
            <div id="color-switcher-container"> <a href="javascript:chooseStyle('none',60)" checked="checked" style="background:#2A7CAB"></a> <a href="javascript:chooseStyle('lightblue-theme',60)" style="background:#41A4CB"></a> <a href="javascript:chooseStyle('darkblue-theme',60)" style="background:#33556E"></a> <a href="javascript:chooseStyle('violet-theme',60)" style="background:#5E5AA4"></a> <a href="javascript:chooseStyle('tealgray-theme',60)" style="background:#405862"></a> <a href="javascript:chooseStyle('indigo-theme',60)" style="background:#38315A"></a> <a href="javascript:chooseStyle('red-theme',60)" style="background:#A2231A"></a> <a href="javascript:chooseStyle('indianred-theme',60)" style="background:#B14A43"></a> <a href="javascript:chooseStyle('purple-theme',60)" style="background:#93396C"></a> <a href="javascript:chooseStyle('orange-theme',60)" style="background:#F48E1F"></a> <a href="javascript:chooseStyle('darkorange-theme',60)" style="background:#BB511F"></a> <a href="javascript:chooseStyle('brown-theme',60)" style="background:#782807"></a> <a href="javascript:chooseStyle('olive-theme',60)" style="background:#9A991B"></a> <a href="javascript:chooseStyle('lime-theme',60)" style="background:#AEC656"></a> <a href="javascript:chooseStyle('green2-theme',60)" style="background:#71A537"></a> <a href="javascript:chooseStyle('green-theme',60)" style="background:#317802"></a> <a href="javascript:chooseStyle('teal-theme',60)" style="background:#1B6F6F"></a> <a href="javascript:chooseStyle('silver-theme',60)" style="background:#B5B5B5"></a> <a href="javascript:chooseStyle('gray-theme',60)" style="background:#67686C"></a> <a href="javascript:chooseStyle('brown2-theme',60)" style="background:#4D4542"></a> <a href="javascript:chooseStyle('black-theme',60)" style="background:#0E0F13"></a> </div>
        </div>
         */?>
        <section id="wrap">
            <section class="wrap_block">

                <header class="main_header">
                    <section class="container" style="padding-top: 41px;">

						<div class="grid_2">
							<div id="logo">
								<a href="<?php echo Yii::app()->getBaseUrl(true); ?>">
									<?php echo CHtml::image(Yii::app()->getBaseUrl(true)."/images/rooms.md.logo7.png"); ?>
								</a>
							</div>
						</div>

						<div class="grid_5" style="width: 400px;">
							<nav id="horizontal">
								<?php $this->widget(
									'zii.widgets.CMenu',
									array(
										 'items'=>array(
                                             array('label'=>'Все квартиры', 'url'=>'/'),
                                             array('label'=>'За сегодня', 'url'=>'/catalog/today'),
                                             array('label'=>'Добавить квартиру', 'url'=>'/ads/create'),
                                             //array('label'=>'Мой поиск квартиры', 'url'=>'/catalog/mySearch'),
                                         ),
										 'id'=>'main-nav',
										 'encodeLabel'=>false
									)
								);
								?>
							</nav>
						</div>

						<div class="grid_5" style="width: 360px;">
							<?php Yii::app()->controller->renderPartial('//blocks/_userMainMenu'); ?>
						</div>

                    </section>
                </header>

                <section class="container layout">
                    <?php /*if(Yii::app()->controller->id=='catalog'): */?><!--
                        <?php /*$this->renderPartial("//blocks/page_title"); */?>
                    --><?php /*endif; */?>
                    <?php echo $content; ?>
                    <div class="clear"></div>
                </section>
                <?php $this->renderPartial("//blocks/footer"); ?>

                <div class="overlay_1" style="display: none;"></div>

            </section>
        </section>

        <div style="text-align: center;">

            <div style="display: none;">
                <!--LiveInternet counter-->
                <script type="text/javascript">
                    document.write("<a href='http://www.liveinternet.ru/click' "+
                            "target=_blank><img src='//counter.yadro.ru/hit?t24.6;r"+
                            escape(document.referrer)+((typeof(screen)=="undefined")?"":
                            ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
                                    screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
                            ";"+Math.random()+
                            "' alt='' title='LiveInternet: показано число посетителей за"+
                            " сегодня' "+
                            "border='0' width='88' height='15'><\/a>");
                </script>
                <!--/LiveInternet-->
            </div>

            <div style="<?php if(Yii::app()->user->isGuest || !Yii::app()->getModule('user')->isAdmin()) echo 'display: none;'; ?>">

                <!-- Yandex.Metrika informer -->
                <a href="http://metrika.yandex.ru/stat/?id=17135701&amp;from=informer"
                   target="_blank" rel="nofollow"><img src="//bs.yandex.ru/informer/17135701/3_1_FFFFFFFF_EFEFEFFF_0_pageviews"
                                                       style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:17135701,type:0,lang:'ru'});return false}catch(e){}"/></a>
                <!-- /Yandex.Metrika informer -->

                <!-- Yandex.Metrika counter -->
                <script type="text/javascript">
                    (function (d, w, c) {
                        (w[c] = w[c] || []).push(function() {
                            try {
                                w.yaCounter17135701 = new Ya.Metrika({id:17135701, enableAll: true});
                            } catch(e) { }
                        });

                        var n = d.getElementsByTagName("script")[0],
                                s = d.createElement("script"),
                                f = function () { n.parentNode.insertBefore(s, n); };
                        s.type = "text/javascript";
                        s.async = true;
                        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

                        if (w.opera == "[object Opera]") {
                            d.addEventListener("DOMContentLoaded", f);
                        } else { f(); }
                    })(document, window, "yandex_metrika_callbacks");
                </script>
                <noscript><div><img src="//mc.yandex.ru/watch/17135701" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
                <!-- /Yandex.Metrika counter -->
            </div>
            
        </div>

        <!--Login window-->
        <?php if(Yii::app()->user->isGuest): ?>
            <div id="loginWindow" class="modal" style="display: none;">
                <?php echo $this->renderPartial('/user/login',array('model'=>new UserLogin())); ?>
            </div>
        <?php endif; ?>
        <!--/Login window-->

    </body>
</html>
