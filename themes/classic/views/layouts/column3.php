<?php $this->beginContent('/layouts/main'); ?>

	<div class="col-md-3 col-sm-4">
		<?php $this->renderPartial("/blocks/leftSidebar"); ?>
	</div>
	<div class="col-md-6 col-sm-8 js-center-content">
		<?php echo $content; ?>
	</div>
	<div class="col-md-3 col-sm-8">
		<?php echo $this->renderPartial("/blocks/rightSidebar"); ?>
	</div>

<?php $this->endContent(); ?>