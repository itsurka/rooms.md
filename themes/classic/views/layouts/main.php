<?php
/**
 * @var $this Controller
 */
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<link rel="icon" type="image/png" href="/images/favicon.ico"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="language" content="en"/>
	<?php
	if(!Meta::isInit())
	{
		Meta::init();
	}
	?>
	<title><?php echo CHtml::encode(Meta::getTitle() ? Meta::getTitle() : $this->pageTitle).' - '.Yii::app()->params['project']; ?></title>
	<meta name="description" content="<?php echo CHtml::encode(Meta::getDescription()); ?>"/>
	<meta name="keywords" content="<?php echo CHtml::encode(Meta::getKeywords()); ?>"/>

	<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
	<?php Yii::app()->clientScript->registerScriptFile('//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js', CClientScript::POS_END); ?>
	<?php Yii::app()->clientScript->registerScriptFile('/js/catalog.js', CClientScript::POS_END); ?>
	<?php Yii::app()->clientScript->registerScriptFile('/js/common.js', CClientScript::POS_END); ?>
	<?php Yii::app()->clientScript->registerScriptFile('/js/imgpreview.full.jquery.js', CClientScript::POS_END); ?>
	<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery.easing.1.3.js', CClientScript::POS_END); ?>
	<?php Yii::app()->clientScript->registerCssFile('/css/bootstrap.min.css'); ?>
	<?php Yii::app()->clientScript->registerCssFile('/css/main_v2.0.css'); ?>

	<?php
	$script='var HOST = "'.Yii::app()->request->hostInfo.'";'."\n";
	$script.='var SCRIPT_URL = "'.Yii::app()->request->requestUri.'";'."\n";
	Yii::app()->clientScript->registerScript('global_vars', $script, CClientScript::POS_BEGIN);
	?>

</head>
<body>

<div id="wrap">
	<div id="popups"></div>
	<div id="top" class="container">
		<?php $this->renderPartial('/blocks/_userMainMenu'); ?>
		<div class="navbar navbar-default">
			<div class="navbar-collapse collapse navbar-responsive-collapse">
				<?php $this->widget(
					'zii.widgets.CMenu',
					array(
						'items'=>array(
							array('label'=>'Все', 'url'=>'/', 'active'=>($this->id=='catalog' && $this->action->id=='index')),
							array('label'=>'За неделю', 'url'=>'/catalog/week', 'active'=>($this->id=='catalog' && Yii::app()->request->getParam('period')=='week')),
							array('label'=>'За месяц', 'url'=>'/catalog/month', 'active'=>($this->id=='catalog' && Yii::app()->request->getParam('period')=='month')),
							/*array(
								'label'=>'Новые <b class="caret"></b>',
								'url'=>'#',
								'active'=>($this->id=='catalog' && in_array(Yii::app()->request->getParam('period'), array('week', 'month'))),
								'itemOptions'=>array('class'=>'dropdown'),
								'linkOptions'=>array('class'=>'dropdown-toggle', 'data-toggle'=>'dropdown'),
								'items'=>array(
									array('label'=>'За неделю', 'url'=>'/catalog/week', 'active'=>($this->id=='catalog' && Yii::app()->request->getParam('period')=='week')),
									array('label'=>'За месяц', 'url'=>'/catalog/month', 'active'=>($this->id=='catalog' && Yii::app()->request->getParam('period')=='month')),
								),
							),*/
							array('label'=>'Добавить объявление', 'url'=>'/ads/create', 'active'=>($this->id=='ads' && $this->action->id=='create'), 'itemOptions'=>array('class'=>'menu-create-add')),
							//array('label'=>'Мой поиск квартиры', 'url'=>'/catalog/mySearch'),
						),
						'htmlOptions'=>array('class'=>'nav navbar-nav'),
						'encodeLabel'=>false,
						'submenuHtmlOptions'=>array('class'=>'dropdown-menu')
					)
				);
				?>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<?php echo $this->renderPartial('/blocks/alerts'); ?>
			<?php echo $content; ?>
		</div>
		<?php if(Yii::app()->controller->id=='catalog' && in_array(Yii::app()->controller->action->id, array('index'))): ?>
			<div class="row">
				<?php $this->renderPartial('/blocks/_bannersBottom'); ?>
			</div>
		<?php endif; ?>
	</div>
	<div class="sub-foot"></div>
</div>

<?php $this->renderPartial("/blocks/footer"); ?>

</body>
</html>
