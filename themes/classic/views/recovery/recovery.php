<!--<h1>--><?php //echo UserModule::t("Restore"); ?><!--</h1>-->
<div class="alert info_msg"><?php echo UserModule::t("Please enter your login or email addres."); ?></div>

<?php if(Yii::app()->user->hasFlash('recoveryMessage')): ?>
        
    <div class="alert succes_msg">
        <?php echo Yii::app()->user->getFlash('recoveryMessage'); ?>
    </div>

<?php else: ?>

    <div class="form_place margin_top_0">
        <?php echo CHtml::beginForm(); ?>

        <?php echo Chtml::errorSummary($form, NULL, NULL, array('class'=>'alert error_msg')); ?>

        <section class="form_row">
            <div>
                <?php echo CHtml::activeLabel($form,'login_or_email'); ?>
                <div class="block_content">
                    <?php echo CHtml::activeTextField($form,'login_or_email', array('class'=>'i-format', 'style'=>'min-width: 200px; width: 230px;')) ?>
<!--                    <br><span class="input_tips">--><?php //echo UserModule::t("Please enter your login or email addres."); ?><!--</span>-->
                </div>
            </div>
            <div class="clear"></div>
        </section>

        <section class="form_row">
            <div>
                <div class="block_content label400">
                    <?php echo CHtml::submitButton(UserModule::t("Restore"), array('class'=>'button blue')); ?>
                </div>
            </div>
            <div class="clear"></div>
        </section>

        <?php echo CHtml::endForm(); ?>
    </div>

<?php endif; ?>
