<?php
/** @var Controller $this */
?>
<div class="collapse" id="login" style="height: 0px;">
	<div class="pull-right" style="padding: 10px 15px 0px 0px;">
		<?php $this->renderPartial('//user/login', array('model'=>new UserLogin())); ?>
	</div>
	<div class="clearfix"></div>
</div>
<div class="navbar  navbar-default" id="user-navbar">
	<div class="navbar-header">
		<button data-target=".navbar-ex1-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a id="logo" class="navbar-brand" href="<?php echo Yii::app()->getHomeUrl(); ?>">
			<?php echo CHtml::image("/images/rooms.md.logo7.1.png"); ?>
		</a>
	</div>

	<div class="collapse navbar-collapse navbar-ex1-collapse">
		<div class="btn-toolbar pull-right">
			<?php if(Yii::app()->user->isGuest): ?>
				<div class="btn-group">
					<?php echo CHtml::link(Yii::app()->getModule('user')->t("Login"), '#', array('class'=>'btn btn-primary navbar-btn btn-sm collapsed', 'data-target'=>'#login', 'data-toggle'=>'collapse')); ?>
				</div>
				<div class="btn-group">
					<?php echo CHtml::link(Yii::app()->getModule('user')->t("Registration"), Yii::app()->getModule('user')->registrationUrl, array('class'=>'btn btn-success navbar-btn btn-sm')); ?>
				</div>
			<?php else: ?>
				<?php if ($this->user->getIsAdmin()): ?>
					<div class="btn-group">
						<?php echo CHtml::link('<span class="glyphicon glyphicon-wrench"></span> Админ панель', Yii::app()->getModule('controlPanel')->mainUrl, array('class'=>'btn btn-primary navbar-btn btn-sm')); ?>
					</div>
				<?php endif; ?>
				<div class="btn-group">
					<?php echo CHtml::link('<span class="glyphicon glyphicon-pencil"></span> Объявления', '/ads/index', array('class'=>'btn btn-default navbar-btn btn-sm')); ?>
				</div>
				<div class="btn-group">
					<?php echo CHtml::link('<span class="glyphicon glyphicon-user"></span> '.$this->user->username, Yii::app()->getModule('user')->editProfileUrl, array('class'=>'btn btn-default navbar-btn btn-sm')); ?>
				</div>
				<div class="btn-group">
					<?php echo CHtml::link('<span class="glyphicon glyphicon-credit-card"></span> '.$this->user->balance . ' лей', '/payment/index', array('class'=>'btn btn-default navbar-btn btn-sm')); ?>
				</div>
				<div class="btn-group">
					<?php echo CHtml::link(Yii::app()->getModule('user')->t("Logout"), Yii::app()->getModule('user')->logoutUrl, array('class'=>'btn btn-default navbar-btn btn-sm')); ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
