<?php if(!Yii::app()->user->isGuest && Yii::app()->controller->id=='profile'): ?>
    <section class="widget">
        <?php /*if(Yii::app()->controller->id == "profile"): */?><!--
            <h2><?/*=UserModule::t("Profile")*/?></h2>
		--><?php /*endif; */?>
            <div class="wgt_in">
                <?php if(Yii::app()->controller->id != "profile"): ?>
					<?/*
                    <?php $this->widget('zii.widgets.CMenu',array(
                            'items'=>array(
                                array('label'=>Yii::t('app', 'Control panel'), 'url'=>Yii::app()->getModule('controlPanel')->mainUrl, 'visible'=>Yii::app()->getModule('user')->isAdmin()),
                                array('label'=>Yii::t('app', 'My advertisements'), 'url'=>array('/ads/index')),
                                array('label'=>Yii::app()->getModule('user')->t("Profile"), 'url'=>Yii::app()->getModule('user')->profileUrl),
                            ),
                            'htmlOptions'=>array('class'=>'vertical-menu')
                    )); ?>
					*/?>
                <?php else: ?>
                    <?php $this->widget('zii.widgets.CMenu',array(
                            'items'=>array(
                                array('label'=>'Личные данные', 'url'=>array('edit')),
                                array('label'=>UserModule::t('Change password'), 'url'=>array('changepassword')),
                            ),
                            'htmlOptions'=>array('class'=>'vertical-menu')
                    )); ?>
                <?php endif; ?>
            </div>
            <div class="clear"></div>
    </section>
<?php endif; ?>

<?php if (Yii::app()->controller->id=='catalog' && in_array(Yii::app()->controller->action->id,array('index'))): ?>
    <?php $this->renderPartial('//blocks/rightSearch'); ?>
<?php endif; ?>

<?php if (Yii::app()->controller->id=='catalog' && in_array(Yii::app()->controller->action->id, array('index'))): ?>
    <?php $this->renderPartial('//blocks/newAdvSubscription'); ?>
<?php endif; ?>

<!--VIP ADV-->
<?php if(Yii::app()->controller->id == 'catalog'): ?>
    <?php $this->renderPartial("//blocks/_vipAdv"); ?>
<?php endif; ?>
<!--/VIP ADV-->

<!--Popular advs-->
<?php if(Yii::app()->controller->id=='catalog' && Yii::app()->controller->action->id=='index'): ?>
    <?php $this->renderPartial("//blocks/_mostViewedAdvs"); ?>
<?php endif; ?>
<!--/Popular advs-->

<!--Help Add Item-->
<?php if(Yii::app()->controller->action->id=='create'): ?>
	<?php $this->renderPartial("//blocks/_helpAddItem"); ?>
<?php endif; ?>
<!--Help Add Item-->

<!--City Regions-->
<?php
/*$allowedControllers = array('catalog');
*/?><!--
<?php /*if(in_array(Yii::app()->controller->id, $allowedControllers)): */?>
	<?php /*$this->renderPartial("//blocks/_advancedSearch"); */?>
<?php /*endif; */?>    -->
<!--/City Regions-->

<?/*
<section class="widget">
	<h2>Recent comments</h2>
	<div class="wgt_in">
		<div class="recent_comments">
			<div class="post_comm">
				<a href="#"><img src="./images/assets/1.png" alt="" /></a>
				<p><a href="#">John Doe:</a> Etiam fermentum semper ligula ac bibendum. Nulla molestie metus est...</p>
			</div>
			<div class="clear"></div>

			<div class="post_comm">
				<a href="#"><img src="./images/assets/2.png" alt="" /></a>
				<p><a href="#">Peter:</a> Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere. Proin id neque ut ligula aliquet...</p>
			</div>
			<div class="clear"></div>

			<div class="post_comm">
				<a href="#"><img src="./images/assets/author_photo.jpg" alt="" /></a>
				<p><a href="#">Smartik:</a> Proin id neque ut ligula aliquet condimentu etiam fermentum semper. Nulla molestie metus est...</p>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</section>

<section class="widget">
	<h2>Flickr photos</h2>
	<div class="wgt_in">
		<div class="flickr_photos">
			<a href="#"><img src="./images/assets/239.jpg" alt="" /></a>
			<a href="#"><img src="./images/assets/244.jpg" alt="" /></a>
			<a href="#"><img src="./images/assets/246.jpg" alt="" /></a>
			<a href="#"><img src="./images/assets/247.jpg" alt="" /></a>
			<a href="#"><img src="./images/assets/249.jpg" alt="" /></a>
			<a href="#"><img src="./images/assets/250.jpg" alt="" /></a>
			<a href="#"><img src="./images/assets/248.jpg" alt="" /></a>
			<a href="#"><img src="./images/assets/254.jpg" alt="" /></a>
		</div>
		<div class="clear"></div>
	</div>
</section>

<section class="widget">
	<h2>Latest twetts</h2>
		<div class="wgt_in">
			<div class="latest_twetts">
				<div class="post_twett">
					<p><a href="#">Smartik</a> - Suppose - HTML5 Template now available on Themeforest <span>- <a href="#">10 minutes ago</a></span></p>
				</div>
				<div class="post_twett">
					<p><a href="#">Smartik</a> - MegaBox - Multipurpose HTML5 Template now available on Themeforest <span>- <a href="#">10 days ago</a></span></p>
				</div>
				<div class="post_twett">
					<p><a href="#">Smartik</a> - KLASS - Creative One Page Portfolio now available on Themeforest <span>- <a href="#">1 month ago</a></span></p>
				</div>
				<div class="post_twett">
					<p><a href="#">Smartik</a> - PixeluXx - HTML Template now available on Themeforest <span>- <a href="#">2 months ago</a></span></p>

				</div>
			</div>
		<div class="clear"></div>
	</div>
</section>
 */?>

<!--KOT V KADRE-->
<!--<section class="widget advertisement-block">
    <div align="center" style="text-align: center;">
        <div id="kot-advertisement">
            <a href="http://kotvkadre.ru" title="Фото приколы с котами">
                <?php /*echo CHtml::image(Yii::app()->getBaseUrl(true).'/images/коты/котвкадре.jpeg', 'Фото приколы с котами', array('style'=>'width: 242px;')); */?><br>
                Кот в кадре
            </a>
        </div>
    </div>
</section>-->
<!--/KOT V KADRE-->