<!--Login window-->
<?php if(Yii::app()->user->isGuest): ?>
	<div id="loginWindow" class="modal" style="display: none;">
		<?php echo $this->renderPartial('/user/login', array('model'=>new UserLogin())); ?>
	</div>
<?php endif; ?>
<!--/Login window-->