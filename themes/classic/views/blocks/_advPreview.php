<div style="text-align: center; padding: 0px 10px 10px;">

        <div class="vip-adv-inner">
            <?php if(count($eachVipAdv->files)>0): ?>
                <?php
                $imageUrl=Yii::app()->baseUrl.'/'.Yii::app()->params['ADV_THUMBS_URL'].$eachVipAdv->files[0]->name.'.'.$eachVipAdv->files[0]->extension;
                ?>
                <div class="vip-adv-image">
                    <a href="<?php echo Yii::app()->createUrl('catalog/view', array('id'=>Translit::encodestring($eachVipAdv->title.'-'.$eachVipAdv->id))); ?>">
                        <?php echo CHtml::image($imageUrl,'',array('style'=>'width: 170px;')); ?>
                    </a>
                </div>
            <?php endif; ?>

            <div class="vip-adv-info">
                <a href="<?php echo Yii::app()->createUrl('catalog/view', array('id'=>Translit::encodestring($eachVipAdv->title.'-'.$eachVipAdv->id))); ?>">
                    <?php echo CHtml::encode(Common::cutStringToWholeLastWord($eachVipAdv->title, 65, '...')); ?>
                </a>
            </div>
        </div>

</div>
