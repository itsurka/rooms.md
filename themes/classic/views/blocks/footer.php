<div id="footer">
	<div class="container">
		<ul class="footer-links">
			<li>
				<?php echo CHtml::link('Главная', Yii::app()->homeUrl); ?>
			</li>
			<li>
				<?php echo CHtml::link('Добавить объявление', array("/ads/create")); ?>
			</li>
			<li>
				Написать нам: <a href="mailto:<?php echo Yii::app()->params['adminEmail']; ?>"><?php echo Yii::app()->params['adminEmail']; ?></a>
			</li>

			<!--			--><?php //echo CHtml::link('Контакты', array("/site/contact")); ?>

		</ul>
		<p class="text-muted credit">&copy; <?php echo Yii::app()->params['projectYearFrom']; ?> - <?php echo date('Y'); ?>
			<strong><?= Yii::app()->params['project'] ?></strong></p>
		<p>
			<?php $this->renderPartial('/blocks/metrika/_yandexMetrika'); ?>
		</p>
	</div>
</div>