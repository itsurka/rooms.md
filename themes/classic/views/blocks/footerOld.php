<footer class="main_footer">
	<section class="container">
		<div class="grid_3">
			<h3 class="ft_title">Разделы сайта</h3>
			<ul class="vnav">
				<li><?php echo CHtml::link('Сдаю квартиру', '/catalog/sdaiu/kolicestvo-komnat/vse-goroda/vse-raiony'); ?></li>
				<li><?php echo CHtml::link('Сниму квартиру', '/catalog/snimu/kolicestvo-komnat/vse-goroda/vse-raiony'); ?></li>
				<li><?php echo CHtml::link('Продаю квартиру', '/catalog/prodaiu/kolicestvo-komnat/vse-goroda/vse-raiony'); ?></li>
				<li><?php echo CHtml::link('Куплю квартиру', '/catalog/cupliu/kolicestvo-komnat/vse-goroda/vse-raiony'); ?></li>
			</ul>
		</div>
		<div class="grid_3">
			<h3 class="ft_title">Полезные ссылки</h3>
			<ul class="vnav">
				<li><?php echo CHtml::link("Добавить квартиру", array("/ads/create")); ?></li>
				<li><?php echo CHtml::link("Мой поиск квартиры", array("/catalog/mySearch")); ?></li>
				<li><?php echo CHtml::link('Написать нам', '/site/contact'); ?></li>
			</ul>
		</div>
		<div class="grid_3">
			<h3 class="ft_title">О сайте</h3>

			<p>На нашем сайте Вы легко и быстро найдете квартиру.</p>

			<p>Здесь есть все что нужно для удобного поиска квартиры, нет ничего лишнего что может Вам помешать)</p>
		</div>
		<div class="grid_3">
			<h3 class="ft_title">Как найти нас</h3>

			<p> Мобильный: <?php echo Yii::app()->params['contactPhoneNumber']; ?><br/>
				Е-майл:
				<a href="mailto:<?php echo Yii::app()->params['supportEmail']; ?>"><?php echo Yii::app()->params['supportEmail']; ?></a>
			</p>

			<p> Если у Вас есть предложения, пожелания, Вы
				можете <?php echo CHtml::link("написать нам", array("/site/contact"), array('style'=>'text-decoration: underline;')); ?>
			</p>
		</div>
	</section>

	<section class="mini_footer">
		<div class="container">
			<div class="alignleft copyright">
				&copy; <?php echo Yii::app()->params['projectYearFrom']; ?>
				- <?php echo date('Y'); ?> <?= Yii::app()->params['project'] ?>
			</div>
		</div>
	</section>
	<div style="text-align: center;">

		<div style="display: none;">
			<?php $this->renderPartial('/blocks/metrika/_liveInternet'); ?>
		</div>

		<div style="<?php if(Yii::app()->user->isGuest || !Yii::app()->getModule('user')->isAdmin()) echo 'display: none;'; ?>">
			<?php $this->renderPartial('/blocks/metrika/_yandexMetrika'); ?>
		</div>

	</div>

	<?php $this->renderPartial('/blocks/_loginPopup'); ?>
</footer>