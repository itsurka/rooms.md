<div class="user-main-menu-block">

    <?/*
	<!--LOGIN BLOCK START-->
	<div class="login-block left">
		<?php if(Yii::app()->user->isGuest): ?>
			<?php echo CHtml::link(Yii::app()->getModule('user')->t("Login"), '#', array('class'=>'login', 'onclick'=>'showLogin()')); ?>&nbsp;|
			<?php echo CHtml::link(Yii::app()->getModule('user')->t("Registration"), Yii::app()->getModule('user')->registrationUrl, array('class'=>'login')); ?>
		<?php else: ?>
            <?php echo CIcon::init('door_out')->getImg(); ?>
            <a href="<?php echo Yii::app()->getModule('user')->logoutUrl; ?>">
                 <?php echo Yii::app()->getModule('user')->t("Logout"); ?>
            </a>
		<?php endif; ?>
	</div>
	<!--/LOGIN BLOCK START-->
    */?>

	<!--USER MENU-->
    <div id="user-main-menu" class="left">
        <ul>
            <?php if(!Yii::app()->user->isGuest): ?>
                <?php if(Yii::app()->getModule('user')->isAdmin()): ?>
                    <li>
                        <?php echo CHtml::link(Yii::t('app', '[Admin]'), Yii::app()->getModule('controlPanel')->mainUrl); ?>
                    </li>
                <?php endif; ?>
                <li>
                    <?php echo CIcon::init('note_edit')->getImg(); ?>
                    <?php echo CHtml::link(Yii::t('app', 'My advertisements'), array('/ads/index')); ?>
                </li>
                <li>
                    <?php echo CIcon::init('page_edit')->getImg(); ?>
                    <?php echo CHtml::link(Yii::app()->user->name, Yii::app()->getModule('user')->profileUrl); ?>
                </li>
                <li>
                    <?php echo CIcon::init('door_out')->getImg(); ?>
                    <?php echo CHtml::link(Yii::app()->getModule('user')->t("Logout"), Yii::app()->getModule('user')->logoutUrl); ?>
                </li>
            <?php else: ?>
                <li>
                    <?php echo CIcon::init('door_in')->getImg(); ?>
                    <?php echo CHtml::link(Yii::app()->getModule('user')->t("Login"), '#', array('onclick'=>'showLogin()')); ?>
                </li>
                <li>
                    <?php echo CIcon::init('user_add')->getImg(); ?>
                    <?php echo CHtml::link(Yii::app()->getModule('user')->t("Registration"), Yii::app()->getModule('user')->registrationUrl); ?>
                </li>
            <?php endif; ?>
        </ul>
        <?php echo CHtml::link('Мой поиск квартиры',array('catalog/mySearch'),array('class'=>'my-searcher-link')); ?>
    </div>
	<!--/USER MENU-->

</div>