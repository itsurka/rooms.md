<?php
/**
 * Generate page title data
 */
$AdvertisementsModel = new Advertisements();
$pageTitleData = array();
/*if(Yii::app()->controller->id=='catalog' && Yii::app()->controller->action->id=='rooms') {
	if(@$_GET['id']>0 && @$_GET['id']<4) $numRooms = @$_GET['id']; else $numRooms = 1;
	switch($numRooms) {
		case '1':
			$pageTitleData['title'] = 'Одна комната';
			break;
		case '2':
			$pageTitleData['title'] = 'Две комнаты';
			break;
		case '3':
			$pageTitleData['title'] = 'Три комнаты';
			break;
		default:
			$pageTitleData['title'] = 'Одна комната';
			break;
	}
	$pageTitleData['countItems'] = $AdvertisementsModel->countItemsByRooms(@$_GET['id']);
} else {
	$pageTitleData['title'] = 'Все квартиры';
	$pageTitleData['countItems'] = $AdvertisementsModel->countItems();
}*/
$pageTitleData['title'] = 'Все квартиры';
$pageTitleData['countItems'] = $AdvertisementsModel->countItems();
?>
<section class="page_title">
	<div class="title"><?=$pageTitleData['title']?></div>
	<div class="slogan">
		За сегодня - <span class="number addedToday"><?=$pageTitleData['countItems'][0]?></span>&nbsp;&nbsp;всего - <span class="number addedAll"><?=$pageTitleData['countItems'][1]?></span>
	</div>
</section>
<div class="clear"></div>