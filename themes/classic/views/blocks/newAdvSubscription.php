<?php
/**
 * @var $form CActiveForm
 * @var $this CatalogController
 */
?>

<?php if(!empty($this->advSearcherModel)): ?>

	<div class="panel panel-info">
		<div class="panel-body">
			<div class="form adv-subscription">
				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'adv-searcher-form',
					'enableAjaxValidation'=>true,
					'clientOptions' => array(
						'validateOnSubmit'=>true,
						'validateOnType'=>false,
						'afterValidate'=>'js:function(form, data, hasError)
						{
							if(!hasError)
							{
								if(Object.keys(data).length === 0)
								{
									$(".adv-subscription").hide();
									$(".success-mailing-subscription").show();
								}
							}
						}'
					),
					'action'=>Yii::app()->createUrl('/advsearcher/ajaxsubscribe'),
					'htmlOptions'=>array(
						'onsubmit'=>'return false;',
					),
				)); ?>

				<div class="form-group bl-info">
					Присылать мне новые объявления на почту
				</div>

				<div class="form-group">
<!--					--><?php //echo $form->labelEx($this->advSearcherModel,'email_address',array()); ?>
					<?php echo $form->textField($this->advSearcherModel,'email_address', array('class'=>'form-control', 'placeholder'=>'E-mail')); ?>
					<?php echo $form->error($this->advSearcherModel,'email_address'); ?>
					<?php echo $form->textField($this->advSearcherModel,'rel_user_id', array('style'=>'display: none;')); ?>
					<?php echo $form->error($this->advSearcherModel,'rel_user_id'); ?>
				</div>

				<div class="form-group buttons">
					<?php echo CHtml::submitButton(
						'Подписаться',
						array(
							'action'=>Yii::app()->createUrl('/advsearcher/ajaxsubscribe'),
							'class'=>'btn btn-primary btn-sm'
						)
					);
					?>
				</div>

				<?php $this->endWidget(); ?>

			</div>

			<div class="success-mailing-subscription" style="display: none;">
				<div class="form-group bl-info" style="font-size: 14px;">
					Спасибо за подписку!
				</div>
				<!--<div class="form-group">
					<?php /*if(!Yii::app()->user->isGuest): */?>
						<?php /*echo CHtml::link('Настроить рассылку', '/catalog/mySearch'); */?>

					<?php /*else: */?>
						<b><?php /*echo CHtml::link('Войти', 'javascript:;', array('onclick'=>'showLogin();')); */?></b> чтобы настроить рассылку
					<?php /*endif; */?>
				</div>-->
			</div>
		</div>
	</div>

<?php endif; ?>