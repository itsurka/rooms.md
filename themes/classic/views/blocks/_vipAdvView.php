<?php /** @var Advertisements $data */ ?>

<div class="adv-image">
	<a href="<?php echo $data->getViewUrl(); ?>">
		<?php echo CHtml::image($data->getMainImageUrl(), $data->title, array('class'=>'media-object')); ?>
	</a>
</div>
<div class="adv-title">
	<a href="<?php echo $data->getViewUrl(); ?>">
		<!--			--><?php //echo $data->city->name; ?><!--, --><?php //echo $data->region->name; ?><!--, --><?php //echo Common::getAppNumRoomsStr_Enhanced($data->number_of_rooms, 'NUM_ROOMS-комн.'); ?><!----><?php //if($data->price>0): ?><!--, --><?php //echo $data->price; ?><!-- --><?php //echo $data->currency->htmlValue; ?><!----><?php //endif; ?>
		<?php echo Common::cutStringToWholeLastWord($data->title, 65, '...'); ?>
	</a>
</div>