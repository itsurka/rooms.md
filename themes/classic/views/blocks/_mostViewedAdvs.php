<?php
// Вытаскиваем самое популярное по просмотрам
// За сутки
/*$criteriaBy24Hours=Yii::app()->cache->get('catalogCriteria');
$criteriaByWeek=$criteriaBy24Hours;

if(!$criteriaBy24Hours)
{
	$criteriaByWeek=$criteriaBy24Hours=new CDbCriteria();
}*/
/*$criteriaByWeek=$criteriaBy24Hours=new CDbCriteria();

$condition=$criteriaBy24Hours->condition;
$since24HoursAgo=date('Y-m-d H:i:s', strtotime('-36 hours'));
$criteriaBy24Hours->addCondition("added > '{$since24HoursAgo}'");
//$criteriaBy24Hours->addCondition("uniqueViews > 0");
$criteriaBy24Hours->order='uniqueViews DESC';
$advBy24Hours=Advertisements::model()->findAll($criteriaBy24Hours);
// За неделю
$since7DaysAgo=date('Y-m-d H:i:s', strtotime('-9 days'));
$criteriaByWeek->condition=$condition;
$criteriaByWeek->addCondition("added > '{$since7DaysAgo}'");
//$criteriaByWeek->addCondition("uniqueViews > 0");
$criteriaByWeek->order='uniqueViews DESC';
$criteriaByWeek->limit=3;
$advsByWeek=Advertisements::model()->findAll($criteriaByWeek);

/** @var Advertisements[] $allAds */
//$allAds=array_merge($advBy24Hours, $advsByWeek);
//shuffle($allAds);*/

$weekAds=Yii::app()->cache->get('weekAds');

if(!$weekAds)
{
	$dbCriteria=new CDbCriteria();
	$dbCriteria->addCondition('added > :added');
	$dbCriteria->params['added']=date('Y-m-d H:i:s', strtotime('-9 days'));
	$dbCriteria->order='uniqueViews DESC';
	$dbCriteria->limit=8;

	if(Advertisements::model()->count($dbCriteria)<$dbCriteria->limit/2)
		$dbCriteria->params['added']=date('Y-m-d H:i:s', strtotime('-18 days'));

	/** @var Advertisements[] $weekAds */
	$weekAds=Advertisements::model()->findAll($dbCriteria);
	Yii::app()->cache->set('weekAds', $weekAds, 60*60*24);
}
?>
<?php if(count($weekAds)>0): ?>
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title text-center">Популярные</h3>
		</div>
		<div class="panel-body">
			<?php foreach($weekAds as $eachAdv): ?>
				<div style="margin: 0 0 10px 0">
					<div class="text-center">
						<a href="<?php echo $eachAdv->getViewUrl(); ?>">
							<!--							--><?php //echo $eachAdv->city->name; ?><!--, -->							<?php //echo $eachAdv->region->name; ?><!--, -->							<?php //echo Common::getAppNumRoomsStr_Enhanced($eachAdv->number_of_rooms, 'NUM_ROOMS-комн.'); ?><!---->							<?php //if($eachAdv->price>0): ?><!--, --><?php //echo $eachAdv->price; ?><!-- -->							<?php //echo $eachAdv->currency->htmlValue; ?><!----><?php //endif; ?>
							<?php echo Common::cutStringToWholeLastWord($eachAdv->title, 65, '...'); ?>
						</a>
					</div>
				</div>

			<?php endforeach; ?>
		</div>
	</div>
<?php endif; ?>

<?php //if($advBy24Hours!==null || count($advsByWeek)>0): ?>
	<!---->
	<!--	<section class="widget adv-popular">-->
	<!--		--><?php //if($advBy24Hours!==null): ?>
	<!--			<h5 align="center" style="margin-top: 3px;">Популярное за сутки</h5>-->
	<!--			--><?php //$this->renderPartial('//blocks/_advPreview', array('eachVipAdv'=>$advBy24Hours)); ?>
	<!--		--><?php //endif; ?>
	<!---->
	<!--		--><?php //if(count($advsByWeek)>0): ?>
	<!--			<h5 align="center" style="margin-top: 3px;">Популярное за неделю</h5>-->
	<!--			--><?php //foreach($advsByWeek as $advByWeek): ?>
	<!--				--><?php //$this->renderPartial('//blocks/_advPreview', array('eachVipAdv'=>$advByWeek)); ?>
	<!--			--><?php //endforeach; ?>
	<!--		--><?php //endif; ?>
	<!--	</section>-->
	<!---->
<?php //endif; ?>