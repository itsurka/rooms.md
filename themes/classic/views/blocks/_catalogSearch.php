<?php
/**
 * @var Controller $this
 */

?>
<?php if($this->beginCache('catalogSearch', ['duration'=>3600*24])): ?>
	<?php

	$advRentTypes=Advertisements::$rentTypes;
	$advNumOfRooms=Advertisements::$numbersOfRooms;

	$cities=Yii::app()->cache->get('cities');
	if($cities===false)
	{
		$cities=Cities::model()->findAll();
		Yii::app()->cache->set('cities', $cities);
	}
	?>
	<section class="widget advs-block">
		<div align="center" style="text-align: center;">

			<form action="" method="post" id="catalog-search-form">

				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">Поиск</h3>
					</div>
					<div class="panel-body">

						<h4>Город</h4>
						<ul class="cs-menu">
							<?php foreach($cities as $k=>$city): ?>
								<?php
								$cityIsSelected=AdvSearch::getExistRequestParam('city', $city->name_translit);
								if(!$cityIsSelected)
									foreach($city->regions as $region)
									{
										if(AdvSearch::getExistRequestParam('region', $region->name_translit, 'city', $city->name_translit))
											$cityIsSelected=true;
									}
								?>
								<li class="cs-menu-item">
									<input onclick="cs_clickOption($(this));" type="checkbox" name="Cities[]" value="<?php echo $city->name_translit; ?>" id="sm-city-<?php echo $city->id; ?>" data-option-type="city" data-option-id="<?php echo $city->id; ?>" <?php if(AdvSearch::getExistRequestParam('city', $city->name_translit)) echo 'checked=""'; ?>>
									<label for="sm-city-<?php echo $city->id; ?>"><?php echo $city->name; ?></label>
									<ul class="cs-sub-menu <?php echo $cityIsSelected ? 'cs-sub-menu-opened' : 'cs-sub-menu-closed'; ?>">
										<?php foreach($city->regions as $region): ?>
											<li class="cs-sub-menu-item">
												<input onclick="cs_clickOption($(this));" type="checkbox" name="Regions[]" value="<?php echo $region->name_translit; ?>" id="sm-region-<?php echo $region->id; ?>" class="rel-city-<?php echo $city->id; ?>" data-option-type="region" data-rel-city-id="<?php echo $city->id; ?>" <?php if(AdvSearch::getExistRequestParam('region', $region->name_translit, 'city', $city->name_translit)) echo 'checked=""'; ?>>
												<label for="sm-region-<?php echo $region->id; ?>"><?php echo $region->name; ?></label>
											</li>
										<?php endforeach; ?>
									</ul>
								</li>
							<?php endforeach; ?>
						</ul>

						<h4>Кол-во комнат</h4>
						<ul class="cs-menu">
							<?php foreach(Advertisements::$numbersOfRoomsTranslit as $numOfRooms=>$numOfRoomsName): ?>
								<li class="cs-menu-item">
									<input onclick="cs_clickOption($(this));" type="checkbox" name="NumOfRooms[]" value="<?php echo $numOfRoomsName; ?>" id="sm-num-of-rooms-<?php echo $numOfRooms; ?>" data-option-type="num-of-rooms" data-option-id="<?php echo $numOfRooms; ?>" <?php if(AdvSearch::getExistRequestParam('numOfRooms', $numOfRoomsName)) echo 'checked=""'; ?>>
									<label for="sm-num-of-rooms-<?php echo $numOfRooms; ?>"><?php echo Advertisements::$numbersOfRooms[$numOfRooms]; ?></label>
								</li>
							<?php endforeach; ?>
						</ul>

						<h4>Тип</h4>
						<ul class="cs-menu">
							<?php foreach(Advertisements::$rentTypesTranslit as $rentType=>$rentTypeName): ?>
								<li class="cs-menu-item">
									<input onclick="cs_clickOption($(this));" type="checkbox" name="RentTypes[]" value="<?php echo $rentTypeName; ?>" id="sm-rent-type-<?php echo $rentType; ?>" data-option-type="rent-type" data-option-id="<?php echo $rentType; ?>" <?php if(AdvSearch::getExistRequestParam('rentType', $rentTypeName)) echo 'checked=""'; ?>>
									<label for="sm-rent-type-<?php echo $rentType; ?>"><?php echo Advertisements::$rentTypes[$rentType]; ?></label>
								</li>
							<?php endforeach; ?>
						</ul>

					</div>
				</div>
			</form>
		</div>
	</section>

	<?php $this->endCache(); ?>

<?php endif; ?>
