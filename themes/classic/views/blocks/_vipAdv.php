<?php if($this->beginCache('vipAdv', ['duration'=>3600*24])): ?>

	<div class="panel panel-success">
		<div class="panel-heading">
			<h3 class="panel-title text-center">ТОП объявления</h3>
		</div>
		<div class="panel-body">
			<ul class="vip-adv-list">
				<?php foreach(Advertisements::getVipAdv() as $eachVipAdv): ?>
					<li><?php $this->renderPartial('/blocks/_vipAdvView', array('data'=>$eachVipAdv)); ?></li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>

	<?php $this->endCache(); ?>

<?php endif; ?>
