<?php
$cssClasses=['success', 'info', 'warning', 'danger']
?>

<?php foreach($cssClasses as $eachCssClass): ?>
	<?php $flashMessage='global_'.$eachCssClass; ?>
	<?php if(Yii::app()->user->hasFlash($flashMessage)): ?>
		<div class="col-md-12">
			<div class="alert alert-<?php echo $eachCssClass; ?> alert-dismissible text-center" role="alert">
				<button type="button" class="close" data-dismiss="alert">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<b><?php echo Yii::app()->user->getFlash($flashMessage); ?></b>
			</div>
		</div>
	<?php endif; ?>
<?php endforeach; ?>