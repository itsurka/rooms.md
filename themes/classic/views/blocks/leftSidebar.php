<?php
/**
 * @var Controller $this
 */

?>
<?php if(!empty($this->menu)): ?>
	<div class="panel panel-default">
		<div class="panel-body">
			<?php $this->widget('zii.widgets.CMenu', array(
				'items'=>$this->menu,
				'htmlOptions'=>array('class'=>'nav nav-pills nav-stacked'),
			)); ?>
		</div>
	</div>
<?php endif; ?>

<?php if(Yii::app()->controller->id=='catalog' && in_array(Yii::app()->controller->action->id, array('index'))): ?>
	<?php $this->renderPartial('/blocks/_catalogSearch'); ?>
<?php endif; ?>

<div class="panel panel-default">
	<div class="panel-body" style="text-align: center; padding: 0">
		<?php
		$leftBanner = Yii::app()->session->get('leftBanner');
		if (is_null($leftBanner)) {
			$leftBanner = [
				'id' => 'alljob',
				'image' => '/images/banners/alljob.jpg',
				'href' => 'http://alljob.info',
				'title' => 'Работа в Кишиневе',
				'activeUntil' => strtotime('+ 2 minutes'),
			];
			Yii::app()->session->add('leftBanner', $leftBanner);
		} elseif ($leftBanner['activeUntil'] < time()) {
			if ($leftBanner['id'] == 'alljob') {
				$leftBanner = [
					'id' => 'dog_wear',
					'image' => '/images/banners/dog_wears.jpg',
					'href' => 'http://luckydoggy.ru',
					'title' => 'Одежда для собак в кишиневе',
					'activeUntil' => strtotime('+ 2 minutes'),
				];
			} else {
				$leftBanner = [
					'id' => 'alljob',
					'image' => '/images/banners/alljob.jpg',
					'href' => 'http://alljob.info',
					'title' => 'Работа в Кишиневе',
					'activeUntil' => strtotime('+ 2 minutes'),
				];
			}
		}
		?>
		<?php echo CHtml::link(
			CHtml::image($leftBanner['image'], $leftBanner['title'], array('class'=>'', 'style' => 'width: 99%')),
			$leftBanner['href'], ['title' => $leftBanner['title']]
		); ?>
	</div>
</div>

<!--Popular advs-->
<?php if(Yii::app()->controller->id=='catalog' && in_array(Yii::app()->controller->action->id, array('view', 'period'))): ?>
	<?php $this->renderPartial("/blocks/_mostViewedAdvs"); ?>
<?php endif; ?>
<!--/Popular advs-->

<!--Help Add Item-->
<?php if(Yii::app()->controller->action->id=='create'): ?>
	<?php $this->renderPartial("//blocks/_helpAddItem"); ?>
<?php endif; ?>
<!--Help Add Item-->

<?php if(Yii::app()->controller->id=='catalog' && in_array(Yii::app()->controller->action->id, array('index'))): ?>
	<?php $this->renderPartial('//blocks/newAdvSubscription'); ?>
<?php endif; ?>
