<div class="login-block">
    <?php if(Yii::app()->user->isGuest): ?>
        <?php
        echo CHtml::link(
            Yii::app()->getModule('user')->t("Login").CHtml::image('/images/icons/login3.png', 'login'),
            '#',
            array('class'=>'login', 'onclick'=>'showLogin()')
        );
        ?>
    <?php else: ?>
        <?php echo CHtml::link(Yii::app()->getModule('user')->t("Logout").CHtml::image('/images/icons/logout.png', 'logout'), Yii::app()->getModule('user')->logoutUrl, array('class'=>'logout')); ?>
    <?php endif; ?>
</div>