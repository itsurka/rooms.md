<?php
$cityRegions = Common::cityRegions();
?>
<section class="widget">
	<h2>Районы</h2>
	<div class="wgt_in">
		<ul class="vertical-menu">
			<?php foreach($cityRegions as $key=>$value): ?>
				<li style="padding: 3px;">
					<?=CHtml::checkbox("city_regions[]", (in_array($value->id, Common::selectedCityRegions()) ? true : false), array('id'=>'city_region_' . $value->id, 'onclick'=>'setCityRegion('.$value->id.')'))?>
					<?=Chtml::label($value->name, 'city_region_' . $value->id)?>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
	<div class="clear"></div>
	<h2>Комнаты</h2>
	<div class="wgt_in">
		<?php foreach(Common::rooms() as $key=>$value): ?>			
			<?=CHtml::checkbox("number_of_rooms[]", (in_array($key, Common::selectedNumberOfRooms()) ? true : false), array('id'=>'number_of_rooms_' . $key, 'onclick'=>'setNumberOfRooms('.$key.')'))?>
			<?=Chtml::label($value, 'number_of_rooms_' . $key)?>&nbsp;
		<?php endforeach; ?>
	</div>
	<h2>Сортировать</h2>
	<?php
	$sortByArray = array("date", "price");
	?>
	<div class="wgt_in">
		<?php foreach(Common::sortSettings() as $key=>$value): ?>
			<?=CHtml::radioButton("sort_by[]", ($key === Common::selectedSortType() ? true : false), array('id'=>'sort_by_' . $key, 'onclick'=>'setSortBy("'.$key.'")'))?>
			<?=Chtml::label($value, 'sort_by_' . $key)?>&nbsp;
		<?php endforeach; ?>
	</div>
	<div class="clear"></div>
		<div style="margin-bottom: 5px; text-align: center;">
            <?php
            $params = array();
            if(!empty($_GET))
            {
                $needParams = array();
                foreach ($_GET as $key=>$value)
                {
                    if(in_array($key, $needParams))
                    {
                        $params[$key] = $value;
                    }
                }
            }
            //$route = Yii::app()->urlManager->parseUrl(Yii::app()->getRequest());
            $currentUrl = Yii::app()->urlManager->createUrl("/catalog/index", $params);
            ?>
			<?=Chtml::image(Yii::app()->params['IMAGES_URL'].'search.png', 'Search', array('width'=>'width: 27px;'))?>
			<?=Chtml::link("Найти", $currentUrl, array("class"=>""))?>
        </div>
	<div class="clear"></div>
</section>
<?php
$script = 'function setCityRegion(id){'."\n";
$script .= '$.ajax({'."\n";
$script .= 'url: "'.$this->createUrl("/catalog/setCityRegion").'",'."\n";
$script .= 'data: "id="+id,'."\n";
$script .= 'type: "POST"'."\n";
$script .= '});'."\n";
$script .= '}'."\n";

$script .= 'function setNumberOfRooms(id){'."\n";
$script .= '$.ajax({'."\n";
$script .= 'url: "'.$this->createUrl("/catalog/setNumberOfRooms").'",'."\n";
$script .= 'data: "id="+id,'."\n";
$script .= 'type: "POST"'."\n";
$script .= '});'."\n";
$script .= '}'."\n";

$script .= 'function setSortBy(id){'."\n";
$script .= '$.ajax({'."\n";
$script .= 'url: "'.$this->createUrl("/catalog/setSortBy").'",'."\n";
$script .= 'data: "id="+id,'."\n";
$script .= 'type: "POST"'."\n";
$script .= '});'."\n";
$script .= '}'."\n";
Yii::app()->clientScript->registerScript('datepicker_init_local', $script, CClientScript::POS_BEGIN);
?>