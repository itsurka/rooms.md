<?php
/**
 * @var Controller $this
 * @var CActiveForm $form
 * @var RegistrationForm $model
 * @var Profile $profile
 */
?>

<?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
	<div class="alert alert-success"><?php echo Yii::app()->user->getFlash('profileMessage'); ?></div>
<?php endif; ?>

<div class="well bs-component panel">
	<?php
	$form=$this->beginWidget('CActiveForm', array(
		'id'=>'profile-form',
		'enableAjaxValidation'=>true,
		'htmlOptions'=>array(
			'enctype'=>'multipart/form-data',
			'class'=>'form-horizontal'
		),
	));
	?>
	<fieldset>
		<legend>Профиль</legend>

<!--		--><?php //echo $form->errorSummary(array($model,$profile), NULL, NULL, array('class'=>'alert error_msg')); ?>

		<div class="form-group <?php echo $profile->hasErrors('phone') ? 'has-error' : ''; ?>">
			<?php echo CHtml::activeLabel($profile, 'phone', array('class'=>'col-lg-4 control-label', 'label'=>'Телефон')); ?>
			<div class="col-lg-6">
				<?php echo $form->numberField($profile, 'phone', array('class'=>'form-control', 'placeholder'=>'мин. 5 цифр')); ?>
				<?php echo $form->error($profile, 'phone', array(
					'hideErrorMessage'=>'true',
				)); ?>
			</div>
		</div>

		<div class="form-group <?php echo $profile->hasErrors('firstname') ? 'has-error' : ''; ?>">
			<?php echo CHtml::activeLabel($profile, 'firstname', array('class'=>'col-lg-4 control-label', 'label'=>'Имя')); ?>
			<div class="col-lg-6">
				<?php echo $form->textField($profile, 'firstname', array('class'=>'form-control')); ?>
				<?php echo $form->error($profile, 'firstname'); ?>
			</div>
		</div>

		<div class="form-group <?php echo $profile->hasErrors('lastname') ? 'has-error' : ''; ?>">
			<?php echo CHtml::activeLabel($profile, 'lastname', array('class'=>'col-lg-4 control-label', 'label'=>'Фамилия')); ?>
			<div class="col-lg-6">
				<?php echo $form->textField($profile, 'lastname', array('class'=>'form-control')); ?>
				<?php echo $form->error($profile, 'lastname'); ?>
			</div>
		</div>

		<div class="form-group <?php echo $model->hasErrors('email') ? 'has-error' : ''; ?>">
			<?php echo CHtml::activeLabel($model, 'email', array('class'=>'col-lg-4 control-label', 'label'=>'E-mail')); ?>
			<div class="col-lg-6">
				<?php echo $form->emailField($model, 'email', array('class'=>'form-control')); ?>
				<?php echo $form->error($model, 'email'); ?>
			</div>
		</div>

		<div class="form-group <?php echo $model->hasErrors('password') ? 'has-error' : ''; ?>">
			<?php echo CHtml::activeLabel($model, 'password', array('class'=>'col-lg-4 control-label', 'label'=>'Пароль')); ?>
			<div class="col-lg-6">
				<?php echo $form->passwordField($model, 'password', array('class'=>'form-control', 'autocomplete'=>'off', 'placeholder'=>'мин. 5 символов')); ?>
				<?php echo $form->error($model, 'password', array(
					'hideErrorMessage'=>'true',
				)); ?>
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-10 col-lg-offset-4">
				<?php echo CHtml::submitButton('Сохранить', array("class"=>"btn btn-primary show-loading-text", 'id'=>'form-submit', 'data-loading-text'=>'Подождите...')); ?>
			</div>
		</div>

	</fieldset>
	<?php $this->endWidget(); ?>
</div>