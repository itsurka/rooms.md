<?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
	<div class="alert succes_msg">
		<?php echo Yii::app()->user->getFlash('profileMessage'); ?>
	</div>
<?php endif; ?>
<table class="style7" style="margin-top: 5px; margin-bottom: 0px;">
    <tr>
        <th class="label"><?php echo CHtml::encode($model->getAttributeLabel('username')); ?>
    </th>
        <td><?php echo CHtml::encode($model->username); ?>
    </td>
    </tr>
    <?php
            $profileFields=ProfileField::model()->forOwner()->sort()->findAll();
            if ($profileFields) {
                foreach($profileFields as $field) {
                ?>
                    <tr>
                        <th class="label"><?php echo CHtml::encode(UserModule::t($field->title)); ?>
                    </th>
                        <td><?php echo (($field->widgetView($profile))?$field->widgetView($profile):CHtml::encode((($field->range)?Profile::range($field->range,$profile->getAttribute($field->varname)):$profile->getAttribute($field->varname)))); ?>
                    </td>
                    </tr>
                <?php
                }
            }
    ?>
    <tr>
        <th class="label"><?php echo CHtml::encode($model->getAttributeLabel('email')); ?>
    </th>
        <td><?php echo CHtml::encode($model->email); ?>
    </td>
    </tr>
    <tr>
        <th class="label"><?php echo CHtml::encode($model->getAttributeLabel('createtime')); ?>
    </th>
        <td><?php echo Yii::app()->dateFormatter->format('d MMM, y', $model->createtime); ?>
    </td>
    </tr>
    <tr>
        <th class="label"><?php echo CHtml::encode($model->getAttributeLabel('lastvisit')); ?>
    </th>
        <td><?php echo Yii::app()->dateFormatter->format('d MMM, y', $model->lastvisit); ?>
    </td>
    </tr>
    <!--<tr>
        <th class="label"><?php /*echo CHtml::encode($model->getAttributeLabel('status')); */?>
    </th>
        <td><?php /*echo CHtml::encode(User::itemAlias("UserStatus",$model->status));
        */?>
    </td>
    </tr>-->
</table>
