<?php
$this->breadcrumbs=array(
    'Объявления'=>array('/catalog/index'),
    $data->number_of_rooms.' - комн.'=>array('/catalog/rooms/'.$data->number_of_rooms),
    $data->title,
);
?>

<?php if(count($data->files)>0): ?>
    <?php foreach($data->files as $Key=>$EachFile): ?>
        <?php echo CHtml::image('/'.Yii::app()->params['ADV_THUMBS_URL'].$EachFile->name.'.'.$EachFile->extension, $data->id, array('width'=>'120')); ?>
    <?php endforeach; ?>
<?php endif; ?>

<?php echo $data->text; ?>