<div class="view">

    <?php echo CHtml::link(CHtml::encode($data->text), array('/items/'.$data->id)); ?>
    
    <?php if(count($data->files)>0): ?>
        <?php echo Chtml::image('/'.Yii::app()->params->ADV_THUMBS_URL.$data->files[0]->name.'.'.$data->files[0]->extension, $data->id, array('width'=>'120')); ?>
    <?php endif; ?>
    <br />

</div>