<div class="form_place margin_top_0">

	<?php if(Yii::app()->user->hasFlash('loginMessage')): ?>
		<div class="alert succes_msg">
			<?php echo Yii::app()->user->getFlash('loginMessage'); ?>
		</div>
	<?php endif; ?>

	<?php echo CHtml::beginForm(Yii::app()->getModule('user')->loginUrl); ?>

		<?=Chtml::errorSummary($model, NULL, NULL, array('class'=>'alert error_msg'))?>

		<section class="form_row">
			<div>
				<?php echo CHtml::activeLabelEx($model,'username'); ?>
				<div class="block_content">
					<?php echo CHtml::activeTextField($model,'username',array('class'=>'i-format', 'style'=>'min-width: 200px; width: 230px;')) ?>
				</div>
			</div>
			<div class="clear"></div>
		</section>

		<section class="form_row">
			<div>
				<?php echo CHtml::activeLabelEx($model,'password'); ?>
				<div class="block_content">
					<?php echo CHtml::activePasswordField($model,'password',array('class'=>'i-format', 'style'=>'min-width: 200px; width: 230px;')) ?>
					<div>
						<?php echo CHtml::link(UserModule::t("Register"),Yii::app()->getModule('user')->registrationUrl); ?>
						&nbsp;|&nbsp;
						<?php echo CHtml::link(UserModule::t("Lost Password?"),Yii::app()->getModule('user')->recoveryUrl); ?>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</section>

		<section class="form_row">
			<div>
				<div class="block_content label400">
					<div style="float: left;">
						<?php echo CHtml::submitButton(UserModule::t("Login"),array('class'=>'button blue')); ?>
					</div>
                    <div style="margin-left: 5px; float: left;">
                        <div id="uniform-undefined" class="checker">
                            <span>
                                <?php echo CHtml::activeCheckBox($model,'rememberMe',array('style'=>'opacity: 0;')); ?>
                            </span>
                        </div>
                        <?php echo CHtml::activeLabelEx($model,'rememberMe'); ?>
                    </div>
                    <div style="clear: both;"></div>
				</div>
			</div>
			<div class="clear"></div>
		</section>
	
	<?php echo CHtml::endForm(); ?>
	
</div>