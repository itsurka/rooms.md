<?php if(Yii::app()->user->hasFlash('registration')): ?>

	<div class="alert succes_msg">
		<?php echo Yii::app()->user->getFlash('registration'); ?>
	</div>

<?php else: ?>

	<div class="form_place margin_top_0">
	<?php $form=$this->beginWidget('UActiveForm', array(
		'id'=>'registration-form',
		'enableAjaxValidation'=>true,
		'disableAjaxValidationAttributes'=>array('RegistrationForm_verifyCode'),
		'htmlOptions' => array('enctype'=>'multipart/form-data'),
	)); ?>

        <div class="alert info_msg">
            <?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?>
        </div>

		<?php echo $form->errorSummary(array($model,$profile), NULL, NULL, array('class'=>'alert error_msg')); ?>

			<section class="form_row">
				<div>
					<?php echo $form->labelEx($model,'username'); ?>
					<div class="block_content">
						<?php echo $form->textField($model,'username',array('class'=>'i-format', 'style'=>'min-width: 200px; width: 230px;')); ?>
					</div>
				</div>
				<div class="clear"></div>
			</section>

			<section class="form_row">
				<div>
					<?php echo $form->labelEx($model,'password'); ?>
					<div class="block_content">
						<?php echo $form->passwordField($model,'password',array('class'=>'i-format', 'style'=>'min-width: 200px; width: 230px;')); ?>
					</div>
				</div>
				<div class="clear"></div>
			</section>

			<section class="form_row">
				<div>
					<?php echo $form->labelEx($model,'verifyPassword'); ?>
					<div class="block_content">
						<?php echo $form->passwordField($model,'verifyPassword',array('class'=>'i-format', 'style'=>'min-width: 200px; width: 230px;')); ?>
					</div>
				</div>
				<div class="clear"></div>
			</section>

			<section class="form_row">
				<div>
					<?php echo $form->labelEx($model,'email'); ?>
					<div class="block_content">
						<?php echo $form->textField($model,'email',array('class'=>'i-format', 'style'=>'min-width: 200px; width: 230px;')); ?>
					</div>
				</div>
				<div class="clear"></div>
			</section>

			<?php
		    $profileFields=$profile->getFields();
			if ($profileFields) {
				foreach($profileFields as $field) {
					?>
					<section class="form_row">
						<div>
							<?php echo $form->labelEx($profile,$field->varname); ?>
							<div class="block_content">
								<?php //echo $form->passwordField($model,'password',array('class'=>'i-format', 'style'=>'min-width: 200px; width: 230px;')); ?>
								<?php
								if ($field->widgetEdit($profile)) {
									echo $field->widgetEdit($profile, array('class'=>'i-format', 'style'=>'min-width: 200px; width: 230px;'));
								} elseif ($field->range) {
									echo $form->dropDownList($profile,$field->varname,Profile::range($field->range), array('class'=>'i-format', 'style'=>'min-width: 200px; width: 230px;'));
								} elseif ($field->field_type=="TEXT") {
									echo $form->textArea($profile,$field->varname,array('rows'=>6, 'cols'=>50, 'class'=>'i-format', 'style'=>'min-width: 200px; width: 230px;'));
								} else {
									echo $form->textField($profile, $field->varname, array('size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255), 'class'=>'i-format', 'style'=>'min-width: 200px; width: 230px;'));
								}
								?>
							</div>
						</div>
						<div class="clear"></div>
					</section>
					<?php
				}
			}
			?>

            <?php if (UserModule::doCaptcha('registration')): ?>
                    <section class="form_row">
                        <?php echo $form->labelEx($model,'verifyCode'); ?>
                        <div class="block_content">
                            <?php $this->widget('CCaptcha'); ?>
                            <?php echo $form->textField($model, 'verifyCode', array('class'=>'i-format', 'style'=>'min-width: 0px; width: 105px;')); ?>
                            <?php echo $form->error($model, 'verifyCode'); ?>
                            <p class="hint">
                                <?php echo UserModule::t("Please enter the letters as they are shown in the image above."); ?>
                                <br/>
                                <?php echo UserModule::t("Letters are not case-sensitive."); ?>
                            </p>
                        </div>
                    </section>
            <?php endif; ?>

			<section class="form_row">
				<div>
					<div class="block_content">
						<?php echo CHtml::submitButton(UserModule::t("Register"), array('class'=>'button darkgray')); ?>
						<?php echo CHtml::resetButton(UserModule::t("Cancel"), array('class'=>'button red')); ?>
					</div>
				</div>
				<div class="clear"></div>
			</section>

	<?php $this->endWidget(); ?>
	</div><!-- form -->
	
<?php endif; ?>
<?php
/* Ну и добавляем скриптик, ктоторый определяет языковый пакет и запускает календарь */
$script = 'jQuery(function() {'."\n";
$script .= 'jQuery.datepicker.setDefaults($.extend({changeMonth: true, changeYear: true, yearRange: "-65:-12"}), $.datepicker.regional[\'ru\']);'."\n";
$script .= '});'."\n";
/* datepicker_init_local - Это идентификатор куска JS который мы добавляем*/
Yii::app()->clientScript->registerScript('datepicker_init_local', $script, CClientScript::POS_BEGIN);
?>