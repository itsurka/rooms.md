<?php
/** @var UserLogin $model */
/** @var CActiveForm $form */
/** @var Controller $this */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'action'=>Yii::app()->getModule('user')->loginUrl,
	'enableAjaxValidation'=>true,
	'enableClientValidation'=>true,
	'htmlOptions'=>array(
		'role'=>'form',
		'class'=>'form-inline',
		'enctype'=>'multipart/form-data',
	),
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'errorMessageCssClass'=>'text-danger',
)); ?>

	<div class="form-group">
		<?php echo CHtml::activeLabelEx($model, 'username', array('class'=>'sr-only')); ?>
		<?php echo CHtml::activeTextField($model, 'username', array('class'=>'form-control input-sm', 'placeholder'=>'Ваш логин или email')) ?>
		<?php echo $form->error($model, 'username', array(
			'errorCssClass'=>'has-error',
			'hideErrorMessage'=>'true',
			'successCssClass'=>'has-success'
		)); ?>
	</div>
	<div class="form-group">
		<?php echo CHtml::activeLabelEx($model, 'password', array('class'=>'sr-only')); ?>
		<?php echo CHtml::activePasswordField($model, 'password', array('class'=>'form-control input-sm', 'placeholder'=>'Пароль')) ?>
		<?php echo $form->error($model, 'password', array(
			'errorCssClass'=>'has-error',
			'hideErrorMessage'=>'true',
			'successCssClass'=>'has-success'
		)); ?>
	</div>
<?php echo CHtml::hiddenField('UserLogin[rememberMe]', '1'); ?>
<?php echo CHtml::submitButton('ОК', array('class'=>'btn btn-primary btn-sm')); ?>

<?php $this->endWidget(); ?>