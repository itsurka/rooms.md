<?php
/**
 * @var Controller $this
 * @var CActiveForm $form
 * @var RegistrationForm $model
 * @var Profile $profile
 */
?>
<div class="row">
	<div class="col-lg-12">

		<div class=" ">
			<div class="col-lg-12 well bs-component panel">
				<div class="col-lg-4"></div>
				<div class="col-lg-4 ">
					<div class="">
						<?php
						$form=$this->beginWidget('CActiveForm', array(
							'id'=>'registration-form',
							'htmlOptions'=>array(
								'enctype'=>'multipart/form-data',
//								'class'=>'form-horizontal'
							),
						));
						?>
						<fieldset>
							<legend>Регистрация</legend>

							<div class="form-group <?php echo $profile->hasErrors('phone') ? 'has-error' : ''; ?>">
<!--								--><?php //echo CHtml::activeLabel($profile, 'phone', array('class'=>'col-lg-4 control-label', 'label'=>'Телефон')); ?>
<!--								<div class="col-lg-6">-->
									<?php echo $form->numberField($profile, 'phone', array('class'=>'form-control', 'placeholder'=>'Телефон')); ?>
									<?php echo $form->error($profile, 'phone', array(
									)); ?>
<!--								</div>-->
							</div>

							<div class="form-group <?php echo $profile->hasErrors('firstname') ? 'has-error' : ''; ?>">
<!--								--><?php //echo CHtml::activeLabel($profile, 'firstname', array('class'=>'col-lg-4 control-label', 'label'=>'Имя')); ?>
<!--								<div class="col-lg-6">-->
									<?php echo $form->textField($profile, 'firstname', array('class'=>'form-control', 'placeholder'=>'Имя')); ?>
									<?php echo $form->error($profile, 'firstname', array(
									)); ?>
<!--								</div>-->
							</div>

							<div class="form-group <?php echo $model->hasErrors('password') ? 'has-error' : ''; ?>">
<!--								--><?php //echo CHtml::activeLabel($model, 'password', array('class'=>'col-lg-4 control-label', 'label'=>'Пароль')); ?>
<!--								<div class="col-lg-6">-->
									<?php echo $form->passwordField($model, 'password', array('class'=>'form-control', 'autocomplete'=>'off', 'placeholder'=>'Пароль')); ?>
									<?php echo $form->error($model, 'password', array(
									)); ?>
								</div>
							</div>

							<div class="form-group">
								<div class="col-lg-10 col-lg-offset-4">
									<?php echo CHtml::submitButton('Отправить', array("class"=>"btn btn-primary show-loading-text", 'id'=>'form-submit', 'data-loading-text'=>'Подождите...')); ?>
								</div>
							</div>

						</fieldset>
						<?php $this->endWidget(); ?>
					</div>
				</div>
				<div class="col-lg-4"></div>
			</div>

		</div>

	</div>

</div>
