<?/*
<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Profile");
$this->breadcrumbs=array(
//	UserModule::t("Profile")=>array('profile'),
//	UserModule::t("Edit"),
);
?>
<?/*<h2><?php echo UserModule::t('Edit profile'); ?></h2>
<?php echo $this->renderPartial('menu'); ?>*/?>


<?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
	<div class="alert succes_msg">
	<?php echo Yii::app()->user->getFlash('profileMessage'); ?>
	</div>
<?php endif; ?>

<div class="form_place margin_top_0">
	
<?php $form=$this->beginWidget('UActiveForm', array(
	'id'=>'profile-form',
	'enableAjaxValidation'=>true,
	'htmlOptions' => array('enctype'=>'multipart/form-data'),
)); ?>

	<div class="alert info_msg"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></div>

	<?php echo $form->errorSummary(array($model,$profile), NULL, NULL, array('class'=>'alert error_msg')); ?>

	<?php
		$profileFields=$profile->getFields();
		if ($profileFields) {
			foreach($profileFields as $field) {
			?>
			<section class="form_row">
				<div>
					<?php echo $form->labelEx($profile,$field->varname);?>
					<div class="block_content">
						<?php
						if ($field->widgetEdit($profile)) {
							echo $field->widgetEdit($profile, array('class'=>'i-format', 'style'=>'min-width: 200px; width: 230px;'));
						} elseif ($field->range) {
							echo $form->dropDownList($profile,$field->varname,Profile::range($field->range, array('class'=>'i-format', 'style'=>'min-width: 200px; width: 230px;')));
						} elseif ($field->field_type=="TEXT") {
							echo $form->textArea($profile,$field->varname, array('rows'=>6, 'cols'=>50, 'class'=>'i-format', 'style'=>'min-width: 200px; width: 230px;'));
						} else {
							echo $form->textField($profile,$field->varname, array('size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255), 'class'=>'i-format', 'style'=>'min-width: 200px; width: 230px;'));
						}
						//echo $form->error($profile,$field->varname);
						?>
					</div>
				</div>
				<div class="clear"></div>
			</section>
			<?php
			}
		}
?>

	<section class="form_row">
		<div>
			<?php echo $form->labelEx($model,'username'); ?>
			<div class="block_content">
				<?php echo $form->textField($model,'username',array('class'=>'i-format', 'style'=>'min-width: 200px; width: 230px;')); ?>
			</div>
		</div>
		<div class="clear"></div>
	</section>

	<section class="form_row">
		<div>
			<?php echo $form->labelEx($model,'email'); ?>
			<div class="block_content">
				<?php echo $form->textField($model,'email',array('class'=>'i-format', 'style'=>'min-width: 200px; width: 230px;')); ?>
			</div>
		</div>
		<div class="clear"></div>
	</section>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? UserModule::t('Create') : UserModule::t('Save'), array('class'=>'button darkgray')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php
/* Ну и добавляем скриптик, ктоторый определяет языковый пакет и запускает календарь */
$script = 'jQuery(function() {'."\n";
$script .= 'jQuery.datepicker.setDefaults($.extend({changeMonth: true, changeYear: true, yearRange: "-65:-12"}), $.datepicker.regional[\'ru\']);'."\n";
$script .= '});'."\n";
/* datepicker_init_local - Это идентификатор куска JS который мы добавляем*/
Yii::app()->clientScript->registerScript('datepicker_init_local', $script, CClientScript::POS_BEGIN);
?>
