<?php
//$this->pageTitle=Yii::app()->name . ' - Contact Us';
//$this->breadcrumbs=array(
//	'Contact',
//);
?>


<section class="grid_7">
	<div id="contact">
		<div id="message"></div>
		<fieldset>
		<div class="clear"></div>
		
		<?php if(Yii::app()->user->hasFlash('contact')): ?>

			<div class="flash-success">
				<?php echo Yii::app()->user->getFlash('contact'); ?>
			</div>

		<?php else: ?>

			<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'contact-form',
				'enableClientValidation'=>true,
				'clientOptions'=>array(
					'validateOnSubmit'=>true,
				),
			)); ?>

			<?/*<div class="alert note_msg"><?=Yii::t("app", 'Fields with * are required')?></div>*/?>

			<?=$form->errorSummary($model, NULL, NULL, array('class'=>'alert error_msg'))?>
								
			<div class="input_label user"><?php echo str_replace("*", "", $form->labelEx($model,'name')); ?></div>
			<?php echo $form->textField($model,'name', array("class"=>"name")); ?>
			<div class="clear"></div>

			<div class="input_label user_email"><?php echo str_replace("*", "", $form->labelEx($model,'email')); ?></div>
			<?php echo $form->textField($model,'email', array("class"=>"email")); ?>
			<div class="clear"></div>

			<div class="input_label user_subject"><?php echo str_replace("*", "", $form->labelEx($model,'subject')); ?></div>
			<?php echo $form->textField($model,'subject', array("class"=>"subject")); ?>
			<div class="clear"></div>

			<?php echo $form->textArea($model,'body',array()); ?>

			<?php if(CCaptcha::checkRequirements()): ?>
				<section class="form_row">
					<?php echo $form->labelEx($model,'verifyCode', array("style"=>"padding-left: 0px;")); ?>
					<div>
					<?php $this->widget('CCaptcha'); ?>
					<?php echo $form->textField($model, 'verifyCode', array('class'=>'i-format', 'style'=>'min-width: 0px; width: 105px;')); ?>
					</div>
					<?/*<div class="hint">Please enter the letters as they are shown in the image above.
					<br/>Letters are not case-sensitive.</div>
					<?php echo $form->error($model,'verifyCode'); ?>*/?>
				</section>
			<?php endif; ?>

			<div class="row buttons">
				<?php echo CHtml::submitButton("Отправить", array("class"=>"button darkgray contact_submit")); ?>
			</div>

			<?php $this->endWidget(); ?>

		<?php endif; ?>
			
		</fieldset>
	</div>
</section>
<?/*
<section class="grid_5">
	<div class="adress_info">
		<h4>Наши контакты</h4>
        <ul class="list14"><li><?php echo Yii::app()->params['contactPhoneNumber']; ?></li></ul>
        <ul class="list15"><li><?php echo Yii::app()->params['supportEmail']; ?></li></ul>
		<!--<ul class="list13"><li>USA, Smartik, 1230 Ave Lorem Ipsum, NY 10020</li></ul>
		<ul class="list14"><li>(0123) 25-456-5151</li></ul>
		<ul class="list15"><li>support@company.com</li></ul>
		<ul class="list16"><li>http://company.com</li></ul>-->
	</div>
	<div class="clear"></div>
</section>
*/?>