<section class="grid_7">
	<div id="contact">
		<div id="message"></div>

		<fieldset>

            <?php if(!$hideForm): ?>

                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'unsubscribe-form',
                )); ?>

                    <p><b>Вы хотите отписаться от рассылки на e-mail: <i><?php echo $user->email; ?></i>?</b></p>
                    <div class="clear"></div>

                    <div class="row buttons">
                        <?php echo CHtml::submitButton("Отписаться", array("class"=>"button darkgray")); ?>
                    </div>

                <?php $this->endWidget(); ?>

            <?php else: ?>
                <p><b><?php echo $message; ?></b></p>
            <?php endif; ?>

		</fieldset>
	</div>
</section>