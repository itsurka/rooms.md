<?php $this->pageTitle=Yii::app()->name; ?>

<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

<p>Congratulations! You have successfully created your Yii application.</p>

<ul>
    <li>
        Developer: <a href="mailto:<?=Yii::app()->params['developerEmail']?>"><?=Yii::app()->params['adminEmail']?></a>
    </li>
</ul>